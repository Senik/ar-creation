import React, { Component, Suspense } from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import firebase from "./config/fbConfig";
import { createStructuredSelector } from "reselect";
//selectors
import { selectProfile, selectAuthUser } from "./redux/auth/auth.selectors";
//actions
import {
  FBorGoogleRedirectCatch,
  setAuthError,
  clearAuthError
} from "./redux/auth/auth.actions";
import { countNotifications, getAllUsers } from "./redux/user/user.actions";
import { getAllPosts } from "./redux/post/post.actions";
//components
import Navbar from "./components/layout/Navbar";
import { WithUser, WithoutUser } from "./Route";
import SideNavLinks from "./components/layout/sidenavLinks/SideNavLinks.component";
import FullPageLoader from "./components/shared/FullPageLoader";
import ErrorBaundary from "./components/error-baundary/error-baundary.component";

class App extends Component {
  subscribeForAuthChang = null;
  componentDidMount() {
    this.subscribeForAuthChang = firebase
      .auth()
      .getRedirectResult()
      .then(authData => {
        const { user } = authData;
        if (user) {
          this.props.FBorGoogleRedirectCatch(user);
        }
      })
      .catch(error => {
        console.log(error);
        this.props.setAuthError(error);
      });
    this.props.getAllPosts();
    this.props.getAllUsers();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.profile.Notifications !== this.props.profile.Notifications) {
      const { Notifications } = this.props.profile;
      if (Notifications) {
        const newNotifications =
          Notifications &&
          Object.keys(Notifications).filter(key => {
            return Notifications[key].NotificationOpened === "False";
          });
        this.props.countNotifications(newNotifications.length);
      } else {
        this.props.countNotifications(0);
      }
    }
  }
  componentWillUnmount() {
    if (typeof this.subscribeForAuthChang === "function") {
      this.subscribeForAuthChang();
    }
    this.props.clearAuthError();
  }

  render() {
    return (
      <div className="App">
        <ErrorBaundary>
          <Navbar />
          <div className="container">
            <div className="row">
              <SideNavLinks />
              {this.props.auth.uid ? (
                <Suspense fallback={<FullPageLoader />}>
                  <Switch>
                    <Route
                      path={WithUser[0].path}
                      component={WithUser[0].component}
                    />
                    {this.props.profile.isLoaded &&
                    this.props.profile.User_NickName === "Nick Name" ? (
                      <Redirect to="/profile" />
                    ) : null}

                    {WithUser.map((route, idx) => {
                      return route.component ? (
                        <Route
                          key={idx}
                          path={route.path}
                          exact={route.exact}
                          render={props => <route.component {...props} />}
                        />
                      ) : null;
                    })}
                    <Redirect to="/" />
                  </Switch>
                </Suspense>
              ) : (
                <Suspense fallback={<FullPageLoader />}>
                  <Switch>
                    {WithoutUser.map((route, idx) => {
                      return route.component ? (
                        <Route
                          key={idx}
                          path={route.path}
                          exact={route.exact}
                          render={props => <route.component {...props} />}
                        />
                      ) : null;
                    })}
                    <Redirect
                      to={{
                        pathname: "/signin",
                        state: { referrer: window.location.href }
                      }}
                    />
                  </Switch>
                </Suspense>
              )}
            </div>
          </div>
        </ErrorBaundary>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  auth: selectAuthUser,
  profile: selectProfile
});

const mapDispatchToProps = dispatch => {
  return {
    FBorGoogleRedirectCatch: user => dispatch(FBorGoogleRedirectCatch(user)),
    setAuthError: err => dispatch(setAuthError(err)),
    clearAuthError: () => dispatch(clearAuthError()),
    countNotifications: notLength => dispatch(countNotifications(notLength)),
    getAllPosts: () => dispatch(getAllPosts()),
    getAllUsers: () => dispatch(getAllUsers())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
