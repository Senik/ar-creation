import { lazy } from "react";
const SignIn = lazy(() => import("./components/auth/SignIn"));
const SignUp = lazy(() => import("./components/auth/SignUp"));
const PasswordForget = lazy(() => import("./components/auth/PasswordForget"));
const ResetEmail = lazy(() => import("./components/auth/ResetEmail"));
//pages
const ArPost = lazy(() => import("./pages/arPost/ArPost"));
const Dashboard = lazy(() => import("./pages/dashboard/Dashboard"));
const XRGallery = lazy(() => import("./pages/xrPost/XRGallery"));
const PrimeSpace = lazy(() => import("./pages/prime-posts/PrimePostsSpace"));
const PrimePost = lazy(() => import("./pages/prime-posts/PrimePost"));
const PrimeUsersSpace = lazy(() =>
  import("./pages/prime-users/PrimeUsersSpace")
);
const PrimeInfo = lazy(() => import("./pages/prime-info/prime-info.component"));
const PrimeUser = lazy(() => import("./pages/prime-users/PrimeUser"));
const PrimeUserPosts = lazy(() =>
  import("./pages/prime-users/posts/PrimeUserPosts")
);
const PrimePromo = lazy(() =>
  import("./pages/prime-promocodes/prime-promocodes.component")
);
const EditPost = lazy(() => import("./pages/postEdit/PostEdit"));
const UserProfile = lazy(() => import("./pages/profile/UserProfile"));
const Creators = lazy(() => import("./pages/creators"));
const CreatorDetails = lazy(() => import("./pages/creators/CreatorDetails"));
const CreatePost = lazy(() => import("./pages/creation"));
const Notifications = lazy(() => import("./pages/notifications/Notifications"));
const Payment = lazy(() => import("./components/payment/Payment"));
const Payout = lazy(() => import("./pages/payout/Payout"));

export const WithUser = [
  { path: "/profile", component: UserProfile, exact: false },
  { path: "/", component: Dashboard, exact: true },
  { path: "/argallery", component: XRGallery, exact: false },
  { path: "/faceargallery", component: XRGallery, exact: false },
  { path: "/editpost/:id", component: EditPost, exact: false },
  { path: "/create", component: CreatePost, exact: false },
  { path: "/creators", component: Creators, exact: true },
  { path: "/creator/:id", component: CreatorDetails, exact: false },
  { path: "/resetEmail", component: ResetEmail, exact: false },
  { path: "/arpost/:id", component: ArPost, exact: false },
  { path: "/prime/posts", component: PrimeSpace, exact: true },
  { path: "/prime/dashboard", component: PrimeInfo, exact: false },
  { path: "/prime/posts/:id", component: PrimePost, exact: true },
  { path: "/prime/users", component: PrimeUsersSpace, exact: true },
  { path: "/prime/users/:id", component: PrimeUser, exact: true },
  { path: "/prime/user/:id/posts", component: PrimeUserPosts },
  { path: "/prime/promo/", component: PrimePromo },
  { path: "/notifications", component: Notifications, exact: true },
  { path: "/payment", component: Payment },
  { path: "/payout", component: Payout, exact: true }
];

export const WithoutUser = [
  { path: "/signin", component: SignIn, exact: false },
  { path: "/signup", component: SignUp, exact: false },
  { path: "/resetPassword", component: PasswordForget, exact: false },
  { path: "/arpost/:id", component: ArPost, exact: false },
  { path: "/argallery", component: XRGallery, exact: false },
  { path: "/faceargallery", component: XRGallery, exact: false },
  { path: "/creator/:id", component: CreatorDetails, exact: false }
];
