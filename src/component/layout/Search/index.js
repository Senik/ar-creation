import React, { Fragment, useEffect } from "react";
import { withRouter } from 'react-router-dom';
//components
import SearchInput from './searchInput'
import SearchType from './searchType'
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from '@material-ui/icons/Search';
import Close from '@material-ui/icons/Close';
import { createStructuredSelector } from "reselect";
import { signOut } from "../../../redux/auth/auth.actions";
import { setKeyword } from "../../../redux/post/post.actions";
import { connect } from "react-redux";
//Selectors
import { selectKeyword, selectResult } from '../../../redux/post/post.selectors';

import classes from "./Search.module.scss";

const Search = props => {
    const [showSearch, setShowSearch] = React.useState(false);
    const [searchValue, setValue] = React.useState('');
    const [search, setSearch] = React.useState(false);
    const { setKeyword, keyword } = props;

    const toggleInput = () => {
        const search = !showSearch && keyword[0] !== '';
        setKeyword(['', keyword[1], search]);
        setValue('');
        setShowSearch(!showSearch);
    };

    useEffect(() => {
        setKeyword(['', 'Post', false]);
        setShowSearch(false);
        const { pathname } = props.location;
        if (
            pathname === '/' ||
            pathname === '/creators' ||
            pathname.includes('/creator/') ||
            pathname === '/argallery' ||
            pathname === '/faceargallery' ||
            pathname === '/prime/posts' ||
            pathname === '/prime/users' ||
            (pathname.includes('/prime/user/') && pathname.includes('/posts'))
        ) {
            setSearch(true)
        } else {
            setSearch(false)
        }
    }, [props.location.pathname]);

    const handleType = e => {
        setKeyword([searchValue, e, searchValue !== '']);
    };
    const handleSearch = e => {
        setValue(e.target.value);
    };

    const handleKeyPress = e => {
        if (e.key === 'Enter') {
            const value = e.target.value.replace(/[^\w\s\-_]/gi, '');
            if (value !== '') {
                setKeyword([value, keyword[1], true]);
                e.target.blur();
            } else if (keyword[2]) {
                setKeyword([value, keyword[1], false]);
            }
        }
    };
    return (
        search ?
            <ul className={`row right ${classes.Search} ${showSearch ? classes.open : ''} `}>
                {showSearch ? (
                    <Fragment>
                        <li>
                            <SearchInput
                                handleKeyPress={handleKeyPress}
                                onChange={handleSearch}
                                searchValue={searchValue}
                                location={keyword[1] === 'User' ? 'User' : props.location.pathname}
                                value={keyword[0]}
                            />
                        </li>
                        {props.location.pathname === '/argallery' ?
                            <li className={classes.argallerySearch}>
                                <SearchType
                                    handleType={handleType}
                                    value={keyword[1]}
                                />
                            </li> : null
                        }
                        <li>
                            <div className={`${classes.searchResult} hide-on-small-only`}>
                                Results: {props.keyword[2] ? props.result : 0}
                            </div>
                        </li>
                    </Fragment>
                ) : null}
                <li className={`right ${classes.searchIcon}`}>
                    <IconButton
                        color="inherit"
                        className={classes.icon}
                        onClick={toggleInput}
                    >
                        {showSearch ? <Close /> : <SearchIcon />}
                    </IconButton>
                </li>
            </ul> : null

    )
};

const mapStateToProps = createStructuredSelector({
    keyword: selectKeyword,
    result: selectResult
});

const mapDispatchToProps = dispatch => {
    return {
        signOut: () => dispatch(signOut()),
        setKeyword: keyword => dispatch(setKeyword(keyword))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(withRouter(Search));
