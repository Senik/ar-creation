import React from "react";
import classes from './Search.module.scss';
import InputBase from "@material-ui/core/InputBase";
const SearchInput = props => {
    let searchName;
    const location = props.location;
    if (location === '/creators') {
        searchName = 'Creators';
    } else if (location === '/argallery') {
        searchName = 'AR Gallery';
    } else if (location === '/prime/posts') {
        searchName = 'Posts';
    } else if (location === '/prime/users') {
        searchName = 'Users';
    } else if (location === '/faceargallery') {
        searchName = 'Face AR';
    } else if (location === 'User') {
        searchName = 'User';
    } else {
        searchName = 'Posts';
    }
    return (
        <InputBase
            placeholder={`Search For ${searchName}`}
            className={classes.searchInput}
            onChange={props.onChange}
            value={props.searchValue}
            onKeyPress={props.handleKeyPress}
            autoFocus
        />
    )
};

export default SearchInput;