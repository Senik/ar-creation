import React from "react";
import classes from "./Search.module.scss";
import TextField from "@material-ui/core/TextField";
import MenuItem from '@material-ui/core/MenuItem';

const currencies = [
    {
        value: 'User',
        label: 'User',
    },
    {
        value: 'Post',
        label: 'Post',
    }
];

const SearchType = props =>{

    const handleChange = event => {
        props.handleType(event.target.value)
    };

    return (
        <TextField
            id="standard-select-currency"
            select
            className={`${classes.textField}`}
            value={props.value}
            onChange={handleChange}
            SelectProps={{
                MenuProps: {
                    className: classes.menu,
                },
            }}
        >}
            {currencies.map(option => (
                <MenuItem key={option.value} value={option.value}>
                    {option.label}
                </MenuItem>
            ))}
        </TextField>
    )
};

export default SearchType;