import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
//selectors
import { selectAuthError } from "../../redux/auth/auth.selectors";
//actions
import { resetPassword, clearAuthError } from "../../redux/auth/auth.actions";
//styles
import classes from "./Auth.module.scss";

function PasswordForget(props) {
  const [email, setEmail] = useState("");

  useEffect(() => {
    return function() {
      props.clearAuthError();
    };
  }, []);
  function handleChange(e) {
    setEmail(e.target.value);
  }
  function handleSubmit(e) {
    e.preventDefault();
    const { resetPassword, history } = props;
    resetPassword(email, history);
  }

  const { authError } = props;
  return (
    <div className="container center">
      <form className={classes.formStyle} onSubmit={handleSubmit}>
        <h5 className="grey-text text-darken-3">Reset Password</h5>
        <div className="input-field">
          <label htmlFor="email">Email</label>
          <input type="email" id="email" onChange={handleChange} />
        </div>

        <div className="input-field">
          <button className="btn pink lighten-1 z-depth-0">Send Email</button>
          <div className="red-text center">
            {authError ? <p>{authError}</p> : null}
          </div>
        </div>
      </form>
    </div>
  );
}

PasswordForget.propTypes = {
  resetPassword: PropTypes.func.isRequired,
  authError: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  authError: selectAuthError
});

const mapDispatchToProps = dispatch => {
  return {
    resetPassword: (email, history) => dispatch(resetPassword(email, history)),
    clearAuthError: () => dispatch(clearAuthError())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PasswordForget);
