import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

//selectors
import { selectAuthError } from "../../redux/auth/auth.selectors";
//actions
import {
  resetemail,
  signIn,
  clearAuthError
} from "../../redux/auth/auth.actions";
//styles
import classes from "./Auth.module.scss";

function ResetEmail(props) {
  const [email, setEmail] = useState("");

  useEffect(() => {
    return () => {
      props.clearAuthError();
    };
  }, []);

  function handleChange(e) {
    setEmail(e.target.value);
  }
  function handleSubmit(e) {
    e.preventDefault();
    const { resetemail, history } = props;
    resetemail(email, history);
  }

  const { authError } = props;
  return (
    <div className="container">
      <form className={classes.formStyle} onSubmit={handleSubmit}>
        <h5 className="grey-text text-darken-3">Reset Email</h5>
        <div className="input-field">
          <label htmlFor="email">Email</label>
          <input type="email" id="email" onChange={handleChange} />
        </div>

        <div className="input-field">
          <button className="btn pink lighten-1 z-depth-0">Reset Email</button>
          <div className="red-text center">
            {authError ? <p>{authError}</p> : null}
          </div>
        </div>
      </form>
    </div>
  );
}

ResetEmail.propTypes = {
  resetemail: PropTypes.func.isRequired,
  signIn: PropTypes.func.isRequired,
  authError: PropTypes.object
};

const mapDispatchToProps = dispatch => {
  return {
    resetemail: (email, history) => dispatch(resetemail(email, history)),
    signIn: creds => dispatch(signIn(creds)),
    clearAuthError: () => dispatch(clearAuthError())
  };
};
const mapStateToProps = createStructuredSelector({
  authError: selectAuthError
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetEmail);
