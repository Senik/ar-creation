import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { createStructuredSelector } from "reselect";

//selectors
import { selectAuthError } from "../../redux/auth/auth.selectors";
//actions
import {
  signIn,
  facebookSignIn,
  googleSignIn,
  clearAuthError
} from "../../redux/auth/auth.actions";
//MUI
import { Visibility, VisibilityOff } from "@material-ui/icons";
//styles
import classes from "./Auth.module.scss";

function SignIn(props) {
  if (props.location.state) {
    const url = new URL(props.location.state.referrer);
    const requestPackage = url.searchParams.get("requestPackage");
    if (requestPackage) {
      localStorage.setItem("requestPackage", requestPackage);
    }
  }
  const email = useFormInput("");
  const password = useFormInput("");
  const [passwordType, setPasswordType] = useState("password");

  useEffect(() => {
    document.title = "Sign in";
    return () => {
      props.clearAuthError();
      props.location.state = null;
    };
  }, []);

  function handleSubmit(e) {
    e.preventDefault();
    props.signIn({ email: email.value, password: password.value });
  }
  function handleFacebookSubmit() {
    props.facebookSignIn();
  }
  // function handleGoogleSubmit() {
  //   props.googleSignIn();
  // }
  function togglePass(e) {
    const passType = passwordType === "password" ? "text" : "password";
    setPasswordType(passType);
  }

  const { authError } = props;
  return (
    <div className="container">
      <form className={classes.formStyle} onSubmit={handleSubmit}>
        <h5 className="grey-text text-darken-3">Sign In</h5>
        <div className="input-field">
          <label htmlFor="email" className="active">
            Email*
          </label>
          <input type="email" id="email" {...email} />
        </div>
        <div className="input-field">
          {passwordType === "password" ? (
            <VisibilityOff
              onClick={togglePass}
              className={classes.showPassIcon}
            />
          ) : (
            <Visibility onClick={togglePass} className={classes.showPassIcon} />
          )}

          <label htmlFor="password" className="active">
            Password*
          </label>
          <input type={passwordType} id="password" {...password} />
        </div>
        <div className="input-field">
          <Link to="/resetPassword" className={classes.resetPassword}>
            Forget password ?
          </Link>
        </div>

        <div className="input-field">
          <button
            className={`btn waves-effect  pink lighten-1 ${classes.signinButton} `}
          >
            Sign in
          </button>
        </div>
        <div className="center" style={{ padding: "0 15px" }}>
          <button
            type="button"
            className={`waves-effect waves-light btn social facebook ${classes.facebookGoogleButton}`}
            onClick={handleFacebookSubmit}
          >
            <i className="fab fa-facebook-f icon-left" />
            Sign in with Facebook
          </button>
          {/* <button
              type="button"
              className={`waves-effect waves-light btn social google ${classes.facebookGoogleButton}`}
              onClick={self.handleGoogleSubmit}
            >
              <i className="fab fa-google"></i>
              Sign in with Google
            </button> */}
          <div style={{ textAlign: "center" }}>
            <span style={{ fontSize: "12px" }}>
              By signing up with Facebook or Google you agree to Terms &
              Conditions and ARize Group Privacy Policy
            </span>
          </div>
          <div className="red-text center">
            {authError ? <p>{authError}</p> : null}
          </div>
        </div>
        <div className={classes.privacyContainer}>
          <a
            href="https://arize.io/privacy"
            className={classes.privacyLink}
            target="_blank"
            rel="noopener noreferrer"
          >
            Privacy-Policy
          </a>
          <a
            href="https://arize.io/terms"
            className={classes.privacyLink}
            target="_blank"
            rel="noopener noreferrer"
          >
            Terms of Use
          </a>
        </div>
      </form>
    </div>
  );
}

SignIn.propTypes = {
  signIn: PropTypes.func.isRequired,
  facebookSignIn: PropTypes.func.isRequired,
  authError: PropTypes.string
};

const mapStateToProps = createStructuredSelector({
  authError: selectAuthError
});

const mapDispatchToProps = dispatch => {
  return {
    signIn: creds => dispatch(signIn(creds)),
    facebookSignIn: () => dispatch(facebookSignIn()),
    googleSignIn: () => dispatch(googleSignIn()),
    clearAuthError: () => dispatch(clearAuthError())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);

function useFormInput(initialValue) {
  const [value, setValue] = useState(initialValue);

  function handleChange(e) {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  };
}
