import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
//selectors
import {
  selectAuthUser,
  selectAuthError
} from '../../redux/auth/auth.selectors';
//actions
import { signUp, clearAuthError } from '../../redux/auth/auth.actions';
//MUI
import { Visibility, VisibilityOff } from '@material-ui/icons';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
//styles
import classes from './Auth.module.scss';

class SignUp extends Component {
  state = {
    email: '',
    password: '',
    formError: '',
    passWordType: 'password',
    isTermsAccepted: false
  };
  componentDidMount() {
    document.title = 'Sign up';
  }
  componentWillUnmount() {
    this.props.clearAuthError();
  }
  handleChange = e => {
    const { id, value } = e.target;
    this.setState({
      [id]: value
    });
  };
  handleCheckBox = (e, checked) => {
    this.setState({
      isTermsAccepted: checked
    });
  };
  handleSubmit = e => {
    e.preventDefault();
    const self = this;
    const { password, confirmPassword } = self.state;
    if (password !== confirmPassword) {
      self.setState({
        formError: 'Passwords are not equal'
      });
      return;
    }
    self.props.signUp(self.state);
  };
  togglePass = e => {
    const passWordType =
      this.state.passWordType === 'password' ? 'text' : 'password';
    e.preventDefault();
    this.setState({
      passWordType
    });
  };
  //test
  render() {
    const self = this;
    const { authError } = self.props;
    const { isTermsAccepted, passWordType, formError } = self.state;
    return (
      <div className="container">
        <form className={classes.formStyle} onSubmit={self.handleSubmit}>
          <h5 className="grey-text text-darken-3">Sign Up</h5>
          <div className="input-field">
            <label htmlFor="email">Email</label>
            <input type="email" id="email" onChange={self.handleChange} />
          </div>
          <div className="input-field">
            {passWordType === 'password' ? (
              <VisibilityOff
                onClick={self.togglePass}
                className={classes.showPassIcon}
              />
            ) : (
                <Visibility
                  onClick={self.togglePass}
                  className={classes.showPassIcon}
                />
              )}

            <label htmlFor="password">Password</label>
            <input
              type={passWordType}
              id="password"
              onChange={this.handleChange}
            />
          </div>
          <div className="input-field">
            <label htmlFor="confirmPassword">Confirm Password</label>
            <input
              type={passWordType}
              id="confirmPassword"
              onChange={this.handleChange}
            />
          </div>
          <FormControlLabel
            control={
              <Checkbox
                id="isTermsAccepted"
                checked={isTermsAccepted}
                onChange={self.handleCheckBox}
              />
            }
            label="I Agree to Terms & Conditions and ARize Group Privacy Policy"
          />
          <div className="input-field">
            <button
              className={`btn pink lighten-1 z-depth-0 ${classes.signinButton}`}
              disabled={!isTermsAccepted}
            >
              Sign Up
            </button>
            <div className="center red-text">
              {authError ? <p>{authError}</p> : null}
              {formError ? <p>{formError}</p> : null}
            </div>
          </div>
          <div className={classes.privacyContainer}>
            <a
              href="https://arize.io/privacy"
              className={classes.privacyLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              Privacy-Policy
            </a>
            <a
              href="https://arize.io/terms"
              className={classes.privacyLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              Terms of Use
            </a>
          </div>
        </form>
      </div>
    );
  }
}

signUp.propTypes = {
  signUp: PropTypes.func.isRequired,
  authError: PropTypes.object,
  aut: PropTypes.object
};

const mapStateToProps = createStructuredSelector({
  authError: selectAuthError,
  auth: selectAuthUser
});

const mapDispatchToProps = dispatch => {
  return {
    signUp: newUser => dispatch(signUp(newUser)),
    clearAuthError: () => dispatch(clearAuthError())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUp);
