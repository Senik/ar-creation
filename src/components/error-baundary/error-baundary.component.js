import React from 'react';

import './error-baundary.styles.scss';

class ErrorBaundary extends React.Component {
  state = {
    hasError: false
  };
  static getDerivedStateFromError(error) {
    return { hasError: true };
  }
  render() {
    return this.state.hasError ? (
      <div className="error-image-overlay">
        <div className="error-image-container " />
        <h2 className="error-image-text ">This page is broken.</h2>
      </div>
    ) : (
      this.props.children
    );
  }
}

export default ErrorBaundary;
