import React, { useState } from "react";
//MUI
import TextField from "@material-ui/core/TextField";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import Fade from "@material-ui/core/Fade";
//styles
import classes from "./css/ButtonWithMessage.module.scss";

const ButtonWithMessage = props => {
  const { name, onChange, disabled, cmpClass } = props;
  const [showTextArea, setShowTextArea] = useState(false);
  const [message, setMessage] = useState("");
  const [anchorEl, setAnchorEl] = useState(null);
  const changeMessage = e => {
    setMessage(e.target.value);
  };
  const sendMessage = e => {
    e.preventDefault();
    onChange(message, e.target.id);
  };
  const open = e => {
    setShowTextArea(!showTextArea);
    setAnchorEl(e.currentTarget);
  };
  return (
    <div className={classes.rejectBtn}>
      <button
        name="False"
        className={cmpClass}
        onClick={open}
        disabled={disabled}
      >
        {name}
      </button>
      <div className={`${classes.centerAlign}  `}>
        <Popper
          open={showTextArea}
          anchorEl={anchorEl}
          placement={"top"}
          transition
        >
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <Paper>
                <div className={` ${classes.showTextArea} `}>
                  <form
                    onSubmit={sendMessage}
                    className={`z-depth-e white ${classes.formContainer} `}
                    id={name}
                  >
                    <TextField
                      id="outlined-dense-multiline"
                      label="Enter Your message"
                      className="text-field"
                      margin="dense"
                      variant="outlined"
                      multiline
                      rowsMax="8"
                      value={message}
                      onChange={changeMessage}
                    />
                    <button
                      type="submit"
                      disabled={name === "MAKE PUBLIC" ? false : !message}
                      className={`red white-text btn ${
                        classes.primeConfirmBtn
                      } ${name === "MAKE PUBLIC" ? "green" : ""}`}
                      onClick={open}
                    >
                      CONFIRM
                    </button>
                  </form>
                </div>
              </Paper>
            </Fade>
          )}
        </Popper>
      </div>
    </div>
  );
};

export default ButtonWithMessage;
