import React from 'react';
import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import classes from './css/Checkbox.module.scss';

const Checkbox = props => {
  const { title, options, defaultValue, name, required, disabled } = props;
  const handleChange = e => {
    props.handleChange(e.target.value, name);
  };
  return (
    <div className={`input-field ${classes.Checkbox}`}>
      <hr />
      <div className="input-field">
        <FormControl component="fieldset" className={classes.formControl}>
          <FormLabel className={classes.label} component="legend">{`${title}${
            required ? '*' : ''
          }`}</FormLabel>
          <RadioGroup
            aria-label="Is content paid"
            name={name}
            id={name}
            className={classes.group}
            value={defaultValue}
            onChange={handleChange}
          >
            {options.map((option, i) => (
              <FormControlLabel
                key={i}
                value={option.value}
                disabled={disabled}
                control={<Radio color="primary" />}
                label={option.label}
                labelPlacement="start"
              />
            ))}
          </RadioGroup>
        </FormControl>
      </div>
    </div>
  );
};

Checkbox.propTypes = {
  post: PropTypes.object
};

export default Checkbox;
