import React from "react";
import classes from "../../pages/creation/Creation.module.scss";

const ColorPicker = props => {
  const { name, defaultValue, label, required } = props;
  const handleChange = e => {
    e.preventDefault();
    props.handleChange(e.target.value, name);
  };
  return (
    <div className="form-contro">
      <label htmlFor="cutOutColor" className={classes.colorPickerLabel}>
        {`${label} ${required ? "*" : ""} `}
      </label>
      <input
        type="color"
        id={name}
        defaultValue={defaultValue || "#4caf50"}
        onChange={handleChange}
      />
    </div>
  );
};

export default ColorPicker;
