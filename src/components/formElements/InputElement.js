import React from 'react';

const InputElement = props => {
    const {
        name,
        defaultValue,
        label,
        type,
        required
    } = props;
    const handleChange = e => {
        e.preventDefault();
        props.handleChange(e.target.value, name);
    };
    return (
        <div className="input-field">

            <label
                htmlFor={name}
                className={defaultValue ? 'active' : null}
            >
                {`${label} ${required ? '*' : ''} `}
            </label>
            {type === 'textarea' ?
                <textarea
                    id={name}
                    type="text"
                    value={defaultValue || ''}
                    onChange={handleChange}
                    className='materialize-textarea'
                /> :
                <input
                    id={name}
                    type="text"
                    value={defaultValue || ''}
                    onChange={handleChange}
                    className='materialize-textarea'
                />
            }
        </div>
    )

};

export default InputElement;



