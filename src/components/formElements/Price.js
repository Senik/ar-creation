import React from "react";
import PropTypes from "prop-types";
import Checkbox from "@material-ui/core/Checkbox";
import classes from "./css/Price.module.scss";

const Price = props => {
  const { price } = props;
  const handleChange = e => {
    e.preventDefault();
    props.handleChange(+e.target.value, "model_price");
  };
  return (
    <div className={`center-align ${classes.Price}`}>
      <label htmlFor="model_price" className={classes.priceLabel}>
        Price (currency &euro;)*
      </label>
      <input
        style={{ width: 100, margin: "0 15px" }}
        type="number"
        id="model_price"
        min="1"
        value={price}
        step="any"
        required
        onChange={handleChange}
      />
    </div>
  );
};

Checkbox.propTypes = {
  post: PropTypes.object
};

export default Price;
