import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
//actions
import { resendVerivication } from '../../redux/auth/auth.actions';
//styles
import classes from './Layout.module.scss';

const EmailVerifyNotification = props => {
  const { resendVerivication } = props;
  return (
    <div
      className={` yellow accent-4 ${classes.verifyContainer} ${
        classes.EmailVerifyNotification
      }`}
    >
      <p className={`${classes.verifyText}`}>
        Please remember to verify your email Address, havent received the
        confirmation email?{' '}
        <span onClick={resendVerivication} className={classes.resendLink}>
          click here
        </span>{' '}
        to resend
      </p>
    </div>
  );
};

EmailVerifyNotification.propTypes = {
  resendVerivication: PropTypes.func.isRequired
};

const mapDispatchToState = dispatch => {
  return {
    resendVerivication: () => dispatch(resendVerivication())
  };
};

export default connect(
  null,
  mapDispatchToState
)(EmailVerifyNotification);
