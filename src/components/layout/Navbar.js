import React, { Component, Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

//selectors
import { selectAuthUser, selectProfile } from '../../redux/auth/auth.selectors';
import { selectNotification } from '../../redux/notification/notification.selectors';
//actions
import { clearAuthError } from '../../redux/auth/auth.actions';
//helper
import { initGa, logPageView } from '../../helpers/react_ga';
//MUI
import Icon from '@material-ui/core/Icon';
//components
import Search from '../../component/layout/Search/';
import SignedOutLinks from './SignedOutLinks';
import SidebarMobile from './sidebarMobile/SidebarMobile.component';
import Notifier from './Notifier';
import EmailVerifyNotification from './EmailVerifyNotification';
//icons
import ArizeLogo from '../../Arize_Logo.png';

function fireTracking() {
  initGa();
  logPageView();
}

class Navbar extends Component {
  state = {
    showSearchIcon: false
  };

  componentDidMount() {
    fireTracking();
    const history = this.props.history;
    const pathname = history.location.pathname;
    if (
      pathname === '/' ||
      pathname === '/creators' ||
      pathname === '/argallery' ||
      pathname === '/faceargallery' ||
      pathname === '/prime/posts' ||
      pathname === '/prime/users' ||
      pathname === '/signup' ||
      pathname === '/signin'
    ) {
      this.setState({
        showSearchIcon: true
      });
    } else {
      this.setState({
        showSearchIcon: false
      });
    }

    this.unlisten = history.listen(location => {
      fireTracking();
      if (
        location.pathname === '/' ||
        location.pathname === '/creators' ||
        location.pathname === '/argallery' ||
        location.pathname === '/faceargallery' ||
        location.pathname === '/prime/posts' ||
        location.pathname === '/prime/users' ||
        location.pathname === '/signin' ||
        location.pathname === '/signup'
      ) {
        this.setState({
          showSearchIcon: true
        });
      } else {
        this.setState({
          showSearchIcon: false
        });
      }
    });
  }
  componentWillUnmount() {
    this.unlisten();
  }

  handleGoBack = () => {
    const { clearAuthError, history } = this.props;
    clearAuthError();
    history.goBack();
  };
  render() {
    const { auth, profile, notification } = this.props;
    const { showSearchIcon } = this.state;
    return (
      <Fragment>
        <div className="navbar-fixed">
          <nav className="nav nav-wrapper">
            <div className="container">
              <Notifier
                message={notification.message}
                error={notification.error}
              />
              {profile.User_NickName === 'Nick Name' ? null : (
                <>
                  <span
                    className="brand-logo left hide-on-small-only"
                    style={{ cursor: 'pointer' }}
                  >
                    {showSearchIcon ? (
                      <img src={ArizeLogo} alt="Arize logo" />
                    ) : (
                        <Icon onClick={this.handleGoBack}>arrow_back_ios</Icon>
                      )}
                  </span>
                  {auth.uid ? (
                    <SidebarMobile profile={profile} />
                  ) : (
                      <span
                        className="brand-logo left"
                        style={{ cursor: 'pointer' }}
                      >
                        <img src={ArizeLogo} alt="Arize logo" />
                      </span>
                    )}
                </>
              )}
              {auth.uid ? null : <SignedOutLinks />}
              <Search profile={profile} />
            </div>
          </nav>
        </div>
        {!auth.emailVerified && !auth.isEmpty ? (
          <EmailVerifyNotification />
        ) : null}
      </Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  auth: selectAuthUser,
  profile: selectProfile,
  notification: selectNotification
});

export default withRouter(
  connect(
    mapStateToProps,
    { clearAuthError }
  )(Navbar)
);
