import React, { useState } from 'react';
import PropTypes from 'prop-types';
//icons
import Snackbar from '@material-ui/core/Snackbar';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorOutline from '@material-ui/icons/ErrorOutline';
//styles
import classes from './Layout.module.scss';

let openSnackBarFn;

const Notifier = props => {
  const { message, error } = props;

  const [isOpen, setNotifierToggle] = useState(props.isOpen);

  const handleClose = () => {
    setNotifierToggle(false);
  };
  openSnackBarFn = () => {
    setNotifierToggle(true);
  };
  return (
    <div className={classes.Notifier}>
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        autoHideDuration={2000}
        onClose={handleClose}
        open={isOpen}
      >
        <p className={classes.notifPopup}>
          {error ? (
            <ErrorOutline className={classes.notifIconError} />
          ) : (
            <CheckCircleIcon className={classes.notifIcon} />
          )}

          {message}
        </p>
      </Snackbar>
    </div>
  );
};

Notifier.propTypes = {
  message: PropTypes.string.isRequired,
  isOpen: PropTypes.bool
};
Notifier.defaultProps = {
  isOpen: false
};

export function handleOpen() {
  openSnackBarFn();
}

export default Notifier;
