import React, { useState } from "react";
import { NavLink } from "react-router-dom";
//MUI
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
//components
import BagReport from "../modals/bugReport";
//styles
import classes from "./Layout.module.scss";

const SidebarExpansionPanels = props => {
  const [expanded, setExpanded] = useState(false);
  const { classesMobile, userID } = props;

  const handlePanelChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div className={`${classes.root} ${classes.SidebarExpansionPanels}`}>
      {userID === "zFodTuG0dmOQgPw0VyHcSWI9NX63" ? (
        <ExpansionPanel
          className={classes.expensionPanel}
          expanded={expanded === "panelPrime"}
          onChange={handlePanelChange("panelPrime")}
        >
          <ExpansionPanelSummary
            classes={{
              expandIcon:
                (classesMobile && classesMobile.expenseShowMoreIcon) ||
                classes.panelMoreIcon
            }}
            expandIcon={<ExpandMoreIcon />}
          >
            <i
              className={`fas fa-key fa-2x ${(classesMobile &&
                classesMobile.expanseIcon) ||
                classes.detailsIcon}`}
            />
            <Typography
              className={
                (classesMobile && classesMobile.expanseText) || classes.heading
              }
            >
              Prime
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <ul className={classes.expensionDetailsList}>
              <li className={classes.detailsItem}>
                <NavLink to="/prime/dashboard" className={classes.panelLink}>
                  <span className={classes.links}>Prime Info</span>
                </NavLink>
              </li>
              <li className={classes.detailsItem}>
                <ExpansionPanel className={classes.postsPanels}>
                  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    Prime Posts
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails className={classes.postsPanelDetails}>
                    <ul className={classes.expensionDetailsList}>
                      <li className={classes.detailsItem}>
                        <NavLink
                          to="/prime/posts?free"
                          className={classes.panelLink}
                        >
                          <span className={classes.links}>Free Posts</span>
                        </NavLink>
                      </li>
                      <li className={classes.detailsItem}>
                        <NavLink
                          to="/prime/posts?plus"
                          className={classes.panelLink}
                        >
                          <span className={classes.links}>Plus Posts</span>
                        </NavLink>
                      </li>
                      <li className={classes.detailsItem}>
                        <NavLink
                          to="/prime/posts?premium"
                          className={classes.panelLink}
                        >
                          <span className={classes.links}>Premium Posts</span>
                        </NavLink>
                      </li>
                    </ul>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </li>
              <li className={classes.detailsItem}>
                <NavLink to="/prime/users" className={classes.panelLink}>
                  <span className={classes.links}>Prime Users</span>
                </NavLink>
              </li>
              <li className={classes.detailsItem}>
                <NavLink to="/prime/promo" className={classes.panelLink}>
                  <span className={classes.links}>Prime Promo</span>
                </NavLink>
              </li>
            </ul>
          </ExpansionPanelDetails>
        </ExpansionPanel>
      ) : null}
      <ExpansionPanel
        className={classes.expensionPanel}
        expanded={expanded === "panelAbout"}
        onChange={handlePanelChange("panelAbout")}
      >
        <ExpansionPanelSummary
          classes={{
            expandIcon:
              (classesMobile && classesMobile.expenseShowMoreIcon) ||
              classes.panelMoreIcon
          }}
          expandIcon={<ExpandMoreIcon />}
        >
          <i
            className={`fas fa-info-circle fa-2x ${(classesMobile &&
              classesMobile.expanseIcon) ||
              classes.detailsIcon}`}
          />
          <Typography
            className={
              (classesMobile && classesMobile.expanseText) || classes.heading
            }
          >
            About
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <ul className={classes.expensionDetailsList}>
            <li className={classes.detailsItem}>
              <a
                href="https://arize.io/privacy"
                className={classes.panelLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                Privacy Policy
              </a>
            </li>
            <li className={classes.detailsItem}>
              <a
                href="https://arize.io/terms"
                className={classes.panelLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                Terms of use
              </a>
            </li>
            <li className={classes.detailsItem}>
              <a
                href="https://arize.io/blog"
                className={classes.panelLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                Blog
              </a>
            </li>
            <li className={classes.detailsItem}>
              <a
                href="https://arize.io/aboutus"
                className={classes.panelLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                About ARize
              </a>
            </li>
          </ul>
        </ExpansionPanelDetails>
      </ExpansionPanel>
      <ExpansionPanel
        className={`${classes.expensionPanel} ${classes.expensionPanelSupport}`}
        onChange={handlePanelChange("panelSupport")}
        expanded={expanded === "panelSupport"}
      >
        <ExpansionPanelSummary
          classes={{
            expandIcon:
              (classesMobile && classesMobile.expenseShowMoreIcon) ||
              classes.panelMoreIcon
          }}
          expandIcon={<ExpandMoreIcon />}
        >
          <i
            className={`fas fa-user-cog fa-2x ${(classesMobile &&
              classesMobile.expanseIcon) ||
              classes.detailsIcon}`}
          />
          <Typography
            className={
              (classesMobile && classesMobile.expanseText) || classes.heading
            }
          >
            Support
          </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <ul className={classes.expensionDetailsList}>
            <li className={classes.detailsItem}>
              <a
                href="https://tawk.to/chat/5bc5e01308387933e5bb7f75/default?$_tawk_sk=5ca4a9066fc2208539491486&$_tawk_tk=6d0c59b6c9e0a2f5c60bc3dcef0c9b18&v=655"
                className={classes.panelLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                Chat with us now
              </a>
            </li>
            <li className={classes.detailsItem}>
              <a
                className={classes.panelLink}
                target="_blank"
                rel="noopener noreferrer"
                href="https://arize.io/glossary"
              >
                References
              </a>
            </li>
            <li className={classes.detailsItem}>
              <a
                href="https://arize.io/tutorials"
                className={classes.panelLink}
                target="_blank"
                rel="noopener noreferrer"
              >
                Tutorials
              </a>
            </li>
            <li className={classes.detailsItem}>
              <BagReport />
            </li>
          </ul>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </div>
  );
};

export default React.memo(SidebarExpansionPanels);
