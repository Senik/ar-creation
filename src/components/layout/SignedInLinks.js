import React, { useState, Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';

//selectors
import { selectKeyword, selectResult } from '../../redux/post/post.selectors';
//actions
import { signOut } from '../../redux/auth/auth.actions';
import { setKeyword, clearSearchLength } from '../../redux/post/post.actions';
//MUI
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import Close from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
//styles
import classes from './Layout.module.scss';

const SignedInLinks = props => {
  const [searchInput, setSearchInput] = useState(false);
  const [text, setSearchText] = useState();
  useEffect(() => {
    setSearchText('');
    setSearchInput(false);
  }, [props.location.pathname]);
  const toggleInput = () => {
    setSearchInput(!searchInput);
    setSearchText('');
    props.setKeyword('');
  };

  const handleSearch = e => {
    setSearchText(e.target.value);
  };

  const handleKeyPress = e => {
    if (e.key === 'Enter') {
      props.setKeyword(text);
      e.target.blur();
    }
  };
  const clearResult = () => props.clearSearchLength();

  const { showSearchIcon, result } = props;
  const location = props.location.pathname;
  let searchName;
  if (location === '/creators') {
    searchName = 'Creators';
  } else if (location === '/argallery') {
    searchName = 'AR Gallery';
  } else if (location === '/prime/posts') {
    searchName = 'posts';
  } else if (location === '/prime/users') {
    searchName = 'users';
  } else if (location === '/faceargallery') {
    searchName = 'Face AR';
  } else {
    searchName = 'Posts';
  }

  return (
    <div className={classes.SignedInLinks}>
      <ul className="right">
        {searchInput && showSearchIcon ? (
          <Fragment>
            <li>
              <div>
                <InputBase
                  placeholder={`Search For ${searchName}`}
                  className={classes.searchInput}
                  defaultValue={text}
                  onChange={handleSearch}
                  onKeyPress={handleKeyPress}
                  autoFocus
                />
              </div>
            </li>
            <li>
              <div className={`${classes.searchResult} hide-on-small-only`}>
                Results: {result}
              </div>
            </li>
          </Fragment>
        ) : null}
      </ul>
      {showSearchIcon ? (
        <ul className="right">
          <li>
            <button className={classes.searchButton} onClick={toggleInput}>
              {searchInput ? <Close onClick={clearResult} /> : <SearchIcon />}
            </button>
          </li>
        </ul>
      ) : null}
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  keyword: selectKeyword,
  result: selectResult
});

const mapDispatchToProps = dispatch => {
  return {
    signOut: () => dispatch(signOut()),
    setKeyword: keyword => dispatch(setKeyword(keyword)),
    clearSearchLength: () => dispatch(clearSearchLength())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SignedInLinks));
