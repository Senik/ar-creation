import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
//actions
import { clearAuthError } from '../../redux/auth/auth.actions';

const SignedOutLinks = props => {
  const { clearAuthError } = props;
  return (
    <ul className="right SignInButtons">
      <li>
        <NavLink to="/signup" onClick={() => clearAuthError()}>
          Sign Up
        </NavLink>
      </li>
      <li>
        <NavLink to="/signin" onClick={() => clearAuthError()}>
          Sign In
        </NavLink>
      </li>
    </ul>
  );
};

SignedOutLinks.propTypes = {
  clearAuthError: PropTypes.func.isRequired
};

export default connect(
  null,
  { clearAuthError }
)(SignedOutLinks);
