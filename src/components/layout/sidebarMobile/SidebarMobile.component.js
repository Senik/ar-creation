import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
//selectors
import { selectCount } from '../../../redux/user/user.selectors';
import { selectCreatorBalance } from '../../../redux/auth/auth.selectors';
//actions
import { signOut } from '../../../redux/auth/auth.actions';
//MUI
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
//icons
import DefaultAvatar from '../../../default_avatar.png';
import ARStudio from '../../../assets/img/AR_studio.png';
import FaceAR from '../../../assets/img/icons/faceAR.png';
import CreatorsLogo from '../../../assets/img/icons/Creator.png';
//components
import SidebarExpansionPanels from '../SidebarExpansionPanels';

//styles
import classes from './sidebarMobile.module.scss';

class SidebarMobile extends React.Component {
  state = {
    top: false,
    left: false,
    bottom: false,
    right: false
  };

  toggleDrawer = (side, open) => () => {
    this.setState({
      [side]: open
    });
  };

  render() {
    const self = this;
    const { profile, signOut, notifLength, creatorBalance } = self.props;
    const { User_FullName, Buyers, User_ProfilePicURL, User_ID, Payout } = profile;
    const userAvatarUrl =
      User_ProfilePicURL !== 'ProfilePic URL'
        ? `${User_ProfilePicURL}?time=${new Date()}`
        : `https://triplee.info/Triple_E_Social/ProfilePictures/${User_ID}.jpg?time=${new Date()}`;
    const sideList = (
      <div className={classes.list}>
        <List>
          {[
            { text: User_FullName, icon: 'avatar', to: '/profile' },
            { text: 'My posts', icon: 'fas fa-home', to: '/' },
            { text: 'AR Gallery', icon: 'fas fa-list-ul', to: '/argallery' },
            { text: 'AR Studio', icon: 'arstudio', to: '/create' },
            { text: 'Creators', icon: 'creators', to: '/creators' },
            { text: 'Notifications', icon: 'fas fa-bell', to: '/notifications' }
          ].map(item => (
            <NavLink to={item.to} key={item.icon}>
              <ListItem button>
                <ListItemIcon className={item.icon === 'fas fa-bell' ? classes.NotifLi : null}>
                  {item.icon === 'avatar' ? (
                    <img
                      src={`${userAvatarUrl}`}
                      style={{ borderRadius: '50%' }}
                      onError={e => (e.target.src = DefaultAvatar)}
                      className={`responsive-img ${classes.avatar}`}
                      alt="Profile"
                    />
                  ) : item.icon === 'arstudio' ? (
                    <img
                      src={ARStudio}
                      className={`responsive-img  ${classes.avatar} ${classes.pageLogo}`}
                      alt="Profile"
                    />
                  ) : item.icon === 'creators' ? (
                    <img
                      src={CreatorsLogo}
                      className={`responsive-img  ${classes.avatar} ${classes.pageLogo}`}
                      alt="Profile"
                    />
                  ) : item.icon === 'faceAr' ? (
                    <img
                      src={FaceAR}
                      className={`responsive-img  ${classes.avatar} ${classes.pageLogo}`}
                      alt="Face ar"
                    />
                  ) : item.icon === 'fas fa-bell' && notifLength && notifLength > 0 ? (
                    <>
                      <i
                        className={`fas fa-bell fa-2x ${classes.icons}`}
                        aria-hidden="true"
                      />
                      <span className={classes.notifLength}>{notifLength}</span>
                    </>
                  ) : (
                              <i className={`${item.icon} fa-2x ${classes.icons}`} />
                            )}
                </ListItemIcon>
                <ListItemText
                  primary={item.text}
                  secondary={
                    item.icon === 'avatar' ? (
                      Buyers ? (
                        <span
                          className={classes.amount}
                        >{`Total Balance: \u20AC${(+creatorBalance).toFixed(2)}`}</span>
                      ) : null
                    ) : null
                  }
                  style={
                    item.icon === 'avatar' ||
                      item.icon === 'arstudio' ||
                      item.icon === 'faceAr'
                      ? { marginLeft: -6 }
                      : item.text === 'My posts'
                        ? { marginLeft: -3 }
                        : null
                  }
                />
              </ListItem>
            </NavLink>
          ))}
          {creatorBalance > 100 || Payout ?
            <NavLink to="/payout">
              <ListItem button>
                <ListItemIcon>
                  <i
                    className={`fas fa-wallet fa-2x ${classes.icons}`}
                    aria-hidden="true"
                  />
                </ListItemIcon>
                <ListItemText
                  primary="Payout"
                />
              </ListItem>
            </NavLink>
            : null}
        </List>
        <Divider />
        <List>
          <ListItem
            onClick={e => {
              e.stopPropagation();
            }}
          >
            <SidebarExpansionPanels classesMobile={classes} />
          </ListItem>
          {[
            {
              text: 'Log out',
              icon: 'fas fa-sign-out-alt',
              logOut: signOut
            }
          ].map(item => (
            <a
              href={item.to}
              key={item.icon}
              target={item.to ? '_blank' : null}
              onClick={item.logOut}
            >
              <ListItem button>
                <ListItemIcon>
                  <i className={`${item.icon} fa-2x ${classes.icons}`} />
                </ListItemIcon>
                <ListItemText primary={item.text} />
              </ListItem>
            </a>
          ))}

          <hr />
          <li>
            <p className={classes.platformVersion}>
              ARize AR platform, version 2.6.2.7
                </p>
          </li>
        </List>
      </div>
    );
    return (
      <Fragment>
        <IconButton
          className={`${classes.menuButton} deep-purple show-on-small hide-on-med-and-up`}
          onClick={self.toggleDrawer('left', true)}
          color="inherit"
        >
          <MenuIcon />
        </IconButton>
        <SwipeableDrawer
          open={self.state.left}
          onClose={self.toggleDrawer('left', false)}
          onOpen={self.toggleDrawer('left', true)}
        >
          <div
            tabIndex={0}
            role="button"
            //if call trigger on mobile when open modal automatically closes
            onClick={self.toggleDrawer('left', false)}
          //onKeyDown={this.toggleDrawer('left', false)}
          >
            {sideList}
          </div>
        </SwipeableDrawer>
      </Fragment>
    );
  }
}

SidebarMobile.propTypes = {
  signOut: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  notifLength: selectCount,
  creatorBalance: selectCreatorBalance
});

const mapDispatchToProps = dispatch => {
  return {
    signOut: () => dispatch(signOut())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SidebarMobile);
