import React from 'react';
import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
//selectors
import {
  selectProfile,
  selectCreatorBalance
} from '../../../redux/auth/auth.selectors';
import { selectCount } from '../../../redux/user/user.selectors';
//components
import SidebarExpansionPanels from '../SidebarExpansionPanels';
//actions
import { signOut } from '../../../redux/auth/auth.actions';
//styles
import classes from './sidenavLinks.module.scss';
//icons
import DefaultAvatar from '../../../default_avatar.png';
import ARStudio from '../../../assets/img/AR_studio.png';
import CreatorsLogo from '../../../assets/img/icons/Creator.png';

const SideNavLinks = props => {
  const { profile, location, signOut, notifLength, creatorBalance } = props;
  const { User_ID, User_ProfilePicURL, Buyers, isEmpty, User_FullName } = {
    ...profile
  };
  const avatarImageUrl =
    User_ProfilePicURL !== 'ProfilePic URL'
      ? `${User_ProfilePicURL}?time=${new Date()}`
      : `https://triplee.info/Triple_E_Social/ProfilePictures/${User_ID}.jpg?time=${new Date()}`;
  return (
    <div className="col  m2 hide-on-small-only">
      {isEmpty || location.pathname.includes('/creator/') ? null : (
        <ul className={classes.sidenav}>
          <li className={classes.item}>
            <NavLink to="/profile" style={{ position: 'relative' }}>
              <img
                style={{ borderRadius: '50%' }}
                src={avatarImageUrl}
                onError={e => (e.target.src = DefaultAvatar)}
                className={`responsive-img  ${classes.avatar}`}
                alt="User Avatar"
              />
              <span className={classes.userName}>{User_FullName}</span>
              {Buyers ? (
                <span
                  className={`${classes.amount} ${classes.amountColor}`}
                >{`Total Balance: \u20AC${(+creatorBalance).toFixed(2)}`}</span>
              ) : null}
            </NavLink>
          </li>
          <li className={` ${classes.item}`}>
            <NavLink exact={true} to="/">
              <i className={`fas fa-home fa-2x ${classes.icons}`} />
              <span className={` ${classes.links}`}>My Posts</span>
            </NavLink>
          </li>
          <li className={` ${classes.item}`}>
            <NavLink to="/argallery">
              <i
                className={`fas fa-list-ul fa-2x ${classes.icons}`}
                aria-hidden="true"
              />
              <span className={` ${classes.links}`}>AR Gallery</span>
            </NavLink>
          </li>
          <li className={classes.item}>
            <NavLink to="/create">
              <img
                src={ARStudio}
                onError={e => (e.target.src = DefaultAvatar)}
                className={`responsive-img profile-img ${classes.avatar} ${classes.pageIcon}`}
                alt="User Avatar"
              />
              <span className={classes.arStudio}>AR Studio</span>
            </NavLink>
          </li>
          <li className={classes.item}>
            <NavLink to="/creators">
              <img
                src={CreatorsLogo}
                onError={e => (e.target.src = DefaultAvatar)}
                className={`responsive-img profile-img ${classes.avatar} ${classes.pageIcon}`}
                alt="User Avatar"
              />
              <span className={classes.arStudio}>Creators</span>
            </NavLink>
          </li>
          <li className={`${classes.item} ${classes.notifLI}`}>
            <NavLink to="/notifications">
              <i
                className={`fas fa-bell fa-2x ${classes.icons}`}
                aria-hidden="true"
              />
              <span className={classes.links}>Notifications</span>
              {notifLength && notifLength > 0 ? (
                <span className={classes.notifLength}>{notifLength}</span>
              ) : null}
            </NavLink>
          </li>
          {(creatorBalance > 100 || profile.Payout) && (
            <li className={classes.item}>
              <NavLink to="/payout">
                <i
                  className={`fas fa-wallet fa-2x ${classes.icons}`}
                  aria-hidden="true"
                />
                <span className={classes.links}>Payout</span>
              </NavLink>
            </li>
          )}
          <li className={classes.item}>
            <SidebarExpansionPanels style={{ cursor: 'pointer' }} userID={User_ID} />
          </li>
          <li
            className={classes.item}
            onClick={signOut}
            style={{ cursor: 'pointer' }}
          >
            <i className={`fas fa-sign-out-alt fa-2x ${classes.icons}`} />
            <span className={classes.links}>Log Out</span>
          </li>
        </ul>
      )}
    </div>
  );
};

SideNavLinks.propTypes = {
  signOut: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  profile: selectProfile,
  notifLength: selectCount,
  creatorBalance: selectCreatorBalance
});

export default connect(
  mapStateToProps,
  { signOut }
)(withRouter(React.memo(SideNavLinks)));
