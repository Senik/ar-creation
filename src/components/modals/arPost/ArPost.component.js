import React from 'react';

//MUI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

//components
import ArpostView from '../../../pages/arPost/ArPostView';
//styles
import './ArPost.styles.scss';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const ArPostModal = props => {
  const { post, postIsBought } = props;
  const [open, setOpen] = React.useState(false);

  function handleClickOpen() {
    setOpen(true);
  }
  function handleClose() {
    setOpen(false);
  }
  return (
    <div className="arpost-modal">
      <Button
        className="btn-large indigo white-text waves-effect waves-light z-depth-3 "
        onClick={handleClickOpen}
      >
        {post.IsPaid
          ? postIsBought
            ? 'View'
            : `\u20AC${post.Price} Buy now`
          : 'View'}
      </Button>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className="app-bar">
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <ArpostView {...props} />
      </Dialog>
    </div>
  );
};

export default ArPostModal;
