import React, { useState, useEffect } from "react";
//MUI
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
//components
import InputElement from "../../formElements/InputElement";
//styles
import "./ArPost.styles.scss";

const EnterPriseModal = props => {
  const {
    isOpen,
    handleCancelPayment,
    handleSubmitPayment,
    handleModalChange,
    credentials
  } = props;
  const { companyName, companyAddress, companyVat } = credentials;
  const [isDisabled, setDisabled] = useState(false);
  useEffect(() => {
    if (!companyName || !companyAddress || !companyVat) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [companyName, companyAddress, companyVat]);
  return (
    <Dialog
      open={isOpen}
      aria-labelledby="responsive-dialog-title"
      fullWidth={true}
      maxWidth={"sm"}
      style={{ textAlign: "center" }}
    >
      <DialogTitle id="responsive-dialog-title">
        Please fill the form!
      </DialogTitle>
      <DialogContent>
        <InputElement
          name="companyName"
          defaultValue={companyName}
          label="Company name"
          required={true}
          handleChange={handleModalChange}
        />
        <InputElement
          name="companyAddress"
          defaultValue={companyAddress}
          label="Company Address"
          required={true}
          handleChange={handleModalChange}
        />
        <InputElement
          name="companyVat"
          defaultValue={companyVat}
          label="Company VAT"
          required={true}
          handleChange={handleModalChange}
        />
      </DialogContent>
      <DialogActions
        style={{ justifyContent: "center", paddingBottom: "15px" }}
      >
        <Button
          onClick={handleCancelPayment}
          className="blue white-text enterprise-modal-cancel-btn"
        >
          Cancel
        </Button>
        <Button
          disabled={isDisabled}
          onClick={handleSubmitPayment}
          className="red white-text enterprise-modal-submit-btn"
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default React.memo(EnterPriseModal);
