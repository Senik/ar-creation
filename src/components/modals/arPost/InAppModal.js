import React from 'react';
//MUI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
//styles
import './ArPost.styles.scss';

const InAppModal = ({ isOpen, close, text, description, btnClassname }) => (
    <Dialog
        open={isOpen}
        onClose={close}
        aria-labelledby="responsive-dialog-title"
        fullWidth={true}
        maxWidth={'sm'}
        style={{ textAlign: 'center' }}
        className="InAppModal"
    >
        <DialogTitle id="responsive-dialog-title">{text}</DialogTitle>
        {description && (
            <DialogContent>
                <DialogContentText className="inappmodal-dialog-content">{description}</DialogContentText>
                <DialogContentText className="inappmodal-dialog-content">You'll be notified once it is approved or rejected.</DialogContentText>
            </DialogContent>
        )}
        <DialogActions style={{ justifyContent: 'center', paddingBottom: '15px' }}>
            <Button onClick={close} className={`${btnClassname ? 'green' : 'red'} white-text`} autoFocus>
                OK
            </Button>
        </DialogActions>
    </Dialog>
);

export default InAppModal;