import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
//MUI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const SignInModal = (props) => {
    const { isOpen, close } = props;
    const [redirectUserTo, setRedirectUserTo] = useState(false);
    const redirectTo = () => setRedirectUserTo(true);

    return (
        redirectUserTo ?
            <Redirect to="/signin" /> :
            <Dialog
                open={isOpen}
                onClose={close}
                aria-labelledby="responsive-dialog-title"
                fullWidth={true}
                maxWidth={'sm'}
                style={{ textAlign: 'center' }}
            >
                <DialogTitle id="responsive-dialog-title">Please sign in</DialogTitle>
                <DialogContent>
                </DialogContent>
                <DialogActions style={{ justifyContent: 'center', paddingBottom: '15px' }}>
                    <Button onClick={close} className="red white-text">
                        OK
            </Button>
                    <Button onClick={redirectTo} className="blue white-text" autoFocus>
                        Sign In
            </Button>
                </DialogActions>
            </Dialog>
    );
}
export default SignInModal;