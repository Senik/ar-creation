import React, { useState } from "react";
import { connect } from "react-redux";
//actions
import { sendReport } from "../../../redux/auth/auth.actions";
//MUI
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";

function BugReport(props) {
  const [open, setOpen] = useState(false);
  const [openSelect, setOpenSelect] = useState(false);
  const [report, setReport] = useState("");
  const [reportText, setReportText] = useState("");

  function handleSelectOpen() {
    setOpenSelect(true);
  }
  function handleSelectClose() {
    setOpenSelect(false);
  }

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  function handleSelectChange(e) {
    setReport(e.target.value);
  }

  function handleChange(e) {
    setReportText(e.target.value);
  }

  function handleSubmit(e) {
    e.preventDefault();
    const title = report.charAt(0).toUpperCase() + report.slice(1);
    const reportObj = {
      title,
      text: reportText
    };
    props.sendReport(reportObj);
    setOpen(false);
  }

  return (
    <div>
      <span
        onClick={handleClickOpen}
        style={{ color: "black", cursor: "pointer" }}
      >
        Report a bug or request a feature
      </span>

      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Report a bug or request a feature"}
        </DialogTitle>
        <DialogContent>
          <form onSubmit={handleSubmit} style={{ marginTop: 0 }}>
            <FormControl style={{ minWidth: 180 }}>
              <InputLabel htmlFor="report">Report Type</InputLabel>
              <Select
                open={openSelect}
                onClose={handleSelectClose}
                onOpen={handleSelectOpen}
                value={report}
                onChange={handleSelectChange}
                inputProps={{
                  name: "report",
                  id: "report"
                }}
              >
                <MenuItem value={"bug"}>Bug Report</MenuItem>
                <MenuItem value={"feature"}>Request Feature</MenuItem>
              </Select>
            </FormControl>
            <div className="input-field">
              <label htmlFor="text">Description*</label>
              <textarea
                id="reportText"
                className="materialize-textarea"
                onChange={handleChange}
              />
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} autoFocus className="grey white-text">
            Cancel
          </Button>
          <Button onClick={handleSubmit} className="blue white-text">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
const mapDispatchToProps = dispatch => {
  return {
    sendReport: report => dispatch(sendReport(report))
  };
};

export default connect(null, mapDispatchToProps)(BugReport);
