import React, { useState } from "react";
//MUI
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import { Delete } from "@material-ui/icons";

function Confirm(props) {
  const [open, setOpen] = useState(false);

  function handleClose() {
    setOpen(false);
  }

  function handleClickOpen() {
    setOpen(true);
  }
  function handleDelete() {
    const { onDelete, post } = props;
    setOpen(false);
    onDelete(post);
  }

  return (
    <div>
      <IconButton
        aria-label="Remove post"
        style={{ backgroundColor: "transparent" }}
        onClick={handleClickOpen}
      >
        <Delete />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure you want to delete post ?"}
        </DialogTitle>

        <DialogActions>
          <Button onClick={handleClose} autoFocus className="blue white-text">
            Cancel
          </Button>
          <Button onClick={handleDelete} className="red white-text">
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}

export default Confirm;
