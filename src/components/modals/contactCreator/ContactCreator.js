import React, { useState } from "react";
import { connect } from "react-redux";
//actions';
import { contact_creator } from "../../../redux/auth/auth.actions";
//MUI
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
//components
import SignInModal from "../../modals/arPost/SignInModal";
//styles
import classes from "./ContactCreator.module.scss";

function ContactCreator(props) {
  const [open, setOpen] = useState(false);
  const text = useFormInput("");
  const title = useFormInput("");

  function handleContactCreator(e) {
    e.preventDefault();
    const { creator, contact_creator, currUser_profile } = props;

    setOpen(false);
    const contactData = new FormData();
    contactData.append("creator_name", creator.User_FullName);
    contactData.append("creator_email", creator.User_Email);
    contactData.append("user_name", currUser_profile.User_FullName);
    contactData.append("user_email", currUser_profile.User_Email);
    contactData.append("msg_title", title.value);
    contactData.append("msg_text", text.value);
    contact_creator(contactData);
  }
  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  const { currUser_profile } = props;
  return (
    <div className="center-align">
      <Button
        className={`waves-effect waves-light white ${classes.contactButton}`}
        color="primary"
        onClick={handleClickOpen}
      >
        Contact
      </Button>
      {!currUser_profile.isEmpty ? (
        <Dialog
          className={classes.ContactCreatorDialog}
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogTitle
            id="customized-dialog-title"
            onClose={handleClose}
            disableTypography
            className={classes.dialogTitle}
          >
            <Typography variant="h6">Contact with creator</Typography>
            <IconButton
              aria-label="Close"
              className={classes.closeButton}
              onClick={handleClose}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent className={classes.dialogContent}>
            <form
              className={classes.contactCreatorForm}
              onSubmit={handleContactCreator}
            >
              <div className="input-field">
                <label htmlFor="title">Title</label>
                <input type="text" id="title" required {...text} />
              </div>
              <div className="input-field">
                <label htmlFor="text">Text</label>
                <input type="text" id="text" required {...title} />
              </div>
              <div className="input-field">
                <button className="btn waves-effect  pink lighten-1">
                  Send message
                </button>
              </div>
            </form>
            <Typography gutterBottom className={classes.descText}>
              By clicking "Send" button, the Creator will receive Your message,
              name and email.
            </Typography>
          </DialogContent>
        </Dialog>
      ) : (
        <SignInModal isOpen={open} close={handleClose} />
      )}
    </div>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    contact_creator: data => dispatch(contact_creator(data))
  };
};

export default connect(null, mapDispatchToProps)(ContactCreator);

function useFormInput(initialValue) {
  const [value, setValue] = useState(initialValue);
  function handleChange(e) {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  };
}
