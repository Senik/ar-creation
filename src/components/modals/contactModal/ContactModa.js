import React, { useState, useEffect } from "react";
//MUI
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
//components
import InputElement from "../../formElements/InputElement";
//styles
import "./ContactModal.styles.scss";

const ContactModal = props => {
  const {
    isOpen,
    cancelContact,
    submitContact,
    handleModalChange,
    credentials,
    modalTitle
  } = props;

  const [isDisabled, setDisabled] = useState(false);
  useEffect(() => {
    if (hasNull(credentials)) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [credentials]);

  function hasNull(obj) {
    for (let prop in obj) {
      if (!obj[prop]) return true;
    }
    return false;
  }

  function convertCamelCase(string) {
    return string.replace(/([a-z0-9])([A-Z])/g, "$1 $2").replace("user ", "");
  }
  return (
    <Dialog
      open={isOpen}
      aria-labelledby="responsive-dialog-title"
      fullWidth={true}
      maxWidth={"sm"}
      style={{ textAlign: "center" }}
    >
      <DialogTitle id="responsive-dialog-title">{modalTitle}</DialogTitle>
      <DialogContent>
        {Object.keys(credentials).map(item => {
          return (
            <InputElement
              key={item}
              name={item}
              defaultValue={credentials[item]}
              label={convertCamelCase(item)}
              required={true}
              handleChange={handleModalChange}
            />
          );
        })}
      </DialogContent>
      <DialogActions
        style={{ justifyContent: "center", paddingBottom: "15px" }}
      >
        <Button
          onClick={cancelContact}
          className="blue white-text enterprise-modal-cancel-btn"
        >
          Cancel
        </Button>
        <Button
          disabled={isDisabled}
          onClick={submitContact}
          className="red white-text enterprise-modal-submit-btn"
        >
          Submit
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default React.memo(ContactModal);
