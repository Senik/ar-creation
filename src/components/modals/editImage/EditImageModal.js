import React, { Component } from 'react';
import ReactCrop from 'react-image-crop';
//MUI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
//styles
import '../triggerImageCrop/CropTargetImage.css';
import classes from './EditImageModal.module.scss';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

class EditImageModal extends Component {
  state = {
    crop: {
      width: 50,
      height: 50,
      x: 10,
      y: 0
    }
  };

  componentDidMount() {
    if (this.props.fixCropSize) {
      const crop = {
        aspect: 1,
        width: 50,
        height: 50,
        x: 10,
        y: 10
      };
      this.setState({ crop: crop });
    }
  }

  onImageLoaded = (image, pixelCrop) => {
    this.imageRef = image;
    this.makeClientCrop(this.state.crop, pixelCrop);
  };
  onCropComplete = (crop, pixelCrop) => {
    this.makeClientCrop(crop, pixelCrop);
  };
  makeClientCrop = async (crop, pixelCrop) => {
    const self = this;
    if (self.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await self.getCroppedImg(
        self.imageRef,
        pixelCrop,
        'newFile.jpeg'
      );
      self.setState({
        croppedImageUrl
      });
      self.props.makeClientCrop(croppedImageUrl);
    }
  };
  getCroppedImg(image, pixelCrop, fileName) {
    const canvas = document.createElement('canvas');
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(
      image,
      pixelCrop.x,
      pixelCrop.y,
      pixelCrop.width,
      pixelCrop.height,
      0,
      0,
      pixelCrop.width,
      pixelCrop.height
    );

    const base64Image = canvas.toDataURL('image/jpeg');
    return base64Image;
  }
  onCropChange = crop => {
    this.setState({ crop });
  };

  render() {
    const self = this;
    const { handleClose, openModal, src } = self.props;

    const closeModal = () => {
      handleClose();
    };

    return (
      <div className={classes.EditImageModal}>
        <Dialog
          fullScreen
          open={openModal}
          onClose={handleClose}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar}>
            <Toolbar>
              <IconButton
                edge='start'
                color='inherit'
                onClick={handleClose}
                aria-label='Close'
              >
                <CloseIcon />
              </IconButton>
              <Typography variant='h6' className={classes.title}>
                Crop Image
              </Typography>
              <Button color='inherit' onClick={closeModal}>
                crop
              </Button>
            </Toolbar>
          </AppBar>
          <div className={classes.cropContainer}>
            <ReactCrop
              src={src}
              crop={self.state.crop}
              onImageLoaded={self.onImageLoaded}
              onComplete={self.onCropComplete}
              onChange={self.onCropChange}
              imageStyle={{ width: 400, maxHeight: 'none' }}
            />
          </div>
        </Dialog>
      </div>
    );
  }
}

export default EditImageModal;
