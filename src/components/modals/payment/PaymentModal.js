import React from 'react';
//MUI
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

const PaymentModal = props => (
  <Dialog
    open={true}
    aria-labelledby="responsive-dialog-title"
    fullWidth={true}
    maxWidth={'sm'}
    style={{ textAlign: 'center' }}
  >
    <DialogTitle id="responsive-dialog-title">{props.children}</DialogTitle>
  </Dialog>
);

export default PaymentModal;