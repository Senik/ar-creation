import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
//MUI
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
//components
import SketchfabViewerIframe from '../../shared/SketchfabViewer';
import GooglePolyIframe from '../../shared/GooglePolyIframe';
//icons
import Guideline from '../../../assets/img/Guideline.png';
//styles
import classes from './TriggerFullModal.module.scss';

class TriggerFullModal extends React.Component {
  state = {
    open: false
  };
  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  showFullTarget = () => {
    const { post, triggerBaseUrl } = this.props;
    const { ARSourceURL, ARTargetName } = { ...post };
    if (post.XRGallery === 'True') {
      switch (post.ARSource) {
        case 'SF':
          return <SketchfabViewerIframe link={ARSourceURL} />;
        case 'GP':
          return <GooglePolyIframe link={ARSourceURL} />;
        default:
          return (
            <img
              className={classes.fullTargetImage}
              src={`${triggerBaseUrl}${ARTargetName}.jpg?${new Date()}`}
              alt="Trigger"
            />
          );
      }
    }
    return (
      <div className="card-image" style={{ textAlign: 'center' }}>
        <img
          className={classes.fullTargetImage}
          src={`${triggerBaseUrl}${ARTargetName}.jpg?${new Date()}`}
          alt="Trigger"
        />
      </div>
    );
  };
  render() {
    const self = this;
    const { post, triggerBaseUrl } = self.props;
    const { open } = self.state;
    return (
      <Fragment>
        <div
          className={`card-image ${classes.TriggerFullModal}`}
          onClick={self.handleClickOpen}
        >
          <img
            className={'CARD_IMAGE'}
            src={`${triggerBaseUrl}${post.ARTargetName}.jpg?${new Date()}`}
            onError={e =>
              (e.target.src =
                'https://triplee.info/Triple_E_WebService/AllImageTargets/correct3.jpg')
            }
            alt="Post trigger"
          />
          <span className={`card-title cardTitle ${classes.cardTitle}`}>
            {post.XRGallery === 'True' && !post.ARTargetID
              ? 'Click to open model'
              : 'You can scan target right from screen'}{' '}
            !
          </span>
        </div>
        <Dialog
          className={classes.TriggerFullModalDialog}
          maxWidth={'md'}
          fullWidth={true}
          onClose={self.handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogTitle
            id="customized-dialog-title"
            onClose={self.handleClose}
            disableTypography
            className={classes.dialogTitle}
          >
            <Typography variant="h6">
              {`${post.PostName}`}
            </Typography>
            <IconButton
              aria-label="Close"
              className={classes.closeButton}
              onClick={self.handleClose}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>

          <div className="card-image" style={{ textAlign: 'center' }}>
            {self.showFullTarget()}
          </div>
          {post.XRGallery !== 'True' && (
            <div className="card-image center-align">
              <img
                className={classes.modalImage}
                src={Guideline}
                alt="Demo helper"
              />
            </div>
          )}
        </Dialog>
      </Fragment>
    );
  }
}

TriggerFullModal.propTypes = {
  post: PropTypes.object.isRequired,
  triggerBaseUrl: PropTypes.string.isRequired
};

export default TriggerFullModal;
