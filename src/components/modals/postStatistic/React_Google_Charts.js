import React from 'react';
import PropTypes from 'prop-types';
import Chart from 'react-google-charts';
import key from '../../../config/googleMapApi';

const React_Google_Charts = ({ cities }) => {
  const locStatData = [['City', 'Number of views']];
  cities &&
    cities.map(city => {
      let count = 0;
      for (let i = 0; i < cities.length; i++) {
        if (city === cities[i]) count++;
      }
      return locStatData.push([city, count]);
    });

  return (
    <div>
      <Chart
        width={'100%'}
        height={'300px'}
        chartType="GeoChart"
        data={locStatData}
        options={{
          displayMode: 'markers',
          colorAxis: { colors: ['green', 'blue'] }
        }}
        mapsApiKey={key}
        rootProps={{ 'data-testid': '1' }}
      />
    </div>
  );
};

React_Google_Charts.propTypes = {
  cities: PropTypes.array.isRequired
};

export default React_Google_Charts;
