import React, { useState } from "react";
import PropTypes from "prop-types";
//helpers
import { countryCodeRequest } from "../../../helpers";
//MUI
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import CloseIcon from "@material-ui/icons/Close";
import Slide from "@material-ui/core/Slide";
import Icon from "@material-ui/core/Icon";
import ToolTip from "@material-ui/core/Tooltip";
import CardContent from "@material-ui/core/CardContent";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
//components
import ReactGoogleCharts from "./React_Google_Charts";
//styles
import classes from "./PostStatistic.module.scss";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function FullScreenDialog(props) {
  const [open, setOpen] = useState(false);
  const [cities, setCities] = useState([]);
  async function handleClickOpen() {
    const { countries } = props.statistics;
    setOpen(true);
    if (countries) {
      const cities = await countryCodeRequest(countries);
      setCities(cities);
    }
  }

  function handleClose() {
    setOpen(false);
  }

  const { post, statistics } = props;
  const {
    maleCountPercent,
    femaleCountPercent,
    otherCountPercent,
    notSpecifiedCountPercent
  } = statistics;

  const moreInfo = [
    {
      views: post.NumberOfViews,
      icon: "fas fa-eye",
      text: "Total AR views"
    },
    {
      views: (post.UniqueViewes && Object.keys(post.UniqueViewes).length) || 0,
      icon: "fas fa-user-circle",
      text: "Unique Views"
    },
    {
      views: `${otherCountPercent || 0}%`,
      icon: "fas fa-users",
      text: "Other viewers"
    },
    {
      views: `${maleCountPercent || 0}%`,
      icon: "fas fa-mars",
      text: "Male Viewers"
    },
    {
      views: `${femaleCountPercent || 0}%`,
      icon: "fas fa-venus",
      text: "Female Viewers"
    },
    {
      views: `${notSpecifiedCountPercent || 0}%`,
      icon: "fas fa-genderless",
      text: "Not specified gender"
    },
    {
      views: post.NumberOfPicsTaken || 0,
      icon: "fas fa-image",
      text: "Pics taken"
    },
    {
      views: post.NumberOfVideosTaken || 0,
      icon: "fas fa-file-video",
      text: "Videos taken"
    },
    {
      views: post.NumberOfGifsTaken || 0,
      icon: "fas fa-images",
      text: "Gifs taken"
    }
  ];

  return (
    <div className={classes.StatModal}>
      <ToolTip title="See more" placement="top-start">
        <IconButton
          className={classes.postInfoIconlink}
          onClick={handleClickOpen}
          aria-label="Show more"
        >
          <Icon>show_chart</Icon>
        </IconButton>
      </ToolTip>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              onClick={handleClose}
              aria-label="Close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.flex}>
              {post.PostName}
            </Typography>
          </Toolbar>
        </AppBar>
        <CardContent className={classes.card_content}>
          <Grid container className={classes.root}>
            <Grid item xs={12}>
              <Grid container className={classes.demo} justify="center">
                {moreInfo.map(value => (
                  <Grid key={value.text} xs={12} md={4} item>
                    <Paper className={classes.paper}>
                      <p className={classes.more_info_content}>{value.views}</p>
                      <i className={`${value.icon} fa-3x`} />
                      <p className={classes.more_info_content}>{value.text}</p>
                    </Paper>
                  </Grid>
                ))}
                <div
                  style={{
                    width: "100%",
                    height: "400px",
                    textAlign: "center"
                  }}
                >
                  <ReactGoogleCharts cities={cities} />
                </div>
              </Grid>
            </Grid>
          </Grid>
        </CardContent>
      </Dialog>
    </div>
  );
}

FullScreenDialog.propTypes = {
  post: PropTypes.object.isRequired,
  countries: PropTypes.array,
  cities: PropTypes.array
};

export default FullScreenDialog;
