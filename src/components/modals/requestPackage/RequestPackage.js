import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { selectProfile } from "../../../redux/auth/auth.selectors";
import axios from "axios";

//MUI
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";

//components
import PackageCard from "../../packageCard/PackageCard";

//data
import { packages } from "./packageDetails.data";
import { EU_Countries } from "../../../helpers/eu_countries";
//styles
import "./RequestPackage.styles.scss";

function RequestPackage(props) {
  const [open, setOpen] = useState(false);
  const [userType, setUserType] = useState(0);

  useEffect(() => {
    const { profile } = props;
    setOpen(props.packageModalOpen);
    let userTypeIndex = profile.User_Type === "enterprise" ? 1 : 0;

    setUserType(userTypeIndex);
  }, [props.profile]);

  function makePaymentSubscription(redirectUrl, country, price) {
    let vat = 0;
    if (
      (EU_Countries.includes(country) &&
        props.profile.User_Type === "individual") ||
      (props.profile.User_Type === "enterprise" && country === "Netherlands")
    ) {
      let decimalVAT = (+price * 21) / 100;
      vat = Math.round(decimalVAT * 100) / 100;
    }
    const subscriptionPrice = +price + +vat;
    const url =
      "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/buyPackage.php";
    const data = new FormData();
    data.append("value", (+subscriptionPrice).toFixed(2));
    data.append("url", redirectUrl);
    props.setLoader();
    axios({
      method: "POST",
      url,
      data
    })
      .then(res => {
        let checkoutURL = res.data._links.checkout.href;
        localStorage.setItem("paymentData", JSON.stringify(res.data));
        localStorage.setItem("User_ID", props.profile.User_ID);
        localStorage.setItem("package_price", price);
        window.location.href = checkoutURL;
      })
      .catch(err => console.log("Payment error", err));
  }

  function handleClickOpen() {
    setOpen(true);
  }

  function handleClose() {
    if (localStorage.getItem("requestPackage")) {
      localStorage.removeItem("requestPackage");
    }
    setOpen(false);
  }
  return (
    <div className="package-modal-container">
      <Button
        className="waves-effect waves-light"
        variant="contained"
        color="primary"
        onClick={handleClickOpen}
        disabled={props.profile.User_Location === "Location"}
      >
        Update Package
      </Button>
      {props.profile.User_Location === "Location" && (
        <div className="red-text">
          <p>Please select your location to continue</p>
        </div>
      )}

      <Dialog
        className="package-modal"
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          disableTypography
          className="modal-title"
        >
          <Typography variant="h6">Request Package</Typography>
          <IconButton
            aria-label="Close"
            className="close-button"
            onClick={handleClose}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent className="modal-content">
          <div className="cards-container">
            {packages[userType].map(packageDetails => {
              return (
                <PackageCard
                  key={packageDetails.name}
                  {...packageDetails}
                  makePayment={makePaymentSubscription}
                  profile={props.profile}
                />
              );
            })}
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
}

const mapStateToProps = createStructuredSelector({
  profile: selectProfile
});

export default connect(mapStateToProps)(React.memo(RequestPackage));
