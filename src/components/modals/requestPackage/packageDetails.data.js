export const packages = [
  [
    {
      name: "Plus",
      price: "per month billed",
      pricePeriod: { monthly: "15", annually: "10" },
      vat: "VAT not included",
      options: [
        "Ability to sell models",
        "Premium Support",
        "12 hours activation time",
        "Commercial use"
      ]
    },
    {
      name: "Premium",
      price: "Get a quote",
      options: [
        "Ability to sell models",
        "Premium Support",
        "1 hour activation time",
        "Commercial use"
      ]
    }
  ],
  [
    {
      name: "Plus",
      price: "per month billed",
      pricePeriod: { monthly: "39.99", annually: "34.99" },
      vat: "VAT not included",
      options: [
        "Ability to sell models",
        "Premium Support",
        "12 hours activation time",
        "Commercial use"
      ]
    },
    {
      name: "Premium",
      price: "Get a quote",
      options: [
        "Ability to sell models",
        "Premium Support",
        "1 hour activation time",
        "Commercial use"
      ]
    }
  ]
];
