import React from 'react';
//MUI
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
//components
import CropTargetImage from './CropTargetImage';
//styles
import classes from './TriggerImageCrop.module.scss';

class CropDialog extends React.Component {
    state = {};
    handleCloseSaveCroppedImage = () => {
        this.props.handleTargetImage(this.state.croppedImage)
    };
    handleTargetImageCrop = img => {
        this.setState({
            croppedImage: img
        });
    };
    handleTargetImageLoaded = () => {
        this.setState({
            targetImageLoaded: true
        });
    };
    render() {
        const self = this;
        const { targetImage, targetID, open, onClose } = self.props;
        const { targetImageLoaded } = self.state;
        return (
            <Dialog
                className={classes.triggerEditModal}
                onClose={onClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <MuiDialogTitle disableTypography className={classes.root}>
                    <Typography variant="h6">
                        Edit {targetID ? 'Trigger' : 'Thumbnail'} Image
                    </Typography>

                    <IconButton
                        aria-label="Close"
                        className={classes.closeButton}
                        onClick={onClose}
                    >
                        <CloseIcon />
                    </IconButton>
                </MuiDialogTitle>
                <MuiDialogContent>
                    <CropTargetImage
                        targetID={targetID}
                        targetImage={targetImage}
                        handleTargetImage={self.handleTargetImageCrop}
                        handleTargetImageLoaded={self.handleTargetImageLoaded}
                        fixCropSize={this.props.fixCropSize}
                    />
                </MuiDialogContent>
                <MuiDialogActions>
                    {targetImageLoaded && (
                        <Button
                            onClick={self.handleCloseSaveCroppedImage}
                            color="primary"
                        >
                            Crop image
                        </Button>
                    )}
                </MuiDialogActions>
            </Dialog>
        );
    }
}

export default CropDialog;
