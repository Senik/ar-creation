import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import ReactCrop from "react-image-crop";
import imageCompression from "browser-image-compression";
//helpers
import { dataURItoBlob } from "../../../helpers";
//MUI
import Button from "@material-ui/core/Button";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
//icons
import SPINNER from "../../../assets/img/spinners/index.gooey-ring-spinner.svg";
//styles
import "./CropTargetImage.css";
import classes from "./TriggerImageCrop.module.scss";

const toDataURL = url =>
  fetch(url)
    .then(response => response.blob())
    .then(
      blob =>
        new Promise((resolve, reject) => {
          const reader = new FileReader();
          reader.onloadend = () => resolve(reader.result);
          reader.onerror = reject;
          reader.readAsDataURL(blob);
        })
    );

class CropTargetImage extends PureComponent {
  state = {
    src: null,
    crop: {
      width: 50,
      height: 50,
      x: 10,
      y: 20
    },
    imageSizeError: "",
    loading: false
  };
  onSelectFile = e => {
    this.setState({
      loading: true
    });
    const currentFile = e.target.files[0];
    if (currentFile.size > 1572864) {
      const reader = new FileReader();
      reader.addEventListener(
        "load",
        async () => {
          const options = {
            maxSizeMB: 2,
            maxWidthOrHeight: 1920,
            useWebWorker: true
          };

          try {
            const compressedFile = await imageCompression(
              dataURItoBlob(reader.result),
              options
            );
            if (compressedFile.size > 2242880) {
              this.setState({
                imageSizeError: "The upladed trigger image limit is 2mb",
                loading: false
              });
              return null;
            }
            const uri = await imageCompression.getDataUrlFromFile(
              compressedFile
            );
            this.setState({
              src: uri,
              loading: false
            });
          } catch (err) {
            console.log(err);
          }
        },
        false
      );
      reader.readAsDataURL(currentFile);
    } else {
      const reader = new FileReader();
      reader.addEventListener("load", () =>
        this.setState({
          src: reader.result,
          loading: false
        })
      );
      reader.readAsDataURL(currentFile);
    }
  };

  onImageLoaded = (image, pixelCrop) => {
    this.props.handleTargetImageLoaded();
    this.imageRef = image;
    this.makeClientCrop(this.state.crop, pixelCrop);
  };

  onCropComplete = (crop, pixelCrop) => {
    this.makeClientCrop(crop, pixelCrop);
  };

  onCropChange = crop => {
    this.setState({ crop });
  };

  makeClientCrop = async (crop, pixelCrop) => {
    const self = this;
    if (self.imageRef && crop.width && crop.height) {
      const croppedImageUrl = await self.getCroppedImg(
        self.imageRef,
        pixelCrop
      );
      self.setState({ croppedImageUrl }, () =>
        self.props.handleTargetImage(self.state.croppedImageUrl)
      );
    }
  };

  getCroppedImg(image, pixelCrop) {
    const canvas = document.createElement("canvas");
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext("2d");

    ctx.drawImage(
      image,
      pixelCrop.x,
      pixelCrop.y,
      pixelCrop.width,
      pixelCrop.height,
      0,
      0,
      pixelCrop.width,
      pixelCrop.height
    );
    const base64Image = canvas.toDataURL("image/jpeg");
    return base64Image;
  }

  componentDidMount() {
    const { targetImage, targetID } = this.props;

    toDataURL(
      `https://cors-anywhere.herokuapp.com/https://triplee.info/${
        !targetID ? "Triple_E_Thumbnail" : "Triple_E_WebService/AllImageTargets"
      }/${targetImage}.jpg?${new Date()}`
    ).then(res =>
      this.setState({
        src: res
      })
    );
    if (this.props.fixCropSize) {
      const crop = {
        aspect: 1,
        width: 50,
        height: 50,
        x: 10,
        y: 10
      };
      this.setState({ crop: crop });
    }
  }

  render() {
    const self = this;
    const { crop, src, imageSizeError, loading } = self.state;

    return (
      <div className={`crop-image ${classes.CropTargetImage}`}>
        <div />
        {src && !loading ? (
          <ReactCrop
            src={src}
            crop={crop}
            onImageLoaded={self.onImageLoaded}
            onComplete={self.onCropComplete}
            onChange={self.onCropChange}
            imageStyle={{ maxWidth: "100%", maxHeight: "none" }}
          />
        ) : (
          <div
            className="dashboard-loader"
            style={{ position: "relative", top: "95px" }}
          >
            <img src={SPINNER} alt="spinner" style={{ width: "190px" }} />
          </div>
        )}
        <input
          accept="image/*"
          onChange={self.onSelectFile}
          className={classes.CropImageInput}
          id="raised-button-file"
          multiple
          type="file"
        />
        <label htmlFor="raised-button-file" className={classes.uploadButton}>
          <Button component="span" className={classes.button}>
            Upload Image
            <CloudUploadIcon className={classes.rightIcon} />
          </Button>
        </label>
        <div className="red-text center">
          {imageSizeError ? <p>{imageSizeError}</p> : null}
        </div>
      </div>
    );
  }
}

CropTargetImage.propTypes = {
  classes: PropTypes.object
};

export default CropTargetImage;
