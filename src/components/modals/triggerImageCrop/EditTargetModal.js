import React from 'react';
//MUI
import Icon from '@material-ui/core/Icon';
//components
import CropDialog from './CropDialog';
//styles
import classes from './TriggerImageCrop.module.scss';

class EditTargetModal extends React.Component {
  state = {
    open: false
  };

  handleClose = () => {
    this.setState({ open: !this.state.open });
  };
  handleCloseSaveCroppedImage = (croppedImage) => {
    this.setState(
      {
        open: false
      },
      () => this.props.handleTargetImage(croppedImage)
    );
  };

  render() {
    const self = this;
    const { targetImage, targetID, fixCropSize } = self.props;
    const { open } = self.state;
    return (
      <div className={classes.EditTargetModal}>
        <div
          className={` target-edit-button ${classes.targetEditButton}`}
          onClick={self.handleClose}
        >
          <Icon className={` target-edit-icon ${classes.targetEditIcon}`}>
            photo_camera
            </Icon>
          <span className={` target-edit-text ${classes.targetEditText}`}>
            Update {targetID ? 'Trigger' : 'Thumbnail'} Image
          </span>
        </div>
        <CropDialog
          open={open}
          onClose={self.handleClose}
          handleTargetImage={this.handleCloseSaveCroppedImage}
          targetImage={targetImage}
          targetID={targetID}
          fixCropSize={fixCropSize}
        />
      </div>
    );
  }
}

export default EditTargetModal;
