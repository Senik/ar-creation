import React, { useState } from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import Button from "@material-ui/core/Button";
import SimpleCrypto from "simple-crypto-js";
import { getCountryCode } from "../../helpers/getUserLocationIP";
import moment from "moment";
//actions
import {
  sendContactUsEmail,
  makePromocodeFreeSubscription,
  getAllPromocodes
} from "../../redux/user/user.actions";
//helper
import { calcSubscriptionExpDate } from "../payment/Payment";
//selectors
import { selectPromocodes } from "../../redux/user/user.selectors";
//modals
import EnterpriseModal from "../modals/arPost/EnterpriseModal";
import ContactModal from "../modals/contactModal";

//styles
import "./package-card.styles.scss";

const PackageCard = props => {
  const {
    name,
    price,
    pricePeriod,
    vat,
    options,
    makePayment,
    profile,
    getAllPromocodes,
    promocodes,
    makePromocodeFreeSubscription
  } = props;
  const [paymentPeriod, setPeriod] = useState("monthly");
  const [openEnterpriseModal, setEnterpriseModal] = useState(false);
  const [openContactModal, setContactModal] = useState(false);
  const [companyCredentials, setCredentials] = useState({
    companyName: null,
    companyAddress: null,
    companyVat: null
  });
  const [userCredentials, setUserCredentials] = useState({
    userPhoneNumber: null,
    userProjectDescription: null,
    userCompanyName: null,
    userCompanyAdress: null
  });
  const [openCouponForm, setCouponForm] = useState(false);
  const [coupon, setCoupon] = useState("");
  const [couponMessage, setCouponMessage] = useState({
    type: null,
    message: null
  });
  const [discount, setDiscount] = useState(0);

  function handlePeriod() {
    let period = paymentPeriod === "monthly" ? "annually" : "monthly";
    setPeriod(period);
  }
  function handleModalChange(val, name) {
    setCredentials({ ...companyCredentials, [name]: val });
  }
  function handlePremiumModalChange(val, name) {
    setUserCredentials({ ...userCredentials, [name]: val });
  }

  function handlePayment() {
    const { companyName, companyAddress, companyVat } = companyCredentials;
    let price = 1;
    if (profile.Premium_Package_Price && !pricePeriod) {
      price *=
        profile.Premium_Package_Price -
        (profile.Premium_Package_Price * discount) / 100;
      if (paymentPeriod === "annually") {
        price =
          (profile.Premium_Package_Price -
            (profile.Premium_Package_Price *
              (+profile.Premium_Package_Discount + +discount)) /
              100) *
          12;
      }
    } else if (name === "Premium") {
      setContactModal(true);
      return;
    } else {
      price =
        pricePeriod[paymentPeriod] -
        (pricePeriod[paymentPeriod] * discount) / 100;
      if (paymentPeriod === "annually") {
        price *= 12;
      }
    }
    if (price == 0) {
      const activatedAt = moment().format("MM-DD-YYYY");
      const data = {
        Type: name,
        Activated: activatedAt,
        ExpirationDate: calcSubscriptionExpDate(
          paymentPeriod,
          profile,
          name,
          activatedAt,
          promocodes[coupon].PackageDuration
        ),
        coupon,
        Amount: price,
        SubsciptionPeriod: paymentPeriod,
        User_ID: profile.User_ID
      };
      makePromocodeFreeSubscription(data);
      return;
    }
    let urlParams = `subscription_period=${paymentPeriod}&package_name=${name}&user_id=${profile.User_ID}&code=${profile.User_Location}`;
    if (profile.User_Type === "enterprise") {
      urlParams += `&cmpName=${companyName}&cmpAddress=${companyAddress}&cmpVat=${companyVat}`;
    }
    if (discount) {
      urlParams += `&coupon=${coupon}&couponDiscount=${discount}`;
    }
    let criptedUrl = generateURL(urlParams);
    let redirectUrl = `${window.location.origin}${criptedUrl}`;
    makePayment(redirectUrl, profile.User_Location, price);
  }

  function handleSubmitPayment() {
    setEnterpriseModal(false);
    handlePayment();
  }

  function handleClick(e) {
    e.preventDefault();
    if (
      (profile.Subscription &&
        !profile.Subscription.Allow_Extend &&
        !profile.Premium_Package_Price) ||
      (profile.Subscription && profile.Subscription.Type === "Premium")
    ) {
      setContactModal(true);
      return;
    }
    if (
      profile.User_Type === "enterprise" &&
      (profile.Premium_Package_Price || name === "Plus")
    ) {
      setEnterpriseModal(true);
      return;
    }
    handlePayment();
  }

  function handleCancelPayment() {
    setEnterpriseModal(false);
    setCredentials({
      companyName: null,
      companyAddress: null,
      companyVat: null
    });
  }

  function handleSubmitPremium() {
    const formData = new FormData();
    Object.keys(userCredentials).forEach(prop => {
      if (userCredentials[prop]) {
        formData.append(prop, userCredentials[prop]);
      }
    });
    formData.append("userEmail", profile.User_Email);
    formData.append("userNickname", profile.User_NickName);
    formData.append("userFullname", profile.User_FullName);
    formData.append("countryCode", profile.User_Location);
    formData.append("packageName", name);
    if (profile.Subscription) {
      formData.append("currentPackageName", profile.Subscription.Type);
      formData.append("expirationDate", profile.Subscription.ExpirationDate);
      formData.append(
        "currentInvoiceId",
        profile.Subscription.CurrentInvoiceID
      );
      formData.append(
        "invoiceUrl",
        profile.Subscription.Invoices[profile.Subscription.CurrentInvoiceID]
          .InvoiceURL
      );
    }
    props.sendContactUsEmail(
      formData,
      "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/contactForPremiumPrice.php"
    );
    setContactModal(false);
    setUserCredentials({
      userPhoneNumber: null,
      userProjectDescription: null,
      userCompanyName: null,
      userCompanyAdress: null
    });
  }

  function handleCancelPremium() {
    setContactModal(false);
    setUserCredentials({
      userPhoneNumber: null,
      userProjectDescription: null,
      userCompanyName: null,
      userCompanyAdress: null
    });
  }

  const generateURL = urlSearchParams => {
    let _secretKey = "44LFGcXvsE21jlFCaCL060vooDXgIdw8Wl04";
    let simpleCrypto = new SimpleCrypto(_secretKey);
    let chiperURL = simpleCrypto.encrypt(urlSearchParams);
    let url = `/payment?${chiperURL}`;
    return url;
  };

  const handleOpenCouponForm = e => {
    if (!promocodes) {
      getAllPromocodes();
    }
    setCouponForm(true);
  };
  const handleCouponInputChange = e => {
    setCoupon(e.target.value);
  };
  const handleCouponFormSubmit = e => {
    e.preventDefault();
    if (
      !promocodes.hasOwnProperty(coupon) ||
      (promocodes[coupon].ActivationLimit &&
        promocodes[coupon].Users &&
        promocodes[coupon].ActivationLimit ==
          Object.values(promocodes[coupon].Users).length) ||
      (promocodes[coupon].ExpirationDate &&
        new Date() > new Date(promocodes[coupon].ExpirationDate)) ||
      (name === "Plus"
        ? coupon[0] !== name[1]
        : coupon[0] !== name[0].toLowerCase())
    ) {
      setCouponMessage({ type: "error", message: "invalid promocode" });
      setCoupon("");
      return;
    }
    if (
      promocodes[coupon].AttachedUserEmail === profile.User_Email &&
      promocodes[coupon].ActivationLimit != 1
    ) {
      setCouponMessage({
        type: "error",
        message: "you can not activate this promocode"
      });
      setCoupon("");
      return;
    }
    setDiscount(promocodes[coupon].Discount);
    setCouponMessage({
      type: "success",
      message: "promocode accepted"
    });
  };

  const contactUsCredentials =
    profile.User_Type === "individual"
      ? {
          userPhoneNumber: userCredentials.userPhoneNumber,
          userProjectDescription: userCredentials.userProjectDescription
        }
      : {
          userPhoneNumber: userCredentials.userPhoneNumber,
          userProjectDescription: userCredentials.userProjectDescription,
          userCompanyName: userCredentials.userCompanyName,
          userCompanyAdress: userCredentials.userCompanyAdress
        };
  let contactModalTitle = "";
  if (profile.Subscription) {
    contactModalTitle =
      "You are about to change the purchased plan. Please contact us to proceed your request.";
  } else if (name === "Premium") {
    contactModalTitle = "Please contact us to get a personalized quote.";
  }
  const disableButton =
    profile.Subscription &&
    profile.Subscription.Type === name &&
    !profile.Subscription.Allow_Extend;
  const showPromo = () => {
    if (
      (name === "Premium" && !profile.Premium_Package_Price) ||
      disableButton
    ) {
      return false;
    }
    return true;
  };
  return (
    <React.Fragment>
      <div className="package-cart">
        <div>
          <h2 className="package-name">{name}</h2>
        </div>
        <div className="package-title">
          <h4 className="package-price">
            {pricePeriod
              ? `\u20AC ${pricePeriod[paymentPeriod] -
                  (pricePeriod[paymentPeriod] * discount) / 100} ${price}`
              : profile.Premium_Package_Price -
                (profile.Premium_Package_Price * discount) / 100
              ? `\u20AC ${
                  paymentPeriod === "monthly"
                    ? profile.Premium_Package_Price -
                      (profile.Premium_Package_Price * discount) / 100
                    : profile.Premium_Package_Price -
                      (profile.Premium_Package_Price *
                        (+profile.Premium_Package_Discount + +discount)) /
                        100
                } per month billed`
              : price}
          </h4>
          {(vat || profile.Premium_Package_Price) && discount != 100 && (
            <div className="button-group">
              <Button
                className={paymentPeriod === "monthly" ? "active" : ""}
                onClick={handlePeriod}
              >
                monthly
              </Button>
              <Button
                className={paymentPeriod === "annually" ? "active" : ""}
                onClick={handlePeriod}
              >
                annually
              </Button>
            </div>
          )}

          <span className="vat">
            {vat || profile.Premium_Package_Price ? "VAT not included" : ""}
          </span>
        </div>
        <div>
          <ul className="options-list">
            {options.map(option => {
              return (
                <li key={option} className="options-item">
                  <span>{option}</span>

                  <span>
                    <i className="fa fa-check"></i>
                  </span>
                </li>
              );
            })}
          </ul>
        </div>

        <div className="center-align coupon-container">
          {showPromo() && (
            <div>
              {openCouponForm ? (
                <form onSubmit={handleCouponFormSubmit}>
                  <input
                    className="coupon-input"
                    type="text"
                    onChange={handleCouponInputChange}
                    value={coupon}
                    required={true}
                  />
                  <input
                    type="submit"
                    value="Apply"
                    className="btn-small red"
                  />
                </form>
              ) : (
                <button className="btn-flat" onClick={handleOpenCouponForm}>
                  Apply Promocode
                </button>
              )}
            </div>
          )}

          {couponMessage.message && (
            <div
              className={
                couponMessage.type === "success" ? "green-text" : "red-text"
              }
            >
              <p>{couponMessage.message}</p>
            </div>
          )}
        </div>

        <Button
          className={`package-button ${disableButton ? "button-disabled" : ""}`}
          onClick={handleClick}
          disabled={disableButton}
        >
          Let's try
        </Button>
      </div>
      <EnterpriseModal
        isOpen={openEnterpriseModal}
        handleModalChange={handleModalChange}
        credentials={companyCredentials}
        handleSubmitPayment={handleSubmitPayment}
        handleCancelPayment={handleCancelPayment}
      />

      <ContactModal
        isOpen={openContactModal}
        modalTitle={contactModalTitle}
        handleModalChange={handlePremiumModalChange}
        credentials={contactUsCredentials}
        cancelContact={handleCancelPremium}
        submitContact={handleSubmitPremium}
      />
    </React.Fragment>
  );
};

const mapStateToProps = createStructuredSelector({
  promocodes: selectPromocodes
});

export default connect(mapStateToProps, {
  sendContactUsEmail,
  getAllPromocodes,
  makePromocodeFreeSubscription
})(React.memo(PackageCard));
