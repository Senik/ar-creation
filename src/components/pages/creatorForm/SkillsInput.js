import React, { Component } from 'react';
import { WithContext as ReactTags } from 'react-tag-input';
import { SKILLS } from './SKILLS';
//styles
import './Skills.css';

const suggestions = SKILLS.map(skil => {
  return {
    id: skil,
    text: skil
  };
});

class SkillsInput extends Component {
  state = {
    tags: [{ id: 'AR', text: 'AR' }],
    suggestions
  };

  handleDelete = i => {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i)
    });
  };

  handleAddition = tag => {
    this.setState({
      tags: [...this.state.tags, tag]
    });
  };

  handleDrag(tag, currPos, newPos) {
    const tags = [...this.state.tags];
    const newTags = tags.slice();

    newTags.splice(currPos, 1);
    newTags.splice(newPos, 0, tag);

    // re-render
    this.setState({ tags: newTags });
  }
  handleTagClick(index) {
    console.log('The tag at index ' + index + ' was clicked');
  }
  handleBlur = tag => {
    if (tag.length <= 0) {
      return;
    }
    const newTag = { id: tag, text: tag };
    this.setState(
      {
        tags: [...this.state.tags, newTag]
      },
      () => {
        this.props.addSkills(newTag);
      }
    );
  };
  render() {
    const self = this;
    const { suggestions } = self.state;
    const { tags, deleteSkills, addSkills } = self.props;
    return (
      <div className="skills">
        <ReactTags
          inputFieldPosition="top"
          tags={tags}
          placeholder="Add new skill *"
          suggestions={suggestions}
          handleDelete={deleteSkills}
          handleAddition={addSkills}
          handleDrag={self.handleDrag}
          handleTagClick={self.handleTagClick}
          handleInputBlur={self.handleBlur}
          allowDeleteFromEmptyInput={false}
          autofocus={false}
          allowDragDrop={false}
        />
      </div>
    );
  }
}

export default SkillsInput;
