import React, { useState } from "react";
import CheckElement from "../../../components/formElements/Checkbox";
//styles
import classes from "./UserInfoForm.module.scss";

function RadioButtonsGroup(props) {
  const [gender, setGender] = useState("");
  const handleCheck = value => {
    setGender(value);
    props.handleChange(value);
  };
  return (
    <div className={classes.Gender}>
      <CheckElement
        title="Gender"
        name="gender2"
        defaultValue={gender || (props.gender && props.gender.toLowerCase())}
        handleChange={handleCheck}
        options={[
          {
            label: "Female",
            value: "female"
          },
          {
            label: "Male",
            value: "male"
          },
          {
            label: "Other",
            value: "other"
          }
        ]}
      />
    </div>
  );
}

export default RadioButtonsGroup;
