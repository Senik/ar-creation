import React, { useState, memo } from "react";
import { connect } from "react-redux";
//actions
import { sendContactUsEmail } from "../../../redux/user/user.actions";
//MUI
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FormControl from "@material-ui/core/FormControl";
//components
import ContactModal from "../../modals/contactModal";
//helpers
import { getCountryCode } from "../../../helpers/getUserLocationIP";
//styles
import classes from "./UserInfoForm.module.scss";

function UserTypeRadiobuttons(props) {
  const [userType, setUserType] = useState("");
  const [openContactModal, setModal] = useState(false);
  const [userTypeChangeCredentials, setCredentials] = useState({
    userPhoneNumber: null,
    userCompanyName: null,
    userCompanyAdress: null,
    userProjectDescription: null
  });

  const handleChange = event => {
    const { user } = props;
    if (user.Subscription) {
      setModal(true);
      return;
    }
    setUserType(event.target.value);
    props.handleUserTypeChange(event.target.value);
  };

  const handleModalChange = (value, name) => {
    setCredentials({ ...userTypeChangeCredentials, [name]: value });
  };
  const handleContactCancel = () => {
    setModal(false);
  };
  const handleContactSubmit = async e => {
    e.preventDefault();
    let code = await getCountryCode();

    const formData = new FormData();
    Object.keys(userTypeChangeCredentials).forEach(prop => {
      if (userTypeChangeCredentials[prop]) {
        formData.append(prop, userTypeChangeCredentials[prop]);
      }
    });
    formData.append("userEmail", user.User_Email);
    formData.append("userNickname", user.User_NickName);
    formData.append("userFullname", user.User_FullName);
    formData.append("countryCode", code);
    if (user.Subscription) {
      formData.append("currentPackageName", user.Subscription.Type);
      formData.append("expirationDate", user.Subscription.ExpirationDate);
      formData.append("currentInvoiceId", user.Subscription.CurrentInvoiceID);
      formData.append(
        "invoiceUrl",
        user.Subscription.Invoices[user.Subscription.CurrentInvoiceID]
          .InvoiceURL
      );
    }

    props.sendContactUsEmail(
      formData,
      "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/contactUserTypeChange.php"
    );

    setCredentials({
      userPhoneNumber: null,
      userCompanyName: null,
      userCompanyAdress: null,
      userProjectDescription: null
    });
    setModal(false);
  };
  const { user } = props;
  const contactUsCredentials =
    user.User_Type === "enterprise"
      ? {
          userPhoneNumber: userTypeChangeCredentials.userPhoneNumber,
          userCompanyName: userTypeChangeCredentials.userCompanyName,
          userCompanyAdress: userTypeChangeCredentials.userCompanyAdress,
          userProjectDescription:
            userTypeChangeCredentials.userProjectDescription
        }
      : {
          userPhoneNumber: userTypeChangeCredentials.userPhoneNumber,
          userProjectDescription:
            userTypeChangeCredentials.userProjectDescription
        };
  return (
    <div className={classes.UserType}>
      <FormControl component="fieldset" className={classes.FormControl}>
        <FormLabel component="legend">
          User Type
          {user.User_Type === "Prime" ? (
            <span className={classes.userTypeText}>Prime</span>
          ) : null}
        </FormLabel>
        <RadioGroup
          aria-label="Is content paid"
          name={"gender2"}
          id={"gender2"}
          disabled={user.User_Type === "Prime" ? true : false}
          className={classes.Group}
          value={userType || (user.User_Type && user.User_Type.toLowerCase())}
          onChange={handleChange}
        >
          <FormControlLabel
            value={"individual"}
            control={<Radio color="primary" />}
            label={"Individual"}
            labelPlacement="start"
            disabled={user.User_Type === "Prime" || false}
          />
          <FormControlLabel
            value={"enterprise"}
            control={<Radio color="primary" />}
            label={"Enterprise"}
            labelPlacement="start"
            disabled={user.User_Type === "Prime" || false}
          />
        </RadioGroup>
      </FormControl>
      <ContactModal
        isOpen={openContactModal}
        modalTitle="You are about to change the user type.
        Please contact us to proceed your request."
        handleModalChange={handleModalChange}
        credentials={contactUsCredentials}
        cancelContact={handleContactCancel}
        submitContact={handleContactSubmit}
      />
    </div>
  );
}

export default connect(null, { sendContactUsEmail })(
  memo(UserTypeRadiobuttons)
);
