import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { connect } from "react-redux";
import SimpleCrypto from "simple-crypto-js";
import moment from "moment";

//actions
import {
  getPaymentInfos,
  getSubscriptionInfo,
  addPostPayment,
  addSubscriptionPayment,
  setPaymentStatus
} from "../../redux/post/post.actions";
//components
import FullPageLoader from "../../components/shared/FullPageLoader";
import PaymentModal from "../modals/payment/PaymentModal";
//data
import { EU_Countries } from "../../helpers/eu_countries";
//styles
import classes from "./Payment.module.scss";

class Payment extends Component {
  state = {
    loading: true,
    postBoughtNotification: false,
    countryCode: null,
    isMounted: false,
    urlError: false
  };
  componentDidMount() {
    try {
      let _secretKey = "44LFGcXvsE21jlFCaCL060vooDXgIdw8Wl04";
      let simpleCrypto = new SimpleCrypto(_secretKey);
      let urlParams = window.location.search.substr(1);
      let dechiperParams = simpleCrypto.decrypt(urlParams);
      let dechiperURL = `${window.location.origin}${window.location.pathname}?${dechiperParams}`;
      let url = new URL(dechiperURL);
      let userID = url.searchParams.get("user_id");
      let postID = url.searchParams.get("post_id");
      let subscriptionPeriod = url.searchParams.get("subscription_period");
      let coupon = url.searchParams.get("coupon");
      let couponDiscount = url.searchParams.get("couponDiscount");
      let packageName = url.searchParams.get("package_name");
      let countryCode = url.searchParams.get("code");
      let companyName = url.searchParams.get("cmpName");
      let companyAddress = url.searchParams.get("cmpAddress");
      let companyVat = url.searchParams.get("cmpVat");
      this.setState({
        isMounted: true,
        countryCode,
        companyName,
        companyAddress,
        companyVat,
        subscriptionPeriod,
        packageName,
        coupon,
        couponDiscount
      });
      if (subscriptionPeriod && localStorage.getItem("paymentData")) {
        this.props.getSubscriptionInfo(userID);
      } else {
        this.props.getPaymentInfos(postID, userID);
      }
    } catch (e) {
      this.setState({
        urlError: true
      });
    }
  }
  componentDidUpdate(prevProps) {
    const { post, user, subscriber } = this.props;
    if (prevProps.post !== post && this.state.isMounted) {
      let isPostBought = this.checkBuyers(post.Buyers);
      if (isPostBought) {
        this.setState({
          postBoughtNotification: true,
          loading: false
        });
        return;
      }

      const paymentData = JSON.parse(localStorage.getItem("paymentData"));
      const boughtPostId = localStorage.getItem("PostID");
      localStorage.clear();
      let vat = 0;
      if (
        post.Price &&
        user &&
        ((EU_Countries.includes(this.state.countryCode) &&
          user.User_Type === "individual") ||
          (this.state.countryCode === "Netherlands" &&
            user.User_Type === "enterprise"))
      ) {
        let decimalVAT = (((+post.Price * 10) / 100) * 21) / 100;
        vat = Math.round(decimalVAT * 100) / 100;
      }
      if (
        paymentData &&
        paymentData.status === "open" &&
        boughtPostId &&
        boughtPostId === post.PostID &&
        user
      ) {
        post && this.checkPaymentTest(paymentData.id, vat);
      } else {
        this.setState({
          urlError: true
        });
      }
      this.setState({
        isMounted: false
      });
    }
    if (
      prevProps.subscriber !== subscriber &&
      this.state.isMounted &&
      localStorage.getItem("paymentData")
    ) {
      const { countryCode } = this.state;
      const paymentData = JSON.parse(localStorage.getItem("paymentData"));
      const price = JSON.parse(localStorage.getItem("package_price"));
      localStorage.clear();
      let vat = 0;
      if (
        (EU_Countries.includes(countryCode) &&
          subscriber.User_Type === "individual") ||
        (subscriber.User_Type === "enterprise" && countryCode === "Netherlands")
      ) {
        let decimalVAT = (+price * 21) / 100;
        vat = Math.round(decimalVAT * 100) / 100;
      }
      if (paymentData && paymentData.status === "open") {
        this.checkSubscription(paymentData, vat);
      } else {
        this.setState({
          urlError: true
        });
      }
      this.setState({
        isMounted: false
      });
    }
  }

  checkBuyers = buyers => {
    if (!buyers) return false;
    const buyersKeys = Object.keys(buyers);
    let result = false;
    buyersKeys.map(key => {
      if (buyers[key].UserID === this.props.user.User_ID) {
        result = true;
      }
      return false;
    });
    return result;
  };

  checkSubscription = (paymentData, vat) => {
    localStorage.clear();

    const { subscriber, setPaymentStatus, addSubscriptionPayment } = this.props;
    const {
      countryCode,
      companyName,
      companyAddress,
      companyVat,
      packageName,
      subscriptionPeriod,
      coupon,
      couponDiscount
    } = this.state;

    const url =
      "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/checkSubscriptionPayment.php";
    const data = new FormData();
    data.append("paymentID", paymentData.id);
    data.append("buyerEmail", subscriber.User_Email);
    data.append("buyerName", subscriber.User_FullName);
    data.append("buyerType", subscriber.User_Type);
    data.append("package_name", packageName);
    data.append("subscription_period", subscriptionPeriod);
    data.append("VAT", vat);
    data.append("countryCode", countryCode);
    if (companyName && companyAddress && companyVat) {
      data.append("buyerCompanyName", companyName);
      data.append("buyerCompanyAddress", companyAddress);
      data.append("buyerCompanyVat", companyVat);
    }
    if (couponDiscount) {
      data.append("coupon", coupon);
      data.append("couponDiscount", couponDiscount);
    }

    axios({
      method: "POST",
      url,
      data
    })
      .then(res => {
        if (res.data.status === "paid") {
          const newSubscription = {
            Type: packageName,
            Activated: moment(res.data.paidAt).format("MM-DD-YYYY"),
            Amount: paymentData.amount.value,
            Invoice: res.data.invoice_url,
            InvoiceID: res.data.invoice_id,
            SubsciptionPeriod: subscriptionPeriod
          };

          newSubscription.ExpirationDate = calcSubscriptionExpDate(
            subscriptionPeriod,
            subscriber
          );
          if (coupon) {
            newSubscription.coupon = coupon;
          }
          addSubscriptionPayment(newSubscription, subscriber);

          setPaymentStatus("bought");
        } else {
          setPaymentStatus("cancelled");
        }
        this.setState({
          loading: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  checkPaymentTest = (paymentDataID, vat) => {
    localStorage.clear();
    const { user, post, author, setPaymentStatus, addPostPayment } = this.props;
    const { countryCode, companyName, companyAddress, companyVat } = this.state;
    this.setState({
      loading: true
    });
    const url =
      "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/checkPaymentTest.php";
    const data = new FormData();
    data.append("paymentID", paymentDataID);
    data.append("buyerEmail", user.User_Email);
    data.append("buyerName", user.User_FullName);
    data.append("buyerType", user.User_Type);
    data.append("postName", post.PostName);
    data.append("postId", post.PostID);
    data.append("post_owner_email", author.User_Email);
    data.append("post_owner_name", author.User_FullName);
    data.append("VAT", vat);
    data.append("countryCode", countryCode);
    if (companyName && companyAddress && companyVat) {
      data.append("buyerCompanyName", companyName);
      data.append("buyerCompanyAddress", companyAddress);
      data.append("buyerCompanyVat", companyVat);
    }
    axios({
      method: "POST",
      url,
      data
    })
      .then(res => {
        if (res.data.status === "paid") {
          const newBuyer = {
            UserID: user.User_ID,
            DateTime: res.data.paidAt,
            PaymentID: paymentDataID,
            Amount: post.Price,
            InvoiceURL: res.data.invoice_url,
            PostName: post.PostName
          };
          addPostPayment(post, newBuyer, user);
          setPaymentStatus("bought");
        } else {
          setPaymentStatus("cancelled");
        }
        this.setState({
          loading: false
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    const { loading, postBoughtNotification, urlError } = this.state;
    const { paymentStatus, status, post } = this.props;
    if (status === "wrong" || urlError) {
      return (
        <PaymentModal>
          <p>Oops! Something went wrong</p>
          <button
            className={`btn #e53935 red darken-1 lighten-1 z-depth-3 ${classes.PaymentResultButton}`}
          >
            <Link to="argallery" style={{ color: "white" }}>
              Go back
            </Link>
          </button>
        </PaymentModal>
      );
    }
    if (loading || paymentStatus === "pending") {
      return <FullPageLoader />;
    } else if (postBoughtNotification) {
      return (
        <PaymentModal>
          <p>Your have already bought the post!</p>
          <button
            className={`btn #188e00 green darken-1 lighten-1 z-depth-3 ${classes.PaymentResultButton}`}
          >
            <Link to={`/arpost/${post.PostID}`}>Take me to AR Post</Link>
          </button>
        </PaymentModal>
      );
    } else {
      if (paymentStatus === "cancelled") {
        return (
          <PaymentModal>
            <p>Your payment has failed!</p>
            <button
              className={`btn #e53935 red darken-1 lighten-1 z-depth-3 ${classes.PaymentResultButton}`}
            >
              <Link to="argallery">Go back</Link>
            </button>
          </PaymentModal>
        );
      } else if (paymentStatus === "bought") {
        return (
          <PaymentModal>
            <p>Your payment has been done successfully!</p>

            <button
              className={`btn #188e00 green darken-1 lighten-1 z-depth-3 ${classes.PaymentResultButton}`}
            >
              {post ? (
                <Link to={`/arpost/${post.PostID}`}>Take me to AR Post</Link>
              ) : (
                <Link to={"/profile"}>Go Back</Link>
              )}
            </button>
          </PaymentModal>
        );
      }
    }
  }
}

const mapStateToProps = state => {
  return {
    user: state.post.buyerInfo,
    post: state.post.postInfo,
    author: state.post.authorInfo,
    paymentStatus: state.post.paymentStatus,
    status: state.post.status,
    subscriber: state.post.subscriberInfo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPaymentInfos: (postID, userID) =>
      dispatch(getPaymentInfos(postID, userID)),
    addPostPayment: (id, buyer, author) =>
      dispatch(addPostPayment(id, buyer, author, true)),
    setPaymentStatus: status => dispatch(setPaymentStatus(status)),
    getSubscriptionInfo: userID => dispatch(getSubscriptionInfo(userID)),
    addSubscriptionPayment: (subscritionData, subscriber) =>
      dispatch(addSubscriptionPayment(subscritionData, subscriber))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Payment);

export function calcSubscriptionExpDate(
  subscriptionPeriod,
  subscriber,
  packageName,
  paidAt,
  duration = null
) {
  let expirationDate;
  if (duration) {
    expirationDate = moment(paidAt)
      .add(+duration, "M")
      .format("MM-DD-YYYY");
  } else if (subscriptionPeriod === "monthly") {
    if (
      subscriber.Subscription &&
      subscriber.Subscription.ExpirationDate >
        moment(paidAt).format("MM-DD-YYYY") &&
      subscriber.Subscription.Type === packageName
    ) {
      expirationDate = moment(subscriber.Subscription.ExpirationDate)
        .add(1, "M")
        .format("MM-DD-YYYY");
    } else {
      expirationDate = moment(paidAt)
        .add(1, "M")
        .format("MM-DD-YYYY");
    }
  } else if (subscriptionPeriod === "annually") {
    if (
      subscriber.Subscription &&
      subscriber.Subscription.ExpirationDate >
        moment(paidAt).format("MM-DD-YYYY") &&
      subscriber.Subscription.Type === packageName
    ) {
      expirationDate = moment(subscriber.Subscription.ExpirationDate)
        .add(365, "d")
        .format("MM-DD-YYYY");
    } else {
      expirationDate = moment(paidAt)
        .add(365, "d")
        .format("MM-DD-YYYY");
    }
  }

  return expirationDate;
}
