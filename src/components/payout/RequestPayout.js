import React, { useState } from "react";
import { connect } from "react-redux";
import axios from "axios";
import { Redirect } from "react-router-dom";
//helper
import { addCredentials } from "../../helpers";
//action
import { setPayoutData } from "../../redux/user/user.actions";
//MUI
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
//components
import { CreatorCredentials } from "../../pages/profile/ProfileModals";
//styles
import classes from "./RequesPayout.module.scss";

const RequestPayout = props => {
  const [bankInfo, setBankInfo] = useState({
    bankName: null,
    bankAccountHolderName: null,
    bankAccountNumber: null,
    bankSwift: null,
    billingAddress: null
  });
  const [isCredentialsModalOpen, setCredentialsModalOpen] = useState(false);
  const [redirectTo, setRedirectTo] = useState(false);
  const { Creator_Credentials } = props.profile;
  const {
    bankName,
    bankAccountHolderName,
    bankAccountNumber,
    bankSwift,
    billingAddress
  } = bankInfo;
  const handlePayoutRequest = () => {
    if (
      !Creator_Credentials ||
      !Creator_Credentials.Bank_Account_Holder_Name ||
      !Creator_Credentials.Bank_Account_Number ||
      !Creator_Credentials.Bank_Name ||
      !Creator_Credentials.Bank_Swift ||
      !Creator_Credentials.Billing_Address
    ) {
      setCredentialsModalOpen(true);
      return;
    }
    makePayoutRequest();
  };
  const handleBankInfoChange = (value, name) => {
    setBankInfo({
      ...bankInfo,
      [name]: value
    });
  };
  const makePayoutRequest = () => {
    const { User_ID } = props.profile;
    const data = new FormData();
    data.append("creator_id", User_ID);
    axios({
      method: "post",
      url:
        "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/create_payout_invoice.php",
      data
    }).then(res => {
      const { created_at, invoice_url, payout, pyout_status } = res.data;
      const payoutData = {
        DateTime: created_at,
        Invoice_URL: invoice_url,
        Amount: payout,
        Payout_Status: pyout_status
      };
      setRedirectTo(true);
      props.setPayoutData(payoutData, User_ID);
    });
  };
  const handleCredentials = async () => {
    const { User_ID } = props.profile;
    const {
      bankName,
      bankAccountHolderName,
      bankAccountNumber,
      bankSwift,
      billingAddress
    } = bankInfo;
    if (bankInfo && bankInfo.bankName) {
      const credentialsData = {
        Bank_Name: bankName,
        Bank_Account_Holder_Name: bankAccountHolderName,
        Bank_Account_Number: bankAccountNumber,
        Bank_Swift: bankSwift,
        Billing_Address: billingAddress
      };
      addCredentials(credentialsData, User_ID).then(() => {
        handlePayoutModalClose();
        makePayoutRequest();
      });
    }
  };
  const handlePayoutModalClose = () => {
    setCredentialsModalOpen(false);
  };
  if (redirectTo) {
    return <Redirect to="/" />;
  } else {
    return (
      <>
        <div className={classes.PayoutBtn}>
          <button
            className="btn blue lighten-1 z-depth-0"
            onClick={handlePayoutRequest}
          >
            request payout
          </button>
        </div>
        <Dialog
          onClose={handlePayoutModalClose}
          aria-labelledby="customized-dialog-title"
          open={isCredentialsModalOpen}
        >
          <DialogContent
            className={`${classes.dialogContent} ${classes.creatorCredentials}`}
          >
            <Typography gutterBottom className={classes.credentialsText}>
              In order to transfer you the money earned from the AR creations
              you've already sold, we need the following information.
            </Typography>
            <CreatorCredentials
              bankInfo={bankInfo}
              handleBankInfoChange={handleBankInfoChange}
            />
          </DialogContent>
          <DialogActions className={classes.dialogActions}>
            <Button
              onClick={handleCredentials}
              color="primary"
              className={`red white-text ${classes.sendRequestBtn}`}
              disabled={
                bankInfo &&
                (!bankName ||
                  !bankAccountHolderName ||
                  !bankAccountNumber ||
                  !bankSwift ||
                  !billingAddress)
              }
            >
              Submit
            </Button>
          </DialogActions>
        </Dialog>
      </>
    );
  }
};

const mapDispatchToProps = dispatch => {
  return {
    setPayoutData: (data, id) => dispatch(setPayoutData(data, id))
  };
};

export default connect(null, mapDispatchToProps)(RequestPayout);
