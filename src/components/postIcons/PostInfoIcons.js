import React from 'react';
//MUI
import ToolTip from '@material-ui/core/Tooltip';
import Icon from '@material-ui/core/Icon';
import IconButton from '@material-ui/core/IconButton';
import { Visibility } from '@material-ui/icons';
//style
import classes from './PostInfoIcons.module.scss';

const PostInfoIcons = props => {
  const {
    post,
    dynamicLinkCreation,
    profile,
    showLikes,
    handleLikePost
  } = props;
  return (
    <div className={classes.PostInfoIcons}>
      <ToolTip title="Number of views" placement="top-start">
        <IconButton
          className={classes.postInfoIcon}
          aria-label="Number of views"
        >
          <Visibility />
          <span className={classes.numberViews}>{post.NumberOfViews}</span>
        </IconButton>
      </ToolTip>
      {post.ARTargetStatus !== 'Processing' ?
        <ToolTip title="Click to copy post link" placement="top-start">
          <IconButton
            className={classes.postInfoIcon}
            aria-label="Post link"
            onClick={() => dynamicLinkCreation(post)}
          >
            <Icon>share</Icon>
            <span className={classes.numberViews}>
              {post.PostShareCount || 0}
            </span>
          </IconButton>
        </ToolTip>
        : null}
      {showLikes && (
        <ToolTip
          title={
            profile.isEmpty
              ? 'Please sign in to like the post'
              : 'Click to like the post'
          }
          placement="top-start"
        >
          <IconButton
            className={classes.postInfoIcon}
            aria-label="Post likes"
            onClick={() => handleLikePost(post)}
          >
            <Icon
              style={
                post.Likes && profile.User_ID in post.Likes
                  ? { color: 'red' }
                  : null
              }
            >
              favorite
            </Icon>
            <span className={classes.numberViews}>
              {(post.Likes && Object.keys(post.Likes).length) || 0}
            </span>
          </IconButton>
        </ToolTip>
      )}
    </div>
  );
};

export default PostInfoIcons;
