import React from 'react';
import { WithContext as ReactTags } from 'react-tag-input';
import { TAGS } from './TAGS';
//components
//styles
import './Tags.css';

const suggestions = TAGS.map(skill => {
  return {
    id: skill,
    text: skill
  };
});

const PostTags = ({ tags, tagsError, deleteTags, addTags, handleBlur }) => {
  return (
    <>
      <ReactTags
        inputFieldPosition="top"
        placeholder="Please press enter to add a tag (tag 1, tag 2, tag 3) *"
        tags={tags}
        suggestions={suggestions}
        handleDelete={deleteTags}
        handleAddition={addTags}
        handleInputBlur={handleBlur}
        allowDeleteFromEmptyInput={false}
        autofocus={false}
        allowDragDrop={false}
      />
      <button
        className="btn blue btn white-text"
        type="button"
      >
        Add Tag
      </button>
      {tagsError ?
        <div className="red-text center">
          <p>Sorry you can't select more than 3 tags!</p>
        </div>
        : null
      }
    </>
  );
}

export default PostTags;