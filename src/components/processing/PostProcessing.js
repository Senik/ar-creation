import React from "react";
//icons
import SPINNER from "../../assets/img/spinners/index.gooey-ring-spinner.svg";

const PostProcessing = props => {
  const { from } = props;
  let processClassName;
  if (from === "singlePage") {
    processClassName = "dashboard-loader";
  }
  return (
    <div className={processClassName}>
      <h5>
        This Trigger image is already in use, please retry after 5 minutes
      </h5>
      <img src={SPINNER} alt="spinner" style={{ width: "90px" }} />
    </div>
  );
};

export default PostProcessing;
