import React from 'react';
//MUI
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
//style
import classes from './SelectARType.module.scss';

const SelectARType = props => {
  const { showPostsType, selectChange, postTypes, label } = props;
  return (
    <FormControl className={classes.formControl}>
      <InputLabel shrink htmlFor="select-multiple-checkbox">{label}</InputLabel>
      <Select
        multiple
        value={showPostsType}
        onChange={selectChange}
        className={classes.selectEmpty}
        input={<Input id="select-multiple-checkbox" />}
        renderValue={selected => selected.join(', ')}
      >
        {postTypes.map(type => (
          <MenuItem key={type} value={type}>
            <Checkbox checked={showPostsType.indexOf(type) > -1} />
            <ListItemText primary={type} />
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default SelectARType;