import React, { memo } from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import AppBar from '@material-ui/core/AppBar';
//styles
import classes from './SharedComponents.module.scss';

const AppBarElement = props => {
  const { value, tabChange, tabs, fromXRGallery } = props;
  const a11yProps = index => {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`
    };
  };
  return (
    <AppBar
      position="static"
      color="default"
      className={classes.AppBarElement}
      style={fromXRGallery ? { maxWidth: '920px' } : { maxWidth: '480px' }}
    >
      {fromXRGallery ? (
        <Tabs
          value={value}
          onChange={tabChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
          aria-label="scrollable auto tabs example"
        >
          {tabs.map((tab, ind) => {
            return (
              <Tab
                label={tab}
                key={ind}
                className={classes.transparentFocus}
                {...a11yProps(ind)}
              />
            );
          })}
        </Tabs>
      ) : (
          <Tabs
            value={value}
            onChange={tabChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
          >
            {tabs.map((tab, ind) => {
              return (
                <Tab
                  label={tab}
                  key={ind}
                  className={classes.transparentFocus}
                  {...a11yProps(ind)}
                />
              );
            })}
          </Tabs>
        )}
    </AppBar>
  );
};

function arePropsEqual(prevProps, nextProps) {
  return prevProps.value === nextProps.value &&
    prevProps.tabs === nextProps.tabs;
}

export default memo(AppBarElement, arePropsEqual);
