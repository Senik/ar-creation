import React, { Fragment } from "react";
import { connect } from "react-redux";
import { Line } from "rc-progress";
import { createStructuredSelector } from "reselect";

//selectors
import {
  selectProgress,
  selectFetchStatus
} from "../../redux/post/post.selectors";
//icons
import SPINNER from "../../assets/img/spinners/index.gooey-ring-spinner.svg";
//styles
import classes from "./SharedComponents.module.scss";

const FullPageLoader = ({
  children,
  progress,
  content,
  status,
  showChildren
}) => {
  return (
    <div className={`${classes.loader} ${classes.FullPageLoader}`}>
      <div className={classes.loderContent}>
        {!showChildren ? null : (
          <Fragment>
            <h3 className="white-text">{children}</h3>
            <p className="white-text">{status}</p>
          </Fragment>
        )}

        <img src={SPINNER} alt="Loader" />
        {progress ? (
          <Fragment>
            <p className={classes.progressText}>
              Uploading {content} file: {progress}%
            </p>
            <Line percent={progress} strokeWidth="4" strokeColor="#0c8611" />
          </Fragment>
        ) : null}
      </div>
    </div>
  );
};

const mapStateToProps = createStructuredSelector({
  progress: selectProgress,
  status: selectFetchStatus
});

export default connect(mapStateToProps)(FullPageLoader);
