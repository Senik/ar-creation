import React from 'react';

const GooglePolyIframe = ({ link, linkCheck }) => {
  let id;
  if (link) {
    const index = link.lastIndexOf('/');
    id = link.slice(index + 1);
    if (linkCheck) {
      if (!link.includes('poly.google.com/view/') || id.length < 1) {
        id += 'falsyURL';
      }
    }
  }
  return (
    <div>
      <iframe
        title={link}
        width="100%"
        height="350px"
        scrolling="no"
        src={`https://poly.google.com/view/${id}/embed`}
        frameBorder="0"
        style={{ border: 'none' }}
        allowvr="yes"
        allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;"
        allowFullScreen={true}
        webkitallowfullscreen="true"
      />
    </div>
  );
};

export default GooglePolyIframe;
