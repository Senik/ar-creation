import React from 'react';
//icons
import SPINNER from '../../assets/img/spinners/index.gooey-ring-spinner.svg';

const PostLoader = ({ loaderWidth }) => (
  <div className="dashboard-loader">
    <img src={SPINNER} alt="spinner" style={{ width: loaderWidth }} />
  </div>
);

PostLoader.defaultProps = {
  loaderWidth: '190px'
};

export default PostLoader;