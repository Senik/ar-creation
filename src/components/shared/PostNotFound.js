import React from 'react';

const PostNotFound = ({ message }) => (
  <div className="col s12 m8 offset-m2 l7 offset-l3 wasNotFound">
    <p>{message}</p>
    <i className="fas fa-times fa-2x" />
  </div>
);

export default PostNotFound;