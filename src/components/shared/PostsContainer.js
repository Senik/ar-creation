import React, { memo } from 'react';
//styles
import classes from './SharedComponents.module.scss';

const PostsContainer = props => {
  const [tabPart, ...rest] = props.children;
  return (
    <div className={`${props.cmpClassName} ${classes.PostsContainer}`}>
      {window.scrollY > 400 ? (
        <button
          onClick={props.scrollToTop}
          className={`btn-floating z-depth-3 btn-large waves-effect waves-light deep-purple darken-3 ${classes.buttonUp}`}
        >
          <i className="fas fa-chevron-up" />
        </button>
      ) : null}
      <div className="row">
        <div className="col s12 m8 offset-m4 l10 offset-l3 xl10 offset-xl2">
          <div className={classes.tabBox}>{tabPart}</div>
          {rest}
        </div>
      </div>
    </div>
  );
};

export default memo(PostsContainer);
