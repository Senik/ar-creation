import React from 'react';
import Masonry from 'react-masonry-component';

const masonryOptions = {
    transitionDuration: 0,
    fitWidth: true
};

const PostsMasonry = props => {
    return (
        <div className="section">
            <Masonry style={{ margin: '0 auto' }} options={masonryOptions}>
                {props.children}
            </Masonry>
        </div>
    );
}

export default PostsMasonry;