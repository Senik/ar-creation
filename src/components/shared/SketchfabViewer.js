import React, { Component } from 'react';

class SketchfabViewerIframe extends Component {
  state = {
    link: ''
  };
  componentDidMount() {
    this.setState(
      {
        link: this.props.link
      },
      () => {
        this.loadModel();
      }
    );
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    if (this.props.link !== nextProps.link) {
      const self = this;
      self.loadModel();
      self.setState(
        {
          link: nextProps.link
        },
        () => {
          self.loadModel();
        }
      );
    }
  }
  loadModel = () => {
    const { link } = this.state;
    const iframe = document.getElementById('api-iframe');
    const check = this.props.linkCheck;
    const index = link.lastIndexOf('-');
    let uid = link.slice(index + 1);
    if (check) {
      if (!link.includes('sketchfab.com/3d-models/') || uid.length < 1) {
        uid += 'falsyURL';
      }
    }
    const client = new window.Sketchfab(iframe);
    client.init(uid, {
      success: function onSuccess(api) {
        api.start();
        api.addEventListener('viewerready', () => {
          console.log('Viewer is reaqdy');
        });
      },
      error: function error() {
        console.log('viewer error');
      },
      continuousRender: 1
    });
  };
  render() {
    return (
      <div>
        <iframe
          title={this.props.link}
          width="100%"
          height="350px"
          src=""
          id="api-iframe"
          allow="autoplay; fullscreen; vr"
          allowFullScreen={true}
          webkitallowfullscreen="true"
        />
      </div>
    );
  }
}

export default SketchfabViewerIframe;
