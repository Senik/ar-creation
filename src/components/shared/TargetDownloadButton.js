import React, { useState } from 'react';
//MUI
import { IconButton } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';
//styles
import axios from "axios";
//components
import PostLoader from '../shared/PostLoader';

const TargetDownloadButton = (props) => {
  const {
    url,
    styles,
    path
  } = props;

  const [name, setName] = useState(props.name);
  const [loading, setLoading] = useState(false);

  const getDownloadLink = () => {
    if (!props.name && url) {
      axios({
        url: `${path}${url}`,
        method: 'GET',
        responseType: 'blob'
      })
        .then(response => {
          const name = window.URL.createObjectURL(new Blob([response.data]));
          setName(name);
          let a = document.createElement('a');
          a.href = name;
          a.download = url;
          a.click();
          setLoading(false)
        })
        .catch(err => console.log(err));
    } else if (props.name && url) {
      let a = document.createElement('a');
      a.href = name;
      a.download = url;
      a.click();
      setLoading(false)
    }
  };

  const downloadFile = () => {
    setLoading(true);
    getDownloadLink()
  };
  return (
    loading ?
      <div className={styles} >
        <PostLoader loaderWidth='50px' />
      </div> :
      <div onClick={downloadFile} className={styles}>
        <IconButton>
          <Icon>get_app</Icon>
        </IconButton>
      </div>

  );
};

export default TargetDownloadButton;
