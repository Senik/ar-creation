import React from 'react';
import { Link } from 'react-router-dom';
//icons
import DefaultAvatar from '../../assets/img/default_avatar.png';
import googlepoly from '../../assets/img/icons/polyIcon.png';
import sketchfab from '../../assets/img/icons/sketchfablogo.png';
//styles
import './user-card.styles.scss';

const UserCard = ({
  user,
  bio,
  socialLinks,
  linkTo,
  followUnfollow,
  profile,
  showFollowButton
}) => {
  const avatarImageUrl =
    user.User_ProfilePicURL !== 'ProfilePic URL'
      ? user.User_ProfilePicURL
      : `https://triplee.info/Triple_E_Social/ProfilePictures/${
      user.User_ID
      }.jpg?time=${new Date()}`;
  const socialList =
    socialLinks &&
    Object.keys(socialLinks).map(key => {
      if (socialLinks[key].length > 1) {
        return (
          <a
            key={key}
            href={socialLinks[key].includes('http') ? socialLinks[key] : `https://${socialLinks[key]}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            {key.toLowerCase() === 'googlepoly' ||
              key.toLowerCase() === 'sketchfab' ? (
                key.toLowerCase() === 'sketchfab' ? (
                  <img
                    src={sketchfab}
                    className="image-logo"
                    alt="Sketchfab logo"
                  />
                ) : (
                    <img
                      src={googlepoly}
                      className="image-logo"
                      alt="Google Poly logo"
                    />
                  )
              ) : (
                <i className={`fab fa-${key.toLowerCase()} social-icon`} />
              )}
          </a>
        );
      }
      return null;
    });
  const userCard = (
    <div>
      <div className="header">
        <img
          src={avatarImageUrl}
          onError={e => (e.target.src = DefaultAvatar)}
          alt="Profile"
        />
      </div>
      <div className="Ocean">
        <div className="Coral">
          <div>
            <span className="Coralwave1" />
            <span className="Coralwave2" />
            <span className="Coralwave3" />
          </div>
        </div>
      </div>
      <div className="body">
        <p>{user.User_FullName}</p>
        <div className="user-bio">
          <p>{bio && bio}</p>
        </div>
        <div className="social">{socialList}</div>
        <div className="hashtags">
          <p>{user.User_Skills}</p>
        </div>
        {showFollowButton && (
          <button className="follow-button" onClick={followUnfollow}>
            {!!user.Followers && profile && profile.User_ID in user.Followers
              ? 'Unfollow'
              : 'Follow'}
          </button>
        )}
      </div>
    </div>
  );

  return (
    <div className="user-card">
      <div className="user">
        {linkTo ? (
          <Link to={`${linkTo}${user.User_ID}`}>{userCard}</Link>
        ) : (
            userCard
          )}
      </div>
    </div>
  );
};

export default UserCard;
