import firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";
import dotenv from "dotenv";
dotenv.config();
export const firebaseKey =
  process.env.NODE_ENV === "development"
    ? process.env.REACT_APP_FIREBASE_KEY
    : "AIzaSyA4P7nIe0blTqY85RcPEI_fNVpJBbbODMc";
// Initialize Firebase Triplee/Arize account
const config = {
  apiKey: firebaseKey,
  authDomain: "tripleearplatform.firebaseapp.com",
  databaseURL: "https://tripleearplatform.firebaseio.com",
  projectId: "tripleearplatform",
  storageBucket: "tripleearplatform.appspot.com",
  messagingSenderId: "613545864525"
};

const devConf = {
  apiKey: "AIzaSyAVxEtEEjaIClxkkl63_Kp6D2ML5cXiE6M",
  authDomain: "ar-creation-92699.firebaseapp.com",
  databaseURL: "https://ar-creation-92699.firebaseio.com",
  projectId: "ar-creation-92699",
  storageBucket: "ar-creation-92699.appspot.com",
  messagingSenderId: "231300731916",
  appId: "1:231300731916:web:4775cde3ce3daa7dac4a29"
};

firebase.initializeApp(config);

export default firebase;
