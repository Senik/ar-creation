import axios from 'axios';

export const getCountryCode = () => {
  return process.env.NODE_ENV === 'development'
    ? axios
        .get('http://ip-api.com/json')
        .then(res => {
          const countryCode = res.data.countryCode;
          return countryCode;
        })
        .catch(err => console.log('Location not fopund error', err))
    : axios
        .get('https://ipinfo.io/json')
        .then(res => {
          const countryCode = res.data.country;
          return countryCode;
        })
        .catch(err => console.log('Location not fopund error', err));
};
