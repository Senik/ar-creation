import React from "react";
import key from "../config/googleMapApi";
import firebase, { firebaseKey } from "../config/fbConfig";
import axios from "axios";
import moment from "moment";

//icons
import scetchfabIcon from "../assets/img/icons/sketchfablogo.png";
import googlepolyIcon from "../assets/img/icons/poly.png";
import video360Icon from "../assets/img/icons/360video.png";
import youtube360Icon from "../assets/img/icons/mq3.png";
//styles
import classes from "./helpers.module.scss";

export function validURL(str) {
  var pattern = new RegExp(
    "^(https?:\\/\\/)?" + // protocol
    "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
    "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
    "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
    "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
    "(\\#[-a-z\\d_]*)?$",
    "i"
  ); // fragment locator
  return !!pattern.test(str);
}

export function snapshotToArray(snapshot) {
  let returnArr = [];

  snapshot.forEach(childSnap => {
    let item = childSnap.val();
    item.key = key && childSnap.key;

    returnArr.push(item);
  });
  return returnArr;
}

export const componentDecorator = (href, text, key) => (
  <a href={href} key={key} target="_blank" rel="noopener noreferrer">
    {text}
  </a>
);

const requestCountryName = async coords => {
  const response = await fetch(
    `https://maps.googleapis.com/maps/api/geocode/json?latlng=${coords}&key=${key}`
  );
  const data = await response.json();
  return data;
};

export const countryCodeRequest = countries => {
  let cities = [];
  countries.map(async country => {
    const res = await requestCountryName(country);
    if (res.status === "OK") {
      let address_components = res.results[0].address_components;
      loop1: for (let i = 0; i < address_components.length; i++) {
        let types = address_components[i].types;
        for (let j = 0; j < types.length; j++) {
          switch (types[j]) {
            case "locality":
            case "administrative_area_level_1":
              cities.push(address_components[i].long_name);
              break loop1;
            default:
              break;
          }
        }
      }
    }
  });
  return cities;
};

export const shareLinkCount = post => {
  const postShareCount = post.PostShareCount || 0;
  firebase
    .database()
    .ref(`Posts/${post.PostID}`)
    .update({
      PostShareCount: +postShareCount + +1
    })
    .then(res => console.log("share added"))
    .catch(err => console.log(err));
};

export const _createDynamicLink = async (post, alertNotification) => {
  try {
    const res = await dynamicLink(post);
    const data = await res.json();
    copyToClipboard(data.shortLink);
    alertNotification("Link copied successfully");
    shareLinkCount(post);
  } catch (err) {
    console.log(err);
  }
};

export const likePost = (post, user) => {
  firebase
    .database()
    .ref(`Posts/${post.PostID}/Likes/${user.User_ID}`)
    .set({ date: Date.now() })
    .then(() => {
      firebase
        .database()
        .ref(`Users/${user.User_ID}/LikedPosts/${post.PostID}`)
        .set({ date: Date.now() })
        .then(() => {
          createNotification(
            "like",
            post,
            `${user.User_NickName} liked your post: ${post.PostName}`,
            user
          );
        })
        .catch(err => console.log(err));
    });
};

export const disLikePost = (post, user) => {
  firebase
    .database()
    .ref(`Posts/${post.PostID}/Likes/${user.User_ID}`)
    .remove()
    .then(() => {
      firebase
        .database()
        .ref(`Users/${user.User_ID}/LikedPosts/${post.PostID}`)
        .remove();
    });
};

export const dynamicLink = post => {
  const url = `https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=${firebaseKey}`;
  const params = {
    longDynamicLink: `https://arize.page.link/?afl=&amv=0&apn=com.Triplee.TripleeSocial&ibi=com.Triplee.TripleeSocial&ifl=&ipfl&isi=1230115561&link=https://platform.arize.io/arpost/${
      post.PostID
      }&st=${post.PostName}&sd=${post.PostText}&si=https://triplee.info/${
      !post.ARTargetID
        ? "Triple_E_Thumbnail"
        : "Triple_E_WebService/AllImageTargets"
      }/${post.ARTargetName}.jpg`
  };
  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(params)
  });
};

export const renderIcon = post => {
  const { ARType, ARSource } = post;
  switch (ARType) {
    case "3DBundle":
      if (ARSource === "ZF") {
        return (
          <i
            className={`fas fa-file-archive ${classes.contentTypeIcon} ${classes.ZFicon}`}
          />
        );
      } else if (ARSource === "SF") {
        return (
          <img
            src={scetchfabIcon}
            className={classes.contentTypeImage}
            alt="sketchfab"
          />
        );
      } else if (ARSource === "GP") {
        return (
          <img
            src={googlepolyIcon}
            className={classes.contentTypeImage}
            alt="googlepolys"
          />
        );
      } else {
        return (
          <i
            className={`fas fa-cube ${classes.contentTypeIcon} ${classes.BundleIcon}`}
          />
        );
      }
    case "Video":
    case "TransParentVideo":
      return (
        <i
          className={`fas fa-file-video ${classes.contentTypeIcon} ${classes.videoIcon}`}
        />
      );
    case "360Video":
      return (
        <img
          src={video360Icon}
          className={classes.contentTypeImage}
          alt="video"
        />
      );
    case "Youtube":
    case "TransParentYoutubeVideo":
      return (
        <i
          className={`fab fa-youtube ${classes.contentTypeIcon} ${classes.youtubeIcon}`}
        />
      );
    case "360YoutubeVideo":
      return (
        <img
          src={youtube360Icon}
          className={classes.contentTypeImage}
          alt="360YoutubeVideo"
        />
      );
    case "WebURL":
      return (
        <i
          className={`fas fa-link ${classes.contentTypeIcon} ${classes.WebURLIcon}`}
        />
      );
    default:
      return null;
  }
};
export const renderType = post => {
  const { ARType, ARSource } = post;
  if (ARType === "3DBundle") {
    switch (ARSource) {
      case "GP":
        return "Google Poly";
      case "SF":
        return "Sketchfab";
      case "ZF":
        return "Zip File";
      default:
        return ARType;
    }
  }
  if (ARType === "TransParentVideo") {
    return "Video";
  } else if (ARType === "TransParentYoutubeVideo") {
    return "Youtube";
  }
  return ARType;
};

export const filterByTab = (posts, tabStatus, checkDeleted = false) => {
  let filteredPosts =
    posts &&
    posts.filter(post => {
      switch (tabStatus) {
        case 0:
          return checkDeleted
            ? post.PostStatus === "TestMode" &&
            post.PostDeleteStatus !== "deleted"
            : post.PostStatus === "TestMode";
        case 1:
          return checkDeleted
            ? post.PostStatus === "InReview" &&
            post.PostDeleteStatus !== "deleted"
            : post.PostStatus === "InReview";
        case 2:
          return checkDeleted
            ? post.PostStatus === "Rejected" &&
            post.PostDeleteStatus !== "deleted"
            : post.PostStatus === "Rejected";
        default:
          return null;
      }
    });
  return filteredPosts;
};

export function VuforiaGetTarget(data) {
  return fetch("https://triplee.info/Triple_E_WebService/GetTargetSenik.php", {
    method: "POST",
    body: data
  });
}

export function countViews(UniqueViewes, users) {
  let countries = [];
  let uniquViewers = [];
  let registeredUsers = [];
  let femaleCount = 0;
  let maleCount = 0;
  let otherCount = 0;
  let notSpecifiedCount = 0;

  Object.values(UniqueViewes).forEach(item => {
    countries.push(item.Location);
    uniquViewers.push(item.ViewerID);
  });
  registeredUsers = users.filter(user => {
    return uniquViewers.indexOf(user.User_ID) > -1;
  });

  notSpecifiedCount += uniquViewers.length - registeredUsers.length;
  registeredUsers.forEach(user => {
    switch (user.User_Gender.toLowerCase()) {
      case "male":
        return maleCount++;
      case "female":
        return femaleCount++;
      case "other":
        return otherCount++;
      default:
        return notSpecifiedCount++;
    }
  });
  let maleCountPercent,
    femaleCountPercent,
    otherCountPercent,
    notSpecifiedCountPercent;
  if (!uniquViewers.length) {
    maleCountPercent = femaleCountPercent = otherCountPercent = notSpecifiedCountPercent = 0;
  } else {
    maleCountPercent = toPersent(uniquViewers.length, maleCount);
    femaleCountPercent = toPersent(uniquViewers.length, femaleCount);
    otherCountPercent = toPersent(uniquViewers.length, otherCount);
    notSpecifiedCountPercent = toPersent(
      uniquViewers.length,
      notSpecifiedCount
    );
  }
  return {
    maleCountPercent,
    femaleCountPercent,
    otherCountPercent,
    notSpecifiedCountPercent,
    countries
  };
}

export function filterByKeyword(
  arr,
  filters,
  keywordArray,
  otherCheck = false
) {
  const keyword = keywordArray[0];
  let result =
    arr &&
    arr.filter(item => {
      if (item.value) {
        item = item.value;
      }
      if (otherCheck) {
        if (!item[otherCheck]) {
          return false;
        }
      }
      let checkResult = false;
      for (let i = 0; i < filters.length; i++) {
        if (
          item[filters[i]] &&
          item[filters[i]]
            .toLowerCase()
            .trim()
            .search(keyword.toLowerCase()) >= 0
        ) {
          checkResult = true;
          break;
        }
      }
      return checkResult;
    });

  return result;
}

export function filterByArr(arr, checkArr) {
  return arr.filter(post => {
    if (post.value) {
      post = post.value;
    }
    let check = true;
    for (let i = 0; i < checkArr.length; i++) {
      if (post[checkArr[i].key] === checkArr[i].value) {
        check = false;
        break;
      }
    }
    return check;
  });
}

export function dataURItoBlob(dataURI) {
  // convert base64 to raw binary data held in a string
  // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
  var byteString = atob(dataURI.split(",")[1]);

  // separate out the mime component
  var mimeString = dataURI
    .split(",")[0]
    .split(":")[1]
    .split(";")[0];

  // write the bytes of the string to an ArrayBuffer
  var ab = new ArrayBuffer(byteString.length);

  // create a view into the buffer
  var ia = new Uint8Array(ab);

  // set the bytes of the buffer to the correct values
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }

  // write the ArrayBuffer to a blob, and you're done
  var blob = new Blob([ab], { type: mimeString });
  return blob;
}

function toPersent(total, count) {
  return ((count * 100) / total).toFixed(2);
}
//follow || unfollow
export function followUser(user, follower) {
  if (user && follower) {
    firebase
      .database()
      .ref(`Users/${user.User_ID}/Following/${follower.User_ID}`)
      .set({ date: Date.now() })
      .then(() => {
        firebase
          .database()
          .ref(`Users/${follower.User_ID}/Followers/${user.User_ID}`)
          .set({ date: Date.now() })
          .then(() => {
            createNotification(
              "follow",
              follower,
              `${user.User_FullName} started following you`,
              user
            );
          })
          .catch(err => console.log(err));
      });
  } else {
    return;
  }
}
export function unFollowUser(user, follower) {
  if (user && follower) {
    firebase
      .database()
      .ref(`Users/${user.User_ID}/Following/${follower.User_ID}`)
      .remove()
      .then(() => {
        firebase
          .database()
          .ref(`Users/${follower.User_ID}/Followers/${user.User_ID}`)
          .remove()
          .then(() => console.log("Follower and followers removed"))
          .catch(err => console.log(err));
      });
  }
}
//NOTIFICATIONS
export const sendNotificationUser = (
  message,
  devIds,
  add_data,
  reason = null
) => {
  if (devIds) {
    const devIdsStr = devIds.join(",");
    const data = new FormData();
    data.append("notif_message", message);
    data.append("device_ids", devIdsStr);
    data.append("additional_data_key", add_data.key);
    data.append("additional_data_value", add_data.value);
    data.append("reason", reason);
    const url =
      "https://triplee.info/Triple_E_WebService/send_notifications.php";

    axios({
      method: "POST",
      url,
      data
    })
      .then(res => console.log(res.data))
      .catch(err => console.log(err));
  }
};
export async function createNotification(
  type,
  post,
  message,
  user,
  reason = null
) {
  try {
    let notifRecipientId = "";
    notifRecipientId =
      type === "tofollowers" ? user.User_ID : post.PostAuthorID || post.User_ID;

    const devIds = await getUserNotifDeviceIds(notifRecipientId);
    const notifData = {
      DateTime: Date.now(),
      Message: message,
      NotificationOpened: "False",
      NotificationType: type
    };
    const additional_data = {
      key: "NotificationType",
      value: "General"
    };

    switch (type) {
      case "like":
      case "purchase":
        notifData.User = { User_ID: user.User_ID };
        notifData.Post_ID = post.PostID;
        notifData.Post_Name = post.PostName;
        if (type === "purchase") {
          additional_data.key = "XRGallery";
          additional_data.value = post.PostID;
        }
        break;
      case "accept":
      case "reject":
        notifData.Post_ID = post.PostID;
        notifData.Post_Name = post.PostName;
        notifData.Reason = reason;
        break;
      case "delete":
        notifData.Post_Name = post.PostName;
        break;
      case "follow":
        notifData.User = { User_ID: user.User_ID };
        break;
      case "tofollowers":
        notifData.Post_ID = post.PostID;
        notifData.Post_Name = post.PostName;
        notifData.Post_Author_Nickname = post.PostAuthorNickName;
        notifData.Post_Author_ID = post.PostAuthorID;
        additional_data.key = "XRGallery";
        additional_data.value = post.PostID;
        break;
      default:
        break;
    }

    await insertNotifToFirebase(notifRecipientId, notifData);
    if (devIds) {
      await sendNotificationUser(message, devIds, additional_data, reason);
    }
  } catch (err) {
    console.log(err);
  }
}
export const notificationSeen = (key, id) => {
  firebase
    .database()
    .ref(`/Users/${id}/Notifications/${key}`)
    .update({ NotificationOpened: "True" });
};
async function getUserNotifDeviceIds(user) {
  const postOwnerRef = await firebase
    .database()
    .ref(`Users/${user}`)
    .once("value");
  const devIds =
    postOwnerRef.val().PushNotifiDeviceIDs &&
    Object.keys(postOwnerRef.val().PushNotifiDeviceIDs);
  return devIds;
}

function insertNotifToFirebase(postOwnerId, data) {
  return firebase
    .database()
    .ref(`Users/${postOwnerId}/Notifications`)
    .push(data);
}
export function generateUUID() {
  return (
    "_" +
    Math.random()
      .toString(36)
      .substr(2, 9)
  );
}

export function generatePromoCode(packageName) {
  let cahar = packageName[1] === "l" ? "l" : "p";
  return (
    cahar +
    Math.random()
      .toString(36)
      .substr(2, 9)
  );
}

export const sendNotificationToFollowers = async post => {
  const usersRef = await firebase
    .database()
    .ref("Users")
    .once("value");

  const Users = usersRef.val();
  const postAuthor = Users[post.PostAuthorID];
  const followers_ids =
    (postAuthor.Followers && Object.keys(postAuthor.Followers)) || [];
  if (
    followers_ids.length > 0 &&
    post.PostPrivacy === "Public" &&
    post.XRGallery === "True"
  ) {
    followers_ids.map(follower_id => {
      createNotification(
        "tofollowers",
        post,
        `${postAuthor.User_NickName} created new post: ${post.PostName}!`,
        Users[follower_id]
      );
    });
  }
};

export const countCreatorAmount = (Buyers, Payout) => {
  if (Buyers) {
    const buyers = Object.values(Buyers);
    const payouts = Payout && Object.values(Payout);
    const lastPayout = getRecentPayout(payouts);
    let sum = buyers.reduce((amount, buyer) => {
      if (
        isNaN(buyer.Amount) ||
        (lastPayout &&
          !(new Date(buyer.DateTime) > new Date(lastPayout[0].DateTime)))
      ) {
        return amount;
      }
      return +buyer.Amount + amount;
    }, 0);
    sum -= (sum * 10) / 100;
    return sum;
  }
};

export const addCredentials = (data, id) => {
  return firebase
    .database()
    .ref(`/Users/${id}/Creator_Credentials`)
    .set({ ...data });
};
export function getRecentPayout(payouts) {
  if (!payouts) return null;
  const recentDate = new Date(
    Math.max.apply(
      null,
      payouts.map(payout => new Date(payout.DateTime))
    )
  );
  const recentPayout = payouts.filter(payout => {
    const d = new Date(payout.DateTime);
    return d.getTime() == recentDate.getTime();
  });
  return recentPayout;
}

export const countDecimals = function (value) {
  if (value % 1 != 0) return value.toString().split(".")[1].length;
  return 0;
};

function copyToClipboard(string) {
  let textarea;
  let result;

  try {
    textarea = document.createElement("textarea");
    textarea.setAttribute("readonly", true);
    textarea.setAttribute("contenteditable", true);
    textarea.style.position = "fixed"; // prevent scroll from jumping to the bottom when focus is set.
    textarea.value = string;

    document.body.appendChild(textarea);

    textarea.focus();
    textarea.select();

    const range = document.createRange();
    range.selectNodeContents(textarea);

    const sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);

    textarea.setSelectionRange(0, textarea.value.length);
    result = document.execCommand("copy");
  } catch (err) {
    console.error(err);
    result = null;
  } finally {
    document.body.removeChild(textarea);
  }

  // manual copy fallback using prompt
  if (!result) {
    const isMac = navigator.platform.toUpperCase().indexOf("MAC") >= 0;
    const copyHotkey = isMac ? "⌘C" : "CTRL+C";
    result = prompt(`Press ${copyHotkey}`, string); // eslint-disable-line no-alert
    if (!result) {
      return false;
    }
  }
  return true;
}

export function countUserPosts(posts) {
  if (!posts) {
    return {
      totalPosts: null,
      paidPosts: 0
    };
  }
  let paidPosts = 0;
  posts.forEach(post => {
    if (post.IsPaid && post.IsPaid.toLowerCase() === "true") {
      paidPosts++;
    }
  });
  return {
    totalPosts: posts,
    paidPosts
  };
}

export function checkLimitForCreation(profile, totalPosts) {
  let reachCreationLimit = false;
  let paidPostsLimit = false;
  let postsCountCurrentMonth = 0;
  let paidPostsCountCurrentMonth = 0;

  if (
    profile.Subscription &&
    moment(profile.Subscription.ExpirationDate) > moment()
  ) {
    if (profile.Subscription.Type === "Plus") {
      totalPosts.forEach(post => {
        if (moment(post.DateTime).isSame(moment(), "month")) {
          if (post.IsPaid) {
            paidPostsCountCurrentMonth++;
          }
          postsCountCurrentMonth++;
        }
      });
      if (profile.User_Type === "individual") {
        reachCreationLimit = postsCountCurrentMonth >= 20;
        paidPostsLimit = paidPostsCountCurrentMonth >= 10;
      } else if (profile.User_Type === "enterprise") {
        reachCreationLimit = postsCountCurrentMonth >= 20;
        paidPostsLimit = paidPostsCountCurrentMonth >= 10;
      }
    } else if (profile.Subscription.Type === "Premium") {
      if (profile.User_Type === "individual") {
        reachCreationLimit = false;
      } else if (profile.User_Type === "enterprise") {
        reachCreationLimit = false;
      }
    }
  } else {
    if (profile.User_Type === "individual") {
      reachCreationLimit = totalPosts.length >= 15;
      paidPostsLimit = paidPostsCountCurrentMonth >= 5;
    } else if (profile.User_Type === "enterprise") {
      reachCreationLimit = totalPosts.length >= 5;
      paidPostsLimit = paidPostsCountCurrentMonth >= 0;
    }
  }
  return {
    postsLimit: reachCreationLimit,
    paidPostsLimit
  };
}

export function writeToLocalStorage(key, value) {
  localStorage.setItem(key, value);
}

export function send_message_telegram_bot(text) {
  return axios.post(
    "https://us-central1-tripleearplatform.cloudfunctions.net/widgets/newPost",
    { message: text }
  );
}