import ReactGA from 'react-ga';

export const initGa = () => {
  console.log('Ga Init');
  ReactGA.initialize('UA-126807753-2');
};
export const logPageView = () => {
  ReactGA.set({ page: window.location.pathname });
  ReactGA.pageview(window.location.pathname + window.location.search);
};
