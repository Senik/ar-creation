import React from "react";
import { connect } from "react-redux";
//selectors
import { selectProfile } from "../../redux/auth/auth.selectors";
import { select_post_by_id } from "../../redux/post/post.selectors";
//componsnts
import ArPostView from "./ArPostView";

class ArPost extends React.Component {
  render() {
    return <ArPostView {...this.props} />;
  }
}

const mapStateToProps = (state, props) => {
  const postId = props.match.params.id;
  return {
    post: select_post_by_id(postId)(state),
    profile: selectProfile(state)
  };
};

export default connect(mapStateToProps)(ArPost);
