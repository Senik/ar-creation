import React from "react";
import { connect } from "react-redux";

import { Link } from "react-router-dom";
import moment from "moment";
import Linkify from "react-linkify";
//selectors
import {
  selectAllUsers,
  selectUserById
} from "../../redux/user/user.selectors";
//actions
import { alertNotification } from "../../redux/auth/auth.actions";
import { addPostPayment } from "../../redux/post/post.actions";
//helpers
import {
  componentDecorator,
  _createDynamicLink,
  renderIcon,
  renderType,
  countViews,
  likePost,
  disLikePost
} from "../../helpers";
//MUI
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
import Typography from "@material-ui/core/Typography";
//components
import SketchfabViewerIframe from "../../components/shared/SketchfabViewer";
import GooglePolyIframe from "../../components/shared/GooglePolyIframe";
import ViewOrBuyPost from "./ViewOrBuyPost";
import TriggerFullModal from "../../components/modals/postFullScreenImage";
import FullScreenDialog from "../../components/modals/postStatistic";
import PostInfoIcons from "../../components/postIcons/PostInfoIcons";
import PostLoader from "../../components/shared/PostLoader";
import PostNotFound from "../../components/shared/PostNotFound";
//icons
import DefaultAvatar from "../../default_avatar.png";
//styles
import classes from "./ArPost.module.scss";
import PostProcessing from "../../components/processing/PostProcessing";

class ArPostView extends React.Component {
  componentDidMount() {
    document.title = "AR Post";
  }
  renderContentIcon = () => {
    const { post } = this.props;
    if (post) {
      return renderIcon(post);
    }
  };
  renderPostType = () => {
    const { post } = this.props;
    if (post) {
      return renderType(post);
    }
  };
  createDynamicLink = post => {
    _createDynamicLink(post, this.props.alertNotification);
  };

  renderTargetImage = () => {
    const { post } = this.props;
    const { ARSource, ARSourceURL, ARTargetID } = post;
    switch (ARSource) {
      case "SF":
        return <SketchfabViewerIframe link={ARSourceURL} />;
      case "GP":
        return <GooglePolyIframe link={ARSourceURL} />;
      case "ZF":
        return (
          <i
            className="fas fa-file-archive ChooseContetnType-icon-218 fa-6x"
            style={{ marginBottom: 25 }}
          />
        );
      default:
        return (
          <TriggerFullModal
            post={post}
            triggerBaseUrl={`https://triplee.info/${
              ARTargetID
                ? "Triple_E_WebService/AllImageTargets/"
                : "Triple_E_Thumbnail/"
            }`}
          />
        );
    }
  };
  goBack = e => {
    e.preventDefault();
    this.props.history.push("/argallery");
  };
  handleLikePost = post => {
    const { profile } = this.props;
    if (profile.isEmpty) {
      return;
    }
    if (post.Likes && profile.User_ID in post.Likes) {
      return disLikePost(post, profile);
    }
    likePost(post, profile);
  };
  render() {
    const self = this;
    const { post, profile, users, user, addPostPayment, history } = self.props;
    if (post && user) {
      if (post.ARTargetStatus === "Processing") {
        return <PostProcessing from="singlePage" />;
      }
      const userID = profile && profile.User_ID;
      const {
        UniqueViewes,
        Buyers,
        PostAuthorID,
        PostID,
        PostStatus,
        PostDeleteStatus
      } = post;
      let statistics = {};
      if (UniqueViewes && users) {
        statistics = countViews(UniqueViewes, users);
      }
      if (userID !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
        if (PostDeleteStatus === "deleted") {
          const buyers = (Buyers && Object.values(Buyers)) || [];
          let isPostBought = buyers.some(buyer => buyer.UserID === userID);
          if (!isPostBought) {
            return (
              <div className="col s12 m8 offset-m2 l7 offset-l3 wasNotFound">
                <p>The post is out of access!</p>
                <i className="fas fa-times fa-2x" />
              </div>
            );
          }
        }
        if (PostAuthorID !== userID) {
          if (
            post.PostPrivacy !== "Public" &&
            post.PostPrivacy !== "Unlisted"
          ) {
            return (
              <div className="col s12 m8 offset-m2 l7 offset-l3 wasNotFound">
                <p>The post is private!</p>
                <i className="fas fa-times fa-2x" />
              </div>
            );
          }
          if (PostStatus !== "TestMode") {
            return (
              <div className="col s12 m8 offset-m2 l7 offset-l3 wasNotFound">
                <p>The post is not published!</p>
                <i className="fas fa-times fa-2x" />
              </div>
            );
          }
        }
      }

      let avatarImageUrl;
      if (user && user.User_ProfilePicURL.includes("facebook")) {
        avatarImageUrl = user.User_ProfilePicURL;
      } else {
        avatarImageUrl = `https://triplee.info/Triple_E_Social/ProfilePictures/${PostAuthorID}.jpg`;
      }

      return (
        <div
          className={`col s12 m8 offset-m2 l7 offset-l3 xl12 ${classes.postContainer}`}
        >
          <div className={`${classes.backButtonContainer}`}>
            {history && (
              <button className={`btn-small grey`} onClick={this.goBack}>
                <i className="fas fa-arrow-left" />
                <span className={classes.backButtonText}>
                  Go back to AR Gallery
                </span>
              </button>
            )}
          </div>

          <Card className={classes.card}>
            <CardHeader
              avatar={
                <Link to={`/creator/${user.User_ID}`}>
                  <Avatar
                    aria-label="Avatar-Image"
                    src={user.User_ID && avatarImageUrl}
                    onError={e => (e.target.src = DefaultAvatar)}
                    className={classes.avatar}
                  />
                </Link>
              }
              action={
                userID === PostAuthorID ? (
                  <Link to={`/editpost/${PostID}`}>
                    <Button className={classes.editLink}>
                      <Icon>edit</Icon>
                    </Button>
                  </Link>
                ) : null
              }
              title={
                <Link to={`/creator/${user.User_ID}`}>
                  {" "}
                  {user.User_FullName}{" "}
                </Link>
              }
              subheader={
                <div>
                  <span>{moment(post.LastEdit).format("MMMM Do YYYY")}</span>
                  <p style={{ margin: 0 }}>
                    {self.renderPostType()}
                    {self.renderContentIcon()}
                  </p>
                </div>
              }
            />
            <div className="card center-align">
              <div className="card-content">
                <span className="card-title center-align">{post.PostName}</span>
              </div>
              {self.renderTargetImage()}
            </div>

            <CardContent>
              <Typography component="p">
                <Linkify componentDecorator={componentDecorator}>
                  {post.PostText}
                </Linkify>
              </Typography>
            </CardContent>
            <CardActions
              className={classes.actions}
              style={{ margin: "15px 0" }}
            >
              <PostInfoIcons
                profile={profile}
                showLikes={true}
                handleLikePost={this.handleLikePost}
                post={post}
                dynamicLinkCreation={self.createDynamicLink}
              />
              {userID === PostAuthorID && (
                <FullScreenDialog
                  post={post}
                  statistics={statistics}
                  style={{ backgroundColor: "transparent" }}
                />
              )}
            </CardActions>
          </Card>
          <ViewOrBuyPost
            user={user}
            profile={profile}
            post={post}
            addPostPayment={addPostPayment}
            direction="fromARPost"
          />
        </div>
      );
    } else if (post === undefined) {
      return <PostNotFound message="Post was not found" />;
    } else {
      return <PostLoader />;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const { post } = ownProps;
  if (post) {
    return {
      user: selectUserById(post.PostAuthorID)(state),
      users: selectAllUsers(state)
    };
  }
  return {};
};

export default connect(mapStateToProps, { alertNotification, addPostPayment })(
  ArPostView
);
