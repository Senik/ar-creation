import React, { useState } from "react";
import axios from "axios";
import SimpleCrypto from "simple-crypto-js";
//helper
import { getCountryCode } from "../../helpers/getUserLocationIP";
//MUI
import Button from "@material-ui/core/Button";
//components
import SignInModal from "../../components/modals/arPost/SignInModal";
import InAppModal from "../../components/modals/arPost/InAppModal";
import EnterpriseModal from "../../components/modals/arPost/EnterpriseModal";
//styles
import classes from "./ArPost.module.scss";
import { EU_Countries } from "../../helpers/eu_countries";

const ViewOrBuyPost = props => {
  const [isSignInDialogOpen, setIsSignInDialogOpen] = useState(false);
  const [isInAppDialogOpen, setIsInAppDialogOpen] = useState(false);
  const [modalMessage, setMessage] = useState("Please open in application");
  const [isEnterpriseDialogOpen, setIsEnterpriseDialogOpen] = useState(false);
  const [companyCredentials, setCompanyCredentials] = useState({
    companyName: null,
    companyAddress: null,
    companyVat: null
  });
  const handleSignInDialogClose = () => setIsSignInDialogOpen(false);
  const handleInAppDialogClose = () => setIsInAppDialogOpen(false);

  const { post, profile, direction } = props;
  const buyers = (post.Buyers && Object.values(post.Buyers)) || [];
  const isPostBought = buyers.some(buyer => buyer.UserID === profile.User_ID);
  const isPostMyne = post.PostAuthorID === profile.User_ID;
  const handleButtonClick = e => {
    if (profile.User_ID) {
      if (post.IsPaid === "True" && !isPostBought && !isPostMyne) {
        if (profile.User_Location === "Location") {
          setMessage("Please update your location in your profile.");
          setIsInAppDialogOpen(true);
          return;
        }
        if (profile.User_Type === "enterprise") {
          setIsEnterpriseDialogOpen(true);
        } else if (
          profile.User_Type === "individual" ||
          profile.User_Type === "Prime"
        ) {
          let country = profile.User_Location;
          const redirectURL = `user_id=${profile.User_ID}&post_id=${post.PostID}&code=${country}`;
          let chiphedURL = `${window.location.origin}${generateURL(
            redirectURL
          )}`;
          makePayment(chiphedURL, country);
        }
      } else {
        setIsInAppDialogOpen(true);
      }
    } else {
      if (post.IsPaid === "True") {
        setIsSignInDialogOpen(true);
      } else {
        setIsInAppDialogOpen(true);
      }
    }
  };

  const handleModalChange = (val, name) => {
    setCompanyCredentials({
      ...companyCredentials,
      [name]: val
    });
  };

  const handleSubmitPayment = () => {
    let code = profile.User_Location;
    const { companyName, companyAddress, companyVat } = companyCredentials;
    const redirectURL = `user_id=${profile.User_ID}&post_id=${post.PostID}&code=${code}&cmpName=${companyName}&cmpAddress=${companyAddress}&cmpVat=${companyVat}`;
    let chiphedURL = `${window.location.origin}${generateURL(redirectURL)}`;
    setIsEnterpriseDialogOpen(false);
    makePayment(chiphedURL, code);
  };

  const handleCancelPayment = () => {
    setIsEnterpriseDialogOpen(false);
    setCompanyCredentials({
      companyName: null,
      companyAddress: null,
      companyVat: null
    });
  };

  const makePayment = (chiphedURL, code) => {
    let vat = 0;
    if (
      (EU_Countries.includes(code) && profile.User_Type === "individual") ||
      (code === "Netherlands" && profile.User_Type === "enterprise")
    ) {
      let decimalVAT = (((+post.Price * 10) / 100) * 21) / 100;
      vat = Math.round(decimalVAT * 100) / 100;
    }
    const postPrice = +post.Price + +vat;
    const url =
      "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/paymentTest.php";
    const data = new FormData();
    data.append("value", (+postPrice).toFixed(2));
    data.append("url", chiphedURL);
    axios({
      method: "POST",
      url,
      data
    })
      .then(res => {
        let checkoutURL = res.data._links.checkout.href;
        localStorage.setItem("paymentData", JSON.stringify(res.data));
        localStorage.setItem("PostID", post.PostID);
        window.location.href = checkoutURL;
      })
      .catch(err => console.log("Payment error", err));
  };

  const generateURL = urlSearchParams => {
    let _secretKey = "44LFGcXvsE21jlFCaCL060vooDXgIdw8Wl04";
    let simpleCrypto = new SimpleCrypto(_secretKey);
    let chiperURL = simpleCrypto.encrypt(urlSearchParams);
    let url = `/payment?${chiperURL}`;
    return url;
  };

  return (
    <>
      <div className={classes.buttonGroup}>
        <Button
          className={`
            btn-large indigo white-text waves-effect waves-light z-depth-3`}
          onClick={handleButtonClick}
        >
          {post.IsPaid === "True" && !isPostBought && !isPostMyne ? (
            <div style={{ margin: 10, marginTop: -7 }}>
              <p>{`\u20AC${post.Price} Buy now`}</p>
              <p style={{ position: "relative", fontSize: 8, top: -13 }}>
                VAT Excluded
              </p>
            </div>
          ) : (
            "View"
          )}
        </Button>
        {direction === "fromARPost" &&
        post.IsPaid === "True" &&
        !isPostBought &&
        !isPostMyne ? (
          <Button
            className={`btn-large indigo white-text waves-effect waves-light z-depth-3 ${classes.demoButton}`}
            onClick={handleButtonClick}
            id="preview"
          >
            Demo
          </Button>
        ) : null}
      </div>

      {isSignInDialogOpen ? (
        <SignInModal
          isOpen={isSignInDialogOpen}
          close={handleSignInDialogClose}
        />
      ) : null}
      {isInAppDialogOpen ? (
        <InAppModal
          isOpen={isInAppDialogOpen}
          close={handleInAppDialogClose}
          text={modalMessage}
        />
      ) : null}
      {isEnterpriseDialogOpen ? (
        <EnterpriseModal
          isOpen={isEnterpriseDialogOpen}
          handleModalChange={handleModalChange}
          credentials={companyCredentials}
          handleSubmitPayment={handleSubmitPayment}
          handleCancelPayment={handleCancelPayment}
        />
      ) : null}
    </>
  );
};

export default React.memo(ViewOrBuyPost);
