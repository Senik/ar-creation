import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";

//selectors
import { selectAuthUser } from "../../redux/auth/auth.selectors";
//actions
import { createPost } from "../../redux/post/post.actions";
//components
import CreateARForm from "./createARForm/CreateARForm";

function CreatePost(props) {
  useEffect(() => {
    document.title = "AR Studio";
  }, []);

  const { history } = props;
  return (
    <div className="col s12 m8 offset-m4 l10 offset-l2">
      <div className="row" style={{ marginTop: 25 }}>
        <CreateARForm history={history} />
      </div>
    </div>
  );
}

CreatePost.propTypes = {
  auth: PropTypes.object.isRequired,
  createPost: PropTypes.func.isRequired
};

const mapStateToProps = createStructuredSelector({
  auth: selectAuthUser
});

export default connect(mapStateToProps, { createPost })(CreatePost);
