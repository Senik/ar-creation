import React, { useState, memo } from 'react';
import PropTypes from 'prop-types';
//styles
import classes from '../Creation.module.scss';
import InputElement from "../../../components/formElements/InputElement";

const ArNameDescription = props => {
  const [postNameError, setError] = useState();
  const { handleChange, prevStep, nextStep, values } = props;
  const { postName, postDescription } = values;
  const handleNext = e => {
    e.preventDefault();
    if (!values.postName) {
      setError('The name field is required');
      return;
    }
    nextStep();
  };
 
  return (
    <div className={`container ${classes.ArNameDescription}`}>
      <div className={`row ${classes.ArNameRow}`}>
        <div className="col s12 m3">
          <button
            className={`btn-small grey ${classes.ArNameBtn}`}
            onClick={prevStep}
          >
            <i className="fas fa-arrow-left" />
          </button>
        </div>
        <div className="col s12 m7 center-align">
          <p className={classes.stepHeader}>Set a name for your Ar Post</p>
        </div>
      </div>
      <div className="row center-align" style={{ marginTop: 75 }}>
        <InputElement
            name='postName'
            defaultValue={postName}
            label='Post Name'
            required = {true}
            handleChange = {handleChange}
        />
        <InputElement
            name='postDescription'
            defaultValue={postDescription}
            label='Post Description'
            type='textarea'
            required = {false}
            handleChange = {handleChange}
        />
        <div className="input-field">
          <button
            className={`btn deep-purple lighten-1 z-depth-0 ${
              classes.ArNameBtn
            }`}
            onClick={handleNext}
          >
            Next
          </button>
          <div className="red-text center">
            {postNameError ? <p>{postNameError}</p> : null}
          </div>
        </div>
      </div>
    </div>
  );
};

ArNameDescription.propTypes = {
  nextStep: PropTypes.func.isRequired,
  prevStep: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired
};

export default memo(ArNameDescription);
