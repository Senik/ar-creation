import React, { useState, Fragment, memo } from "react";
import { Link } from "react-router-dom";
//helpers
import { countDecimals, writeToLocalStorage } from "../../../helpers";
//styles
import classes from "../Creation.module.scss";
import CheckElement from "../../../components/formElements/Checkbox";
import Price from "../../../components/formElements/Price";

const Choose3DType = props => {
  const [postNameError, setError] = useState();
  const {
    handleChange,
    prevStep,
    nextStep,
    values,
    isCreator,
    reachedPaidPostsLimit
  } = props;
  const handleNext = e => {
    e.preventDefault();
    if (!values.model_type) {
      setError("Sorry model types are required");
      return;
    }
    if (!values.isPaid && isCreator === "True") {
      setError("Sorry price is required");

      return;
    }
    if (values.isPaid === "True" && !values.model_price) {
      setError("Oops price can not be empty");
      return;
    }
    if (values.model_price < 1 && values.isPaid === "True") {
      setError("Oops min price is 1");
      return;
    }
    nextStep();
  };
  const handleUpgradeLinkClick = e => {
    writeToLocalStorage("requestPackage", "true");
  };
  const renderUserLimitText = () => {
    const { profile } = props;

    if (profile.Subscription) {
      return (
        <p>
          You have reached the monthly limit of paid posts included in your
          plan.
          <br /> Please{" "}
          <Link to="/profile" onClick={handleUpgradeLinkClick}>
            Upgrade
          </Link>{" "}
          your plan in order to continue. You can still create free posts on
          ARize.
        </p>
      );
    } else {
      if (profile.User_Type === "individual") {
        return (
          <p>
            You have reached the limit of the paid posts included in the Free
            plan. <br /> Please{" "}
            <Link to="/profile" onClick={handleUpgradeLinkClick}>
              Upgrade
            </Link>{" "}
            your plan in order to continue. You can still create free posts on
            ARize.
          </p>
        );
      } else if (profile.User_Type === "enterprise") {
        return (
          <p>
            In order to be able to create paid posts, please{" "}
            <Link to="/profile" onClick={handleUpgradeLinkClick}>
              Upgrade
            </Link>{" "}
            your plan. You can still create free posts on ARize.
          </p>
        );
      }
    }
  };

  return (
    <div className={`container ${classes.Choose3DType}`}>
      <div className="row">
        <div className="col s12 m3">
          <button className="btn-small grey" onClick={prevStep}>
            <i className="fas fa-arrow-left" />
          </button>
        </div>
        <div className="col s12 m7 center-align">
          <p className={classes.stepHeader}>
            Choose the type of your 3D model and set price
          </p>
        </div>
      </div>
      <div className="row center-align" style={{ marginTop: 75 }}>
        <CheckElement
          title="3D Model Type"
          name="model_type"
          defaultValue={values.model_type}
          required={true}
          handleChange={handleChange}
          options={[
            {
              label: "AR Gallery",
              value: "xrGallery"
            },
            {
              label: "Attached to Trigger Image",
              value: "triggerImage"
            },
            {
              label: "Both Options",
              value: "both"
            }
          ]}
        />

        {values.model_type && isCreator === "True" && (
          <Fragment>
            <CheckElement
              title="Model Price"
              name="isPaid"
              defaultValue={values.isPaid}
              required={true}
              handleChange={handleChange}
              disabled={reachedPaidPostsLimit}
              options={[
                {
                  label: "Free",
                  value: "False"
                },
                {
                  label: "Paid",
                  value: "True"
                }
              ]}
            />
            {values.isPaid && values.isPaid === "True" ? (
              <Price price={values.model_price} handleChange={handleChange} />
            ) : null}
          </Fragment>
        )}

        <div className="input-field">
          <button
            className="btn deep-purple lighten-1 z-depth-0"
            onClick={handleNext}
          >
            Next
          </button>
          <div className="red-text center">
            {postNameError ? <p>{postNameError}</p> : null}
            {values.model_type && reachedPaidPostsLimit
              ? renderUserLimitText()
              : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(Choose3DType);
