import React, { PureComponent } from 'react';
//icons
import scetchfabIcon from '../../../assets/img/icons/sketchfablogo.png';
import faceArIcon from '../../../assets/img/icons/faceAR_white.png';
import unityIcon from '../../../assets/img/icons/unityIconwhiteIcon.png';
import googlepolyIcon from '../../../assets/img/icons/poly.png';
import youtube360Icon from '../../../assets/img/icons/mq3.png';
import video360Icon from '../../../assets/img/icons/360video.png';
//styles
import classes from '../Creation.module.scss';

class ChooseContentType extends PureComponent {
  state = {
    content_type: ''
  };
  handleSelectContent = e => {
    e.preventDefault();
    this.props.setContent(e.currentTarget.id);
  };

  render() {
    const { content_type, prevStep } = this.props;
    return (
      <div className={`container ${classes.ChooseContentType}`}>
        <div className={`row ${classes.ContentTypeRow}`}>
          <div className="col s12 m3">
            <button className="btn-small grey" onClick={prevStep}>
              <i className="fas fa-arrow-left" />
            </button>
          </div>
          <div className="col s12 m7 center-align">
            <p className={classes.stepHeader}>
              Choose your AR type (one of the options below)
            </p>
          </div>
        </div>
        <div className="row center-align" style={{ marginTop: 75 }}>
          <div className={`${classes.contentSection}`}>
            <div
              className={`white-text blue accent-2 z-depth-4 ${classes.divider}`}
            >
              <p>Video</p>
            </div>
            <div className={`${classes.arTypeContainer}`}>
              {content_type
                .filter(c => c.category === 'video')
                .map(content => {
                  return (
                    <button
                      id={content.id}
                      className={`btn-floating btn-large indigo waves-effect waves-light z-depth-3 ${classes.ContentTypeBtn}`}
                      key={content.id}
                      onClick={this.handleSelectContent}
                    >
                      <i
                        className={`${content.content_icon} ${classes.icon}`}
                      />
                      <span className={classes.buttonText}>
                        {content.content_name}
                      </span>
                    </button>
                  );
                })}
            </div>
          </div>
          <div className={classes.contentSection}>
            <div
              className={` white-text blue accent-2 z-depth-4 ${classes.divider}`}
            >
              <p>360 Video</p>
            </div>
            <div className={`${classes.arTypeContainer}`}>
              {content_type
                .filter(c => c.category === '360Video')
                .map(content => {
                  return (
                    <button
                      id={content.id}
                      className={`btn-floating btn-large indigo waves-effect waves-light z-depth-3 ${classes.ContentTypeBtn}`}
                      key={content.id}
                      onClick={this.handleSelectContent}
                    >
                      {content.content_name === 'youtube video' ? (
                        <img
                          src={youtube360Icon}
                          className={`${classes.iconImage} ${classes.youtubevideo360}`}
                          alt="Youtube 360 video"
                        />
                      ) : (
                          <img
                            src={video360Icon}
                            className={`${classes.iconImage} ${classes.video360}`}
                            alt="360 video"
                          />
                        )}
                      <span className={`${classes.iconImage}`}>
                        {content.content_name}
                      </span>
                    </button>
                  );
                })}
            </div>
          </div>
          <div className={classes.contentSection}>
            <div
              className={`white-text blue accent-2 z-depth-4 ${classes.divider}`}
            >
              <p>3D Model</p>
            </div>
            <div className={classes.arTypeContainer}>
              {content_type
                .filter(c => c.category === '3D')
                .map(content => {
                  return (
                    <button
                      id={content.id}
                      className={`btn-floating btn-large indigo waves-effect waves-light z-depth-3 ${classes.ContentTypeBtn}`}
                      key={content.id}
                      onClick={this.handleSelectContent}
                    >
                      {content.content_name === 'zip file' ? (
                        <i
                          className={`${content.content_icon} ${classes.icon}`}
                        />
                      ) : content.content_name === 'sketchfab' ? (
                        <img
                          src={scetchfabIcon}
                          className={classes.iconImage}
                          alt="Sketchfab"
                        />
                      ) : (
                            <img
                              src={googlepolyIcon}
                              className={`${classes.iconImage} ${classes.polyIcon}`}
                              alt="Google Poly"
                            />
                          )}

                      <span className={classes.buttonText}>
                        {content.content_name}
                      </span>
                    </button>
                  );
                })}
            </div>
          </div>
          <div className={classes.contentSection}>
            <div
              className={`white-text blue accent-2 z-depth-4 ${classes.divider}`}
            >
              <p>Other</p>
            </div>
            <div className={`${classes.arTypeContainer}`}>
              {content_type
                .filter(c => c.category === 'other')
                .map(content => {
                  return (
                    <button
                      id={content.id}
                      className={`btn-floating btn-large indigo waves-effect waves-light z-depth-3  ${classes.ContentTypeBtn}`}
                      key={content.id}
                      onClick={this.handleSelectContent}
                      disabled={content.disabled}
                    >
                      {content.content_name === 'unity' ? (
                        <img
                          style={{ margin: '0 35px 0', marginLeft: '20px' }}
                          src={unityIcon}
                          className={classes.iconImage}
                          alt="Unity AR Type"
                        />
                      ) : content.content_name === 'Face AR' ? (
                        <img
                          style={{ margin: '0 35px 0' }}
                          src={faceArIcon}
                          className={classes.iconImage}
                          alt="Face Ar"
                        />
                      ) : (
                            <i
                              className={`${content.content_icon} ${classes.icon}`}
                            />
                          )}
                      <span className={classes.buttonText}>
                        {content.content_name}
                      </span>
                    </button>
                  );
                })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ChooseContentType;
