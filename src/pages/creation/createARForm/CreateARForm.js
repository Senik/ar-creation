import React, { PureComponent, Fragment } from "react";
import { connect } from "react-redux";
import axios from "axios";
//selectors
import {
  selectWrong,
  selectOpenPopup,
  selectUserLimitations
} from "../../../redux/post/post.selectors";
//actions
import {
  createPost,
  somethingWrong,
  postCreationPopup
} from "../../../redux/post/post.actions";
//components
import CreatARIntro from "./CreateARIntro";
import ChooseContentType from "./ChooseContentType";
import ArNameDescription from "./ArNameDescription";
import UploadContent from "./UploadContent";
import UploadTargetImage from "./UploadTargetImage";
import PostPreview from "./PostPreview";
import FullPageLoader from "../../../components/shared/FullPageLoader";
import Choose3DType from "./Choose3DType";
import InAppModal from "../../../components/modals/arPost/InAppModal";
//helpers
import { generateUUID } from "../../../helpers";

const initialState = {
  allowDownload: true,
  castingShadow: true,
  content_type_select: null,
  isTransparent: false,
  postName: null,
  postPrivacy: "Public",
  proximityActivation: false,
  proximityActivationSelectOption: "",
  selectedFile: null,
  showLoader: false,
  soundReactivity: false,
  soundReactivityFile: null,
  step: 1,
  targetImage: "",
  trigger_position: "laying",
  model_type: null,
  isPaid: "False",
  model_price: null,
  link: null,
  tags: [],
  tagsError: false
};
class CreateARForm extends PureComponent {
  state = {
    ...initialState
  };

  componentDidMount() {
    process.env.NODE_ENV === "development"
      ? axios
          .get("http://ip-api.com/json")
          .then(res => {
            const postLocation = `${res.data.lat}, ${res.data.lon}`;
            this.setState({
              postLocation
            });
          })
          .catch(err => console.log(err))
      : axios
          .get("https://ipinfo.io/json")
          .then(res => {
            const postLocation = res.data.loc;
            this.setState({
              postLocation
            });
          })
          .catch(err => console.log(err));
  }
  handleSelectChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };

  //Proceed to next step
  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1
    });
  };
  // Go back to prev step
  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1
    });
  };
  //handle fields change
  handleChange = e => {
    const { id, value, name } = e.target;
    if (id) {
      if (id === "model_price") {
        this.setState({
          [id]: +value
        });
        return;
      }
      this.setState({
        [id]: value
      });
    } else {
      this.setState({
        [name]: value
      });
    }
  };
  ChangeElement = (value, name) => {
    this.setState({
      [name]: value
    });
  };
  handleFileUpload = file => {
    this.setState({
      selectedFile: file
    });
  };
  handleSoundUpload = file => {
    this.setState({
      soundReactivityFile: file
    });
  };
  //choose content type
  handleContentType = type => {
    const { step } = this.state;
    this.setState({
      ...initialState,
      content_type_select: type,
      step: step + 1
    });
  };
  handleCheckbox = e => {
    let option = e.target.id;
    if (option === "proximityActivation" && this.state.proximityActivation) {
      this.setState({
        proximityActivation: false,
        proximityActivationSelectOption: ""
      });
    } else if (option === "soundReactivity" && this.state.soundReactivity) {
      this.setState({
        soundReactivity: false
      });
    } else {
      this.setState({
        [option]: !this.state[option]
      });
    }
  };
  handleSubmit = e => {
    const self = this;
    const { profile, history } = self.props;
    const {
      link,
      castingShadow,
      content_type_select,
      model_type,
      model_price,
      cutOutColor,
      selectedFile,
      soundReactivityFile,
      soundReactivity,
      postPrivacy,
      postLocation,
      postName,
      postDescription,
      proximityActivation,
      proximityActivationSelectOption,
      isTransparent,
      trigger_position,
      isPaid,
      allowDownload,
      targetImage,
      tags
    } = self.state;
    e.preventDefault();
    const UID = generateUUID();
    let uniqID = `${profile.User_ID}${Date.now()}${UID}`;
    const newPost = {
      postPrivacy: postPrivacy,
      triggerPosition:
        content_type_select === "Youtube" ||
        content_type_select === "360YoutubeVideo" ||
        content_type_select === "360Video" ||
        content_type_select === "Video"
          ? "hanging"
          : trigger_position,
      castingShadow: castingShadow,
      proximityActivation: proximityActivation,
      postLocation: postLocation,
      postAuthor: profile.User_FullName,
      postAuthorID: profile.User_ID,
      postAuthorNickName: profile.User_NickName,
      postName: postName,
      postText: postDescription || "",
      arType: isTransparent
        ? content_type_select === "Youtube"
          ? `TransParent${content_type_select}Video`
          : `TransParent${content_type_select}`
        : content_type_select,
      allowDownload: allowDownload ? "True" : "False",
      isTransparent: isTransparent ? "True" : "False",
      postTargetImage: targetImage,
      arTargetName: uniqID,
      postID: uniqID,
      postStatus:
        profile.User_Type === "Prime" && content_type_select !== ""
          ? "TestMode"
          : "InReview",
      soundReactivity
    };
    newPost.isPaid = isPaid === "True" ? true : false;
    if (newPost.isPaid) {
      newPost.model_price = model_price;
    }
    if (proximityActivation) {
      newPost.interactionType = proximityActivationSelectOption;
    }
    if (soundReactivityFile) {
      newPost.audioFile = soundReactivityFile;
    }
    if (
      newPost.arType === "sketchfab" ||
      newPost.arType === "googlepoly" ||
      newPost.arType === "zipFile" ||
      newPost.arType === "unityPackage"
    ) {
      newPost.model_type = model_type;
    }
    if (isTransparent) {
      newPost.cutOutColor =
        (cutOutColor &&
          cutOutColor
            .match(/[A-Za-z0-9]{2}/g)
            .map(function(v) {
              return parseInt(v, 16);
            })
            .join(",")) ||
        "76, 175, 80";
    }
    if (
      content_type_select === "WebURL" ||
      content_type_select === "360YoutubeVideo" ||
      content_type_select === "Youtube" ||
      content_type_select === "sketchfab" ||
      content_type_select === "googlepoly"
    ) {
      newPost.arContentURL = link;
    } else if (
      content_type_select === "360Video" ||
      content_type_select === "Video" ||
      content_type_select === "zipFile" ||
      content_type_select === "unityPackage" ||
      content_type_select === "FaceAR_2DImage"
    ) {
      newPost.contentFile = selectedFile;
    }
    if (tags.length) {
      const tagString = tags.map(tag => tag["id"]).join();
      newPost.tags = tagString;
    }
    if (profile.User_IsCreator) {
      newPost.postAuthorIsCreator = profile.User_IsCreator;
    }
    self.props.createPost(newPost, history);
    self.setState({
      showLoader: true
    });
  };
  handleTargetImage = img => {
    this.setState({ targetImage: img });
  };
  handleInAppDialogClose = () => {
    this.props.somethingWrongAction();
    this.setState({ showLoader: false });
  };
  handleAddTags = tag => {
    const { tags } = this.state;
    let checkedTag = tag;
    checkedTag.text.trim();
    if (!checkedTag.text) {
      return;
    }
    if (tags.length >= 3) {
      this.setState({
        tagsError: true
      });
      return;
    }
    if (checkedTag.text[0] === "#") {
      checkedTag.id = checkedTag.text = tag.text.substr(1);
    }
    this.setState({
      tags: [...tags, checkedTag]
    });
  };
  handleDeleteTags = i => {
    const { tags } = this.state;
    const newTags = tags.filter((tag, index) => index !== i);
    this.setState({
      tags: newTags,
      tagsError: false
    });
  };
  handleTagsBlur = tag => {
    const { tags } = this.state;
    if (!tag.length || !tag.trim()) {
      return;
    }
    let checkedTag = {
      id: tag.trim(),
      text: tag.trim()
    };
    if (tags.length >= 3) {
      this.setState({
        tagsError: true
      });
      return;
    }
    if (tag[0] === "#") {
      checkedTag.id = checkedTag.text = tag.text.substr(1);
    }
    this.setState({
      tags: [...tags, checkedTag]
    });
  };
  handleCreationPopup = () => {
    const { postCreationPopup, history } = this.props;
    postCreationPopup();
    history.push("/");
  };

  render() {
    const self = this;
    const {
      step,
      showLoader,
      content_type_select,
      targetImage,
      tags,
      tagsError
    } = self.state;
    const { somethingWrong, profile, openPopup, userLimitations } = self.props;
    const content_type = [
      {
        id: "Video",
        content_name: "video",
        content_icon: "fas fa-file-video",
        category: "video"
      },
      {
        id: "360Video",
        content_name: "360 Video",
        content_icon: "fas fa-street-view",
        category: "360Video"
      },
      {
        id: "Youtube",
        content_name: "youtube video",
        content_icon: "fab fa-youtube",
        category: "video"
      },
      {
        id: "360YoutubeVideo",
        content_name: "youtube video",
        content_icon: "fas fa-blog",
        category: "360Video"
      },
      {
        id: "zipFile",
        content_name: "zip file",
        content_icon: "fas fa-file-archive",
        category: "3D"
      },
      {
        id: "sketchfab",
        content_name: "sketchfab",
        content_icon: "fab fa-sketch",
        category: "3D"
      },
      {
        id: "googlepoly",
        content_name: "google poly",
        content_icon: "fab fa-google",
        category: "3D"
      },
      {
        id: "WebURL",
        content_name: "link",
        content_icon: "fas fa-link",
        category: "other"
      },
      {
        id: "unityPackage",
        content_name: "unity",
        content_icon: "fas fa-medal",
        category: "other"
      },
      {
        id: "FaceAR_2DImage",
        content_name: "Face AR",
        content_icon: "fas fa-medal",
        category: "other",
        disabled: true
      }
    ];
    let activTime = "48 hours";
    if (profile.Subscription) {
      if (profile.Subscription.Type === "Plus") {
        activTime = "12 hours";
      } else if (profile.Subscription.Type === "Premium") {
        activTime = "1 hour";
      }
    }
    if (somethingWrong) {
      return (
        <InAppModal
          isOpen={!!somethingWrong}
          close={this.handleInAppDialogClose}
          text={somethingWrong}
        />
      );
    }
    if (openPopup) {
      return (
        <InAppModal
          isOpen={openPopup}
          close={self.handleCreationPopup}
          text="Your post has been created successfully!"
          description={`The activation may take up to ${activTime}.`}
          btnClassname
        />
      );
    }
    switch (step) {
      case 1:
        return (
          <CreatARIntro
            postsLimit={userLimitations.postsLimit}
            profile={profile}
            nextStep={self.nextStep}
          />
        );
      case 2:
        return (
          <ChooseContentType
            content_type={content_type}
            prevStep={self.prevStep}
            setContent={self.handleContentType}
          />
        );
      case 3:
        return (
          <ArNameDescription
            nextStep={self.nextStep}
            prevStep={self.prevStep}
            handleChange={self.ChangeElement}
            values={self.state}
          />
        );
      case 4:
        if (
          content_type_select === "zipFile" ||
          content_type_select === "sketchfab" ||
          content_type_select === "googlepoly" ||
          content_type_select === "unityPackage"
        ) {
          return (
            <Choose3DType
              nextStep={self.nextStep}
              prevStep={self.prevStep}
              handleChange={self.ChangeElement}
              values={self.state}
              profile={profile}
              isCreator={profile.User_IsCreator && profile.User_IsCreator}
              reachedPaidPostsLimit={userLimitations.paidPostsLimit}
            />
          );
        }
        return (
          <UploadContent
            values={self.state}
            nextStep={self.nextStep}
            prevStep={self.prevStep}
            handleExtTrackingCheckbox={self.handleCheckbox}
            handleChange={self.ChangeElement}
            handleFileUpload={self.handleFileUpload}
            params={self.state}
            isCreator={profile.User_IsCreator && profile.User_IsCreator}
          />
        );
      case 5:
        if (
          content_type_select === "zipFile" ||
          content_type_select === "sketchfab" ||
          content_type_select === "googlepoly" ||
          content_type_select === "unityPackage"
        ) {
          return (
            <UploadContent
              nextStep={self.nextStep}
              prevStep={self.prevStep}
              handleExtTrackingCheckbox={self.handleCheckbox}
              handleChange={self.ChangeElement}
              handleFileUpload={self.handleFileUpload}
              params={self.state}
            />
          );
        }
        return (
          <UploadTargetImage
            values={self.state}
            handleChange={self.ChangeElement}
            handleTargetImage={self.handleTargetImage}
            targetImage={targetImage}
            nextStep={self.nextStep}
            prevStep={self.prevStep}
          />
        );
      case 6:
        if (
          content_type_select === "zipFile" ||
          content_type_select === "sketchfab" ||
          content_type_select === "googlepoly" ||
          content_type_select === "unityPackage"
        ) {
          return (
            <UploadTargetImage
              values={self.state}
              handleTargetImage={self.handleTargetImage}
              handleChange={self.ChangeElement}
              targetImage={targetImage}
              nextStep={self.nextStep}
              prevStep={self.prevStep}
            />
          );
        }
        return (
          <Fragment>
            {showLoader ? (
              <FullPageLoader showChildren={true} content={content_type_select}>
                Creating Post
              </FullPageLoader>
            ) : null}
            <PostPreview
              handleSelectChange={self.handleSelectChange}
              handleChange={self.ChangeElement}
              handleCheckbox={self.handleCheckbox}
              submitPost={self.handleSubmit}
              postData={self.state}
              prevStep={self.prevStep}
            />
          </Fragment>
        );
      case 7:
        return (
          <Fragment>
            {showLoader ? (
              <FullPageLoader showChildren={true} content={content_type_select}>
                Creating Post
              </FullPageLoader>
            ) : null}
            <PostPreview
              handleSoundUpload={self.handleSoundUpload}
              handleSelectChange={self.handleSelectChange}
              handleChange={self.ChangeElement}
              handleCheckbox={self.handleCheckbox}
              submitPost={self.handleSubmit}
              postData={self.state}
              prevStep={self.prevStep}
              tags={tags}
              tagsError={tagsError}
              addTags={self.handleAddTags}
              deleteTags={self.handleDeleteTags}
              handleBlur={self.handleTagsBlur}
            />
          </Fragment>
        );
      default:
        return null;
    }
  }
}

const mapTostateToProps = state => {
  return {
    profile: state.firebase.profile,
    somethingWrong: selectWrong(state),
    openPopup: selectOpenPopup(state),
    userLimitations: selectUserLimitations(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    somethingWrongAction: () => dispatch(somethingWrong()),
    createPost: (newPost, history) => dispatch(createPost(newPost, history)),
    postCreationPopup: () => dispatch(postCreationPopup())
  };
};

export default connect(mapTostateToProps, mapDispatchToProps)(CreateARForm);
