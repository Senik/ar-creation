import React, { memo } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

//helpers
import { writeToLocalStorage } from "../../../helpers";

const CreateARIntro = props => {
  const { postsLimit, nextStep, profile } = props;
  const handleUpgradeLinkClick = e => {
    writeToLocalStorage("requestPackage", "true");
  };
  const renderUserLimitWarning = () => {
    if (profile.Subscription) {
      return (
        <p>
          You have reached the maximum number of post included in your plan.
          <br />
          You can{" "}
          <Link to="/profile" onClick={handleUpgradeLinkClick}>
            Upgrade
          </Link>{" "}
          your plan in order to continue, or wait till the next month.
        </p>
      );
    } else {
      return (
        <p>
          You have reached the maximum number of post included in the Free plan.
          <br />
          Please{" "}
          <Link to="/profile" onClick={handleUpgradeLinkClick}>
            Upgrade
          </Link>{" "}
          your plan in order to continue.
        </p>
      );
    }
  };

  return !postsLimit ? (
    <div className="center-align">
      <div>
        <h3>Welcome to "AR Studio"</h3>
        <p>Here we help you to Create your own Augmented Reality Experience.</p>
        <p>You are just a few steps away from your AR post</p>
        <br />
        <p>--ARize team--</p>
      </div>
      <div>
        <button
          className="btn-large waves-effect waves-light z-depth-3 deep-purple"
          onClick={() => nextStep()}
        >
          Tap to start
        </button>
      </div>
    </div>
  ) : (
    <div className="center-align">
      <div>
        <h3>Welcome to "AR Studio"</h3>
        <div className="red-text center">
          <p>{renderUserLimitWarning()}</p>
        </div>

        <br />
        <p>--ARize team--</p>
      </div>
    </div>
  );
};

CreateARIntro.propTypes = {
  nextStep: PropTypes.func.isRequired
};

function arePropsEqual(prevProps, nextProps) {
  return prevProps.postsLimit === nextProps.postsLimit;
}

export default memo(CreateARIntro, arePropsEqual);
