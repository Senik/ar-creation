import React, { useState, memo, useEffect } from "react";
import ReactPLayer from "react-player";
import DropZone from "react-dropzone";
import CheckElement from "../../../components/formElements/Checkbox";
//MUI
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
//components
import SketchfabViewerIframe from "../../../components/shared/SketchfabViewer";
import GooglePolyIframe from "../../../components/shared/GooglePolyIframe";
import InputElement from "../../../components/formElements/InputElement";
import PostTags from "../../../components/postTags/PostTags";
//styles
import classes from "../Creation.module.scss";

const PostPreview = props => {
  const [error, setError] = useState();
  const [isDisabled, setIsDisabled] = useState(true);
  const {
    prevStep,
    postData,
    submitPost,
    handleChange,
    handleCheckbox,
    handleSoundUpload,
    handleSelectChange,
    addTags,
    deleteTags,
    tags,
    tagsError,
    handleBlur
  } = props;
  const {
    link,
    postName,
    targetImage,
    model_price,
    selectedFile,
    postDescription,
    content_type_select,
    soundReactivity,
    soundReactivityFile,
    proximityActivation,
    proximityActivationSelectOption
  } = postData;

  useEffect(() => {
    if (
      !postName ||
      ((postData.model_type === "xrGallery" ||
        postData.model_type === "both") &&
        tags &&
        !tags.length) ||
      (proximityActivation && !proximityActivationSelectOption) ||
      (!proximityActivation && proximityActivationSelectOption) ||
      (soundReactivity && !soundReactivityFile)
    ) {
      setIsDisabled(true);
    } else {
      setIsDisabled(false);
    }
  }, [
    postName,
    proximityActivation,
    proximityActivationSelectOption,
    soundReactivity,
    soundReactivityFile,
    tags
  ]);
  const handleErrors = () => {
    setError("Sorry we accept only .mp3 format");
  };

  return (
    <div className={`container ${classes.PostPreview}`}>
      <div className="row">
        <div className="col s6 m2 ">
          <button className="btn-small grey" onClick={prevStep}>
            <i className="fas fa-arrow-left" />
          </button>
        </div>

        <div className="col s6 m3 push-m7">
          <button
            style={{ minWidth: 130 }}
            className={`btn deep-purple lighten-1 z-depth-0 ${classes.postCreateBtn}`}
            onClick={submitPost}
            disabled={isDisabled}
          >
            Create Post
          </button>
        </div>
        <div className="col s12 m7 pull-m3 center-align">
          <p className={classes.stepHeader}>Your post review</p>
        </div>
      </div>
      {(postData.model_type === "xrGallery" ||
        postData.model_type === "both") &&
      tags &&
      !tags.length ? (
        <div className="row center-align post-preview">
          <p className={classes.tagLabel}>Please add tags to your post!</p>
        </div>
      ) : null}
      <div className="row center-align post-preview" style={{ marginTop: 75 }}>
        <InputElement
          name="postName"
          defaultValue={postName}
          label="Post Name"
          required={true}
          handleChange={handleChange}
        />
        <InputElement
          name="postDescription"
          defaultValue={postDescription}
          label="Post Description"
          type="textarea"
          required={false}
          handleChange={handleChange}
        />

        <div className="input-field">
          <p className={classes.linkLabel}>
            Post type:{" "}
            <span className={classes.postType}>{content_type_select}</span>
          </p>
        </div>
        {model_price ? (
          <div className="input-field">
            <p className={classes.linkLabel}>Price: {model_price}&euro;</p>
          </div>
        ) : null}
        {content_type_select === "Youtube" ||
        content_type_select === "TransParentYoutube" ||
        content_type_select === "360YoutubeVideo" ||
        content_type_select === "WebURL" ? (
          <div className="input-field">
            <p className={classes.linkLabel}>
              Post Content {content_type_select}:{"   "}
              <a href={link} target="_blank" rel="noopener noreferrer">
                {link}
              </a>
            </p>

            {(content_type_select === "Youtube" ||
              content_type_select === "360YoutubeVideo" ||
              content_type_select === "TransParentYoutube") && (
              <ReactPLayer
                width={"100%"}
                url={link}
                playing
                config={{
                  youtube: {
                    playerVars: { showinfo: 1 }
                  }
                }}
              />
            )}
          </div>
        ) : content_type_select === "Video" ||
          content_type_select === "360Video" ? (
          <div className="input-field">
            <p>Post content: {selectedFile[0].name}</p>
            {(content_type_select === "Video" ||
              content_type_select === "360Video") && (
              <video controls preload="metadata" className="responsive-video">
                <source
                  src={URL.createObjectURL(selectedFile[0])}
                  type="video/mp4"
                />
                Video not supported.
              </video>
            )}
          </div>
        ) : null}
        {content_type_select === "FaceAR_2DImage" && (
          <div className="col m12">
            <p>Face ar</p>
            <img
              src={URL.createObjectURL(selectedFile[0])}
              style={{ maxWidth: "100%", maxHeight: "350px" }}
              alt="Uploaded target"
            />
          </div>
        )}
        {content_type_select === "sketchfab" ? (
          <SketchfabViewerIframe link={link} />
        ) : null}
        {content_type_select === "googlepoly" ? (
          <GooglePolyIframe link={link} />
        ) : null}
        {content_type_select === "zipFile" ||
        content_type_select === "unityPackage" ? (
          <div>
            <p>
              <i className="fas fa-file-archive ChooseContetnType-icon-218 fa-4x" />
            </p>
            <p className={classes.uploadedFileName}>
              <span>{selectedFile[0].name}</span>
            </p>
          </div>
        ) : null}
        <CheckElement
          title="Post Availability"
          name="postPrivacy"
          defaultValue={postData.postPrivacy}
          required={true}
          handleChange={handleChange}
          options={[
            {
              label: "Public",
              value: "Public"
            },
            {
              label: "Private",
              value: "PrivateToMe"
            },
            {
              label: "Unlisted",
              value: "Unlisted"
            }
          ]}
        />
        {postData.model_type === "xrGallery" ||
        postData.model_type === "both" ? (
          <PostTags
            tags={tags}
            tagsError={tagsError}
            addTags={addTags}
            deleteTags={deleteTags}
            handleBlur={handleBlur}
          />
        ) : null}
        <div className="col s12 center-align">
          <FormControlLabel
            control={
              <Checkbox
                checked={postData.allowDownload}
                onChange={handleCheckbox}
                id="allowDownload"
              />
            }
            label="Allow Download"
          />
        </div>
        {(content_type_select === "unityPackage" ||
          content_type_select === "zipFile" ||
          content_type_select === "sketchfab" ||
          content_type_select === "googlepoly") && (
          <div className="col m12">
            <p>Additional effects</p>
            <div className={classes.effects}>
              <div className="col s12">
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={postData.castingShadow}
                      onChange={handleCheckbox}
                      id="castingShadow"
                    />
                  }
                  label="Casting Shadow"
                />
              </div>
              <div className="col s12">
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={proximityActivation}
                      onChange={handleCheckbox}
                      id="proximityActivation"
                    />
                  }
                  label="Proximity Activation"
                />
                {proximityActivation && (
                  <div className="col s12">
                    <FormControl style={{ width: "40%" }}>
                      <InputLabel>Select option</InputLabel>
                      <Select
                        value={postData.proximityActivationSelectOption}
                        onChange={handleSelectChange}
                        inputProps={{
                          name: "proximityActivationSelectOption",
                          id: "proximityActivationSelectOption"
                        }}
                      >
                        <MenuItem value={"ToggleVisability"}>
                          Visible/Invisible
                        </MenuItem>
                        <MenuItem value={"Bouncing"}>Bouncing</MenuItem>
                        <MenuItem value={"Spinning"}>Spinning</MenuItem>
                        <MenuItem value={"ToggleAnimation"}>
                          Playing/Stopping Animation
                        </MenuItem>
                      </Select>
                    </FormControl>
                  </div>
                )}
              </div>
              {(content_type_select === "googlepoly" ||
                content_type_select === "sketchfab") && (
                <div className="col s12">
                  {content_type_select === "googlepoly" && (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={soundReactivity}
                          onChange={handleCheckbox}
                          id="soundReactivity"
                        />
                      }
                      label="Sound Reactivity"
                    />
                  )}

                  <div>
                    <DropZone
                      className={classes.dropzone}
                      onDropAccepted={handleSoundUpload}
                      onDropRejected={handleErrors}
                      multiple={false}
                      accept={"audio/mp3, audio/mpeg"}
                      id="sound"
                    >
                      <button
                        className={`btn waves-effect waves-light z-depth-3 light-green darken-1 ${classes.uploadMoreButton}`}
                      >
                        Upload MP3
                      </button>
                      <p>
                        {soundReactivityFile &&
                          postData.soundReactivityFile[0].name}
                      </p>
                    </DropZone>
                    <div className="red-text center">
                      {error ? <p>{error}</p> : null}
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
        {targetImage && (
          <div className="col m12">
            <p>Post Target Image</p>
            <img
              src={targetImage}
              style={{ maxWidth: "100%", maxHeight: "350px" }}
              alt="Uploaded target"
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default memo(PostPreview);
