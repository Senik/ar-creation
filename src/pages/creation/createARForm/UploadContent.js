import React, { useState, Fragment, memo } from "react";
import DropZone from "react-dropzone";
//actions
import Checkbox from "@material-ui/core/Checkbox";
//helper
import { validURL } from "../../../helpers";
//MUI
import FormControlLabel from "@material-ui/core/FormControlLabel";
//components
import GooglePolyIframe from "../../../components/shared/GooglePolyIframe";
import SketchfabViewerIframe from "../../../components/shared/SketchfabViewer";
import ReactPLayer from "react-player";
import CheckElement from "../../../components/formElements/Checkbox";
import PriceElement from "../../../components/formElements/Price";
import InputElement from "../../../components/formElements/InputElement";
import ColorPicker from "../../../components/formElements/ColorPicker";

//styles
import classes from "../Creation.module.scss";

const UploadContent = props => {
  const {
    params,
    prevStep,
    nextStep,
    handleChange,
    handleFileUpload,
    handleExtTrackingCheckbox,
    values,
    isCreator
  } = props;
  const {
    content_type_select,
    link,
    selectedFile,
    isTransparent,
    cutOutColor
  } = params;
  const [postNameError, setError] = useState();
  const handleNext = e => {
    e.preventDefault();
    if (
      content_type_select === "Youtube" ||
      content_type_select === "360YoutubeVideo" ||
      content_type_select === "WebURL" ||
      content_type_select === "sketchfab" ||
      content_type_select === "googlepoly"
    ) {
      if (!link) {
        setError("The link field is required");
        return;
      }
      if (!validURL(link)) {
        setError("Please insert valid url");
        return;
      }
    }
    if (
      !selectedFile &&
      (content_type_select === "Video" ||
        content_type_select === "360Video" ||
        content_type_select === "zipFile" ||
        content_type_select === "unityPackage" ||
        content_type_select === "FaceAR_2DImage")
    ) {
      setError("The file is required");
      return;
    }
    if (
      content_type_select === "FaceAR_2DImage" &&
      !values.isPaid &&
      isCreator === "True"
    ) {
      setError("Sorry price is required");
      return;
    }
    if (content_type_select === "sketchfab") {
      const index = link.lastIndexOf("/");
      let id = link.slice(index + 1);
      if (!link.includes("sketchfab.com/3d-models/") || !id.length) {
        setError("Please insert valid url");
        return;
      }
    } else if (
      content_type_select === "Youtube" ||
      content_type_select === "360YoutubeVideo"
    ) {
      if (link.includes("youtube.com")) {
        const index = link.lastIndexOf("v=");
        let id = link.slice(index + 2);
        if (!link.includes("/watch?v=") || !id.length) {
          setError("Please insert valid url");
          return;
        }
      } else if (link.includes("youtu.be")) {
        const index = link.lastIndexOf("e/");
        let id = link.slice(index + 2);
        if (!link.includes("youtu.be/") || !id.length) {
          setError("Please insert valid url");
          return;
        }
      } else {
        setError("Please insert valid url");
        return;
      }
    } else if (content_type_select === "googlepoly") {
      const index = link.lastIndexOf("/");
      let id = link.slice(index + 1);
      if (!link.includes("poly.google.com/view/") || !id.length) {
        setError("Please insert valid url");
        return;
      }
    }
    nextStep();
  };
  const handleDropAccept = file => {
    setError(null);
    return handleFileUpload(file);
  };
  const handleErrors = () => {
    if (content_type_select === "Video") {
      setError("Sorry we accept only .mp4 format");
    } else if (content_type_select === "zipFile") {
      setError("Sorry we accept only .zip format");
    } else if (content_type_select === "360Video") {
      setError("Sorry we accept only .mp4 format");
    } else if (content_type_select === "FaceAR_2DImage") {
      setError("Sorry we accept only .jpg format");
    } else if (content_type_select === "unityPackage") {
      setError("Sorry we accept only .unitypackage format");
    }
  };
  let content = "";
  switch (content_type_select) {
    case "Video":
    case "360Video":
    case "zipFile":
    case "unityPackage":
    case "FaceAR_2DImage":
      content = (
        <div className="row">
          <div className={`form-control ${classes.dropzoneContainer}`}>
            {selectedFile && (
              <div>
                <p>
                  <i className="fas fa-file-archive ChooseContetnType-icon-218 fa-4x" />
                </p>
                <p className={classes.uploadedFileName}>
                  <span>{selectedFile[0].name}</span>
                </p>
              </div>
            )}
            <DropZone
              className={selectedFile ? classes.hideDropzone : ""}
              onDropAccepted={handleDropAccept}
              onDropRejected={handleErrors}
              multiple={false}
              accept={
                content_type_select === "Video" ||
                content_type_select === "360Video"
                  ? "video/mp4"
                  : content_type_select === "zipFile"
                  ? ".zip"
                  : content_type_select === "unityPackage"
                  ? ".unitypackage"
                  : content_type_select === "FaceAR_2DImage"
                  ? "image/jpeg, image/png"
                  : ""
              }
            >
              {selectedFile ? (
                <button
                  className={`btn waves-effect waves-light z-depth-3 light-green darken-1 ${classes.uploadMoreButton}`}
                >
                  Upload again
                </button>
              ) : (
                <p>Drag and drop or click to upload files</p>
              )}
            </DropZone>

            {content_type_select === "Video" && (
              <Fragment>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={isTransparent}
                      onChange={handleExtTrackingCheckbox}
                      id="isTransparent"
                    />
                  }
                  label="This is a chromakey video"
                />
                {isTransparent ? (
                  <ColorPicker
                    name="cutOutColor"
                    defaultValue={cutOutColor}
                    label="Pick up transparent color"
                    required={false}
                    handleChange={handleChange}
                  />
                ) : null}
              </Fragment>
            )}
            {content_type_select === "FaceAR_2DImage" && isCreator === "True" && (
              <Fragment>
                <CheckElement
                  title="Model Price"
                  name="isPaid"
                  defaultValue={values.isPaid}
                  required={true}
                  handleChange={handleChange}
                  options={[
                    {
                      label: "Free",
                      value: "False"
                    },
                    {
                      label: "Paid",
                      value: "True"
                    }
                  ]}
                />

                {values.isPaid && values.isPaid === "True" ? (
                  <PriceElement
                    price={values.model_price}
                    handleChange={handleChange}
                  />
                ) : null}
              </Fragment>
            )}
          </div>
        </div>
      );
      break;
    case "sketchfab":
    case "googlepoly":
      const contentName =
        content_type_select === "googlepoly" ? "Google Poly" : "Sketchfab";
      content = (
        <div className="row">
          <p>Please insert a link of a {contentName} model</p>
          <InputElement
            name="link"
            defaultValue={link}
            label={`${contentName} model link`}
            required={false}
            handleChange={handleChange}
          />
          {link &&
            (content_type_select === "googlepoly" ? (
              <GooglePolyIframe link={link} linkCheck={true} />
            ) : (
              <SketchfabViewerIframe link={link} linkCheck={true} />
            ))}
        </div>
      );
      break;
    case "Youtube":
    case "360YoutubeVideo":
      content = (
        <div className="row">
          <p>Please insert a link of a youtube video</p>
          <InputElement
            name="link"
            defaultValue={link}
            label="Youtbe link"
            required={false}
            handleChange={handleChange}
          />
          {link && (
            <ReactPLayer
              width={"100%"}
              url={link}
              playing
              config={{
                youtube: {
                  playerVars: { showinfo: 1 }
                }
              }}
            />
          )}

          {content_type_select === "Youtube" && (
            <FormControlLabel
              control={
                <Checkbox
                  checked={isTransparent}
                  onChange={handleExtTrackingCheckbox}
                  id="isTransparent"
                />
              }
              label="This is a chromakey video"
            />
          )}

          {isTransparent ? (
            <ColorPicker
              name="cutOutColor"
              defaultValue={cutOutColor}
              label="Pick up transparent color"
              required={false}
              handleChange={handleChange}
            />
          ) : null}
          {content_type_select === "Youtube" && (
            <div>
              <p>
                * Check the box above if your video has a pure color background.
              </p>
              <p>It will be automatically cut out while streaming.</p>
            </div>
          )}
        </div>
      );
      break;
    case "WebURL":
      content = (
        <div className="row">
          <InputElement
            name="link"
            defaultValue={link}
            label="Put your link"
            required={false}
            handleChange={handleChange}
          />
        </div>
      );
      break;
    default:
      content = null;
  }
  return (
    <div className={`container ${classes.UploadContent}`}>
      <div className="row">
        <div className="col s12 m3">
          <button className="btn-small grey" onClick={prevStep}>
            <i className="fas fa-arrow-left" />
          </button>
        </div>
        <div className="col s12 m7 center-align">
          <p className={classes.stepHeader}>
            Upload your {content_type_select} file
          </p>
        </div>
      </div>
      <div className="row center-align" style={{ marginTop: 75 }}>
        {content}
        <div className="input-field">
          <button
            className="btn deep-purple lighten-1 z-depth-0"
            onClick={handleNext}
          >
            Next
          </button>
          <div className="red-text center">
            {postNameError ? <p>{postNameError}</p> : null}
          </div>
        </div>
      </div>
    </div>
  );
};

export default memo(UploadContent);
