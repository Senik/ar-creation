import React, { PureComponent } from 'react';
import Dropzone from 'react-dropzone';
import imageCompression from 'browser-image-compression';
//helpers
import { dataURItoBlob } from '../../../helpers';
//components
import EditImageModal from '../../../components/modals/editImage/EditImageModal';
import CheckElement from '../../../components/formElements/Checkbox';
//icons
import SPINNER from '../../../assets/img/spinners/index.gooey-ring-spinner.svg';
//styles
import classes from '../Creation.module.scss';

class UploadTargetImage extends PureComponent {
  state = {
    imgSrc: null,
    error: '',
    imageModal: false,
    showLoader: false,
    maxSize: 2242880000
  };
  handleNext = e => {
    e.preventDefault();
    const { targetImage, nextStep } = this.props;
    if (!targetImage) {
      this.setState({
        error: 'The target image is required!!!'
      });
      return;
    }
    nextStep();
  };

  handleFileUploadError = files => {
    console.log(files);
    const currentFile = files[0];

    if (
      currentFile.type !== 'image/jpeg' ||
      currentFile.type !== 'image/jpeg'
    ) {
      this.setState({
        error: 'Sorry we accept only .jpg and .png image types'
      });
    } else if (currentFile.size > 2242880) {
      this.setState({
        error: 'Sorry the image size must not be greater than 2 mb'
      });
    }
  };
  openModal = () => {
    this.setState({
      imageModal: true
    });
  };
  closeModal = () => {
    this.setState({
      imageModal: false
    });
  };
  handleLoader = () => {
    this.setState({
      showLoader: true
    });
  };
  handleOnDrop = files => {
    this.setState({
      error: ''
    });
    const currentFile = files[0];
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      async () => {
        const options = {
          maxSizeMB: 2,
          maxWidthOrHeight: 1920,
          useWebWorker: true
        };

        try {
          const compressedFile = await imageCompression(
            dataURItoBlob(reader.result),
            options
          );
          if (compressedFile.size > 2242880) {
            this.setState({
              error: 'Sorry the image size must not be greater than 2 mb'
            });
            return null;
          }
          const uri = await imageCompression.getDataUrlFromFile(compressedFile);
          this.setState({
            imgSrc: uri,
            showLoader: false
          });
          this.openModal();
        } catch (err) {
          console.log(err);
        }
      },
      false
    );
    reader.readAsDataURL(currentFile);
  };

  render() {
    const self = this;
    const {
      values,
      prevStep,
      targetImage,
      handleChange,
      handleTargetImage
    } = self.props;
    const { imageModal, imgSrc, error, maxSize, showLoader } = self.state;

    return showLoader ? (
      <div
        className='dashboard-loader'
        style={{ position: 'relative', top: '20vh' }}
      >
        <img src={SPINNER} alt='spinner' style={{ width: '190px' }} />
      </div>
    ) : (
      <div className={`container ${classes.UploadTargetImage}`}>
        <div className='row'>
          <div className='col s12 m3'>
            <button className='btn-small grey' onClick={prevStep}>
              <i className='fas fa-arrow-left' />
            </button>
          </div>
          <div className='col s12 m7 center-align'>
            <p className={classes.stepHeader}>
              Upload your{' '}
              {values &&
              (values.model_type === 'xrGallery' ||
                values.content_type_select === 'FaceAR_2DImage')
                ? 'thumbnail'
                : 'target image'}
            </p>
          </div>
        </div>
        <div className='row center-align' style={{ marginTop: 75 }}>
          <div className={`form-control ${classes.dropzoneContainer}`}>
            {targetImage && (
              <img
                src={targetImage}
                width='400'
                alt='Target'
                style={{ maxWidth: '100%' }}
              />
            )}
            <Dropzone
              className={targetImage ? classes.hideDropzone : ''}
              onDropAccepted={self.handleOnDrop}
              onDrop={self.handleLoader}
              accept='image/jpeg, image/png'
              maxSize={maxSize}
              onDropRejected={self.handleFileUploadError}
              multiple={false}
            >
              {targetImage ? (
                <button
                  className={`btn waves-effect waves-light z-depth-3 light-green darken-1 ${classes.uploadMoreButton}`}
                >
                  Upload again
                </button>
              ) : (
                <p>Drop files here or click</p>
              )}
            </Dropzone>
            {!(
              values.content_type_select === 'WebURL' ||
              values.content_type_select === 'FaceAR_2DImage' ||
              values.model_type === 'xrGallery' ||
              values.content_type_select === '360YoutubeVideo' ||
              values.content_type_select === 'Youtube' ||
              values.content_type_select === '360Video' ||
              values.content_type_select === 'Video'
            ) && (
              <div className='input-field align-center'>
                <CheckElement
                  title='AR Trigger Position'
                  name='trigger_position'
                  defaultValue={values.trigger_position}
                  required={true}
                  handleChange={handleChange}
                  options={[
                    {
                      label: 'Hanging',
                      value: 'hanging'
                    },
                    {
                      label: 'Laying',
                      value: 'laying'
                    }
                  ]}
                />
              </div>
            )}

            <EditImageModal
              openModal={imageModal}
              handleClose={self.closeModal}
              src={imgSrc || targetImage}
              makeClientCrop={handleTargetImage}
              fixCropSize={values.content_type_select === 'FaceAR_2DImage'}
            />
          </div>
          <div className='input-field'>
            <button
              className='btn deep-purple waves-effect waves-light lighten-1 z-depth-3'
              onClick={self.handleNext}
            >
              Next
            </button>
            <div className='red-text center'>
              {error ? <p>{error}</p> : null}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UploadTargetImage;
