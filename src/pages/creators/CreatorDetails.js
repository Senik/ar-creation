import React, { Component } from "react";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { ClipLoader } from "react-spinners";
//helpers
import { followUser, unFollowUser } from "../../helpers";
import { filterByKeyword, filterByArr } from "../../helpers";
//actions
import {
  setSearchLength,
  clearKeyword,
  clearSearchLength
} from "../../redux/post/post.actions";
//selectors
import { selectProfile } from "../../redux/auth/auth.selectors";
import { selectUserById } from "../../redux/user/user.selectors";
import {
  selectUsersPostsById,
  selectKeyword
} from "../../redux/post/post.selectors";
//components
import SignInModal from "../../components/modals/arPost/SignInModal";
import UserCard from "../../components/user-card/user-card.component";
import CreatorPostList from "./CreatorPostList";
import ContactCreator from "../../components/modals/contactCreator";
import PostLoader from "../../components/shared/PostLoader";
import PostNotFound from "../../components/shared/PostNotFound";
//styles
import classes from "./Creators.module.scss";
class CreatorDetails extends Component {
  state = {
    prevPostIndex: 0,
    nextPostIndex: 9,
    postsToShow: null,
    openModal: false
  };

  componentDidMount() {
    document.title = "Creators";
  }

  componentWillUnmount() {
    const { clearKeyword, clearSearchLength } = this.props;
    clearKeyword();
    clearSearchLength();
  }
  UNSAFE_componentWillUpdate(nextProps) {
    if (nextProps.keyword !== this.props.keyword) {
      this.filterPost(nextProps);
    }
  }

  filterPost(props) {
    const { userPosts, keyword, setSearchLength } = props;
    let searchPosts;
    if (keyword && keyword[2] && userPosts) {
      searchPosts = filterByKeyword(
        userPosts,
        ["PostName", "PostText"],
        keyword
      );
      searchPosts = filterByArr(searchPosts, [
        { key: "PostPrivacy", value: "PrivateToMe" },
        { key: "PostPrivacy", value: "Unlisted" },
        { key: "PostDeleteStatus", value: "deleted" },
        { key: "PostStatus", value: "InReview" },
        { key: "PostStatus", value: "Rejected" }
      ]);
      this.setState({ postsToShow: searchPosts });
      setSearchLength(searchPosts.length);
    }
  }
  fetchMoreData = () => {
    let test = this.state.nextPostIndex + 9;
    this.setState({
      nextPostIndex: test
    });
  };
  handleFollow = () => {
    const { user, profile } = this.props;
    if (!profile.isEmpty) {
      if (!!profile.Following && user.User_ID in profile.Following) {
        unFollowUser(profile, user);
      } else {
        followUser(profile, user);
      }
    } else {
      this.setState({
        openModal: true
      });
    }
  };
  handleClickClose = () => {
    this.setState({
      openModal: false
    });
  };
  render() {
    const { user, profile, userPosts, keyword } = this.props;
    const { prevPostIndex, nextPostIndex, postsToShow, openModal } = this.state;
    const userPostsToShow = keyword[0] ? postsToShow : userPosts;
    if (user === null) {
      return <PostLoader />;
    } else if (!user) {
      return <PostNotFound message="Creator was not found" />;
    } else {
      return (
        <div className={`col m12 ${classes.CreatorDetails}`}>
          <div className="row">
            <div className={`col m12 l2 ${classes.creatorCardContainer}`}>
              <UserCard
                showFollowButton={user.User_ID !== profile.User_ID}
                user={user}
                profile={profile}
                socialLinks={user.User_SocialLinks}
                bio={user.User_Bio}
                followUnfollow={this.handleFollow}
              />
              <div
                className="center-align"
                style={{ position: "relative", marginBottom: 100 }}
              >
                <ContactCreator creator={user} currUser_profile={profile} />
              </div>
            </div>
            {openModal && (
              <SignInModal isOpen={openModal} close={this.handleClickClose} />
            )}
            <div>
              <h4 className={classes.postsHeading}>Artworks</h4>
              {userPostsToShow ? (
                <InfiniteScroll
                  dataLength={
                    userPostsToShow.slice(prevPostIndex, nextPostIndex).length
                  }
                  next={this.fetchMoreData}
                  hasMore={nextPostIndex < userPostsToShow.length}
                  loader={
                    <ClipLoader sizeUnit={"px"} size={150} color={"#123abc"} />
                  }
                  style={{ overflow: "hidden" }}
                  endMessage={
                    <p style={{ textAlign: "center" }}>
                      <b style={{ color: "black" }}>
                        Yay! You have seen it all
                      </b>
                    </p>
                  }
                >
                  <CreatorPostList
                    posts={userPostsToShow.slice(prevPostIndex, nextPostIndex)}
                    user={user}
                    profile={profile}
                    fetchMoreData={this.fetchMoreData}
                    end={nextPostIndex < userPostsToShow.length}
                  />
                </InfiniteScroll>
              ) : (
                  <p style={{ textAlign: "center" }}>
                    <b style={{ color: "black" }}>Yay! You have seen it all</b>
                  </p>
                )}
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;

  return {
    user: selectUserById(id)(state), //user creator
    profile: selectProfile(state), // logged in user
    userPosts: selectUsersPostsById(id)(state),
    keyword: selectKeyword(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setSearchLength: searchLength => dispatch(setSearchLength(searchLength)),
    clearSearchLength: () => dispatch(clearSearchLength()),
    clearKeyword: () => dispatch(clearKeyword())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreatorDetails);
