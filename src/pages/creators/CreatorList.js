import React, { memo } from 'react';
//components
import PostsMasonry from '../../components/shared/PostsMasonry';
import UserCard from '../../components/user-card/user-card.component';

const CreatorList = ({ posts }) => {
  let creators =
    posts &&
    posts.map(post => {
      return post.value ? (
        <UserCard
          user={post.value}
          key={post.value.User_ID}
          linkTo="/creator/"
        />
      ) : (
          <UserCard user={post} key={post.User_ID} linkTo="/creator/" />
        );
    });
  return <PostsMasonry>{creators.length > 0 && creators}</PostsMasonry>;
};

export default memo(CreatorList);
