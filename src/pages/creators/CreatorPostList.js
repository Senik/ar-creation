import React, { memo } from "react";
//components
import CreatorPostSummary from "./CreatorPostSummary";
import PostsMasonry from "../../components/shared/PostsMasonry";

const CreatorPostList = ({ posts, user, profile, fetchMoreData, end }) => {
  let creatorPosts =
    posts &&
    posts.filter(
      post =>
        post.PostDeleteStatus !== "deleted" &&
        (post.PostPrivacy === "Public" || !post.PostPrivacy) &&
        post.PostStatus === "TestMode"
    );

  if (creatorPosts.length < 3 && end) {
    fetchMoreData();
  } else {
    creatorPosts = creatorPosts.map(post => {
      return (
        <CreatorPostSummary
          post={post}
          key={post.PostID}
          user={user}
          profile={profile}
        />
      );
    });
    return (
      <PostsMasonry>{creatorPosts.length > 0 && creatorPosts}</PostsMasonry>
    );
  }
};

export default memo(CreatorPostList);
