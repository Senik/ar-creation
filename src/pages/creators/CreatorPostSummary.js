import React from "react";
import { isEmpty } from "react-redux-firebase";
import Linkify from "react-linkify";
import moment from "moment";
import { connect } from "react-redux";
//actions
import { alertNotification } from "../../redux/auth/auth.actions";
//helpers
import {
  componentDecorator,
  renderIcon,
  renderType,
  _createDynamicLink,
  likePost,
  disLikePost
} from "../../helpers";
//MUI
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { Visibility } from "@material-ui/icons";
import ToolTip from "@material-ui/core/Tooltip";
import Icon from "@material-ui/core/Icon";
//components
import ViewOrBuyPost from "../arPost/ViewOrBuyPost";
import TriggerFullModal from "../../components/modals/postFullScreenImage";
//styles
import classes from "./Creators.module.scss";

class CreatoPostSummary extends React.Component {
  state = {
    cites: []
  };
  renderContentIcon = () => {
    const { post } = this.props;
    if (post) {
      return renderIcon(post);
    }
  };
  renderPostType = () => {
    const { post } = this.props;
    if (post) {
      return renderType(post);
    }
  };
  renderTargetImage = () => {
    const { post } = this.props;
    const baseURL = !post.ARTargetID
      ? "https://triplee.info/Triple_E_Thumbnail/"
      : "https://triplee.info/Triple_E_WebService/AllImageTargets/";
    return <TriggerFullModal post={post} triggerBaseUrl={baseURL} />;
  };
  createDynamicLink = post => {
    _createDynamicLink(post, this.props.alertNotification);
  };
  handleLikePost = post => {
    const { profile } = this.props;
    if (profile.isEmpty) {
      return;
    }
    if (post.Likes && profile.User_ID in post.Likes) {
      return disLikePost(post, profile);
    }
    likePost(post, profile);
  };
  render() {
    const { post, profile, user } = this.props;
    if (isEmpty(post)) {
      return null;
    }
    return (
      <Card className={classes.CreatorPostSummary}>
        <CardHeader
          title={post.PostName}
          subheader={
            <div>
              <span>{moment(post.LastEdit).format("MMMM Do YYYY")}</span>
              <p style={{ margin: 0 }}>
                {this.renderPostType()}
                {this.renderContentIcon()}
              </p>
            </div>
          }
        />
        <div className="card center-align">{this.renderTargetImage()}</div>
        <CardContent>
          <Typography component="p" style={{ marginBottom: "20px" }}>
            <Linkify componentDecorator={componentDecorator}>
              {post.PostText}
            </Linkify>
          </Typography>
          <CardActions className={classes.actions}>
            <div className={classes.postViews}>
              <ToolTip title="Number of views" placement="top-start">
                <IconButton
                  className={classes.postInfoIcon}
                  aria-label="Number of views"
                >
                  <Visibility />
                  {
                    <span className={classes.numberViews}>
                      {post.NumberOfViews}
                    </span>
                  }
                </IconButton>
              </ToolTip>
              <ToolTip title="Click to copy post link" placement="top-start">
                <IconButton
                  className={classes.postInfoIcon}
                  aria-label="Post link"
                  onClick={() => this.createDynamicLink(post)}
                >
                  <Icon>share</Icon>
                  <span className={classes.numberViews}>
                    {post.PostShareCount || 0}
                  </span>
                </IconButton>
              </ToolTip>
              <ToolTip
                title={
                  profile.isEmpty
                    ? "Please sign in to like the post"
                    : "Click to like the post"
                }
                placement="top-start"
              >
                <IconButton
                  className={classes.postInfoIcon}
                  aria-label="Post likes"
                  onClick={() => this.handleLikePost(post)}
                >
                  <Icon
                    style={
                      post.Likes && profile.User_ID in post.Likes
                        ? { color: "red" }
                        : null
                    }
                  >
                    favorite
                  </Icon>
                  <span className={classes.numberViews}>
                    {(post.Likes && Object.keys(post.Likes).length) || 0}
                  </span>
                </IconButton>
              </ToolTip>
            </div>
            {user ? (
              <ViewOrBuyPost
                user={user}
                profile={profile}
                post={post}
                direction="fromXRPost"
              />
            ) : null}
          </CardActions>
        </CardContent>
      </Card>
    );
  }
}

export default connect(null, { alertNotification })(CreatoPostSummary);
