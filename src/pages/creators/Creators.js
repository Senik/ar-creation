import React, { Component } from "react";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { ClipLoader } from "react-spinners";
import { createStructuredSelector } from "reselect";

//selectors
import { selectProfile } from "../../redux/auth/auth.selectors";
import { selectKeyword } from "../../redux/post/post.selectors";
import { selectAllCreators } from "../../redux/user/user.selectors";
//actions
import {
  setSearchLength,
  clearSearchLength,
  clearKeyword
} from "../../redux/post/post.actions";
//helpers
import { filterByKeyword } from "../../helpers";
//components
import CreatorList from "./CreatorList";
import PostLoader from "../../components/shared/PostLoader";
//styles
import classes from "./Creators.module.scss";

class Creators extends Component {
  state = {
    prevPostIndex: 0,
    nextPostIndex: 9,
    filteredCreators: []
  };
  componentDidMount() {
    document.title = "Creators";
  }
  componentDidUpdate(prevProps) {
    if (prevProps.keyword !== this.props.keyword && this.props.keyword[0]) {
      const { creators, keyword, setSearchLength } = this.props;
      const filteredCreators = filterByKeyword(
        creators,
        ["User_NickName", "User_FullName", "User_Skills"],
        keyword
      );
      this.setState({
        filteredCreators
      });
      setSearchLength(filteredCreators.length);
    }
  }
  componentWillUnmount() {
    const { clearSearchLength, clearKeyword } = this.props;
    clearSearchLength();
    clearKeyword();
  }
  fetchMoreData = () => {
    let test = this.state.nextPostIndex + 9;
    this.setState({
      nextPostIndex: test
    });
  };
  render() {
    const { profile, creators, keyword } = this.props;
    const { prevPostIndex, nextPostIndex, filteredCreators } = this.state;
    const usersToShow = keyword[0] ? filteredCreators : creators;

    return (
      <div className={classes.Creators}>
        <div className="row">
          {usersToShow && usersToShow.length > 0 ? (
            <div className="col s12 m8 offset-m4 l10 offset-l2 xl10 offset-xl2">
              <p className={classes.heading}>ARize Creators</p>
              <InfiniteScroll
                dataLength={
                  usersToShow.slice(prevPostIndex, nextPostIndex).length
                }
                next={this.fetchMoreData}
                hasMore={nextPostIndex < usersToShow.length}
                loader={
                  <ClipLoader sizeUnit={"px"} size={150} color={"#123abc"} />
                }
                style={{ overflow: "hidden" }}
                endMessage={
                  <p style={{ textAlign: "center" }}>
                    <b style={{ color: "black" }}>Yay! You have seen it all</b>
                  </p>
                }
              >
                <CreatorList
                  profile={profile}
                  posts={usersToShow.slice(prevPostIndex, nextPostIndex)}
                />
              </InfiniteScroll>
            </div>
          ) : (
            <PostLoader />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  creators: selectAllCreators,
  profile: selectProfile,
  keyword: selectKeyword
});

const mapDispatchToProps = dispatch => {
  return {
    setSearchLength: searchLength => dispatch(setSearchLength(searchLength)),
    clearSearchLength: () => dispatch(clearSearchLength()),
    clearKeyword: () => dispatch(clearKeyword())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Creators);
