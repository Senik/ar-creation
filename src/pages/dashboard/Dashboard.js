import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import { ClipLoader } from "react-spinners";
import { createStructuredSelector } from "reselect";
import AppBarElement from "../../components/shared/AppBarElement";
//selectors
import { selectProfile } from "../../redux/auth/auth.selectors";
import { selectAllUsers } from "../../redux/user/user.selectors";
import {
  selectKeyword,
  selectAllPosts,
  selectUserPosts
} from "../../redux/post/post.selectors";
//actions
import {
  clearKeyword,
  setSearchLength,
  clearSearchLength
} from "../../redux/post/post.actions";
//helpers
import { filterByTab, filterByKeyword } from "../../helpers/";
//components
import ProjectList from "./ProjectList";
import PostsContainer from "../../components/shared/PostsContainer";
import PostLoader from "../../components/shared/PostLoader";
import SelectARType from "../../components/selectARType/SelectARType";

const postTypes = [
  "All",
  "3DBundle",
  "Video",
  "TransParentVideo",
  "TransParentYoutubeVideo",
  "360Video",
  "Youtube",
  "360YoutubeVideo",
  "WebURL",
  "FaceAR_2DImage",
  "FaceAR_3DBundle"
];
class Dashboard extends PureComponent {
  state = {
    prevPostIndex: 0,
    nextPostIndex: 9,
    showPosts: 0,
    showPostsType: [...postTypes],
    filteredPosts: [],
    changedType: false
  };
  componentDidMount() {
    document.title = "My Posts";
  }
  componentWillUnmount() {
    const { clearKeyword, clearSearchLength } = this.props;
    clearKeyword();
    clearSearchLength();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.keyword !== this.props.keyword ||
      prevState.showPosts !== this.state.showPosts
    ) {
      const { setSearchLength, posts, allPosts } = this.props;
      const { showPosts } = this.state;
      let searchedPosts;
      if (showPosts === 0 || showPosts === 1 || showPosts === 2) {
        searchedPosts = filterByTab(
          this.postsFilterByKeyword(posts),
          showPosts,
          true
        );
      } else {
        searchedPosts = this.postsFilterByKeyword(allPosts);
      }
      const filteredPosts = this.filterByPostType(searchedPosts);
      this.setState({ filteredPosts });
      filteredPosts && setSearchLength(filteredPosts.length);
    }

    if (prevState.showPostsType !== this.state.showPostsType) {
      const { posts, allPosts, setSearchLength } = this.props;
      const { showPosts } = this.state;
      let searchedPosts;

      if (showPosts === 0 || showPosts === 1 || showPosts === 2) {
        searchedPosts = filterByTab(
          this.postsFilterByKeyword(posts),
          showPosts,
          true
        );
      } else {
        searchedPosts = this.postsFilterByKeyword(allPosts);
      }
      let filteredPosts = this.filterByPostType(searchedPosts);
      this.setState({
        filteredPosts
      });
      setSearchLength(filteredPosts.length);
    }
  }

  postsFilterByKeyword = posts => {
    let searchPosts;
    const self = this;
    const { keyword } = self.props;
    const { showPosts } = self.state;
    if (keyword && keyword[2] && posts) {
      if (showPosts === 0 || showPosts === 1 || showPosts === 2) {
        searchPosts = filterByKeyword(posts, ["PostName", "PostText"], keyword);
      } else if (showPosts === 4) {
        searchPosts = filterByKeyword(
          self.filterByBought(posts),
          ["PostName", "PostText"],
          keyword,
          false,
          true
        );
      } else if (showPosts === 3) {
        searchPosts = filterByKeyword(
          self.filterByLikes(posts),
          ["PostName", "PostText"],
          keyword,
          false,
          [
            { key: "PostPrivacy", value: "Unlisted" },
            { key: "PostDeleteStatus", value: "deleted" }
          ]
        );
      }
      return searchPosts;
    } else {
      return posts;
    }
  };

  filterByPostType = posts => {
    const { showPostsType } = this.state;
    if (showPostsType.includes("All")) {
      return posts;
    }
    let result = posts.filter(post => {
      let checking = false;
      for (let i = 0; i < showPostsType.length; i++) {
        if (post.ARType === showPostsType[i]) {
          checking = true;
          break;
        }
      }
      return checking;
    });
    return result;
  };

  fetchMoreData = () => {
    let test = this.state.nextPostIndex + 9;
    this.setState({
      nextPostIndex: test
    });
  };
  scrollTop = () => {
    window.scroll({ top: 0, left: 0, behavior: "smooth" });
  };
  handleTabChange = (event, newValue) => {
    this.setState({ showPosts: newValue });
  };
  filterByLikes = posts => {
    let filteredPosts = [];
    posts &&
      Object.keys(posts).filter(postID => {
        let likes = posts[postID].Likes;
        likes &&
          Object.keys(likes).filter(likedID => {
            if (likedID === this.props.profile.User_ID) {
              filteredPosts.push(posts[postID]);
            }
            return null;
          });
        return likes;
      });
    return filteredPosts;
  };
  filterByBought = posts => {
    let filteredPosts = [];
    posts &&
      Object.keys(posts).filter(postID => {
        let buyers = posts[postID].Buyers;
        buyers &&
          Object.keys(buyers).filter(key => {
            if (buyers[key].UserID === this.props.profile.User_ID) {
              filteredPosts.push(posts[postID]);
            }
            return null;
          });
        return buyers;
      });
    return filteredPosts;
  };

  handleSelectChange = e => {
    const value = e.target.value;
    const { showPostsType } = this.state;
    if (value.includes("All") && !showPostsType.includes("All")) {
      this.setState({
        showPostsType: [...postTypes],
        changedType: false
      });
      return;
    } else if (!value.includes("All") && showPostsType.includes("All")) {
      this.setState({
        showPostsType: [],
        changedType: false
      });
      return;
    } else if (value.includes("All")) {
      let newTypes = [...value];
      let allIndex = newTypes.indexOf("All");
      newTypes.splice(allIndex, 1);
      this.setState({
        showPostsType: newTypes,
        changedType: true
      });
      return;
    }
    this.setState({
      showPostsType: value,
      changedType: true
    });
  };

  render() {
    let resp = localStorage.getItem("requestPackage");
    if (resp) {
      return <Redirect to="/profile" />;
    }
    const self = this;
    const {
      prevPostIndex,
      nextPostIndex,
      showPosts,
      showPostsType,
      filteredPosts,
      changedType
    } = self.state;
    const { posts, allPosts, users, profile, keyword } = self.props;
    let postsToShow = keyword[0] || changedType ? filteredPosts : posts;
    let likedOrPurchasedPosts =
      keyword[0] || changedType ? filteredPosts : allPosts;
    let filteringFunction;
    if (showPosts === 0 || showPosts === 1 || showPosts === 2) {
      filteringFunction = self.filterByPostType(
        filterByTab(postsToShow, showPosts, true)
      );
    } else if (showPosts === 3) {
      filteringFunction = self.filterByPostType(
        self.filterByLikes(likedOrPurchasedPosts)
      );
    } else if (showPosts === 4) {
      filteringFunction = self.filterByPostType(
        self.filterByBought(likedOrPurchasedPosts)
      );
    }
    let activTime = '48 hours';
    if (profile.Subscription) {
      if (profile.Subscription.Type === 'Plus') {
        activTime = '12 hours';
      } else if (profile.Subscription.Type === 'Premium') {
        activTime = '1 hour';
      }
    }

    if (postsToShow) {
      let tabs = [
        `Published ${
        self.filterByPostType(filterByTab(posts, 0, true)).length
        }`,
        `In Review ${
        self.filterByPostType(filterByTab(posts, 1, true)).length
        }`,
        `Rejected ${self.filterByPostType(filterByTab(posts, 2, true)).length}`,
        `Liked ${self.filterByPostType(self.filterByLikes(allPosts)).length}`,
        `Purchases ${
        self.filterByPostType(self.filterByBought(allPosts)).length
        }`
      ];

      return (
        <PostsContainer scrollToTop={self.scrollTop}>
          <AppBarElement
            value={showPosts}
            tabChange={self.handleTabChange}
            fromXRGallery={true}
            tabs={tabs}
          />
          {showPosts === 1 ? (
            <p className="center-align" style={{ color: 'red' }}>
              The activation may take up to {activTime}, you'll be notified once it is approved or rejected.
            </p>
          ) : null}
          <SelectARType
            showPostsType={showPostsType}
            selectChange={self.handleSelectChange}
            postTypes={postTypes}
            label="Select post type"
          />
          <InfiniteScroll
            dataLength={
              filteringFunction.slice(prevPostIndex, nextPostIndex).length
            }
            next={self.fetchMoreData}
            hasMore={nextPostIndex < filteringFunction.length}
            loader={<ClipLoader sizeUnit={"px"} size={150} color={"#123abc"} />}
            style={{ overflow: "hidden" }}
            endMessage={
              <p style={{ textAlign: "center" }}>
                <b style={{ color: "black" }}>Yay! You have seen it all</b>
              </p>
            }
          >
            <ProjectList
              profile={profile}
              users={users}
              posts={filteringFunction.slice(prevPostIndex, nextPostIndex)}
              showViewButton={showPosts}
            />
          </InfiniteScroll>
        </PostsContainer>
      );
    } else {
      return <PostLoader />;
    }
  }
}
const mapStateToProps = createStructuredSelector({
  users: selectAllUsers,
  posts: selectUserPosts,
  profile: selectProfile,
  keyword: selectKeyword,
  allPosts: selectAllPosts
});

const mapDispatchToProps = dispatch => {
  return {
    setSearchLength: searchLength => dispatch(setSearchLength(searchLength)),
    clearSearchLength: () => dispatch(clearSearchLength()),
    clearKeyword: () => dispatch(clearKeyword())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
