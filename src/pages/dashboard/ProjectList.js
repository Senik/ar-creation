import React, { memo } from 'react';
import PropTypes from 'prop-types';
//components
import ProjectSummary from './ProjectSummary';
import PostsMasonry from '../../components/shared/PostsMasonry';

const ProjectList = ({ posts, profile, users, showViewButton }) => {
  let projects =
    posts &&
    posts.map(post => {
      return (
        <ProjectSummary
          users={users}
          profile={profile}
          post={post}
          key={post.PostID}
          showViewButton={showViewButton}
        />
      );
    });
  return <PostsMasonry>{projects.length > 0 && projects}</PostsMasonry>;
};

ProjectList.propTypes = {
  profile: PropTypes.object.isRequired,
  posts: PropTypes.array.isRequired,
  users: PropTypes.array
};

export default memo(ProjectList);
