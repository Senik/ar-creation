import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { isEmpty } from "react-redux-firebase";
import PropTypes from "prop-types";
import moment from "moment";
import Linkify from "react-linkify";
//selectors
import { selectUserById } from "../../redux/user/user.selectors";
//actions
import {
  deletePost,
  deletePaidPost,
  addPostPayment
} from "../../redux/post/post.actions";
import { alertNotification } from "../../redux/auth/auth.actions";
//helpers
import {
  componentDecorator,
  _createDynamicLink,
  renderIcon,
  renderType,
  countViews,
  likePost,
  disLikePost
} from "../../helpers";

//MUI
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import ToolTip from "@material-ui/core/Tooltip";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";
//components
import Confirm from "../../components/modals/confirm";
import FullScreenDialog from "../../components/modals/postStatistic";
import TriggerFullModal from "../../components/modals/postFullScreenImage";
import PostInfoIcons from "../../components/postIcons/PostInfoIcons";
//icons
import DefaultAvatar from "../../default_avatar.png";
//styles
import classes from "./Dashboard.module.scss";
import PostProcessing from "../../components/processing/PostProcessing";

function ProjectSummary(props) {
  const renderContentIcon = () => {
    const { post } = props;
    if (post) {
      return renderIcon(post);
    }
  };
  const renderPostType = () => {
    const { post } = props;
    if (post) {
      return renderType(post);
    }
  };
  const createDynamicLink = post => {
    _createDynamicLink(post, props.alertNotification);
  };
  const handleLikePost = post => {
    const { profile } = props;
    if (profile.isEmpty) {
      return;
    }
    if (post.Likes && profile.User_ID in post.Likes) {
      return disLikePost(post, profile);
    }
    likePost(post, profile);
  };

  const onDelete = post => {
    const { deletePost, deletePaidPost } = props;
    if (post.Buyers) {
      return deletePaidPost(post);
    }
    deletePost(post);
  };
  const renderTargetImage = () => {
    const { post } = props;
    if (post.ARTargetStatus === "Processing") {
      return <PostProcessing />;
    }
    const baseURL = !post.ARTargetID
      ? "https://triplee.info/Triple_E_Thumbnail/"
      : "https://triplee.info/Triple_E_WebService/AllImageTargets/";
    return <TriggerFullModal post={post} triggerBaseUrl={baseURL} />;
  };

  const renderPostBalance = () => {
    const buyers = post.Buyers;
    if (!buyers) return 0;
    let sum = Object.keys(buyers).reduce((amount, key) => {
      if (isNaN(buyers[key].Amount)) {
        return amount;
      }
      return amount + +buyers[key].Amount;
    }, 0);
    return sum;
  };

  const { post, user, profile, users, showViewButton } = props;
  const { UniqueViewes } = post;
  if (isEmpty(post)) {
    return null;
  }
  let statistics = {};
  if (UniqueViewes && users) {
    statistics = countViews(UniqueViewes, users);
  }
  let avatarImageUrl;
  if (
    user &&
    user.User_ProfilePicURL &&
    user.User_ProfilePicURL.includes("facebook")
  ) {
    avatarImageUrl = user.User_ProfilePicURL;
  } else {
    avatarImageUrl = `https://triplee.info/Triple_E_Social/ProfilePictures/${post.PostAuthorID}.jpg`;
  }

  return (
    <Card className={`${classes.card} ${classes.ProjectSummary}`}>
      <CardHeader
        avatar={
          <Avatar
            aria-label="Avatar-Image"
            src={`${profile.User_ID && avatarImageUrl}?time=${new Date()}`}
            onError={e => (e.target.src = DefaultAvatar)}
            className={classes.avatar}
          />
        }
        action={
          post.PostAuthorID === profile.User_ID &&
            post.ARTargetStatus !== "Processing" &&
            post.PostDeleteStatus !== "deleted" ? (
              <React.Fragment>
                <Link to={`/editpost/${post.PostID}`}>
                  <Button className={classes.editLink}>
                    <Icon>edit</Icon>
                  </Button>
                </Link>
              </React.Fragment>
            ) : null
        }
        title={post.PostAuthor}
        subheader={
          <div>
            <span>{moment(post.LastEdit).format("MMMM Do YYYY")}</span>
            <p style={{ margin: 0 }}>
              {renderPostType()}
              {renderContentIcon()}
            </p>
            {post.IsPaid === "True" && post.PostAuthorID === profile.User_ID ? (
              <span
                className={classes.postAmount}
              >{`Balance: \u20AC${renderPostBalance()}`}</span>
            ) : null}
          </div>
        }
      />
      <div className="card center-align">
        <div className="card-content">
          <span className="card-title center-align">{post.PostName}</span>
        </div>
        {renderTargetImage()}
      </div>

      <CardContent>
        <Typography component="p" className={classes.postText}>
          <Linkify componentDecorator={componentDecorator}>
            {post.PostText}
          </Linkify>
        </Typography>
      </CardContent>
      <CardActions className={classes.actions}>
        <PostInfoIcons
          showLikes={
            showViewButton === 0 || showViewButton === 3 || showViewButton === 4
          }
          profile={profile}
          post={post}
          handleLikePost={handleLikePost}
          dynamicLinkCreation={createDynamicLink}
        />
        {post.PostAuthorID === profile.User_ID ? (
          <>
            <FullScreenDialog
              post={post}
              statistics={statistics}
              style={{ backgroundColor: "transparent" }}
            />
            {post.ARTargetStatus !== "Processing" ? (
              <ToolTip title="Delete post" placement="top-start">
                <div>
                  <Confirm onDelete={onDelete} post={post} />
                </div>
              </ToolTip>
            ) : null}
          </>
        ) : null}
      </CardActions>
    </Card>
  );
}

ProjectSummary.propTypes = {
  deletePost: PropTypes.func.isRequired,
  post: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  users: PropTypes.array
};

const mapStateToProps = (state, ownProps) => {
  const { post } = ownProps;
  if (post) {
    return {
      user: selectUserById(post.PostAuthorID)(state)
    };
  }
};

export default connect(mapStateToProps, {
  deletePost,
  deletePaidPost,
  alertNotification,
  addPostPayment
})(React.memo(ProjectSummary));
