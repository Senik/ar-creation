import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
//selectors
import { selectUserById } from "../../redux/user/user.selectors";
//styles
import classes from "./Notifications.module.scss";
//icons
import DefaultAvatar from "../../default_avatar.png";

const NotificationSummary = props => {
  const { notification, notifUserData } = props;

  const { NotificationType, Message, Post_ID, Post_Name, Reason } = notification;
  const typeClassName = `NotifType_${NotificationType}`;
  let message;
  switch (NotificationType) {
    case "like":
    case "purchase":
      message = notifUserData && (
        <>
          <div className={classes.NotifAvatar}>
            <img
              src={`${notifUserData.User_ProfilePicURL}?time=${new Date()}`}
              alt="avatar"
              onError={e => (e.target.src = DefaultAvatar)}
            />
          </div>
          <p>
            <Link to={`/creator/${notifUserData.User_ID}`} target="_blank">
              {notifUserData.User_FullName}
            </Link>{" "}
            {NotificationType}d your post:
            <Link to={`/arpost/${Post_ID}`} target="_blank">
              {` ${Post_Name}`}
            </Link>
          </p>
        </>
      );
      break;
    case "accept":
      message = (
        <p>
          Your
          <Link to={`/arpost/${Post_ID}`} target="_blank">
            {` ${Post_Name} `}
          </Link>
          post has been {NotificationType}ed:
        </p>
      );
      break;
    case "reject":
      message = (
        <div>
          <p>
            Your
            <Link to={`/arpost/${Post_ID}`} target="_blank">
              {` ${Post_Name} `}
            </Link>
            post has been {NotificationType}ed:
          </p>
          {Reason && <p><b>Reason: </b>{Reason}</p>}
        </div>
      );
      break;
    case "follow":
      message = notifUserData && (
        <>
          <div className={classes.NotifAvatar}>
            <img
              src={`${notifUserData.User_ProfilePicURL}?time=${new Date()}`}
              alt="avatar"
              onError={e => (e.target.src = DefaultAvatar)}
            />
          </div>
          <p>
            <Link to={`/creator/${notifUserData.User_ID}`} target="_blank">
              {notifUserData.User_FullName}
            </Link>{" "}
            started to follow you
          </p>
        </>
      );
      break;
    case "delete":
      message = <p>Your post has been deleted: {Post_Name}</p>;
      break;
    case "creatorStatusAccepted":
    case "creatorStatusRejected":
      message = <p> {Message}</p>;
      break;
    case "tofollowers":
      message = (
        <>
          <div className={classes.NotifAvatar}>
            <img
              src={`https://triplee.info/Triple_E_Social/ProfilePictures/${
                notification.Post_Author_ID
                }.jpg?time=${new Date()}`}
              alt="avatar"
            />
          </div>
          <p>
            <Link
              to={`/creator/${notification.Post_Author_ID}`}
              target="_blank"
            >
              {notification.Post_Author_Nickname}
            </Link>{" "}
            created
            <Link to={`/arpost/${Post_ID}`} target="_blank">
              &nbsp; new post: {Post_Name}
            </Link>
          </p>
        </>
      );
      break;
    default:
      return null;
  }
  return (
    <div className={`${classes.NotifList} ${classes[typeClassName]}`}>
      {!props.isNotOpened ? (
        <div className={classes.newNotification}></div>
      ) : null}
      <div className={classes.NotifCard}>{message}</div>
    </div>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { notification } = ownProps;
  if (notification.User) {
    const uid = notification.User.User_ID;

    return {
      notifUserData: selectUserById(uid)(state)
    };
  }
  return {
    notifUserData: null
  };
};

export default connect(mapStateToProps)(NotificationSummary);
