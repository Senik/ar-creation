import React, { Component } from 'react';
import { connect } from 'react-redux';
//helpers
import { notificationSeen } from '../../helpers/';
//actions
import { clearNotifications } from '../../redux/user/user.actions';
//MUI
import Button from '@material-ui/core/Button';
//components
import NotificationSummary from './NotificationSummary';
import PostLoader from '../../components/shared/PostLoader';
//styles
import classes from './Notifications.module.scss';

class Notifications extends Component {
  state = {
    oldNotifications: null,
    newNotifications: null
  }
  componentWillUnmount() {
    this.state.newNotifications && this.state.newNotifications.map(newNotification => {
      return notificationSeen(newNotification.key, this.props.uid);
    })
  }
  componentDidMount() {
    document.title = 'Notifications';
    this.setState({
      notificationsAreReady: false
    })
  }
  componentDidUpdate(prevProps) {
    const { notifications } = this.props;
    if (notifications !== prevProps.notifications) {
      this.setState({
        oldNotifications: null,
        newNotifications: null,
        notificationsAreReady: false
      })
    }
    if (notifications && !this.state.notificationsAreReady) {
      const newNotifications = [];
      const oldNotifications = [];
      notifications && Object.keys(notifications).map(key => {
        if (notifications[key].NotificationOpened === 'False') {
          newNotifications.push({
            key: key,
            ...notifications[key]
          });
          newNotifications.sort((a, b) => b.DateTime - a.DateTime);
        } else if (notifications[key].NotificationOpened === 'True') {
          oldNotifications.push({
            key: key,
            ...notifications[key]
          });
          oldNotifications.sort((a, b) => b.DateTime - a.DateTime);
        }
        return null;
      });
      this.setState({
        oldNotifications,
        newNotifications,
        notificationsAreReady: true
      });
    }
  }

  handleClearNotifications = () => {
    const { clearNotifications, uid } = this.props;
    clearNotifications(uid);
  }

  render() {
    const { isProfileEmpty, notifications } = this.props;
    const { newNotifications, oldNotifications } = this.state;
    if (isProfileEmpty) {
      return <PostLoader />;
    } else if (!notifications && !isProfileEmpty) {
      return (
        <div className="row">
          <div className="col s12 m8 offset-m4">
            <h5 className={classes.ZeroNotification}>You don't have any notifications!</h5>
          </div>
        </div>
      );
    } else {
      return (
        <div className={classes.Notifications}>
          <div className="row">
            <div className="col s12 m8 offset-m4">
              <Button
                className="red white-text btn waves-effect waves-light z-depth-3"
                onClick={this.handleClearNotifications}
              >
                Clear All
                  </Button>
            </div>
          </div>
          <div className="row">
            <div className="col s12 m8 offset-m4">
              <div>
                {
                  newNotifications && newNotifications.map(newNotification => (
                    <NotificationSummary
                      key={newNotification.key}
                      notification={newNotification}
                      isNotOpened={true}
                    />
                  ))
                }
                {
                  oldNotifications && oldNotifications.map(oldNotification => (
                    <NotificationSummary
                      key={oldNotification.key}
                      notification={oldNotification}
                    />
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    notifications: state.firebase.profile.Notifications,
    uid: state.firebase.profile.User_ID,
    isProfileEmpty: state.firebase.profile.isEmpty
  }
};

const mapDispatchToProps = dispatch => {
  return {
    clearNotifications: uid => dispatch(clearNotifications(uid))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Notifications);