import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
//selectors
import {
  selectProfile,
  selectCreatorBalance
} from "../../redux/auth/auth.selectors";
//MUI
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
//components
import PostLoader from "../../components/shared/PostLoader";
import PayoutSummary from "./PayoutSummary";
import RequestPayout from "../../components/payout/RequestPayout";
//styles
import classes from "./Payout.module.scss";

function Payout(props) {
  const { profile, creatorBalance } = props;
  if (profile.isEmpty) {
    return <PostLoader />;
  } else if (!profile.Payout && !profile.isEmpty) {
    return (
      <div className="row">
        <div className="col s12 m8 offset-m4 l10 offset-l2">
          <h5 className={classes.NoPayout}>
            You don't have any payout requests!
          </h5>
          {creatorBalance > 100 && <RequestPayout profile={profile} />}
        </div>
      </div>
    );
  } else {
    const payouts = profile.Payout;
    return (
      <div className={classes.Payout}>
        <div className="row">
          <div className="col s12 m8 offset-m4 l10 offset-l2">
            <Paper className={classes.root}>
              <Table className={classes.PayoutTable} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Payout Status</TableCell>
                    <TableCell align="right">Date</TableCell>
                    <TableCell align="right">Amount</TableCell>
                    <TableCell align="right">Invoice URL</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {Object.keys(payouts).map(key => (
                    <TableRow key={key}>
                      <PayoutSummary payout={payouts[key]} />
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Paper>
            {creatorBalance > 100 && <RequestPayout profile={profile} />}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  profile: selectProfile,
  creatorBalance: selectCreatorBalance
});

export default connect(mapStateToProps)(Payout);
