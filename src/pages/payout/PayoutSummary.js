import React from 'react';
import moment from 'moment';
//MUI
import TableCell from '@material-ui/core/TableCell';

const PayoutSummary = props => {
  const payout = props.payout;
  const { Amount, DateTime, Invoice_URL, Payout_Status } = payout;
  return (
    <>
      <TableCell component="th" scope="row">{Payout_Status}</TableCell>
      <TableCell align="right">{moment(DateTime).format('MMM DD, YYYY')}</TableCell>
      <TableCell align="right">{Amount}</TableCell>
      <TableCell align="right">
        <a target="_blank" rel="noopener noreferrer" href={Invoice_URL}>{Invoice_URL.substring(Invoice_URL.lastIndexOf('/') + 1)}</a>
      </TableCell>
    </>
  );
}

export default PayoutSummary;