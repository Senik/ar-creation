import React, { useRef, useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import StarRatingComponent from "react-star-rating-component";
import ReactPLayer from "react-player";
import DropZone from "react-dropzone";
//selectors
import {
  selectAuthUser,
  selectProfile,
  selectAuthError,
  selectForwardUser
} from "../../redux/auth/auth.selectors";
import {
  selectWrong,
  select_post_by_id,
  selectUserLimitations
} from "../../redux/post/post.selectors";
//actions
import { editPost, somethingWrong } from "../../redux/post/post.actions";
import {
  checkUserEmail,
  clearAuthError,
  clearUserEmail
} from "../../redux/auth/auth.actions";
//MUI
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Tooltip from "@material-ui/core/Tooltip";
import Icon from "@material-ui/core/Icon";
import Checkbox from "@material-ui/core/Checkbox";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
//helpers
import {
  VuforiaGetTarget,
  generateUUID,
  writeToLocalStorage
} from "../../helpers";
//components
import FullPageLoader from "../../components/shared/FullPageLoader";
import SketchfabViewerIframe from "../../components/shared/SketchfabViewer";
import GooglePolyIframe from "../../components/shared/GooglePolyIframe";
import EditTargetModal from "../../components/modals/triggerImageCrop";
import PostLoader from "../../components/shared/PostLoader";
import PostNotFound from "../../components/shared/PostNotFound";
import CheckElement from "../../components/formElements/Checkbox";
import PriceElement from "../../components/formElements/Price";
import InputElement from "../../components/formElements/InputElement";
import PostTags from "../../components/postTags/PostTags";
import ColorPicker from "../../components/formElements/ColorPicker";
//styles
import classes from "./PostEdit.module.scss";
import InAppModal from "../../components/modals/arPost/InAppModal";
import PostProcessing from "../../components/processing/PostProcessing";

const theme = createMuiTheme({
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: 16,
        backgroundColor: "black"
      }
    }
  },
  typography: {
    useNextVariants: true
  }
});

const EditPost = props => {
  const {
    post,
    auth,
    authError,
    forwardUser,
    profile,
    editPost,
    history,
    checkUserEmail,
    somethingWrong,
    userLimits
  } = props;
  const {
    Price,
    ARType,
    ARTags,
    Buyers,
    IsPaid,
    PostID,
    ARFusion,
    PostText,
    PostName,
    ARSource,
    IsYoutube,
    XRGallery,
    PostAuthor,
    PostStatus,
    ARTargetID,
    CutOutColor,
    ARSourceURL,
    PostPrivacy,
    PostAuthorID,
    ARContentURL,
    ARTargetName,
    PostDeleteStatus,
    ExtendedTracking,
    PostAuthorNickName,
    SoundReactivity,
    AudioFile
  } = { ...post };
  const [postName, setPostName] = useState("");
  const [postDescription, setPostDescription] = useState("");
  const postForwardEmail = useFormInput();
  const [postExtTracking, setExttracking] = useState(false);
  const [fusionMode, setFusionMode] = useState(false);
  const [postTargetImage, setTargetImage] = useState("");
  const [showLoader, setLoader] = useState(false);
  const [copySuccess, setCopySuccess] = useState("");
  const [paid, setPaid] = useState(null);
  const [price, setPrice] = useState(0);
  const [postPrivacy, setPostPrivacy] = useState();
  const textAreaRef = useRef(null);
  const [targetImageRating, setTargetRating] = useState("");
  const [arContentUrl, setContentUrl] = useState();
  const [redirectTo, setRedirectTo] = useState(false);
  const [newSelectedFile, setNewSelectedFile] = useState(null);
  const [postNameError, setError] = useState();
  const [videoCutOutColor, setVideoCutOutColor] = useState();
  const [postCutOutColor, setPostCutOutColor] = useState(false);
  const [contentPath, setContentPath] = useState(null);
  const [isDisabled, setIsDisabled] = useState(false);
  const [changeStatus, setChangeStatus] = useState(false);
  const [xrGallery, setXRGallery] = useState(false);
  const [tags, setTags] = useState([]);
  const [tagsError, setTagsError] = useState(false);
  const [soundReactivity, setSoundReactivity] = useState();
  const [soundError, setSoundError] = useState("");
  const [soundReactivityFile, setSoundFile] = useState();

  useEffect(() => {
    document.title = "Edit Post";
    const { clearUserEmail, clearAuthError } = props;
    return () => {
      clearAuthError();
      clearUserEmail();
    };
  }, []);

  useEffect(() => {
    let isSubscribed = true;
    if (post && isSubscribed) {
      if (auth.uid !== PostAuthorID || PostDeleteStatus === "deleted") {
        setRedirectTo(true);
      } else {
        if (IsPaid === "True" && Price) {
          setPaid("True");
          setPrice(Price);
        }
        if (ARTags) {
          const tagsArray = ARTags.split(",");
          let tags = [];
          tagsArray && tagsArray.map(tag => tags.push({ id: tag, text: tag }));
          setTags(tags);
        }
        if (ARSource === "ZF") {
          setContentPath(
            `https://triplee.info/Triple_E_3DModels/${ARSourceURL}`
          );
        }
        if (ARSource === "UP") {
          setContentPath(
            `https://triplee.info/Triple_E_Unity_Packages/${ARSourceURL}`
          );
        }
        if (ARType === "3DBundle") {
          setContentUrl(ARSourceURL);
        }
        if (
          ARType === "Youtube" ||
          (ARType === "360Video" && IsYoutube === "True") ||
          ARType === "TransParentYoutubeVideo" ||
          ARType === "WebURL" ||
          ARType === "FaceAR_2DImage" ||
          ARType === "WebURL"
        ) {
          setContentUrl(ARContentURL);
        }
        if (ARType === "Video" || ARType === "TransParentVideo") {
          setContentUrl(ARContentURL);
          setContentPath(
            `https://triplee.info/Triple_E_Videos/ARVideos/${ARContentURL}`
          );
        } else if (ARType === "360Video" && IsYoutube !== "True") {
          setContentUrl(ARContentURL);
          setContentPath(
            `https://triplee.info/Triple_E_360Videos/${ARContentURL}`
          );
        }
        if (CutOutColor) {
          let colors = CutOutColor.split(",");
          let RGBColor =
            "#" +
            ((1 << 24) + (+colors[0] << 16) + (+colors[1] << 8) + +colors[2])
              .toString(16)
              .slice(1);
          setVideoCutOutColor(RGBColor);
          setPostCutOutColor(true);
        }
        setXRGallery(XRGallery === "True");
        setPostPrivacy(PostPrivacy || "Public");
        setPostName(PostName);
        setPostDescription(PostText);
        setExttracking(ExtendedTracking && ExtendedTracking === "True");
        setFusionMode(ARFusion && ARFusion === "True");
        setSoundReactivity(SoundReactivity && SoundReactivity === "True");
        const fetchData = async id => {
          const data = new FormData();
          data.append("targetID", id);
          try {
            const res = await VuforiaGetTarget(data);
            let text = await res.text();
            let index = text.indexOf("{");
            let responseData = JSON.parse(text.slice(index));
            if (responseData.status === "success") {
              setTargetRating(responseData.target_record.tracking_rating);
            }
          } catch (err) {
            console.log("error", err);
          }
        };
        if (XRGallery !== "True" && isSubscribed) {
          fetchData(ARTargetID);
        }
      }
    }
    return () => {
      isSubscribed = false;
    };
  }, [post, redirectTo]);

  useEffect(() => {
    if (
      !postName ||
      (xrGallery && !tags.length) ||
      (paid === "True" && !price) ||
      (paid === "True" && price <= 0)
    ) {
      setIsDisabled(true);
    } else if (ARSource === "SF") {
      if (arContentUrl) {
        const index = arContentUrl.lastIndexOf("/");
        let id = arContentUrl.slice(index + 1);
        if (!arContentUrl.includes("sketchfab.com/3d-models/") || !id.length) {
          setIsDisabled(true);
        } else {
          setIsDisabled(false);
        }
      }
    } else if (ARSource === "GP") {
      if (soundReactivity && !soundReactivityFile && !AudioFile) {
        setIsDisabled(true);
        setSoundError("Sound file can not be empty");
        return;
      } else {
        setIsDisabled(false);
        setSoundError(null);
      }
      if (arContentUrl) {
        const index = arContentUrl.lastIndexOf("/");
        let id = arContentUrl.slice(index + 1);
        if (!arContentUrl.includes("poly.google.com/view/") || !id.length) {
          setIsDisabled(true);
        } else {
          setIsDisabled(false);
        }
      }
    } else if (
      ARType === "Youtube" ||
      ARType === "TransParentYoutubeVideo" ||
      (ARType === "360Video" && IsYoutube === "True")
    ) {
      if (arContentUrl && arContentUrl.includes("youtube.com")) {
        const index = arContentUrl.lastIndexOf("v=");
        let id = arContentUrl.slice(index + 2);
        if (!arContentUrl.includes("/watch?v=") || !id.length) {
          setIsDisabled(true);
        } else {
          setIsDisabled(false);
        }
      } else if (arContentUrl && arContentUrl.includes("youtu.be")) {
        const index = arContentUrl.lastIndexOf("e/");
        let id = arContentUrl.slice(index + 2);
        if (!arContentUrl.includes("youtu.be/") || !id.length) {
          setIsDisabled(true);
        } else {
          setIsDisabled(false);
        }
      } else {
        setIsDisabled(true);
      }
    } else {
      setIsDisabled(false);
    }
  }, [
    postName,
    paid,
    price,
    arContentUrl,
    authError,
    tags,
    xrGallery,
    soundReactivityFile,
    soundReactivity
  ]);

  const handleInAppDialogClose = () => {
    props.somethingWrongAction();
    setLoader(false);
    setTargetImage(null);
  };
  if (post) {
    if (post.ARTargetStatus === "Processing") {
      return <PostProcessing from="singlePage" />;
    }
    if (redirectTo) {
      return <Redirect to="/" />;
    }
    if (somethingWrong) {
      return (
        <InAppModal
          isOpen={!!somethingWrong}
          close={handleInAppDialogClose}
          text={somethingWrong}
        />
      );
    }
    const handleFileUpload = file => {
      if (PostStatus === "Rejected" || PostStatus === "TestMode") {
        setChangeStatus(true);
      }
      setError(null);
      setNewSelectedFile(file);
    };
    const handleErrors = () => {
      if (ARType === "Video" || ARType === "TransParentVideo") {
        setError("Sorry we accept only .mp4 format");
      } else if (ARSource === "ZF") {
        setError("Sorry we accept only .zip format");
      } else if (ARType === "FaceAR_2DImage") {
        setError("Sorry we accept only .jpg format");
      } else {
        setSoundError("Sorry we accept only .mp3 format");
      }
    };
    const copyToClipboard = e => {
      e.preventDefault();
      textAreaRef.current.select();
      document.execCommand("copy");
      e.target.focus();
      setCopySuccess("Copied");
      setTimeout(() => {
        setCopySuccess("");
      }, 2000);
    };

    const handleEmailCheck = e => {
      if (postForwardEmail.value) {
        const email = postForwardEmail.value.toLowerCase().trim();
        checkUserEmail(email, PostName);
      }
    };
    const handleTargetImageUpload = img => {
      if (PostStatus === "Rejected" || PostStatus === "TestMode") {
        setChangeStatus(true);
      }

      setTargetImage(img);
    };
    const handleContentUrlChange = e => {
      if (PostStatus === "Rejected" || PostStatus === "TestMode") {
        setChangeStatus(true);
      }
      setContentUrl(e.target.value);
    };
    const handleSoundReactivity = e => {
      setSoundReactivity(!soundReactivity);
    };
    const handleSoundUpload = file => {
      setSoundFile(file);
    };
    const handleExtTrackingCheckbox = e => {
      setExttracking(!postExtTracking);
    };
    const handleFusionMode = e => {
      setFusionMode(!fusionMode);
    };
    const changePaid = Paid => {
      setPaid(Paid);
    };
    const changePrice = price => {
      setPrice(price);
    };
    const changePostPrivacy = privacy => {
      setPostPrivacy(privacy);
    };
    const changeName = name => {
      setPostName(name);
    };
    const changeDescription = Description => {
      setPostDescription(Description);
    };
    const changeType = () => {
      setXRGallery(!xrGallery);
    };
    const handleCutOutCheckbox = () => {
      setPostCutOutColor(!postCutOutColor);
    };
    const handleCutOutColorChange = value => {
      setVideoCutOutColor(value);
    };

    const handleAddTags = tag => {
      let checkedTag = tag;
      checkedTag.text.trim();
      if (!checkedTag.text) {
        return;
      }
      if (tags.length >= 3) {
        setTagsError(true);
        return;
      }
      if (tag.text[0] === "#") {
        checkedTag.id = checkedTag.text = tag.text.substr(1);
      }
      setTags([...tags, checkedTag]);
    };
    const handleDeleteTags = i => {
      const newTags = tags.filter((tag, index) => index !== i);
      setTags(newTags);
      setTagsError(false);
    };
    const handleTagsBlur = tag => {
      if (!tag.length || !tag.trim()) {
        return;
      }
      let checkedTag = {
        id: tag.trim(),
        text: tag.trim()
      };
      if (tags.length >= 3) {
        setTagsError(true);
        return;
      }
      if (tag[0] === "#") {
        checkedTag.id = checkedTag.text = tag.text.substr(1);
      }
      setTags([...tags, checkedTag]);
    };
    const handleUpgradeLinkClick = e => {
      writeToLocalStorage("requestPackage", "true");
    };
    const handleSubmit = e => {
      e.preventDefault();
      const data = {
        postId: PostID,
        PostName: postName,
        PostText: postDescription,
        ForwardedUserID: (forwardUser && forwardUser.User_ID) || PostAuthorID,
        ForwardedUserFullname:
          (forwardUser && forwardUser.User_FullName) || PostAuthor,
        ForwardedUserNickname:
          (forwardUser && forwardUser.User_NickName) || PostAuthorNickName,
        ARContentURL: arContentUrl,
        ExtTracking: postExtTracking ? "True" : null,
        ARFusion: fusionMode ? "True" : null,
        targetImage: postTargetImage,
        targetImageName: ARTargetName,
        targetImageID: ARTargetID,
        postArType: ARType,
        isYoutube: IsYoutube || false,
        isPaid: paid,
        xrGallery: xrGallery ? xrGallery : null,
        postPrivacy,
        postStatusFirebase: PostStatus,
        soundReactivity,
        soundReactivityFile
      };
      if (
        data.targetImage &&
        data.targetImage.length > 0 &&
        data.targetImageID
      ) {
        data.targetImageNameNew = generateUUID();
      }
      if (paid === "True") {
        data.price = price;
      }
      if (xrGallery) {
        data.model_type = "xrGallery";
      }
      if (ARType === "3DBundle") {
        data.ARContentURL = arContentUrl;
        data.model_source = ARSource;
      }
      if (newSelectedFile) {
        data.contentFile = newSelectedFile;
      }
      if (ARType === "FaceAR_2DImage") {
        data.model_type = "FaceAR_2DImage";
      }
      if (
        (changeStatus && profile.User_Type !== "Prime") ||
        (PostPrivacy === "PrivateToMe" &&
          postPrivacy === "Public" &&
          PostStatus === "TestMode") //||
        //data.postArType === "3DBundle"
      ) {
        data.PostStatus = "InReview";
      }
      if ((post.hasOwnProperty("ARTags") || tags.length) && xrGallery) {
        const tagString = tags.map(tag => tag["id"]).join();
        if (ARTags !== tagString) {
          data.ARTags = tagString;
        }
      } else {
        data.ARTags = null;
      }
      if (videoCutOutColor && postCutOutColor) {
        data.CutOutColor = videoCutOutColor;
      } else if (!videoCutOutColor && postCutOutColor) {
        data.CutOutColor = "76, 175, 80";
      }
      if (CutOutColor && !postCutOutColor) {
        data.CutOutColor = "empty";
      }
      if (soundReactivityFile && AudioFile) {
        data.oldSoundFileName = AudioFile.replace(/.mp3/g, "");
      }
      if (data.CutOutColor && data.CutOutColor !== "empty") {
        let formatRgb = data.CutOutColor.match(/[A-Za-z0-9]{2}/g)
          .map(function (v) {
            return parseInt(v, 16);
          })
          .join(",");
        data.CutOutColor = formatRgb;
      }
      setLoader(true);
      editPost(data, history);
    };
    const renderContentView = () => {
      if (
        ARType === "Youtube" ||
        ARType === "360Video" ||
        (ARType === "360Video" && IsYoutube) ||
        ARType === "TransParentYoutubeVideo" ||
        ARType === "Video" ||
        ARType === "TransParentVideo"
      ) {
        return (
          <ReactPLayer
            width={"100%"}
            url={contentPath ? contentPath : arContentUrl}
            playing
            loop
            controls
            config={{
              youtube: {
                playerVars: { showinfo: 1 }
              }
            }}
          />
        );
      } else if (ARType === "FaceAR_2DImage") {
        return (
          <div>
            <img
              src={`https://triplee.info/Triple_E_FaceAR/2DFaceAR/${ARContentURL}`}
              onError={e =>
                (e.target.src =
                  "https://triplee.info/Triple_E_WebService/AllImageTargets/correct3.jpg")
              }
              className={classes.targetPreview}
              alt="Target Trigger"
            />
          </div>
        );
      } else if (xrGallery || ARTargetID) {
        switch (ARSource) {
          case "SF":
            return (
              <div className={classes.targetImageInfo}>
                <SketchfabViewerIframe link={arContentUrl} linkCheck={true} />
              </div>
            );
          case "GP":
            return (
              <div className={classes.targetImageInfo}>
                <GooglePolyIframe link={arContentUrl} linkCheck={true} />
              </div>
            );
          case "ZF":
          case "UP":
            return (
              <i
                className="fas fa-file-archive ChooseContetnType-icon-218 fa-6x"
                style={{ marginBottom: 25 }}
              />
            );
          default:
            return null;
        }
      }
    };
    const renderUserWarningText = () => {
      const { profile } = props;
      if (profile.Subscription) {
        return (
          <p>
            You have reached the monthly limit of paid posts included in your
            plan. <br />
            Please{" "}
            <Link to="/profile" onClick={handleUpgradeLinkClick}>
              Upgrade
            </Link>{" "}
            your plan in order to continue.
          </p>
        );
      } else {
        if (profile.User_Type === "individual") {
          return (
            <p>
              You have reached the limit of the paid posts included in the Free
              plan. Please{" "}
              <Link to="/profile" onClick={handleUpgradeLinkClick}>
                Upgrade
              </Link>{" "}
              your plan in order to continue.
            </p>
          );
        } else if (profile.User_Type === "enterprise") {
          return (
            <p>
              In order to be able to create paid posts, please{" "}
              <Link to="/profile" onClick={handleUpgradeLinkClick}>
                Upgrade
              </Link>{" "}
              your plan.
            </p>
          );
        }
      }
    };

    return (
      <div className={`col s12 m8 offset-m4 l10 offset-l3 ${classes.PostEdit}`}>
        <div className="row">
          {showLoader ? (
            <FullPageLoader showChildren={true}>Updating Post</FullPageLoader>
          ) : null}

          <p className={classes.postStatus}>
            Post Status:
            <span className={classes[PostStatus]}>
              {" "}
              {PostStatus === "TestMode" ? "Published" : PostStatus}
            </span>
          </p>
          <div className="col m10 offset-m2 l6 ">
            <div className={classes.targetImageInfo}>
              {ARTargetID && (
                <div className={`${classes.ratingContainer} input-field`}>
                  <div className="row">
                    <div className="col s12 m4">
                      <span className={classes.ratingText}>
                        Image Trackability
                      </span>
                    </div>
                    <div className="col s12 m6">
                      <StarRatingComponent
                        className={classes.ratingStars}
                        name="Post Raiting"
                        editing={false}
                        starColor={"#ec4079"}
                        renderStarIcon={() => (
                          <span>
                            <i className="fas fa-star" />
                          </span>
                        )}
                        starCount={5}
                        value={+targetImageRating}
                      />
                    </div>
                    <div className="col s12 m2">
                      <MuiThemeProvider theme={theme}>
                        <Tooltip
                          style={{
                            top: 14,
                            position: "relative",
                            fontSize: 24
                          }}
                          title="The star rating of a trigger image ranges between 1 and 5 stars; although targets with low rating (1 or 2 stars) can usually detect and track well. For best results, you should aim for targets with 4 or 5 stars."
                          placement="top"
                        >
                          <Icon className={classes.infoIcon}>info</Icon>
                        </Tooltip>
                      </MuiThemeProvider>
                    </div>
                  </div>
                </div>
              )}
            </div>

            <div className="card">
              <div className="card-image target-image">
                <img
                  src={
                    postTargetImage ||
                    `https://triplee.info/${
                    ARTargetID
                      ? "Triple_E_WebService/AllImageTargets"
                      : "Triple_E_Thumbnail"
                    }/${ARTargetName}.jpg?${new Date()}`
                  }
                  onError={e =>
                    (e.target.src =
                      "https://triplee.info/Triple_E_WebService/AllImageTargets/correct3.jpg")
                  }
                  className={classes.targetPreview}
                  alt="Target Trigger"
                />
                {PostStatus !== "InReview" && !Buyers && (
                  <EditTargetModal
                    targetImage={ARTargetName}
                    targetID={ARTargetID}
                    handleTargetImage={handleTargetImageUpload}
                    fixCropSize={ARType === "FaceAR_2DImage"}
                  />
                )}
              </div>
            </div>
            {renderContentView()}
          </div>
          <div className="col m10 offset-m2 l6 ">
            <form className={classes.formStyle} onSubmit={handleSubmit}>
              <h5 className="grey-text text-darken-3 text-center">Edit post</h5>
              <InputElement
                name="postName"
                defaultValue={postName}
                label="Post Name"
                required={true}
                handleChange={changeName}
              />
              <InputElement
                name="postDescription"
                defaultValue={postDescription}
                label="Post Description"
                type="textarea"
                required={false}
                handleChange={changeDescription}
              />
              {arContentUrl &&
                (ARType === "Video" ||
                  ARType === "FaceAR_2DImage" ||
                  ARSource === "ZF" ||
                  ARSource === "UP" ||
                  ARType === "TransParentVideo" ||
                  (ARType === "360Video" && IsYoutube !== "True")) ? (
                  <div className={classes.labelText}>
                    <hr className={classes.straightLine} />
                    <p className={classes.uploadText}>Update {ARType} file</p>
                    <div className="input-field">
                      <DropZone
                        className={classes.hideDropzone}
                        onDropAccepted={handleFileUpload}
                        onDropRejected={handleErrors}
                        multiple={false}
                        disabled={PostStatus === "InReview" || Buyers}
                        accept={
                          ARType === "Video" ||
                            ARType === "360Video" ||
                            ARType === "TransParentVideo"
                            ? "video/mp4"
                            : ARSource && ARSource === "ZF"
                              ? ".zip"
                              : ARSource && ARSource === "UP"
                                ? ".unitypackage"
                                : ARType === "FaceAR_2DImage"
                                  ? "image/jpeg, image/png"
                                  : ""
                        }
                      >
                        <button
                          className="btn waves-effect waves-light z-depth-3 light-green darken-1"
                          type="button"
                          disabled={PostStatus === "InReview"}
                        >
                          Upload again
                      </button>
                      </DropZone>
                      {newSelectedFile && (
                        <div>
                          <p className={classes.uploadedFileName}>
                            <span>{newSelectedFile[0].name}</span>
                          </p>
                        </div>
                      )}
                      {postNameError ? (
                        <div className="center red-text">
                          <p>{postNameError}</p>
                        </div>
                      ) : null}
                    </div>
                  </div>
                ) : (
                  <div className="input-field">
                    <label
                      htmlFor="arContentUrl"
                      className={arContentUrl ? "active" : null}
                    >
                      AR Content URL
                  </label>
                    <input
                      id="arContentUrl"
                      type="text"
                      disabled={PostStatus === "InReview" || Buyers}
                      defaultValue={arContentUrl}
                      onChange={handleContentUrlChange}
                    />
                  </div>
                )}

              {profile.User_Type === "Prime" ? (
                <div className="input-field">
                  <label
                    htmlFor="emailForward"
                    className={postForwardEmail.value ? "active" : null}
                  >
                    Transfer to user email
                  </label>
                  <input
                    type="text"
                    id="emailForward"
                    defaultValue={postForwardEmail.value}
                    onChange={postForwardEmail.onChange}
                    onBlur={handleEmailCheck}
                    disabled={PostStatus !== "TestMode"}
                  />
                  <div className="center red-text">
                    {authError ? <p>{authError}</p> : null}
                  </div>
                  <div className="input-field">
                    {forwardUser && (
                      <button
                        className="btn blue lighten-1 z-depth-0"
                        disabled={
                          authError || PostStatus !== "TestMode" ? true : false
                        }
                      >
                        Transfer Post
                      </button>
                    )}
                  </div>
                  <Tooltip
                    title="You can transfer the post ownership to another user with the user's email registered in ARize"
                    classes={{ tooltip: classes.customWidth }}
                    placement="top"
                  >
                    <Icon className={classes.infoIcon}>info</Icon>
                  </Tooltip>
                </div>
              ) : null}
              {xrGallery ? (
                <>
                  <PostTags
                    tags={tags}
                    tagsError={tagsError}
                    addTags={handleAddTags}
                    deleteTags={handleDeleteTags}
                    handleBlur={handleTagsBlur}
                  />
                  {!tags.length && xrGallery ? (
                    <p className={classes.tagError}>
                      Please add tags to your post!
                    </p>
                  ) : null}
                </>
              ) : null}

              <div className="input-field">
                <p
                  className={classes.labelCopyPostId}
                  onClick={copyToClipboard}
                >
                  Click to copy Post ID
                </p>
                <svg
                  xmlns="https://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  onClick={copyToClipboard}
                  className={classes.copyTextIcon}
                >
                  <path d="M16 1H4c-1.1 0-2 .9-2 2v14h2V3h12V1zm3 4H8c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h11c1.1 0 2-.9 2-2V7c0-1.1-.9-2-2-2zm0 16H8V7h11v14z" />
                </svg>

                <input
                  type="text"
                  name="postID"
                  style={{ opacity: 0 }}
                  defaultValue={PostID}
                  ref={textAreaRef}
                  readOnly
                />
                <p className={copySuccess && classes.clipboardText}>
                  {copySuccess}
                </p>
              </div>
              {(ARType === "3DBundle" || ARType === "FaceAR_2DImage") &&
                profile.User_IsCreator === "True" && (
                  <CheckElement
                    title="Model Price"
                    name="isPaid"
                    defaultValue={paid || "False"}
                    required={true}
                    handleChange={changePaid}
                    disabled={userLimits.paidPostsLimit && !IsPaid}
                    options={[
                      {
                        label: "Free",
                        value: "False"
                      },
                      {
                        label: "Paid",
                        value: "True"
                      }
                    ]}
                  />
                )}
              {userLimits.paidPostsLimit && !IsPaid && (
                <div className="red-text">{renderUserWarningText()}</div>
              )}
              {paid === "True" && (
                <PriceElement price={price} handleChange={changePrice} />
              )}
              {postPrivacy && (
                <CheckElement
                  title="Post Availability"
                  name="postPrivacy"
                  defaultValue={postPrivacy}
                  required={true}
                  disabled={!!Buyers}
                  handleChange={changePostPrivacy}
                  options={[
                    {
                      label: "Public",
                      value: "Public"
                    },
                    {
                      label: "Private",
                      value: "PrivateToMe"
                    },
                    {
                      label: "Unlisted",
                      value: "Unlisted"
                    }
                  ]}
                />
              )}
              {ARType === "3DBundle" && PostStatus !== "InReview" ? (
                <div>
                  {ARSource === "GP" && (
                    <FormControlLabel
                      control={
                        <Checkbox
                          checked={!!soundReactivity}
                          onChange={handleSoundReactivity}
                          id="soundReactivity"
                        />
                      }
                      label="Sound Reactivity"
                    />
                  )}
                  {(ARSource === "GP" || ARSource === "SF") && (
                    <React.Fragment>
                      <DropZone
                        className={classes.dropzone}
                        onDropAccepted={handleSoundUpload}
                        onDropRejected={handleErrors}
                        multiple={false}
                        accept={"audio/mp3, audio/mpeg"}
                        id="sound"
                      >
                        <button
                          type="button"
                          className={`btn waves-effect waves-light z-depth-3 light-green darken-1 ${classes.uploadMoreButton}`}
                        >
                          {AudioFile ? "Upload again" : "Upload MP3"}
                        </button>
                        <p>
                          {(soundReactivityFile &&
                            soundReactivityFile[0].name) ||
                            (AudioFile && `...${AudioFile.substr(-8)}`)}
                        </p>
                      </DropZone>
                      <div className="red-text center">
                        {soundError ? <p>{soundError}</p> : null}
                      </div>
                    </React.Fragment>
                  )}
                </div>
              ) : null}
              {ARType === "3DBundle" && ARTargetID ? (
                <FormControlLabel
                  control={
                    <Checkbox checked={!!xrGallery} onChange={changeType} />
                  }
                  label="Add in AR Gallery"
                />
              ) : null}
              <FormControlLabel
                control={
                  <Checkbox
                    checked={!!postExtTracking}
                    onChange={handleExtTrackingCheckbox}
                  />
                }
                label="Extended Tracking"
              />
              {ARType === "TransParentVideo" ||
                ARType === "TransParentYoutubeVideo" ||
                ARType === "Video" ||
                ARType === "Youtube" ? (
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={postCutOutColor}
                        onChange={handleCutOutCheckbox}
                      />
                    }
                    label="This is a chromakey video"
                  />
                ) : null}
              {postCutOutColor ? (
                <ColorPicker
                  name="cutOutColor"
                  defaultValue={videoCutOutColor}
                  label="Pick up transparent color"
                  required={false}
                  handleChange={handleCutOutColorChange}
                />
              ) : null}
              {ARType === "3DBundle" ? (
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={!!fusionMode}
                      onChange={handleFusionMode}
                    />
                  }
                  label="Fusion mode"
                />
              ) : null}
              <div className="input-field">
                <button
                  className={`btn pink lighten-1 z-depth-0 ${classes.saveBtn}`}
                  disabled={isDisabled}
                >
                  Save changes
                </button>
                <div className="center red-text" />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  } else if (post === undefined) {
    return <PostNotFound message="Post was not found" />;
  } else {
    return <PostLoader />;
  }
};

const mapStateToProps = (state, props) => {
  const postId = props.match.params.id;
  return {
    post: select_post_by_id(postId)(state),
    auth: selectAuthUser(state),
    profile: selectProfile(state),
    authError: selectAuthError(state),
    forwardUser: selectForwardUser(state),
    somethingWrong: selectWrong(state),
    userLimits: selectUserLimitations(state)
  };
};
const mapDispatchToProps = dispatch => {
  return {
    editPost: (data, history) => dispatch(editPost(data, history)),
    checkUserEmail: (email, postName) =>
      dispatch(checkUserEmail(email, postName)),
    somethingWrongAction: () => dispatch(somethingWrong()),
    clearAuthError: () => dispatch(clearAuthError()),
    clearUserEmail: () => dispatch(clearUserEmail())
  };
};

EditPost.propTypes = {
  post: PropTypes.object
};

export default connect(mapStateToProps, mapDispatchToProps)(EditPost);

function useFormInput(initialValue) {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  };
  return {
    value,
    onChange: handleChange
  };
}
