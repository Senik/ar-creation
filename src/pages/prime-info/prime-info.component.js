import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import axios from "axios";
//selectors
import { selectAuthUser } from "../../redux/auth/auth.selectors";
import { selectAllUsers } from "../../redux/user/user.selectors";
import { selectAllPosts } from "../../redux/post/post.selectors";
//components
import PostsContainer from "../../components/shared/PostsContainer";
import PostLoader from "../../components/shared/PostLoader";
//MUI
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

function PrimeInfo(props) {
  const [num_subscribers, setSubscribers] = useState(null);
  const [num_contact_us_users, setContactUsUsers] = useState(null);

  useEffect(() => {
    let isSubscribed = true;
    async function getUsersData() {
      try {
        const res = await axios.post(
          "https://cors-anywhere.herokuapp.com/https://arize.io/assets/php/getAllSubscribers.php"
        );
        setSubscribers(res.data.subscribers);
        setContactUsUsers(res.data.contact_us_users);
      } catch (err) {
        console.log(err);
      }
    }
    if (isSubscribed) {
      getUsersData();
    }
    return () => {
      isSubscribed = false;
    };
  }, []);

  const { auth, users, posts } = props;
  if (auth.uid !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
    return <Redirect to="/" />;
  }
  if (users && posts) {
    return (
      <PostsContainer>
        <p>Users info</p>
        <Paper style={{ overflowX: "scroll" }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>All Users</TableCell>
                <TableCell>Creators</TableCell>
                <TableCell>Prime Users</TableCell>
                <TableCell>Subscribers</TableCell>
                <TableCell>Contact us Users</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>{users.length}</TableCell>
                <TableCell>
                  {users.filter(user => user.User_IsCreator).length}
                </TableCell>
                <TableCell>
                  {users.filter(user => user.User_Type === "Prime").length}
                </TableCell>
                <TableCell>{num_subscribers}</TableCell>
                <TableCell>{num_contact_us_users}</TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>

        <p>Posts info</p>
        <Paper style={{ overflowX: "scroll" }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>All Posts</TableCell>
                <TableCell>Video</TableCell>
                <TableCell>Youtube Video</TableCell>
                <TableCell>360 Video</TableCell>
                <TableCell>3D Bundle</TableCell>
                <TableCell>Unity Package</TableCell>
                <TableCell>Face AR</TableCell>
                <TableCell>Link</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>{posts.length}</TableCell>
                <TableCell>
                  {
                    posts.filter(
                      post =>
                        post.ARType === "Video" ||
                        post.ARType === "TransParentVideo"
                    ).length
                  }
                </TableCell>
                <TableCell>
                  {
                    posts.filter(
                      post =>
                        post.ARType === "Youtube" ||
                        post.ARType === "TransParentYoutubeVideo"
                    ).length
                  }
                </TableCell>
                <TableCell>
                  {posts.filter(post => post.ARType === "360Video").length}
                </TableCell>
                <TableCell>
                  {posts.filter(post => post.ARType === "3DBundle").length}
                </TableCell>
                <TableCell>
                  {" "}
                  {
                    posts.filter(
                      post =>
                        post.ARType === "3DBundle" && post.ARSource === "UP"
                    ).length
                  }
                </TableCell>
                <TableCell>
                  {
                    posts.filter(
                      post =>
                        post.ARType === "FaceAR_2DImage" ||
                        post.ARType === "FaceAR_3DBundle"
                    ).length
                  }
                </TableCell>
                <TableCell>
                  {posts.filter(post => post.ARType === "WebURL").length}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
      </PostsContainer>
    );
  } else {
    return <PostLoader />;
  }
}

const mapStateToProps = createStructuredSelector({
  auth: selectAuthUser,
  users: selectAllUsers,
  posts: selectAllPosts
});

export default connect(mapStateToProps)(PrimeInfo);
