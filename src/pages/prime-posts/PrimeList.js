import React, { memo } from 'react';
//components
import PrimeSummary from './PrimeSummary';
import PostsMasonry from '../../components/shared/PostsMasonry';

const PrimeList = props => {
  const { posts } = props;
  let primePosts =
    posts &&
    posts.map(post => {
      return <PrimeSummary key={post.PostID} post={post} />;
    });
  return (
    <PostsMasonry>
      {primePosts.length > 0 && primePosts}
    </PostsMasonry>
  );
};

export default memo(PrimeList);
