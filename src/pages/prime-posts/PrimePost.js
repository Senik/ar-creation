import React, { PureComponent, Fragment } from "react";
import { Redirect, Link } from "react-router-dom";
import { connect } from "react-redux";
import Linkify from "react-linkify";
import ReactPLayer from "react-player";
import moment from "moment";
//selectors
import { select_post_by_id } from "../../redux/post/post.selectors";
import { selectAuthUser } from "../../redux/auth/auth.selectors";
import { selectNotification } from "../../redux/notification/notification.selectors";
//actions
import {
  changePostStatus,
  changePostStatusToInReview,
  deleteUserPost,
  changePostUpdateVersion,
  upload3DBundlePackage
} from "../../redux/prime/prime.actions";
//helpers
import {
  componentDecorator,
  VuforiaGetTarget,
  dynamicLink
} from "../../helpers";
//MUI
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
//MUI
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
//components
import SketchfabViewerIframe from "../../components/shared/SketchfabViewer";
import GooglePolyIframe from "../../components/shared/GooglePolyIframe";
import TargetDownloadButton from "../../components/shared/TargetDownloadButton";
import PostLoader from "../../components/shared/PostLoader";
import PostNotFound from "../../components/shared/PostNotFound";
//styles
import classes from "./Prime.module.scss";
import ButtonWithMessage from "../../components/formElements/ButtonWithMessage";
import FullPageLoader from "../../components/shared/FullPageLoader";

class PrimePost extends PureComponent {
  state = {
    url: "",
    path: "",
    showDownloadIcon: true,
    openSelect: false,
    report: "ios",
    showLoader: false
  };
  UNSAFE_componentWillReceiveProps(props) {
    const { post, notification } = props;
    if (post && post.ARTargetID) {
      const trackData = new FormData();
      trackData.append("targetID", post.ARTargetID);
      VuforiaGetTarget(trackData)
        .then(res => res.text())
        .then(text => {
          let index = text.indexOf("{");
          let responseData = JSON.parse(text.slice(index));
          if (responseData.status === "success") {
            this.setState({
              targetImageRating: responseData.target_record.tracking_rating
            });
          }
        })
        .catch(err => console.log(err));
    }
    if (!this.state.url && post) {
      this.setDownloadLink(post);
    }
    if (notification.isOpen) {
      this.setState({ showLoader: false });
    }
  }

  handlePostStatus = (message, id) => {
    const { post, history, changePostStatus } = this.props;
    dynamicLink(post)
      .then(res => res.json())
      .then(data => {
        const Link = data.shortLink;
        changePostStatus(post, message, Link, history, id);
      })
      .catch(err => {
        console.log(err);
      });
  };
  handlePostStatusToInReview = () => {
    const { post, history, changePostStatusToInReview } = this.props;
    changePostStatusToInReview(post.PostID, history);
  };
  handleDeletePost = message => {
    const { post, history, deleteUserPost } = this.props;
    this.setState({ showDownloadIcon: false });
    deleteUserPost(post, message, history);
  };

  setDownloadLink = post => {
    if (
      (post.ARSource && post.ARSource === "ZF") ||
      (post.ARSource && post.ARSource === "UP") ||
      post.ARType === "Video" ||
      post.ARType === "TransParentVideo" ||
      post.ARType === "360Video"
    ) {
      const { ARSource, ARSourceURL, ARType, ARContentURL, IsYoutube } = post;
      let url, path;
      if (ARSource === "ZF") {
        url = ARSourceURL;
        path =
          "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_3DModels/";
      } else if (ARSource === "UP") {
        url = ARSourceURL;
        path =
          "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Unity_Packages/";
      } else {
        if (ARType === "Video" || ARType === "TransParentVideo") {
          url = ARContentURL;
          path =
            "https://cors-anywhere.herokuapp.com/triplee.info/Triple_E_Videos/ARVideos/";
        } else if (ARType === "360Video" && IsYoutube !== "True") {
          url = ARContentURL;
          path =
            "https://cors-anywhere.herokuapp.com/triplee.info/Triple_E_360Videos/";
        }
      }
      this.setState({
        url,
        path
      });
    }
  };
  renderTargetImage = () => {
    const { ARTargetName, ARTargetID } = this.props.post;
    return (
      <img
        src={`https://triplee.info/${
          !ARTargetID
            ? "Triple_E_Thumbnail"
            : "Triple_E_WebService/AllImageTargets"
        }/${ARTargetName}.jpg`}
        onError={e =>
          (e.target.src =
            "https://triplee.info/Triple_E_WebService/AllImageTargets/correct3.jpg")
        }
        alt="Target"
        style={{ maxWidth: "100%" }}
      />
    );
  };
  renderContentView = () => {
    let showDownloadIcon = false;
    const { post } = this.props;
    const { ARSource, ARSourceURL, ARType, ARContentURL, IsYoutube } = post;
    if (ARType === "3DBundle") {
      switch (ARSource) {
        case "SF":
          return <SketchfabViewerIframe link={ARSourceURL} />;
        case "GP":
          return <GooglePolyIframe link={ARSourceURL} />;
        case "ZF":
        case "UP":
          return (
            <>
              <i
                className={
                  ARSource === "ZF"
                    ? "fas fa-file-archive ChooseContetnType-icon-218 fa-6x"
                    : "fas fa-file fa-6x"
                }
                style={{ marginBottom: 25 }}
              />
              <TargetDownloadButton
                url={this.state.url}
                path={this.state.path}
                styles={classes.loadingWrap}
              />
            </>
          );
        default:
          return null;
      }
    } else if (
      ARType === "Youtube" ||
      ARType === "360Video" ||
      ARType === "TransParentYoutubeVideo" ||
      ARType === "TransParentVideo" ||
      ARType === "Video"
    ) {
      let url = "";
      switch (ARType) {
        case "Video":
        case "TransParentVideo":
          showDownloadIcon = true;
          url = `https://triplee.info/Triple_E_Videos/ARVideos/${ARContentURL}`;
          break;
        case "360Video":
          if (IsYoutube === "False") {
            showDownloadIcon = true;
            url = `https://triplee.info/Triple_E_360Videos/${ARContentURL}`;
          } else {
            url = ARContentURL;
          }
          break;
        default:
          url = ARContentURL;
      }
      if (
        ARType !== "Video" &&
        ARType !== "360Video" &&
        ARType !== "TransParentVideo"
      ) {
        if (!url.includes("youtube.com")) {
          url = `https://www.youtube.com/watch?v=${ARContentURL}`;
        }
      }
      return (
        <>
          <ReactPLayer
            className={classes.primePlayer}
            width={"100%"}
            url={url}
            playing
            controls
            config={{
              youtube: {
                playerVars: { showinfo: 1 }
              }
            }}
          />
          {showDownloadIcon ? (
            <TargetDownloadButton
              url={this.state.url}
              path={this.state.path}
              styles={classes.loadingWrap}
            />
          ) : null}
        </>
      );
    } else if (ARType === "FaceAR_2DImage") {
      return (
        <img
          src={`https://triplee.info/Triple_E_FaceAR/2DFaceAR/${ARContentURL}`}
          onError={e =>
            (e.target.src =
              "https://triplee.info/Triple_E_WebService/AllImageTargets/correct3.jpg")
          }
          alt="AR Content"
          style={{ maxWidth: "100%" }}
        />
      );
    }
  };
  handleInputChange = e => {
    if (e.target.id === "packageFile") {
      return this.setState({
        [e.target.id]: e.target.files[0]
      });
    }
    this.setState({
      [e.target.id]: e.target.value
    });
  };
  handlePostUpdate = () => {
    const { updateVersion } = this.state;
    const { post, changePostUpdateVersion } = this.props;
    changePostUpdateVersion(post, updateVersion);
  };
  handleSelectOpen = () => {
    this.setState({
      openSelect: true
    });
  };
  handleSelectClose = () => {
    this.setState({
      openSelect: false
    });
  };
  handleSelectChange = e => {
    const { name, value } = e.target;
    this.setState({
      [name]: value
    });
  };
  handlePackageUpload = e => {
    e.preventDefault();
    const { packageFile, report } = this.state;
    const { post, upload3DBundlePackage } = this.props;
    upload3DBundlePackage(post, report, packageFile);
    this.setState({ showLoader: true });
  };
  render() {
    const self = this;
    const { post, auth } = self.props;
    const { report, openSelect, showLoader } = self.state;
    if (auth && auth.uid !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
      return <Redirect to="/" />;
    }
    if (post) {
      const postKeys = Object.keys(post);
      let tableData = postKeys.map(key => {
        if (
          key === "Position" ||
          key === "Rotation" ||
          key === "Scale" ||
          key === "Buyers" ||
          key === "UniqueViewes" ||
          key === "NumberOfViews" ||
          typeof post[key] === "object"
        ) {
          return null;
        }

        return (
          <TableRow key={key}>
            <TableCell component="th" scope="row">
              {key}
            </TableCell>
            <TableCell align="right" className={classes.tableRow}>
              <Linkify componentDecorator={componentDecorator}>
                {post[key] === "TestMode" ? (
                  "Published"
                ) : key === "Update" ? (
                  <React.Fragment>
                    <button
                      className="btn green"
                      onClick={this.handlePostUpdate}
                    >
                      Update
                    </button>
                    <input
                      type="number"
                      id="updateVersion"
                      style={{
                        width: "50px",
                        padding: "0 15px",
                        margin: "0 10px"
                      }}
                      defaultValue={post[key]}
                      min="0"
                      onChange={this.handleInputChange}
                    />
                  </React.Fragment>
                ) : key === "LastEdit" ||
                  key === "SubmissionDate" ||
                  key === "ReviewDate" ? (
                  moment(post[key]).format("MMMM Do YYYY")
                ) : key === "PostAuthorID" ? (
                  <Link to={`/prime/user/${post[key]}/posts`}>{post[key]}</Link>
                ) : (
                  post[key]
                )}
              </Linkify>
            </TableCell>
          </TableRow>
        );
      });
      return (
        <div className={classes.Prime}>
          {showLoader ? (
            <FullPageLoader showChildren={true}>
              Uploading Package
            </FullPageLoader>
          ) : null}

          <div className="row">
            <div className="col s12 m8 offset-m4 l10 offset-l2">
              <Paper className={classes.root}>
                <Table className={classes.table}>
                  <TableBody>
                    {tableData}
                    {post.AudioFile && (
                      <TableRow>
                        <Fragment>
                          <TableCell>AudioFile</TableCell>
                          <TableCell align="right">
                            <TargetDownloadButton
                              url={post.AudioFile}
                              styles={classes.loadingWrap}
                              path="https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Audio/"
                            />
                          </TableCell>
                        </Fragment>
                      </TableRow>
                    )}
                    {post.ARTargetID && (
                      <TableRow>
                        <TableCell>Target image Trackability</TableCell>
                        <TableCell align="right">
                          {self.state.targetImageRating}
                        </TableCell>
                      </TableRow>
                    )}
                  </TableBody>
                </Table>
              </Paper>
            </div>
            {post.ARType === "3DBundle" && (
              <div className="col s12 m8 offset-m4 l10 offset-l2 ">
                <form
                  onSubmit={this.handlePackageUpload}
                  className="form white"
                >
                  <h6
                    style={{
                      textAlign: "center",
                      marginBottom: "30px"
                    }}
                  >
                    Upload Packages
                  </h6>

                  <input
                    type="file"
                    id="packageFile"
                    required={true}
                    onChange={this.handleInputChange}
                  />
                  <FormControl style={{ minWidth: 180 }}>
                    <InputLabel htmlFor="report">
                      Choose Package Type
                    </InputLabel>
                    <Select
                      open={openSelect}
                      onClose={self.handleSelectClose}
                      onOpen={self.handleSelectOpen}
                      value={report}
                      onChange={self.handleSelectChange}
                      inputProps={{
                        name: "report",
                        id: "report"
                      }}
                    >
                      <MenuItem value={"ios"}>IOS</MenuItem>
                      <MenuItem value={"android"}>Android</MenuItem>
                    </Select>
                  </FormControl>
                  <input type="submit" value="Upload" className="btn green" />
                </form>
              </div>
            )}

            <div className="col s12 m8 offset-m4 l10 offset-l2">
              <div className={`row ${classes.postTargetContent}`}>
                <div className="col l6 m12">{self.renderTargetImage()}</div>
                <div className={`col l6 m12 ${classes.contentBox}`}>
                  {self.renderContentView()}
                </div>
              </div>
            </div>
            <div className="col s12 m8 offset-m4 l10 offset-l2">
              <Grid container justify="center">
                <Grid item>
                  <ButtonWithMessage
                    name="MAKE PUBLIC"
                    cmpClass="btn blue white-text"
                    onChange={self.handlePostStatus}
                    disabled={post.PostStatus !== "InReview"}
                  />
                  {post.PostStatus === "Rejected" && (
                    <button
                      className={`btn orange white-text ${classes.toInReview}`}
                      onClick={self.handlePostStatusToInReview}
                    >
                      IN REVIEW
                    </button>
                  )}
                  <ButtonWithMessage
                    name="REJECT"
                    cmpClass="btn #e53935 red darken-1 lighten-1 z-depth-3"
                    onChange={self.handlePostStatus}
                    disabled={post.PostStatus === "Rejected"}
                  />
                  <ButtonWithMessage
                    name="DELETE POST"
                    cmpClass="btn #e53935 red darken-1 lighten-1 z-depth-3"
                    onChange={self.handleDeletePost}
                  />
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
      );
    } else if (typeof post === "undefined" && self.state.showDownloadIcon) {
      return <PostNotFound message="Post was not found" />;
    } else {
      return <PostLoader />;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const postID = ownProps.match.params.id;
  return {
    auth: selectAuthUser(state),
    notification: selectNotification(state),
    post: select_post_by_id(postID)(state)
  };
};

export default connect(mapStateToProps, {
  changePostStatus,
  changePostStatusToInReview,
  deleteUserPost,
  changePostUpdateVersion,
  upload3DBundlePackage
})(PrimePost);
