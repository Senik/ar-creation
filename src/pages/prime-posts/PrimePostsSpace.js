import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ClipLoader } from "react-spinners";
import InfiniteScroll from "react-infinite-scroll-component";
import { createStructuredSelector } from "reselect";
//selectors
import { selectAuthUser, selectProfile } from "../../redux/auth/auth.selectors";
import {
  selectPrimePosts,
  selectPostsBySubscribtionPlan
} from "../../redux/prime/prime.selectors";
import { selectKeyword } from "../../redux/post/post.selectors";
//actions
import {
  setSearchLength,
  clearSearchLength,
  setKeyword,
  clearKeyword
} from "../../redux/post/post.actions";
//helpers
import { filterByTab, filterByKeyword } from "../../helpers/";
//components
import PrimeList from "./PrimeList";
import PostsContainer from "../../components/shared/PostsContainer";
import PostLoader from "../../components/shared/PostLoader";
import AppBarElement from "../../components/shared/AppBarElement";
import SelectARType from "../../components/selectARType/SelectARType";
//styles
import classes from "./Prime.module.scss";

const postTypes = [
  "All",
  "3DBundle",
  "Video",
  "TransParentVideo",
  "TransParentYoutubeVideo",
  "360Video",
  "Youtube",
  "360YoutubeVideo",
  "WebURL",
  "FaceAR_2DImage",
  "FaceAR_3DBundle"
];

class PrimeSpace extends Component {
  state = {
    prevPostIndex: 0,
    nextPostIndex: 9,
    showPosts: 0,
    showPostsType: [...postTypes],
    filteredPosts: [],
    changedType: false
  };
  componentDidMount() {
    document.title = "Prime space Posts";
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      prevProps.keyword !== this.props.keyword ||
      prevState.showPosts !== this.state.showPosts
    ) {
      const { posts, keyword, setSearchLength } = this.props;
      const filteredPosts = this.filterPostsByTab(
        filterByKeyword(
          posts,
          ["PostAuthor", "PostAuthorNickName", "PostName", "PostText"],
          keyword,
          "PostAuthor"
        )
      );
      this.setState({ filteredPosts });
      setSearchLength(filteredPosts.length);
    }
    if (prevState.showPostsType !== this.state.showPostsType) {
      const { posts, keyword, setSearchLength } = this.props;
      const filteredPosts = filterByKeyword(
        posts,
        ["PostAuthor", "PostAuthorNickName", "PostName", "PostText"],
        keyword,
        "PostAuthor"
      );
      const filterByType = this.filterByPostType(
        this.state.showPostsType,
        this.filterPostsByTab(filteredPosts)
      );
      this.setState({
        filteredPosts: filterByType
      });
      setSearchLength(filterByType.length);
    }
  }
  componentWillUnmount() {
    const { clearSearchLength, clearKeyword } = this.props;
    clearSearchLength();
    clearKeyword();
  }
  handleSelectChange = e => {
    const value = e.target.value;
    const { showPostsType } = this.state;
    if (value.includes("All") && !showPostsType.includes("All")) {
      this.setState({
        showPostsType: [...postTypes],
        changedType: false
      });
      return;
    } else if (!value.includes("All") && showPostsType.includes("All")) {
      this.setState({
        showPostsType: [],
        changedType: false
      });
      return;
    } else if (value.includes("All")) {
      let newTypes = [...value];
      let allIndex = newTypes.indexOf("All");
      newTypes.splice(allIndex, 1);
      this.setState({
        showPostsType: newTypes,
        changedType: true
      });
      return;
    }
    this.setState({
      showPostsType: value,
      changedType: true
    });
  };
  handleTabChange = (event, newValue) => {
    this.setState({ showPosts: newValue });
  };
  scrollTop = () => {
    window.scroll({ top: 0, left: 0, behavior: "smooth" });
  };
  fetchMoreData = () => {
    let test = this.state.nextPostIndex + 9;
    this.setState({
      nextPostIndex: test
    });
  };
  filterPostsByTab = posts => {
    if (posts) {
      return filterByTab(posts, this.state.showPosts);
    }
  };
  filterByPostStatus = status => {
    let filteredPosts = this.props.posts.filter(
      post => post.PostStatus === status
    );
    return filteredPosts;
  };
  filterByPostType = (types, posts) => {
    if (types.includes("All")) {
      return posts;
    }
    let result = posts.filter(post => {
      let checking = false;
      for (let i = 0; i < types.length; i++) {
        if (post.ARType === types[i]) {
          checking = true;
          break;
        }
      }
      return checking;
    });
    return result;
  };

  render() {
    const self = this;
    const { posts, keyword, auth } = self.props;
    if (auth.uid !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
      return <Redirect to="/" />;
    }
    if (posts && posts.length === 0) {
      return (
        <div className="col s12 m8 offset-m4 l10 offset-l3 xl10 offset-xl2">
          <p className="center-align">There are no posts to show</p>
        </div>
      );
    }
    const {
      prevPostIndex,
      nextPostIndex,
      showPosts,
      showPostsType,
      filteredPosts,
      changedType
    } = self.state;
    const postsToShow = keyword[0] || changedType ? filteredPosts : posts;
    if (postsToShow && posts.length > 0) {
      let tabs = [
        `Published ${
          self.filterByPostType(
            showPostsType,
            self.filterByPostStatus("TestMode")
          ).length
        }`,
        `In Review ${
          self.filterByPostType(
            showPostsType,
            self.filterByPostStatus("InReview")
          ).length
        }`,
        `Rejected ${
          self.filterByPostType(
            showPostsType,
            self.filterByPostStatus("Rejected")
          ).length
        }`
      ];
      return (
        <PostsContainer
          cmpClassName={classes.PrimeSpace}
          scrollToTop={self.scrollTop}
        >
          <AppBarElement
            value={showPosts}
            tabChange={self.handleTabChange}
            variant="fullWidth"
            tabs={tabs}
          />
          <SelectARType
            showPostsType={showPostsType}
            selectChange={self.handleSelectChange}
            postTypes={postTypes}
            label="Select post type"
          />
          <InfiniteScroll
            dataLength={
              self
                .filterByPostType(
                  showPostsType,
                  self.filterPostsByTab(postsToShow)
                )
                .slice(prevPostIndex, nextPostIndex).length
            }
            next={self.fetchMoreData}
            hasMore={
              nextPostIndex <
              self.filterByPostType(
                showPostsType,
                self.filterPostsByTab(postsToShow)
              ).length
            }
            loader={<ClipLoader sizeUnit={"px"} size={150} color={"#123abc"} />}
            style={{ overflow: "hidden" }}
            endMessage={
              <p style={{ textAlign: "center" }}>
                <b style={{ color: "black" }}>Yay! You have seen it all</b>
              </p>
            }
          >
            <PrimeList
              posts={self
                .filterByPostType(
                  showPostsType,
                  self.filterPostsByTab(postsToShow)
                )
                .slice(prevPostIndex, nextPostIndex)}
            />
          </InfiniteScroll>
        </PostsContainer>
      );
    } else {
      return <PostLoader />;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const subScriptionType = ownProps.location.search.substring(1);
  return {
    auth: selectAuthUser(state),
    profile: selectProfile(state),
    posts: selectPostsBySubscribtionPlan(subScriptionType)(state),
    keyword: selectKeyword(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setKeyword: keyword => dispatch(setKeyword(keyword)),
    setSearchLength: searchLength => dispatch(setSearchLength(searchLength)),
    clearSearchLength: () => dispatch(clearSearchLength()),
    clearKeyword: () => dispatch(clearKeyword())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PrimeSpace);
