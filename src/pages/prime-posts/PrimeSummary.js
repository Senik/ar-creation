import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import moment from "moment";
import Linkify from "react-linkify";
import { Link } from "react-router-dom";
//actions
import { alertNotification } from "../../redux/auth/auth.actions";
//helpers
import { renderIcon, renderType, _createDynamicLink } from "../../helpers/";
//MUI
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
//components
import PostInfoIcons from "../../components/postIcons/PostInfoIcons";
//icons
import DefaultAvatar from "../../default_avatar.png";
//styles
import classes from "./Prime.module.scss";

function PrimeSummary(props) {
  const renderContentIcon = () => {
    const { post } = props;
    if (post) {
      return renderIcon(post);
    }
  };
  const renderPostType = () => {
    const { post } = props;
    if (post) {
      return renderType(post);
    }
  };
  const renderTargetImage = () => {
    const { post } = props;
    if (post) {
      return (
        <div className="card-image">
          <img
            className={classes.cardImage}
            src={`https://triplee.info/${
              !post.ARTargetID
                ? "Triple_E_Thumbnail"
                : "Triple_E_WebService/AllImageTargets"
            }/${post.ARTargetName}.jpg`}
            onError={e =>
              (e.target.src =
                "https://triplee.info/Triple_E_WebService/AllImageTargets/correct3.jpg")
            }
            alt="Trigger"
          />
        </div>
      );
    }
  };
  const componentDecorator = (href, text, key) => (
    <a href={href} key={key} target="_blank" rel="noopener noreferrer">
      {text}
    </a>
  );
  const createDynamicLink = post => {
    _createDynamicLink(post, props.alertNotification);
  };

  const { post } = props;
  const avatarImageUrl =
    post &&
    `https://triplee.info/Triple_E_Social/ProfilePictures/${post.PostAuthorID}.jpg`;
  return (
    <Card className={`${classes.card} ${classes.PrimeSummary}`}>
      <CardHeader
        avatar={
          <Avatar
            aria-label="Avatar-Image"
            src={post.PostAuthorID && avatarImageUrl}
            onError={e => (e.target.src = DefaultAvatar)}
            className={classes.avatar}
          />
        }
        title={post.PostAuthor}
        subheader={
          <div>
            <span>{moment(post.LastEdit).format("MMMM Do YYYY")}</span>
            <p style={{ margin: 0 }}>
              {renderPostType()}
              {renderContentIcon()}
            </p>
          </div>
        }
      />
      <div className="card center-align">
        <div className={`${classes.postName} card-content`}>
          <span className="card-title center-align">{post.PostName}</span>
          {post.IsPaid === "True" && (
            <span className={`${classes.postPrice} center-align`}>
              &euro;{post.Price}
            </span>
          )}
        </div>
        {renderTargetImage()}
      </div>
      <CardContent>
        <Typography component="p" style={{ marginBottom: "20px" }}>
          <Linkify componentDecorator={componentDecorator}>
            {post.PostText}
          </Linkify>
        </Typography>
        <CardActions className={classes.actions}>
          <PostInfoIcons post={post} dynamicLinkCreation={createDynamicLink} />
          <Typography className={classes.btnPosition}>
            <Link to={`/prime/posts/${post.PostID}`}>
              <Button
                className={`btn-large indigo white-text waves-effect waves-light z-depth-3 ${classes.alignButtons}`}
              >
                View
              </Button>
            </Link>
          </Typography>
        </CardActions>
      </CardContent>
    </Card>
  );
}

export default compose(connect(null, { alertNotification }))(
  React.memo(PrimeSummary)
);
