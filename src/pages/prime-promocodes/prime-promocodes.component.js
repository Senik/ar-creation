import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { createStructuredSelector } from "reselect";
import axios from "axios";
import MaterialTable from "material-table";
import firebase from "../../config/fbConfig";
import moment from "moment";
//selectors
import { selectAuthUser } from "../../redux/auth/auth.selectors";
import {
  selectAllUsers,
  selectPromocodes
} from "../../redux/user/user.selectors";
//actions
import { getAllPromocodes } from "../../redux/user/user.actions";
//helpers
import { generatePromoCode } from "../../helpers";
import PostLoader from "../../components/shared/PostLoader";
//MUI
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";

//styles
import "./prime-promocodes.styles.scss";

const PrimePromo = props => {
  const { auth, users, promos, getAllPromocodes } = props;
  const [state, setState] = useState({
    columns: [
      { title: "Code", field: "code", editable: "never" },
      {
        title: "Attached User Email",
        field: "AttachedUserEmail",
        editable: "onAdd"
      },
      { title: "Discount in %", field: "Discount", type: "numeric" },
      {
        title: "Package Name",
        field: "PackageName",
        emptyValue: "Plus",
        initialEditValue: "Plus",
        editable: "onAdd"
      },
      {
        title: "Activation Limit",
        field: "ActivationLimit",
        type: "numeric",
        initialEditValue: "1",
        emptyValue: 1
      },
      {
        title: "Activation Count",
        field: "ActivationCount",
        type: "numeric",
        editable: "never"
      },
      {
        title: "Package Duration months",
        field: "PackageDuration",
        type: "numeric",
        editable: "onAdd",
        emptyValue: "",
        initialEditValue: "1"
      },
      {
        title: "Creation Date",
        field: "DateTime",
        editable: "never",
        type: "date"
      },
      {
        title: "Expiration Date",
        field: "ExpirationDate",
        type: "date"
      }
    ],
    data: []
  });
  useEffect(() => {
    let isSubscribed = true;
    if (isSubscribed) {
      getAllPromocodes();
    }

    return () => {
      isSubscribed = false;
    };
  }, []);
  useEffect(() => {
    if (promos) {
      const promosArr = [];
      Object.keys(promos).forEach(key => {
        if (promos[key]) {
          let activationCount = promos[key].Users
            ? Object.values(promos[key].Users).length
            : 0;
          promosArr.push({
            ...promos[key],
            code: key,
            ActivationCount: activationCount
          });
        }
      });
      setState({ ...state, data: promosArr });
    }
    return () => {
      setState({ ...state, data: [] });
    };
  }, [promos]);

  const renderRowDetails = rowData => {
    if (!rowData.Users) {
      return <p className="center-align">No Details to show!</p>;
    }
    return (
      <Table className="#bdbdbd grey lighten-1">
        <TableHead>
          <TableRow>
            <TableCell>User ID</TableCell>
            <TableCell>Package Name</TableCell>
            <TableCell>Payment Period</TableCell>
            <TableCell>Bought at</TableCell>
            <TableCell>Amount</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {Object.keys(rowData.Users).map(key => {
            return (
              <TableRow key={key}>
                <TableCell>{key}</TableCell>
                <TableCell>{rowData.Users[key].PackageName}</TableCell>
                <TableCell>{rowData.Users[key].PaymentPeriod}</TableCell>
                <TableCell>{rowData.Users[key].BoughtAt}</TableCell>
                <TableCell>{rowData.Users[key].Amount}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    );
  };
  if (auth.uid !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
    return <Redirect to="/" />;
  }
  if (users) {
    return (
      <div className="col s12 m8 offset-m4 l10 offset-l3 xl10 offset-xl3 promo-table">
        <h3 className="center-align">PROMOCODES</h3>
        <MaterialTable
          title="Promocodes"
          detailPanel={rowData => {
            return renderRowDetails(rowData);
          }}
          columns={state.columns}
          data={state.data}
          onRowClick={(event, rowData, togglePanel) => togglePanel()}
          editable={{
            onRowAdd: newData => {
              return new Promise((resolve, reject) => {
                (async () => {
                  try {
                    let promoCode = generatePromoCode(newData.PackageName);
                    const newPromo = {
                      PackageName: newData.PackageName,
                      ActivationLimit: newData.ActivationLimit || null,
                      AttachedUserEmail: newData.AttachedUserEmail,
                      Discount: newData.Discount,
                      DateTime: new Date().toISOString(),
                      ExpirationDate: newData.ExpirationDate
                        ? newData.ExpirationDate.toISOString()
                        : null
                    };
                    if (newData.Discount == 100) {
                      newPromo.PackageDuration = newData.PackageDuration;
                    }
                    await firebase
                      .database()
                      .ref(`Promocodes/${promoCode}`)
                      .set(newPromo);
                    const url =
                      "https://triplee.info/Triple_E_Payment/promocode_send_notif_email_user.php";
                    const data = new FormData();
                    data.append("user_email", newData.AttachedUserEmail);
                    data.append("coupon", promoCode);
                    data.append("coupon_discount", newData.Discount);
                    data.append("package_name", newData.PackageName);
                    if (
                      newData.ActivationLimit &&
                      newData.ActivationLimit > 1
                    ) {
                      data.append("activation_limit", newData.ActivationLimit);
                    }
                    if (newData.ExpirationDate) {
                      data.append(
                        "expiration_date",
                        moment(newData.ExpirationDate.toISOString()).format(
                          "MM-DD-YYYY"
                        )
                      );
                    }
                    if (newData.Discount == 100) {
                      data.append("package_duration", newPromo.PackageDuration);
                    }
                    const res = await axios.post(url, data);
                    if (res.data.status === "success") {
                      resolve();
                    } else {
                      reject("fail");
                    }
                  } catch (err) {
                    reject(err);
                  }
                })();
              });
            },

            onRowUpdate: (newData, oldData) => {
              return new Promise((resolve, reject) => {
                (async () => {
                  try {
                    const updatedPromo = {
                      ActivationLimit: newData.ActivationLimit || null,
                      Discount: newData.Discount,
                      DateTime: new Date().toISOString(),
                      ExpirationDate: newData.ExpirationDate
                        ? newData.ExpirationDate.toISOString()
                        : null
                    };
                    await firebase
                      .database()
                      .ref(`Promocodes/${oldData.code}`)
                      .update(updatedPromo);
                    const url =
                      "https://triplee.info/Triple_E_Payment/promocode_send_notif_email_user.php";
                    const data = new FormData();
                    data.append("user_email", oldData.AttachedUserEmail);
                    data.append("coupon", oldData.code);
                    data.append("coupon_discount", newData.Discount);
                    data.append("package_name", newData.PackageName);
                    if (
                      newData.ActivationLimit &&
                      newData.ActivationLimit > 1
                    ) {
                      data.append("activation_limit", newData.ActivationLimit);
                    }
                    if (newData.ExpirationDate) {
                      data.append(
                        "expiration_date",
                        moment(newData.ExpirationDate.toISOString()).format(
                          "MM-DD-YYYY"
                        )
                      );
                    }
                    const res = await axios.post(url, data);
                    if (res.data.status === "success") {
                      resolve();
                    } else {
                      reject("fail");
                    }
                  } catch (err) {
                    reject(err);
                  }
                })();
              });
            }
          }}
        />
      </div>
    );
  } else {
    return <PostLoader />;
  }
};

const mapStateToProps = createStructuredSelector({
  auth: selectAuthUser,
  users: selectAllUsers,
  promos: selectPromocodes
});

export default connect(mapStateToProps, { getAllPromocodes })(PrimePromo);
