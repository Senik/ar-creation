import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { Link, Redirect } from "react-router-dom";
//helpers
import { countCreatorAmount, validURL } from "../../helpers";
//selectors
import { selectProfile, selectAuthUser } from "../../redux/auth/auth.selectors";
import { selectUserById } from "../../redux/user/user.selectors";
//actions
import {
  changeUserStatus,
  cahngeUserPayoutStatus,
  changePremiumData
} from "../../redux/prime/prime.actions";
//MUI
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
//component
import FullPageLoader from "../../components/shared/FullPageLoader";
import PostNotFound from "../../components/shared/PostNotFound";
import PostLoader from "../../components/shared/PostLoader";
import ButtonWithMessage from "../../components/formElements/ButtonWithMessage";
//icons
import googlepoly from "../../assets/img/icons/polyIcon.png";
import sketchfab from "../../assets/img/icons/sketchfablogo.png";
//styles
import "./PrimeUser.styles.scss";
import { TableHead } from "@material-ui/core";

function PrimeUser(props) {
  const [showLoader, setLoader] = useState(false);
  const [premium, setPremium] = useState({ price: "", discount: "" });

  useEffect(() => {
    const { user } = props;
    if (user && user.Premium_Package_Discount && user.Premium_Package_Price) {
      const { Premium_Package_Discount, Premium_Package_Price } = user;
      setPremium({
        ...premium,
        price: Premium_Package_Price,
        discount: Premium_Package_Discount
      });
    }
  }, [props.user]);

  const handleUserStatus = e => {
    const { user, changeUserStatus, history } = props;
    changeUserStatus(user, e.target.name, history);
    setLoader(true);
  };
  const handleRejectForm = message => {
    const { user, changeUserStatus, history } = props;
    changeUserStatus(user, "False", history, message);
    setLoader(true);
  };
  const renderSocialLinks = () => {
    const { user } = props;
    let socialList;
    if (user) {
      const socialLinks = user.User_SocialLinks;
      socialList =
        socialLinks &&
        Object.keys(socialLinks).map(key => {
          if (socialLinks[key].length > 1) {
            return (
              <a
                key={key}
                href={
                  socialLinks[key].includes("http")
                    ? socialLinks[key]
                    : `https://${socialLinks[key]}`
                }
                target="_blank"
                rel="noopener noreferrer"
              >
                {key.toLowerCase() === "googlepoly" ||
                key.toLowerCase() === "sketchfab" ? (
                  key.toLowerCase() === "sketchfab" ? (
                    <img
                      src={sketchfab}
                      className="image-logo"
                      alt="sketchfab"
                    />
                  ) : (
                    <img
                      src={googlepoly}
                      className="image-logo"
                      alt="googlepoly"
                    />
                  )
                ) : (
                  <i className={`fab fa-${key.toLowerCase()} social-icon`} />
                )}
              </a>
            );
          }
          return null;
        });
    }

    return socialList;
  };
  const handlePayoutComplete = e => {
    e.preventDefault();
    const payoutId = e.target.id;
    const { user, changePayoutStatus } = props;
    changePayoutStatus(user, payoutId);
  };

  const handlePackageChange = e => {
    const { value, id } = e.target;
    setPremium({
      ...premium,
      [id]: value
    });
  };

  const handlePackageSubmit = () => {
    const { price, discount } = premium;
    const { changePremiumData, match, history } = props;
    changePremiumData(match.params.id, price, discount, history);
  };

  const { user, auth } = props;

  let tableData = null;
  let payOutTable = null;
  let subscriptionTable = null;
  if (auth && auth.uid !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
    return <Redirect to="/" />;
  }

  if (user) {
    const keys = Object.keys(user);
    tableData = keys.map(key => {
      if (key === "Creator_Credentials") {
        const bankInfoKeys = Object.keys(user.Creator_Credentials);
        let bankData = bankInfoKeys.map(bankInfoKey => (
          <TableRow key={bankInfoKey}>
            <TableCell component="th" scope="row">
              {bankInfoKey}
            </TableCell>
            <TableCell align="right" className="table-row">
              {user[key][bankInfoKey]}
            </TableCell>
          </TableRow>
        ));
        return bankData;
      }
      if (key === "Payout") {
        const payoutKeys = Object.keys(user.Payout);
        const payOutData = payoutKeys.map(payoutKey => {
          const { DateTime, Payout_Status, Amount, Invoice_URL } = user.Payout[
            payoutKey
          ];
          const date = new Date(DateTime);

          return (
            <TableRow key={payoutKey}>
              <TableCell align="right" className="table-row">
                {Payout_Status === "Pending" ? (
                  <button
                    id={payoutKey}
                    onClick={handlePayoutComplete}
                    className="btn waves-effect waves-light #009688 teal"
                  >
                    Complete
                  </button>
                ) : (
                  Payout_Status
                )}
              </TableCell>
              <TableCell align="right" className="table-row">
                {Amount}
              </TableCell>
              <TableCell align="right" className="table-row">
                {`${date.toLocaleDateString()} `}
              </TableCell>
              <TableCell align="right" className="table-row">
                <a href={Invoice_URL} target="_blank" rel="noopener noreferrer">
                  {Invoice_URL}
                </a>
              </TableCell>
            </TableRow>
          );
        });
        payOutTable = (
          <Paper className="root">
            <h5 className="tableHeading">Payouts</h5>
            <Table className="table">
              <TableHead>
                <TableRow>
                  <TableCell>Payout Status</TableCell>
                  <TableCell>Payout Amaount</TableCell>
                  <TableCell>Payout Date</TableCell>
                  <TableCell>Payout Invoice URL</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>{payOutData}</TableBody>
            </Table>
          </Paper>
        );
        return null;
      }
      let invoices = [];
      let promoCodes = [];
      if (key === "Subscription") {
        const {
          Type,
          Activated,
          ExpirationDate,
          CurrentInvoiceID,
          Invoices
        } = user[key];
        Invoices &&
          Object.keys(Invoices).forEach(key => {
            let currentInvoice = Invoices[key];
            if (key === CurrentInvoiceID) {
              return;
            }
            if (currentInvoice.Promocode) {
              promoCodes.push(currentInvoice.Promocode);
            } else if (
              currentInvoice.InvoiceURL &&
              validURL(Invoices[key].InvoiceURL)
            ) {
              invoices.push(currentInvoice.InvoiceURL);
            }
          });
        const currentURL = Invoices[CurrentInvoiceID].InvoiceURL;
        const currentPromoCode = Invoices[CurrentInvoiceID].Promocode;
        subscriptionTable = (
          <Paper className="root">
            <h5 className="tableHeading">Packages</h5>
            <Table className="table">
              <TableHead>
                <TableRow>
                  <TableCell>Package type</TableCell>
                  <TableCell>Activation date</TableCell>
                  <TableCell>Expiration date</TableCell>
                  <TableCell>Invoices</TableCell>
                  <TableCell>Promocodes</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow>
                  <TableCell className="table-row">{Type}</TableCell>
                  <TableCell className="table-row">{Activated}</TableCell>
                  <TableCell className="table-row">{ExpirationDate}</TableCell>
                  <TableCell className="table-row">
                    {currentURL ? (
                      <p>
                        <a
                          href={currentURL}
                          className="current-invoice"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {currentURL}
                        </a>
                      </p>
                    ) : null}
                    {invoices.map(invoice => (
                      <p key={invoice}>
                        <a
                          href={invoice}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          {invoice}
                        </a>
                      </p>
                    ))}
                  </TableCell>
                  <TableCell className="table-row">
                    {currentPromoCode ? <p>{currentPromoCode}</p> : null}
                    {promoCodes.map(promocode => (
                      <p key={promocode}>{promocode}</p>
                    ))}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Paper>
        );
        return null;
      }
      if (typeof user[key] === "object") {
        return null;
      }
      return (
        <TableRow key={key}>
          <TableCell component="th" scope="row">
            {key}
          </TableCell>
          <TableCell align="right" className="table-row">
            {user[key]}
          </TableCell>
        </TableRow>
      );
    });

    return (
      <div className="col s12 m8 offset-m5 l10 offset-l3 PrimeUser">
        <div className="userPosts">
          <Link to={`/prime/user/${user.User_ID}/posts`}>See user posts</Link>
        </div>
        {showLoader ? (
          <FullPageLoader showChildren={true}>
            Updating User Status
          </FullPageLoader>
        ) : null}
        <Paper className="root">
          <Table className="table">
            <TableBody>
              {tableData}
              <TableRow>
                <TableCell>User Current payout Balance</TableCell>
                <TableCell align="right">
                  {countCreatorAmount(user.Buyers, user.Payout)}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </Paper>
        {user && user.Subscription && subscriptionTable}
        {user && user.Payout && payOutTable}
        <div className="premium-package">
          <div
            className="center-align premium-inputs"
            style={{ marginTop: 75 }}
          >
            <div className="input-field">
              <label
                htmlFor="price"
                className={`${premium.price ? "active" : null}`}
              >
                Premium package price
              </label>
              <input
                id="price"
                type="number"
                defaultValue={premium.price}
                onChange={handlePackageChange}
                className="materialize-textarea"
              />
            </div>
            <div className="input-field">
              <label
                htmlFor="discount"
                className={`${premium.discount ? "active" : null}`}
              >
                Premium package discount
              </label>
              <input
                id="discount"
                type="number"
                value={premium.discount}
                onChange={handlePackageChange}
                className="materialize-textarea"
              />
            </div>

            <div className="input-field">
              <button
                className="btn deep-purple lighten-1 z-depth-0"
                onClick={handlePackageSubmit}
              >
                Save
              </button>
            </div>
          </div>
        </div>

        {user && user.User_SocialLinks && (
          <div className="social-container">
            <h6>Social icons</h6>
            <div className="social">{renderSocialLinks()}</div>
          </div>
        )}
        {user.User_IsCreator && user.User_IsCreator !== "False" && (
          <div className="button-group">
            <button
              name="True"
              onClick={handleUserStatus}
              className="btn #66bb6a green lighten-1 z-depth-3"
              disabled={user.User_IsCreator === "True"}
            >
              Confirm
            </button>
            <ButtonWithMessage
              name="Decline"
              onChange={handleRejectForm}
              disabled={user.User_IsCreator === "False"}
              cmpClass="btn #e53935 red darken-1 lighten-1 z-depth-3"
            />
          </div>
        )}
      </div>
    );
  } else if (user === null) {
    return <PostLoader />;
  } else if (!user) {
    return <PostNotFound message="User was not found" />;
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;

  return {
    user: selectUserById(id)(state),
    profile: selectProfile(state),
    auth: selectAuthUser(state)
  };
};

const mapDispatchToState = dispatch => {
  return {
    changeUserStatus: (user, status, history, message) =>
      dispatch(changeUserStatus(user, status, history, message)),
    changePayoutStatus: (user, payoutId) =>
      dispatch(cahngeUserPayoutStatus(user, payoutId)),
    changePremiumData: (id, price, discount, history) =>
      dispatch(changePremiumData(id, price, discount, history))
  };
};

export default connect(mapStateToProps, mapDispatchToState)(PrimeUser);
