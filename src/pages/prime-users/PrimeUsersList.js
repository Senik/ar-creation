import React, { memo } from 'react';
import { withRouter } from 'react-router-dom';

//components
import UserCard from '../../components/user-card/user-card.component';
import PostsMasonry from '../../components/shared/PostsMasonry';

const PrimeUsersList = props => {
  const { users } = props;
  let primeUsers =
    users &&
    users.map(user => {
      return (
        <UserCard key={user.User_ID} user={user} linkTo="/prime/users/" />
      );
    });
  return <PostsMasonry>{primeUsers.length > 0 && primeUsers}</PostsMasonry>;
};

export default memo(withRouter(PrimeUsersList));
