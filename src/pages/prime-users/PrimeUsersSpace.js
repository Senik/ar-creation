import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { ClipLoader } from "react-spinners";
import InfiniteScroll from "react-infinite-scroll-component";
import { createStructuredSelector } from "reselect";
//selectors
import { selectAuthUser, selectProfile } from "../../redux/auth/auth.selectors";
import { selectKeyword } from "../../redux/post/post.selectors";
import { selectAllUsers } from "../../redux/user/user.selectors";
//actions
import {
  setSearchLength,
  clearSearchLength,
  setKeyword,
  clearKeyword
} from "../../redux/post/post.actions";
//helpers
import { filterByKeyword } from "../../helpers";
//components
import PrimeUsersList from "./PrimeUsersList";
import PostLoader from "../../components/shared/PostLoader";
import AppBarElement from "../../components/shared/AppBarElement";
//styles
import "./PrimeUser.styles.scss";
class PrimeUsersSpace extends Component {
  state = {
    prevUserIndex: 0,
    nextUserIndex: 9,
    showUsers: 0,
    filteredUsers: []
  };
  componentDidMount() {
    document.title = "Prime space Users";
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      (prevProps.keyword !== this.props.keyword ||
        prevState.showUsers !== this.state.showUsers) &&
      this.props.keyword[0]
    ) {
      const { users, keyword, setSearchLength } = this.props;
      const filteredUsers = filterByKeyword(
        users,
        ["User_Email", "User_FullName", "User_NickName"],
        keyword,
        "User_NickName"
      );
      this.setState({
        filteredUsers: filteredUsers
      });
      setSearchLength(this.filterUsersByTab(filteredUsers).length);
    }
  }
  componentWillUnmount() {
    const { clearSearchLength, clearKeyword } = this.props;
    clearSearchLength();
    clearKeyword();
  }
  handleTabChange = (event, newValue) => {
    this.setState({ showUsers: newValue });
  };

  scrollTop = () => {
    window.scroll({ top: 0, left: 0, behavior: "smooth" });
  };
  fetchMoreData = () => {
    let test = this.state.nextUserIndex + 9;
    this.setState({
      nextUserIndex: test
    });
  };
  filterUsersByTab = users => {
    let filteredUsers = users.filter(user => {
      switch (this.state.showUsers) {
        case 0:
          return user.User_IsCreator === "Pending";
        case 1:
          return user.User_IsCreator === "True";
        case 2:
          return (
            user.User_IsCreator !== "Pending" && user.User_IsCreator !== "True"
          );
        default:
          return null;
      }
    });
    return filteredUsers;
  };
  filterByUserStatus = (users, status) => {
    let filteredUsers = users.filter(user => user.User_IsCreator === status);
    return filteredUsers;
  };

  render() {
    const self = this;
    const { users, keyword, auth } = self.props;
    if (auth.uid !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
      return <Redirect to="/" />;
    }
    const {
      prevUserIndex,
      nextUserIndex,
      showUsers,
      filteredUsers
    } = self.state;

    const usersToShow = keyword[0] ? filteredUsers : users;
    if (usersToShow && usersToShow.length > 0) {
      return (
        <div className="PrimeSpace">
          {window.scrollY > 400 ? (
            <button
              onClick={self.scrollTop}
              className="btn-floating z-depth-3 btn-large waves-effect waves-light deep-purple darken-3 
                buttonUp
              "
            >
              <i className="fas fa-chevron-up" />
            </button>
          ) : null}
          <div className="row">
            <div className="col s12 m8 offset-m4 l10 offset-l2">
              <div className="tabBox">
                <AppBarElement
                  value={showUsers}
                  tabChange={self.handleTabChange}
                  variant="fullWidth"
                  tabs={[
                    `Requests to become creator ${
                      self.filterByUserStatus(usersToShow, "Pending").length
                    }`,
                    `Creators ${
                      self.filterByUserStatus(usersToShow, "True").length
                    }`,
                    `All Users ${usersToShow.length}`
                  ]}
                />
              </div>

              <InfiniteScroll
                dataLength={
                  self
                    .filterUsersByTab(usersToShow)
                    .slice(prevUserIndex, nextUserIndex).length
                }
                next={self.fetchMoreData}
                hasMore={
                  nextUserIndex < self.filterUsersByTab(usersToShow).length
                }
                loader={
                  <ClipLoader sizeUnit={"px"} size={150} color={"#123abc"} />
                }
                style={{ overflow: "hidden" }}
                endMessage={
                  <p style={{ textAlign: "center" }}>
                    <b style={{ color: "black" }}>
                      Yay! You have seen them all
                    </b>
                  </p>
                }
              >
                <PrimeUsersList
                  users={self
                    .filterUsersByTab(usersToShow)
                    .slice(prevUserIndex, nextUserIndex)}
                />
              </InfiniteScroll>
            </div>
          </div>
        </div>
      );
    } else {
      return <PostLoader />;
    }
  }
}

const mapStateToProps = createStructuredSelector({
  auth: selectAuthUser,
  profile: selectProfile,
  users: selectAllUsers,
  keyword: selectKeyword
});

const mapDispatchToProps = dispatch => {
  return {
    setKeyword: keyword => dispatch(setKeyword(keyword)),
    clearKeyword: () => dispatch(clearKeyword()),
    setSearchLength: searchLength => dispatch(setSearchLength(searchLength)),
    clearSearchLength: () => dispatch(clearSearchLength())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PrimeUsersSpace);
