import React, { Component } from "react";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { Redirect } from "react-router-dom";
import { ClipLoader } from "react-spinners";
//selectors
import { selectAuthUser } from "../../../redux/auth/auth.selectors";
import {
  selectUsersPostsById,
  selectKeyword
} from "../../../redux/post/post.selectors";
//actions
import {
  setSearchLength,
  clearSearchLength,
  clearKeyword
} from "../../../redux/post/post.actions";
//helpers
import { filterByTab, filterByKeyword } from "../../../helpers";
//MUI
import Button from "@material-ui/core/Button";
//components
import PostLoader from "../../../components/shared/PostLoader";
import PostNotFound from "../../../components/shared/PostNotFound";
import PostsContainer from "../../../components/shared/PostsContainer";
import AppBarElement from "../../../components/shared/AppBarElement";
import PrimeList from "../../prime-posts/PrimeList";
import SelectARType from "../../../components/selectARType/SelectARType";
//style
import classes from "./PrimeUserPosts.module.scss";

const postTypes = [
  "All",
  "3DBundle",
  "Video",
  "TransParentVideo",
  "TransParentYoutubeVideo",
  "360Video",
  "Youtube",
  "360YoutubeVideo",
  "WebURL",
  "FaceAR_2DImage",
  "FaceAR_3DBundle"
];
const postPrivacy = ["Public", "PrivateToMe", "Unlisted"];

class PrimeUserPosts extends Component {
  state = {
    prevPostIndex: 0,
    nextPostIndex: 9,
    postsTabValue: 0,
    showPostsType: [...postTypes],
    showPostsPrivacy: [...postPrivacy],
    filteredPosts: null
  };
  componentDidUpdate(prevProps) {
    if (prevProps.keyword !== this.props.keyword) {
      // const filteredPosts = filterByKeyword(
      //   this.props.posts,
      //   ["PostName", "PostText"],
      //   this.props.keyword
      // );
      // this.setState({ filteredPosts });
      // this.props.setSearchLength(filteredPosts.length);
      this.handleFilter();
    }
  }

  componentWillUnmount() {
    const { clearKeyword, clearSearchLength } = this.props;
    clearKeyword();
    clearSearchLength();
  }

  fetchMoreData = () => {
    let test = this.state.nextPostIndex + 9;
    this.setState({
      nextPostIndex: test
    });
  };

  scrollTop = () => {
    window.scroll({ top: 0, left: 0, behavior: "smooth" });
  };

  handlePrivacyChange = e => {
    this.setState({
      showPostsPrivacy: e.target.value
    });
  };

  handleSelectChange = e => {
    const value = e.target.value;
    const { showPostsType } = this.state;
    if (value.includes("All") && !showPostsType.includes("All")) {
      this.setState({
        showPostsType: [...postTypes]
      });
      return;
    } else if (!value.includes("All") && showPostsType.includes("All")) {
      this.setState({
        showPostsType: []
      });
      return;
    } else if (value.includes("All")) {
      let newTypes = [...value];
      let allIndex = newTypes.indexOf("All");
      newTypes.splice(allIndex, 1);
      this.setState({
        showPostsType: newTypes
      });
      return;
    }
    this.setState({
      showPostsType: value
    });
  };

  handleTabChange = (event, newValue) => {
    this.setState({ postsTabValue: newValue });
  };

  filterPostsByTab = posts => {
    if (posts) {
      return filterByTab(posts, this.state.postsTabValue);
    }
  };

  handleFilter = () => {
    const { showPostsType, showPostsPrivacy } = this.state;
    const { posts, setSearchLength, keyword } = this.props;

    if (posts) {
      const byKeywordAndTab = filterByKeyword(
        posts,
        ["PostName", "PostText"],
        keyword
      );
      const byType = showPostsType.includes("All")
        ? byKeywordAndTab
        : byKeywordAndTab.filter(post => showPostsType.includes(post.ARType));

      const byPrivcay = byType.filter(post => {
        if (
          !post.hasOwnProperty("PostPrivacy") &&
          showPostsPrivacy.includes("Public")
        ) {
          return true;
        } else {
          return showPostsPrivacy.includes(post.PostPrivacy);
        }
      });

      this.setState({ filteredPosts: byPrivcay });
      setSearchLength(byPrivcay.length);
    }
  };

  render() {
    const self = this;
    const { posts, auth } = self.props;
    const {
      prevPostIndex,
      nextPostIndex,
      postsTabValue,
      showPostsType,
      showPostsPrivacy,
      filteredPosts
    } = self.state;
    const postsToShow = filteredPosts ? filteredPosts : posts;

    if (auth.uid !== "zFodTuG0dmOQgPw0VyHcSWI9NX63") {
      return <Redirect to="/" />;
    }

    if (postsToShow === "pending") {
      return <PostLoader />;
    } else if (postsToShow === null) {
      return <PostNotFound message="User doesn't have any posts" />;
    } else {
      let tabs = [
        `Published ${filterByTab(postsToShow, 0).length}`,
        `In Review ${filterByTab(postsToShow, 1).length}`,
        `Rejected ${filterByTab(postsToShow, 2).length}`
      ];
      return (
        <PostsContainer
          cmpClassName={classes.PrimeUserPosts}
          scrollToTop={self.scrollTop}
        >
          <AppBarElement
            value={postsTabValue}
            tabChange={self.handleTabChange}
            variant="fullWidth"
            tabs={tabs}
          />

          <SelectARType
            showPostsType={showPostsType}
            selectChange={self.handleSelectChange}
            postTypes={postTypes}
            label="Select post type"
          />
          <SelectARType
            showPostsType={showPostsPrivacy}
            selectChange={self.handlePrivacyChange}
            postTypes={postPrivacy}
            label="Select post privacy"
          />
          <div className={classes.filterButton}>
            <Button onClick={self.handleFilter} className="blue white-text">
              Filter
            </Button>
          </div>
          <InfiniteScroll
            dataLength={
              filterByTab(postsToShow, postsTabValue).slice(
                prevPostIndex,
                nextPostIndex
              ).length
            }
            next={self.fetchMoreData}
            hasMore={
              nextPostIndex < filterByTab(postsToShow, postsTabValue).length
            }
            loader={<ClipLoader sizeUnit={"px"} size={150} color={"#123abc"} />}
            style={{ overflow: "hidden" }}
            endMessage={
              <p style={{ textAlign: "center" }}>
                <b style={{ color: "black" }}>Yay! You have seen it all</b>
              </p>
            }
          >
            <PrimeList
              posts={filterByTab(postsToShow, postsTabValue).slice(
                prevPostIndex,
                nextPostIndex
              )}
            />
          </InfiniteScroll>
        </PostsContainer>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const id = ownProps.match.params.id;
  return {
    auth: selectAuthUser(state),
    posts: selectUsersPostsById(id)(state),
    keyword: selectKeyword(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setSearchLength: searchLength => dispatch(setSearchLength(searchLength)),
    clearSearchLength: () => dispatch(clearSearchLength()),
    clearKeyword: () => dispatch(clearKeyword())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PrimeUserPosts);
