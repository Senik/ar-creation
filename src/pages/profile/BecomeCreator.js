import React, { useState } from "react";
//MUI
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
//components
import { BecomeCreatorInfo, SocialLinksAndSkills } from "./ProfileModals";
//styles
import classes from "./Profile.module.scss";
import "react-responsive-carousel/lib/styles/carousel.min.css";

function BecomeCreator(props) {
  const [open, setOpen] = useState(false);
  const [step, setStep] = useState(1);

  const {
    disabled,
    handleSocialChange,
    handleCreatorRequest,
    profile,
    addSkills,
    deleteSkills,
    tags,
    bio,
    socialLinks
  } = props;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setStep(1);
  };

  const handleNextStep = () => {
    setStep(step + 1);
  };
  const handlePrevStep = () => {
    setStep(step - 1);
  };

  let showModal;
  switch (step) {
    case 1:
      showModal = (
        <>
          <DialogContent className={classes.dialogContent}>
            <BecomeCreatorInfo />
          </DialogContent>
          <DialogActions className={classes.dialogActions}>
            <Button onClick={handleNextStep} className="btn blue white-text">
              Become Creator
            </Button>
          </DialogActions>
        </>
      );
      break;
    case 2:
      showModal = (
        <>
          <DialogContent
            className={`${classes.dialogContent} ${classes.skillsSocialLinksAction}`}
          >
            <SocialLinksAndSkills
              handleSocialChange={handleSocialChange}
              profile={profile}
              addSkills={addSkills}
              deleteSkills={deleteSkills}
              tags={tags}
              bio={bio}
              socialLinks={socialLinks}
            />
          </DialogContent>
          <DialogActions className={`${classes.dialogActions}`}>
            <Button
              onClick={handlePrevStep}
              className={`red white-text ${classes.sendRequestBtn}`}
            >
              Back
            </Button>
            <Button
              onClick={handleCreatorRequest}
              className={`blue white-text ${classes.sendRequestBtn}`}
            >
              Send Request
            </Button>
          </DialogActions>
        </>
      );
      break;
    default:
      return null;
  }

  return (
    <div className={classes.BecomeCreator}>
      <Button
        className="waves-effect waves-light white"
        style={{ zIndex: 0 }}
        variant="outlined"
        color="secondary"
        disabled={disabled}
        onClick={handleClickOpen}
      >
        Become Creator
      </Button>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle
          onClose={handleClose}
          id="customized-dialog-title"
          disableTypography
          className={classes.dialogTitle}
        >
          <Typography variant="h6">Become Creator</Typography>
          <IconButton
            aria-label="Close"
            className={classes.dialogTitleCloseButton}
            onClick={handleClose}
          >
            <CloseIcon />
          </IconButton>
        </DialogTitle>

        {showModal}
      </Dialog>
    </div>
  );
}

export default BecomeCreator;
