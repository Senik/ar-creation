import React from 'react';
import { Carousel } from 'react-responsive-carousel';
//MUI
import Typography from '@material-ui/core/Typography';
//components
import InputElement from '../../components/formElements/InputElement';
import SocialLinks from './SocialLinks';
//icons
import slide1 from '../../assets/img/creator_profile.jpg';
import slide2 from '../../assets/img/creator_profile_slide2.jpg';
import slide3 from '../../assets/img/creator_profile.jpg';
//styles
import classes from './Profile.module.scss';

const BecomeCreatorInfo = () => {
  return (
    <>
      <Carousel
        showArrows={false}
        showThumbs={false}
        showStatus={false}
        showIndicators={false}
        swipeable={false}
        infiniteLoop
        dynamicHeight
        autoPlay
      >
        <div>
          <img src={slide1} alt="" />
        </div>
        <div>
          <img src={slide2} alt="" />
        </div>
        <div>
          <img src={slide3} alt="" />
        </div>
      </Carousel>
      <Typography gutterBottom className={classes.descText}>
        By becoming a member of Content Creators Community, your profile
        will be featured in our Creators page, where the potential buyers
        of your professional services will have an opportunity to find and
        look through your XR art and contents and also contact you
        directly.
      </Typography>
    </>
  );
}

const CreatorCredentials = ({ bankInfo, handleBankInfoChange }) => {
  const { bankName, bankAccountHolderName, bankAccountNumber, bankSwift, billingAddress } = bankInfo;
  return (
    <>
      <InputElement
        name="bankName"
        defaultValue={bankName}
        label="Bank Name"
        required={true}
        handleChange={handleBankInfoChange}
        id="bankInfo"
      />
      <InputElement
        name="bankAccountHolderName"
        defaultValue={bankAccountHolderName}
        label="Bank Account Holder Name"
        required={true}
        handleChange={handleBankInfoChange}
        id="bankInfo"
      />
      <InputElement
        name="bankAccountNumber"
        defaultValue={bankAccountNumber}
        label="Bank Account Number"
        required={true}
        handleChange={handleBankInfoChange}
        id="bankInfo"
      />
      <InputElement
        name="bankSwift"
        defaultValue={bankSwift}
        label="Swift Code/ BIC"
        required={true}
        handleChange={handleBankInfoChange}
        id="bankInfo"
      />
      <InputElement
        name="billingAddress"
        defaultValue={billingAddress}
        label="Billing Address"
        required={true}
        handleChange={handleBankInfoChange}
        id="bankInfo"
      />
    </>
  );
}

const SocialLinksAndSkills = props => {
  const {
    handleSocialChange,
    profile,
    addSkills,
    deleteSkills,
    tags,
    bio,
    socialLinks
  } = props;
  return (
    <SocialLinks
      handleSocialInputChange={handleSocialChange}
      profile={profile}
      addSkills={addSkills}
      deleteSkills={deleteSkills}
      tags={tags}
      bio={bio}
      creatorSocialLinks={socialLinks}
    />
  );
}

export { BecomeCreatorInfo, SocialLinksAndSkills, CreatorCredentials };