import React, { useState } from 'react';
//components
import SkillsInput from '../../components/pages/creatorForm/SkillsInput';
//icons
import SketchfabIcon from '../../assets/img/icons/sketchfablogo.png';
import GooglePolyIcon from '../../assets/img/icons/polyIcon.png';
//styles
import classes from './Profile.module.scss';

const SocialLinks = props => {
  const [showSocialLinks, setShowSocialLinks] = useState(false);

  const handleSocialLinks = e => {
    e.preventDefault();
    setShowSocialLinks(!showSocialLinks);
  };
  const socialLinks = [
    'googlepoly',
    'sketchfab',
    'pinterest',
    'instagram',
    'twitter',
    'facebook',
    'flickr',
    'tumblr',
    'linkedin',
    'youtube',
    'behance'
  ];
  const {
    handleSocialInputChange,
    profile,
    addSkills,
    deleteSkills,
    tags,
    bio,
    creatorSocialLinks
  } = props;
  const { User_Bio } = profile;
  return (
    <>
      <div className={`input-field ${classes.SocialLinksFields}`}>
        <button
          className="waves-effect waves-light btn-small indigo"
          onClick={handleSocialLinks}
        >
          Add social network links *
                      <i
            className={`fas fa-sort-${
              showSocialLinks ? 'up' : 'down'
              } ${classes.addSocialLinksIcons}`}
          />
        </button>
        {showSocialLinks ? (
          <div className={classes.socialLinksContainer}>
            {socialLinks.map(link => {
              if (link === 'googlepoly') {
                return (
                  <div className="input-field" key={link}>
                    <div>
                      <img
                        className={classes.socialLinkPng}
                        src={GooglePolyIcon}
                        alt="Google poly"
                      />
                    </div>
                    <input
                      type="text"
                      name={`${link}Link`}
                      className={classes.socialLinkInput}
                      placeholder={`${link} profile URL`}
                      onChange={handleSocialInputChange}
                      defaultValue={
                        creatorSocialLinks.googlepolyLink
                      }
                      id="socialLink"
                    />
                  </div>
                );
              }
              if (link === 'sketchfab') {
                return (
                  <div className="input-field" key={link}>
                    <div>
                      <span>
                        <img
                          className={classes.socialLinkPng}
                          src={SketchfabIcon}
                          alt="Sketchfab"
                        />
                      </span>
                    </div>
                    <input
                      type="text"
                      name={`${link}Link`}
                      className={classes.socialLinkInput}
                      placeholder={`${link} profile URL`}
                      onChange={handleSocialInputChange}
                      defaultValue={
                        creatorSocialLinks.sketchfabLink
                      }
                      id="socialLink"
                    />
                  </div>
                );
              }
              return (
                <div className="input-field" key={link}>
                  <div>
                    <span>
                      <i
                        className={`fab fa-${link} ${classes.socialLinkIcon}`}
                      />
                    </span>
                  </div>
                  <input
                    type="text"
                    name={`${link}Link`}
                    className={classes.socialLinkInput}
                    placeholder={`${link} profile URL`}
                    onChange={handleSocialInputChange}
                    defaultValue={
                      creatorSocialLinks[`${link}Link`]
                    }
                    id="socialLink"
                  />
                </div>
              );
            })}
          </div>
        ) : null}
      </div>
      <div className={`input-field ${classes.SocialLinksFields}`}>
        <SkillsInput
          addSkills={addSkills}
          deleteSkills={deleteSkills}
          tags={tags}
        />
      </div>
      <div className={`input-field ${classes.SocialLinksFields}`}>
        <textarea
          className="materialize-textarea"
          style={{ minHeight: '3rem' }}
          name="bio"
          onChange={handleSocialInputChange}
          placeholder="Short Bio"
          defaultValue={bio || User_Bio}
          required
        />
      </div>
    </>
  );
}

export default SocialLinks;