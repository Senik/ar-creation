import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import moment from "moment";
import DatePicker from "react-datepicker";
import axios from "axios";
import Avatar from "react-avatar-edit";
import ReactFlagsSelect from "react-flags-select";
import { createStructuredSelector } from "reselect";
import { validURL } from "../../helpers";
//selectors
import {
  selectAuthError,
  selectProfile,
  selectCreatorBalance
} from "../../redux/auth/auth.selectors";
import { selectUserPosts } from "../../redux/post/post.selectors";
//actions
import {
  updateUserInfo,
  checkUserNickname,
  alertNotification,
  become_creator,
  clearAuthError
} from "../../redux/auth/auth.actions";
import { setPayoutData } from "../../redux/user/user.actions";
//components
import { COUNTRIES } from "../../components/pages/userInfoForm/Countries";
import BecomeCreator from "./BecomeCreator";
import { TRADEMARKS } from "../../components/shared/TrademarksData";
import { SocialLinksAndSkills, CreatorCredentials } from "./ProfileModals";
import UserTypeRadiobuttons from "../../components/pages/userInfoForm/UserType";
import Gender from "../../components/pages/userInfoForm/Gender";
import PostLoader from "../../components/shared/PostLoader";
import RequestPackage from "../../components/modals/requestPackage";
import InAppModal from "../../components/modals/arPost/InAppModal";
//icons
import DefaultAvatar from "../../default_avatar.png";
//styles
import classes from "./Profile.module.scss";
import "react-datepicker/dist/react-datepicker.css";
import "../../../node_modules/react-flags-select/css/react-flags-select.css";
class UserProfile extends React.PureComponent {
  state = {
    openPackageRequest: false,
    loading: true,
    preview: null,
    src: "",
    tags: [],
    trademarkError: "",
    socialLinks: {
      googlepolyLink: "",
      sketchfabLink: "",
      pinterestLink: "",
      instagramLink: "",
      twitterLink: "",
      facebookLink: "",
      flickrLink: "",
      tumblrLink: "",
      linkedinLink: "",
      youtubeLink: "",
      behanceLink: ""
    }
  };
  componentDidMount() {
    document.title = "Profile";
    const requestPackage = localStorage.getItem("requestPackage");
    if (requestPackage) {
      this.setState({ openPackageRequest: true });
    }
    const url = new URL(window.location.href);
    const reqPackage = url.searchParams.get("requestPackage");
    if (reqPackage) {
      this.setState({ openPackageRequest: true });
    }
    this.setState({
      profileReady: false
    });
  }
  componentDidUpdate() {
    const self = this;
    if (self.props.profile.isLoaded && !self.state.profileReady) {
      const { profile, alertNotification } = self.props;
      const {
        User_Skills,
        User_Birthdate,
        User_FullName,
        User_NickName,
        isEmpty,
        User_Location,
        Creator_Credentials,
        User_Bio,
        User_SocialLinks
      } = profile;
      if (!isEmpty && User_NickName === "Nick Name") {
        alertNotification("Please fill your profile to continue");
      }
      if (profile.isLoaded) {
        const newState = {};
        const newTags = User_Skills && User_Skills.split(",");
        const tags = [];
        newTags && newTags.map(tag => tags.push({ id: tag, text: tag }));

        if (User_Birthdate && moment(User_Birthdate).isValid()) {
          newState.birthDate = User_Birthdate;
        }
        if (User_Location) {
          let countryCode = Object.keys(COUNTRIES).find(
            key => COUNTRIES[key] === User_Location
          );
          newState.country = User_Location;
          newState.countryCode = countryCode;
          newState.loading = false;
        } else {
          process.env.NODE_ENV === "development"
            ? axios.get("http://ip-api.com/json").then(res => {
              const { countryCode, country } = res.data;
              this.setState({
                loading: false,
                country: country,
                countryCode: countryCode
              });
            })
            : axios.get("https://ipinfo.io/json").then(res => {
              const { country } = res.data;
              this.setState({
                loading: false,
                country: COUNTRIES[country],
                countryCode: country
              });
            });
        }
        if (Creator_Credentials) {
          newState.bankInfo = {
            bankName: Creator_Credentials.Bank_Name,
            bankAccountHolderName: Creator_Credentials.Bank_Account_Holder_Name,
            bankAccountNumber: Creator_Credentials.Bank_Account_Number,
            bankSwift: Creator_Credentials.Bank_Swift,
            billingAddress: Creator_Credentials.Billing_Address
          };
        }
        if (User_Bio) {
          newState.bio = User_Bio;
        }
        if (User_SocialLinks) {
          newState.socialLinks = {};
          Object.keys(User_SocialLinks).forEach(key => {
            let lowerCaseKey = `${key.toLowerCase()}Link`;
            newState.socialLinks[lowerCaseKey] = User_SocialLinks[key];
          });
        }
        newState.tags = tags;
        newState.fullName = User_FullName;
        newState.nickName = User_NickName;
        newState.profileReady = true;
        this.setState({
          ...newState
        });
      }
    }
  }

  componentWillUnmount() {
    this.props.clearAuthError();
  }
  setLoader = () => {
    this.setState({ loading: true });
  };
  checkInputValue = e => {
    let value = e.target.value;
    if (value === "Full Name" || value === "Nick Name") {
      e.target.value = "";
    }
  };
  handleDateChange = date => {
    if (date) {
      this.setState({
        birthDate: moment(date).format("YYYY-MM-DD")
      });
    } else {
      this.setState({
        birthDate: moment(this.props.profile.User_Birthdate).format(
          "YYYY-MM-DD"
        )
      });
    }
  };
  handleInputChange = e => {
    const { id, value, name } = e.target;
    if (id === "nickName") {
      const str = TRADEMARKS.toString().toLocaleLowerCase();
      const arr = str.split(",");

      if (arr.indexOf(value.trim().toLowerCase()) > -1) {
        this.setState({
          trademarkError:
            "This name has copyright, please contact us with permission that you are allowed to use this name"
        });
        return;
      } else {
        this.setState({
          trademarkError: "",
          [name]: value
        });
      }
      this.props.checkUserNickname(value);
    } else if (id === "socialLink") {
      this.setState({
        socialLinks: {
          ...this.state.socialLinks,
          [name]: value
        }
      });
    } else {
      this.setState({
        [name]: value
      });
    }
  };

  handleBankInfoChange = (value, name) => {
    this.setState({
      bankInfo: {
        ...this.state.bankInfo,
        [name]: value
      }
    });
  };

  handleGenderChange = value => {
    this.setState({
      gender: value
    });
  };
  handleCountrySelect = countryCode => {
    this.setState({
      country: COUNTRIES[countryCode]
    });
  };
  addSkills = tag => {
    this.setState({
      tags: [...this.state.tags, tag]
    });
  };
  deleteSkills = i => {
    const { tags } = this.state;
    this.setState({
      tags: tags.filter((tag, index) => index !== i)
    });
  };
  getUserType = e => {
    this.setState({
      user_type: e
    });
  };
  checkValidity = socialLinks => {
    let result = "valid";
    Object.values(socialLinks).forEach(link => {
      if (link) {
        if (!validURL(link)) {
          result = "invalid";
        }
      }
    });
    return result;
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      fullName,
      nickName,
      user_type,
      gender,
      country,
      preview,
      birthDate,
      tags,
      socialLinks,
      bankInfo,
      bio
    } = this.state;
    const { profile, history, updateUserInfo, posts } = this.props;
    const {
      User_Bio,
      User_Type,
      User_Gender,
      User_IsCreator,
      Creator_Credentials,
      User_FullName,
      User_NickName
    } = { ...profile };

    const data = {
      fullName: fullName,
      nickName: nickName,
      user_type: user_type || User_Type,
      gender: gender || User_Gender,
      country: country
    };

    if (preview) {
      data.profileImage = preview;
    }
    if (User_IsCreator && User_IsCreator.toLowerCase() === "true") {
      const userSocialLinks = {
        Twitter: socialLinks.twitterLink,
        Facebook: socialLinks.facebookLink,
        Pinterest: socialLinks.pinterestLink,
        Linkedin: socialLinks.linkedinLink,
        Youtube: socialLinks.youtubeLink,
        Instagram: socialLinks.instagramLink,
        Behance: socialLinks.behanceLink,
        Sketchfab: socialLinks.sketchfabLink,
        Googlepoly: socialLinks.googlepolyLink,
        Flickr: socialLinks.flickrLink,
        Tumblr: socialLinks.tumblrLink
      };
      let URLValidity = this.checkValidity(userSocialLinks);
      if (URLValidity === "invalid") {
        this.setState({
          socialLinksError: "Please fill valid social links!",
          isInAppDialogOpen: true
        });
        return;
      } else {
        data.user_social = userSocialLinks;
        this.setState({
          socialLinksError: null
        });
      }
      if (
        (bankInfo && bankInfo.bankName) ||
        (Creator_Credentials && Creator_Credentials.Bank_Name)
      ) {
        const {
          bankName,
          bankAccountHolderName,
          bankAccountNumber,
          bankSwift,
          billingAddress
        } = bankInfo;
        const {
          Bank_Name,
          Bank_Account_Holder_Name,
          Bank_Account_Number,
          Bank_Swift,
          Billing_Address
        } = Creator_Credentials;
        data.user_credentials = {
          bankName: bankName || Bank_Name,
          bankAccountHolderName:
            bankAccountHolderName || Bank_Account_Holder_Name,
          bankAccountNumber: bankAccountNumber || Bank_Account_Number,
          bankSwift: bankSwift || Bank_Swift,
          billingAddress: billingAddress || Billing_Address
        };
      }
      const skills = tags.map(tag => tag["id"]);
      data.user_bio = bio || User_Bio;
      data.user_skills = skills.join();
      data.isCreator = User_IsCreator;
    }

    if (birthDate) {
      data.birthDate = moment(birthDate).format("YYYY-MM-DD");
    }

    let userPostsIDs = [];
    if (fullName !== User_FullName || nickName !== User_NickName) {
      posts.forEach(post => {
        userPostsIDs.push(post.PostID)
      });
    }

    updateUserInfo(data, history, userPostsIDs);
  };

  handleCreatorRequest = () => {
    this.setState({
      loading: true
    });

    const { profile, history, become_creator } = this.props;
    const { tags, bio } = this.state;

    const linksArray = [
      "Twitter",
      "Facebook",
      "Pinterest",
      "Linkedin",
      "Youtube",
      "Instagram",
      "Behance",
      "Sketchfab",
      "Googlepoly",
      "Flickr",
      "Tumblr"
    ];
    const socialLinks = {};
    let showError = false;
    linksArray.map(link => {
      let currentLink = this.state.socialLinks[
        `${link.toLocaleLowerCase()}Link`
      ];
      socialLinks[link] = currentLink;
      if (currentLink) {
        showError = true;
      }
      return null;
    });
    let URLValidity = this.checkValidity(socialLinks);
    if (URLValidity === "invalid") {
      showError = false;
      this.setState({
        socialLinksError: "Please fill valid social links!"
      });
    } else {
      this.setState({
        socialLinksError: null
      });
    }

    if (!showError || !tags.length || !bio) {
      this.setState({
        isInAppDialogOpen: true,
        loading: false
      });
      return;
    }
    const formData = new FormData();
    formData.append("user_email", profile.User_Email);
    formData.append("user_id", profile.User_ID);
    become_creator(formData, tags, socialLinks, bio, history);
  };

  onClose = () => {
    this.setState({ preview: null });
  };

  onCrop = preview => {
    this.setState({ preview });
  };

  handleInAppDialogClose = () => {
    this.setState({
      isInAppDialogOpen: false,
      socialLinksError: null
    });
  };

  isButtonDisabled = () => {
    const { authError, profile } = this.props;
    const {
      trademarkError,
      nickName,
      fullName,
      bankInfo,
      tags,
      bio,
      socialLinks,
      country
    } = this.state;
    const {
      googlepolyLink,
      sketchfabLink,
      pinterestLink,
      instagramLink,
      twitterLink,
      facebookLink,
      flickrLink,
      tumblrLink,
      linkedinLink,
      youtubeLink,
      behanceLink
    } = socialLinks;

    return (
      authError ||
      trademarkError ||
      !nickName ||
      nickName === "Nick Name" ||
      !fullName ||
      fullName === "Full Name" ||
      !country ||
      country === "Location" ||
      (profile.User_IsCreator === "True" &&
        ((profile.Creator_Credentials &&
          (!bankInfo.bankName ||
            !bankInfo.bankAccountHolderName ||
            !bankInfo.bankAccountNumber ||
            !bankInfo.bankSwift ||
            !bankInfo.billingAddress)) ||
          !tags.length ||
          !bio ||
          (profile.User_SocialLinks &&
            !googlepolyLink &&
            !sketchfabLink &&
            !pinterestLink &&
            !instagramLink &&
            !twitterLink &&
            !facebookLink &&
            !flickrLink &&
            !tumblrLink &&
            !linkedinLink &&
            !youtubeLink &&
            !behanceLink)))
    );
  };
  render() {
    const self = this;
    const { profile, authError } = self.props;
    const {
      User_Type,
      User_Email,
      User_Gender,
      User_NickName,
      User_FullName,
      User_IsCreator,
      User_Birthdate,
      User_ProfilePicURL,
      User_ID,
      Creator_Credentials,
      User_Location
    } = { ...profile };
    const {
      loading,
      preview,
      src,
      birthDate,
      trademarkError,
      tags,
      countryCode,
      bankInfo,
      bio,
      socialLinks,
      socialLinksError,
      isInAppDialogOpen
    } = self.state;

    const userAvatarPreview =
      User_ProfilePicURL !== "ProfilePic URL"
        ? `${User_ProfilePicURL}?time=${new Date()}`
        : `https://triplee.info/Triple_E_Social/ProfilePictures/${User_ID}.jpg?time=${new Date()}`;

    let selectedBirthDate = birthDate
      ? new Date(birthDate)
      : moment(User_Birthdate).isValid()
        ? new Date(User_Birthdate)
        : null;

    return loading ? (
      <PostLoader />
    ) : (
        <div
          className={`col s12 m8 offset-m4 l10 offset-l4 xl10 offset-xl3 ${classes.UserProfile}`}
        >
          <div className="row ">
            <div className="col s12 m12 push-l6 l5" style={{ marginTop: "6%" }}>
              <div className="row">
                <div className="col m12">
                  <img
                    className="responsive-img circle center-align avatar-preview"
                    style={{ maxWidth: 100 }}
                    src={`${preview || userAvatarPreview}`}
                    onError={e => (e.target.src = DefaultAvatar)}
                    alt="Preview"
                  />
                </div>
                <div className="col m12">
                  <Avatar
                    width={300}
                    height={150}
                    label="Click or drop image"
                    labelStyle={{
                      color: "#fff",
                      fontSize: "24px",
                      cursor: "pointer"
                    }}
                    onCrop={self.onCrop}
                    onClose={self.onClose}
                    src={src}
                  />
                  {profile.User_NickName !== "Nick Name" && (
                    <RequestPackage
                      packageModalOpen={this.state.openPackageRequest}
                      setLoader={this.setLoader}
                    />
                  )}
                </div>
              </div>
            </div>
            <div className="col s12 m12 pull-l5 l6">
              <form className="white" onSubmit={self.handleSubmit}>
                <h5 className="grey-text text-darken-3 center-align">
                  Hello {User_FullName}
                </h5>
                {profile.Subscription && (
                  <div className={classes.activePackageInfo}>
                    <p>Active Package: {profile.Subscription.Type}</p>
                    <p>Expires: {profile.Subscription.ExpirationDate}</p>
                  </div>
                )}

                <div className="input-field">
                  <label htmlFor="fullName" className="active">
                    Full Name*
                </label>
                  <input
                    type="text"
                    id="fullName"
                    name="fullName"
                    defaultValue={User_FullName}
                    onChange={self.handleInputChange}
                    required
                  />
                </div>
                <div className="input-field">
                  <label htmlFor="nickNaem" className="active">
                    Nickname*
                </label>
                  <input
                    type="text"
                    id="nickName"
                    name="nickName"
                    defaultValue={User_NickName}
                    onFocus={self.checkInputValue}
                    onChange={self.handleInputChange}
                    required
                  />
                  <div className="center red-text">
                    {authError || trademarkError ? (
                      <p>{authError || trademarkError}</p>
                    ) : null}
                  </div>
                </div>
                <div className="input-field">
                  <label htmlFor="email" className={User_Email ? "active" : null}>
                    Email
                </label>
                  <input type="text" defaultValue={User_Email} readOnly />
                  <Link to="/resetEmail" className={classes.updateEmail}>
                    Update Email
                </Link>
                </div>
                {isInAppDialogOpen ? (
                  <InAppModal
                    text={socialLinksError || "Please fill all fields!"}
                    isOpen={isInAppDialogOpen}
                    close={self.handleInAppDialogClose}
                  />
                ) : null}
                {User_IsCreator && User_IsCreator.toLowerCase() === "true" ? (
                  <>
                    {Creator_Credentials ? (
                      <CreatorCredentials
                        bankInfo={bankInfo}
                        handleBankInfoChange={self.handleBankInfoChange}
                      />
                    ) : null}
                    <SocialLinksAndSkills
                      handleSocialChange={self.handleInputChange}
                      profile={profile}
                      addSkills={self.addSkills}
                      deleteSkills={self.deleteSkills}
                      tags={tags}
                      bio={bio}
                      socialLinks={socialLinks}
                    />
                  </>
                ) : null}

                <div className="input-field">
                  <label htmlFor="birthDate" className="active">
                    Birth Date
                </label>
                  <DatePicker
                    id="birthDate"
                    name="Birth Date"
                    selected={selectedBirthDate}
                    showYearDropdown={true}
                    onChange={self.handleDateChange}
                  />
                </div>
                {!User_IsCreator ||
                  (User_IsCreator && User_IsCreator.toLowerCase() === "false") ? (
                    <div className="input-field">
                      <BecomeCreator
                        profile={profile}
                        disabled={
                          trademarkError ||
                          !profile.User_FullName ||
                          profile.User_FullName === "Full Name" ||
                          !profile.User_NickName ||
                          profile.User_NickName === "Nick Name"
                        }
                        handleSocialChange={self.handleInputChange}
                        handleCreatorRequest={self.handleCreatorRequest}
                        tags={tags}
                        addSkills={self.addSkills}
                        deleteSkills={self.deleteSkills}
                        bankInfo={bankInfo}
                        bio={bio}
                        socialLinks={socialLinks}
                      />
                    </div>
                  ) : null}

                {User_IsCreator && User_IsCreator.toLowerCase() === "pending" ? (
                  <div className="input-field">
                    <p className="green-text">
                      Your request for becoming creator is in progress
                  </p>
                  </div>
                ) : null}

                <div className="input-field">
                  <UserTypeRadiobuttons
                    handleUserTypeChange={self.getUserType}
                    user={profile}
                  />
                </div>

                <div className="input-field">
                  <Gender
                    handleChange={self.handleGenderChange}
                    gender={User_Gender}
                  />
                </div>
                <div className="input-field">
                  {countryCode || User_Location ? (
                    <ReactFlagsSelect
                      defaultCountry={countryCode}
                      showSelectedLabel={true}
                      searchable={true}
                      onSelect={self.handleCountrySelect}
                      placeholder="Select a country *"
                    />
                  ) : null}
                </div>
                {self.isButtonDisabled() && (
                  <div className="input-field" style={{ color: "red" }}>
                    <p>Please fill required fields!</p>
                  </div>
                )}
                <div className="input-field">
                  <button
                    className="btn pink lighten-1 z-depth-0"
                    disabled={self.isButtonDisabled()}
                  >
                    Save changes
                </button>
                  <div className="center red-text" />
                </div>
              </form>
            </div>
          </div>
        </div>
      );
  }
}

const mapStateToProps = createStructuredSelector({
  profile: selectProfile,
  authError: selectAuthError,
  balance: selectCreatorBalance,
  posts: selectUserPosts
});

const mapDispatchToProps = dispatch => {
  return {
    updateUserInfo: (userInfo, history, IDs) =>
      dispatch(updateUserInfo(userInfo, history, IDs)),
    checkUserNickname: nickname => dispatch(checkUserNickname(nickname)),
    alertNotification: message => dispatch(alertNotification(message)),
    become_creator: (data, bankInfo, tags, socialLinks, bio) =>
      dispatch(become_creator(data, bankInfo, tags, socialLinks, bio)),
    setPayoutData: (data, id) => dispatch(setPayoutData(data, id)),
    clearAuthError: () => dispatch(clearAuthError())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);
