import React, { Component } from "react";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { ClipLoader } from "react-spinners";
//selectors
import { selectKeyword, selectARPosts } from "../../redux/post/post.selectors";
import { selectProfile, selectAuthUser } from "../../redux/auth/auth.selectors";
import { selectAllUsers } from "../../redux/user/user.selectors";
//actions
import {
  setSearchLength,
  clearKeyword,
  clearSearchLength
} from "../../redux/post/post.actions";
//helpers
import { filterByKeyword, filterByArr } from "../../helpers";
//components
import XRGalleryList from "./XRGalleryList";
import PostsContainer from "../../components/shared/PostsContainer";
import PostLoader from "../../components/shared/PostLoader";
import CreatorList from "../creators/CreatorList";
import AppBarElement from "../../components/shared/AppBarElement";

class XRGallery extends Component {
  state = {
    scrollY: 0,
    value: 0,
    prevPostIndex: 0,
    nextPostIndex: 9,
    postsToShow: null,
    user: false
  };

  componentDidMount() {
    document.title = "AR Gallery";
  }

  UNSAFE_componentWillUpdate(nextProps, nextState) {
    if (
      nextProps.keyword !== this.props.keyword ||
      nextState.value !== this.state.value
    ) {
      this.filterPost(nextProps, nextState.value);
    }
    if (nextState.value !== this.state.value) {
      this.setState({
        changedValue: true
      });
    }
  }
  componentWillUnmount() {
    const { clearKeyword, clearSearchLength } = this.props;
    clearKeyword();
    clearSearchLength();
  }
  fetchMoreData = () => {
    let test = this.state.nextPostIndex + 9;
    this.setState({
      nextPostIndex: test
    });
  };
  handleScroll = () => {
    this.setState({
      scrollY: window.scrollY
    });
  };
  filterPost(props, value) {
    const { ARPosts, keyword, setSearchLength, users } = props;
    let filteredPosts, searchPosts;
    if (ARPosts) {
      if (value === 0) {
        filteredPosts = ARPosts;
      } else if (value === 1) {
        filteredPosts = ARPosts.filter(post => {
          return post.IsFeatured === "True";
        });
      }
      if (keyword && keyword[2]) {
        if (keyword[1] === "User") {
          searchPosts = filterByKeyword(
            users,
            ["User_FullName", "User_NickName", "User_Email"],
            keyword
          );
        } else {
          searchPosts = filterByKeyword(
            filteredPosts,
            ["PostName", "PostText", "ARTags"],
            keyword
          );
          searchPosts = filterByArr(searchPosts, [
            { key: "PostPrivacy", value: "PrivateToMe" },
            { key: "PostPrivacy", value: "Unlisted" },
            { key: "PostDeleteStatus", value: "deleted" },
            { key: "PostStatus", value: "InReview" },
            { key: "PostStatus", value: "Rejected" }
          ]);
        }
        this.setState({
          postsToShow: searchPosts,
          user: keyword[1] === "User"
        });
        setSearchLength(searchPosts.length);
      } else {
        this.setState({ postsToShow: filteredPosts, user: false });
      }
    }
  }
  scrollTop = () => {
    window.scroll({ top: 0, left: 0, behavior: "smooth" });
  };

  handleTabChange = (event, newValue) => {
    this.setState({ value: newValue });
  };

  render() {
    const self = this;
    const { auth, profile, keyword, ARPosts } = self.props;
    const {
      prevPostIndex,
      nextPostIndex,
      value,
      postsToShow,
      changedValue
    } = self.state;
    const xrPostsToShow = keyword[0] || changedValue ? postsToShow : ARPosts;
    if (xrPostsToShow && ARPosts.length > 0) {
      return (
        <PostsContainer scrollToTop={self.scrollTop}>
          {this.state.user ? null : (
            <AppBarElement
              value={value}
              tabChange={self.handleTabChange}
              tabs={["All", "Featured"]}
              style={{ maxWidth: "480px", margin: "0 auto" }}
            />
          )}
          <InfiniteScroll
            dataLength={
              xrPostsToShow.slice(prevPostIndex, nextPostIndex).length
            }
            next={self.fetchMoreData}
            hasMore={nextPostIndex < xrPostsToShow.length}
            loader={<ClipLoader sizeUnit={"px"} size={150} color={"#123abc"} />}
            style={{ overflow: "hidden" }}
            endMessage={
              <p style={{ textAlign: "center" }}>
                <b style={{ color: "black" }}>Yay! You have seen it all</b>
              </p>
            }
          >
            {this.state.user ? (
              <CreatorList
                profile={profile}
                posts={xrPostsToShow.slice(prevPostIndex, nextPostIndex)}
              />
            ) : (
                <XRGalleryList
                  posts={xrPostsToShow.slice(prevPostIndex, nextPostIndex)}
                  auth={auth}
                  profile={profile}
                />
              )}
          </InfiniteScroll>
        </PostsContainer>
      );
    } else {
      return <PostLoader />;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  const path = ownProps.location.pathname;
  return {
    ARPosts: selectARPosts(path)(state),
    auth: selectAuthUser(state),
    keyword: selectKeyword(state),
    profile: selectProfile(state),
    users: selectAllUsers(state)
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setSearchLength: searchLength => dispatch(setSearchLength(searchLength)),
    clearKeyword: () => dispatch(clearKeyword()),
    clearSearchLength: () => dispatch(clearSearchLength())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XRGallery);
