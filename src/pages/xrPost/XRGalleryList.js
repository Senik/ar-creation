import React, { memo } from 'react';
//components
import XRPost from './XRPost';
import PostsMasonry from '../../components/shared/PostsMasonry';

const XRGalleryList = props => {
  const { posts, auth, profile } = props;
  let XRPosts =
    posts &&
    posts
      .filter(post => {
        return (
          post.PostDeleteStatus !== 'deleted' &&
          post.PostPrivacy !== 'Unlisted' &&
          post.PostPrivacy !== 'PrivateToMe'
        );
      })
      .map(post => {
        return (
          <XRPost
            key={post.PostID}
            id={auth.uid}
            auth={auth}
            post={post}
            profile={profile}
          />
        );
      });
  return <PostsMasonry>{XRPosts.length > 0 && XRPosts}</PostsMasonry>;
};

export default memo(XRGalleryList);
