import React, { PureComponent } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import moment from "moment";
import Linkify from "react-linkify";
//selectors
import { selectUserById } from "../../redux/user/user.selectors";
//actions
import { alertNotification } from "../../redux/auth/auth.actions";
import { addPostPayment } from "../../redux/post/post.actions";
//helpers
import {
  componentDecorator,
  _createDynamicLink,
  renderIcon,
  renderType,
  likePost,
  disLikePost
} from "../../helpers";
//MUI
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { Visibility } from "@material-ui/icons";
import ToolTip from "@material-ui/core/Tooltip";
import Icon from "@material-ui/core/Icon";
//components
import ViewOrBuyPost from "../arPost/ViewOrBuyPost";
//icons
import DefaultAvatar from "../../default_avatar.png";
//styles
import classes from "./xrPost.module.scss";

class XRPost extends PureComponent {
  state = {
    cities: null,
    postIsBought: false,
    tags: null
  };
  componentDidMount = () => {
    const { post, id } = this.props;
    if (post.ARTags) {
      const tagsArray = post.ARTags.split(",");
      let tags = [];
      tagsArray && tagsArray.map(tag => tags.push({ id: tag, text: tag }));
      this.setState({ tags });
    }
    if (id && post && post.IsPaid === "True" && !this.state.postIsBought) {
      const buyers = post.Buyers;
      if (post.PostAuthorID === id) {
        return this.setState({ postIsBought: true });
      }
      if (!buyers) return;
      const buyersKeys = Object.keys(buyers);
      buyersKeys.map(key => {
        if (buyers[key].UserID === id) {
          return this.setState({ postIsBought: true });
        }
        return null;
      });
    }
  };

  renderContentIcon = () => {
    const { post } = this.props;
    if (post) {
      return renderIcon(post);
    }
  };
  renderPostType = () => {
    const { post } = this.props;
    if (post) {
      return renderType(post);
    }
  };
  renderTargetImage = () => {
    const { post } = this.props;
    if (post) {
      return (
        <div className="card-image">
          <img
            className={classes.cardImage}
            src={`https://triplee.info/${
              !post.ARTargetID
                ? "Triple_E_Thumbnail"
                : "Triple_E_WebService/AllImageTargets"
            }/${post.ARTargetName}.jpg`}
            onError={e =>
              (e.target.src =
                "https://triplee.info/Triple_E_WebService/AllImageTargets/correct3.jpg")
            }
            alt="Trigger"
          />
        </div>
      );
    }
  };

  createDynamicLink = post => {
    _createDynamicLink(post, this.props.alertNotification);
  };

  handleLikePost = post => {
    const { profile } = this.props;
    if (profile.isEmpty) {
      return;
    }
    if (post.Likes && profile.User_ID in post.Likes) {
      return disLikePost(post, profile);
    }
    likePost(post, profile);
  };
  render() {
    const { post, profile, user } = this.props;
    const { tags } = this.state;
    let avatarImageUrl;
    if (user && user.User_ProfilePicURL.includes("facebook")) {
      avatarImageUrl = user.User_ProfilePicURL;
    } else {
      avatarImageUrl = `https://triplee.info/Triple_E_Social/ProfilePictures/${post.PostAuthorID}.jpg`;
    }

    return (
      <Card className={`${classes.card} ${classes.XRPost}`}>
        <CardHeader
          avatar={
            <Link
              to={`/creator/${post.PostAuthorID}`}
              className={classes.creatorLink}
            >
              <Avatar
                aria-label="Avatar-Image"
                src={post.PostAuthorID && avatarImageUrl}
                onError={e => (e.target.src = DefaultAvatar)}
                className={classes.avatar}
              />
            </Link>
          }
          title={
            <Link
              to={`/creator/${post.PostAuthorID}`}
              className={classes.creatorLink}
            >
              {post.PostAuthor}
            </Link>
          }
          subheader={
            <div>
              <span>{moment(post.LastEdit).format("MMMM Do YYYY")}</span>
              <p style={{ margin: 0 }}>
                {this.renderPostType()}
                {this.renderContentIcon()}
              </p>
            </div>
          }
        />
        <div className="card center-align">
          <div className="card-content">
            <span className="card-title center-align">{post.PostName}</span>
          </div>
          {this.renderTargetImage()}
        </div>
        <CardContent>
          {tags ? (
            <div className={classes.XRTags}>
              {tags.map(tag => (
                <p key={tag["id"]}>{tag["id"]} </p>
              ))}
            </div>
          ) : null}
          <Typography component="p" style={{ marginBottom: "20px" }}>
            <Linkify componentDecorator={componentDecorator}>
              {post.PostText}
            </Linkify>
          </Typography>
          <CardActions className={classes.actions}>
            <div className={classes.postViews}>
              <ToolTip title="Number of views" placement="top-start">
                <IconButton
                  className={classes.postInfoIcon}
                  aria-label="Number of views"
                >
                  <Visibility />
                  {
                    <span className={classes.numberViews}>
                      {post.NumberOfViews}
                    </span>
                  }
                </IconButton>
              </ToolTip>
              <ToolTip title="Click to copy post link" placement="top-start">
                <IconButton
                  className={classes.postInfoIcon}
                  aria-label="Post link"
                  onClick={() => this.createDynamicLink(post)}
                >
                  <Icon>share</Icon>
                  <span className={classes.numberViews}>
                    {post.PostShareCount || 0}
                  </span>
                </IconButton>
              </ToolTip>
              <ToolTip
                title={
                  profile.isEmpty
                    ? "Please sign in to like the post"
                    : "Click to like the post"
                }
                placement="top-start"
              >
                <IconButton
                  className={classes.postInfoIcon}
                  aria-label="Post likes"
                  onClick={() => this.handleLikePost(post)}
                >
                  <Icon
                    style={
                      post.Likes && profile.User_ID in post.Likes
                        ? { color: "red" }
                        : null
                    }
                  >
                    favorite
                  </Icon>
                  <span className={classes.numberViews}>
                    {(post.Likes && Object.keys(post.Likes).length) || 0}
                  </span>
                </IconButton>
              </ToolTip>
            </div>
            {user ? (
              <ViewOrBuyPost
                user={user}
                profile={profile}
                post={post}
                direction="fromXRPost"
              />
            ) : null}
          </CardActions>
        </CardContent>
      </Card>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const { post } = ownProps;
  if (post) {
    return {
      user: selectUserById(post.PostAuthorID)(state)
    };
  }
  return {
    user: null
  };
};

export default connect(mapStateToProps, { alertNotification, addPostPayment })(
  XRPost
);
