import { Auth_Actions } from "./auth.types";
import axios from "axios";
//utils
import { sendEmailCreatorRequest } from "./auth.utils";
import { send_message_telegram_bot } from "../../helpers/";

export const signIn = credentials => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();

    firebase
      .auth()
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .then(res => {
        dispatch({ type: Auth_Actions.LOGIN_SUCCESS });
      })
      .catch(err => dispatch({ type: Auth_Actions.LOGIN_ERROR, err }));
  };
};

export const signOut = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .auth()
      .signOut()
      .then(() => {
        let sortedPosts = [];
        let sortedUsers = [];
        dispatch({ type: "POST_SEARCH_SUCCESS", sortedPosts });
        dispatch({ type: "USER_SEARCH_SUCCESS", sortedUsers });
        dispatch({ type: Auth_Actions.SIGNOUT_SUCCESS });
      });
  };
};

export const signUp = newUser => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .auth()
      .createUserWithEmailAndPassword(newUser.email, newUser.password)
      .then(resp => {
        if (resp && resp.user.emailVerified === false) {
          resp.user.sendEmailVerification().then(resp => {
            console.log("email sent.");
          });
        }
        return firebase
          .database()
          .ref(`Users/${resp.user.uid}`)
          .set({
            User_ID: resp.user.uid,
            User_FullName: "Full Name",
            User_Email: resp.user.email,
            User_NickName: "Nick Name",
            User_Birthdate: "Birth Date",
            User_Type: "individual",
            User_Gender: "Gender",
            User_Location: "Location",
            User_ProfilePicURL: "ProfilePic URL",
            SignUp_AT: new Date().toISOString(),
            initials: ""
          });
      })
      .then(() => {
        dispatch({ type: "SIGNUP_SUCCESS" });
      })
      .catch(err => {
        dispatch({ type: "SIGNUP_ERROR", err });
      });
  };
};

export const FBorGoogleRedirectCatch = user => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref("Users")
      .orderByKey()
      .equalTo(`${user.uid}`)
      .once("value", snapshot => {
        if (snapshot.exists()) {
          const userData = snapshot.val();
          console.log("exists", userData);
          return;
        } else {
          return firebase
            .database()
            .ref(`Users/${user.uid}`)
            .set({
              User_ID: user.uid,
              User_FullName: user.displayName,
              User_Email: user.email,
              User_NickName: "Nick Name",
              User_Birthdate: "Birth Date",
              User_Type: "individual",
              User_Gender: "Gender",
              User_Location: "Location",
              User_ProfilePicURL: user.photoURL,
              SignUp_AT: new Date().toISOString(),
              initials: user.displayName[0]
            })
            .then(() => {
              dispatch({ type: "FACEBOOK_GOOGLE_SIGNIN_SUCCESS" });
            })
            .catch(err => {
              dispatch({ type: "FACEBOOK_GOOGLE_SIGNIN_ERROR", err });
            });
        }
      });
  };
};

export const facebookSignIn = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithRedirect(provider);
  };
};

export const googleSignIn = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
  };
};

export const resetPassword = (email, history) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        dispatch({ type: "RESET_USER_PASSWORD" });
        dispatch({
          type: "SET_NOTIFICATION",
          message: "We sent email for updating password"
        });
        history.goBack();
      })
      .catch(err => {
        dispatch({
          type: "RESET_USER_PASSWORD_ERROR",
          message:
            "There is no record corresponding to this identifier, please try another email"
        });
      });
  };
};

export const setAuthError = err => {
  return (dispatch, getState, { getFirebase }) => {
    dispatch({ type: "FACEBOOK_GOOGLE_SIGNIN_ERROR", err });
  };
};

export const resetemail = (newemail, history) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    let user = firebase.auth().currentUser;
    if (user) {
      user
        .updateEmail(newemail)
        .then(() => {
          dispatch({ type: "RESET_USER_EMAIL" });
          dispatch({
            type: "SET_NOTIFICATION",
            message: "Email Updated successfully"
          });
          history.goBack();
        })
        .then(() => {
          firebase
            .database()
            .ref(`Users/${user.uid}`)
            .update({ User_Email: newemail })
            .then(res => {
              if (user.emailVerified === false) {
                user.sendEmailVerification().then(resp => {
                  console.log("email sent.");
                });
              }
            })
            .catch(err => console.log(err));
        })
        .catch(err => {
          dispatch({ type: "RESET_USER_EMAIL_ERROR", err });
        });
    }
  };
};

export const sendReport = report => {
  return (dispatch, getState) => {
    const state = getState();
    const profile = state.firebase.profile;
    const reportData = new FormData();
    reportData.append("user_email", profile.User_Email);
    reportData.append("user_name", profile.User_FullName);
    reportData.append("country", profile.User_Location);
    reportData.append("report_title", report.title);
    reportData.append("report_text", report.text);
    fetch("https://arize.io/assets/php/send_Report.php", {
      method: "POST",
      body: reportData
    })
      .then(res => res.json())
      .then(data => {
        if (data.status === "success") {
          dispatch({
            type: "SET_NOTIFICATION",
            message: "Your requst has been sent. We will contact you soon."
          });
        }
      })
      .catch(err => console.log(err));
  };
};

export const updateUserInfo = (userInfo, history, IDs) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const userId = getState().firebase.auth.uid;
    const userData = {
      User_FullName: userInfo.fullName,
      User_NickName: userInfo.nickName,
      initials: userInfo.nickName[0] + userInfo.nickName[1],
      User_Type: userInfo.user_type,
      User_Gender: userInfo.gender,
      User_Location: userInfo.country
    };
    if (userInfo.isCreator === "True") {
      userData.User_Bio = userInfo.user_bio;
      userData.User_SocialLinks = userInfo.user_social;
      userData.User_Skills = userInfo.user_skills;
      if (userInfo.user_credentials) {
        const {
          bankName,
          bankAccountHolderName,
          bankAccountNumber,
          bankSwift,
          billingAddress
        } = userInfo.user_credentials;
        userData.Creator_Credentials = {
          Bank_Name: bankName,
          Bank_Account_Holder_Name: bankAccountHolderName,
          Bank_Account_Number: bankAccountNumber,
          Bank_Swift: bankSwift,
          Billing_Address: billingAddress
        };
      }
    }
    if (userInfo.birthDate) {
      userData.User_Birthdate = userInfo.birthDate;
    }
    const updateProfileImage = async function() {
      if (userInfo.profileImage) {
        const params = new FormData();
        params.append("avatarImage", userInfo.profileImage);
        params.append("userUid", userId);
        const res = await axios.post(
          "https://triplee.info/testUpload.php",
          params
        );
        userData.User_ProfilePicURL = res.data;
      }
      firebase
        .database()
        .ref(`Users/${userId}`)
        .update(userData)
        .then(() => {
          dispatch({ type: "USER_INFO_UPDATE_SUCCESS" });
          if (IDs.length) {
            IDs.forEach(id => {
              firebase
                .database()
                .ref(`Posts/${id}`)
                .update({
                  PostAuthor: userInfo.fullName,
                  PostAuthorNickName: userInfo.nickName
                })
                .then(() => {
                  dispatch({
                    type: "SET_NOTIFICATION",
                    message: "Profile Updated successfully"
                  });
                })
                .catch(err => {
                  dispatch({ type: "USER_INFO_UPDATE_ERROR", err });
                });
            });
          } else {
            dispatch({
              type: "SET_NOTIFICATION",
              message: "Profile Updated successfully"
            });
          }

          history.push("/");
        })
        .catch(err => {
          dispatch({ type: "USER_INFO_UPDATE_ERROR", err });
        });
    };
    updateProfileImage();
  };
};

export const checkUserNickname = userNickname => {
  return (dispatch, getState, { getFirebase }) => {
    const state = getState();
    const userId = state.firebase.auth.uid;
    const users = Object.values(state.users.allUsers);
    const searchedUser = users.some(
      user =>
        user.User_NickName === userNickname.trim() && user.User_ID !== userId
    );
    if (searchedUser && searchedUser !== userId) {
      dispatch({ type: "USER_NICKNAME_EXISTS" });
    } else {
      dispatch({ type: "USER_NICKNAME_NOT_EXIST" });
    }
  };
};

export const checkUserEmail = userEmail => {
  return (dispatch, getState, { getFirebase }) => {
    const state = getState();
    if (userEmail.length === 0) {
      return dispatch({ type: "USER_EMAIL_NOT_EXIST" });
    }
    const user = Object.values(state.users.allUsers).find(
      user => user.User_Email === userEmail
    );
    if (user) {
      dispatch({
        type: "USER_EMAIL_EXIST",
        user
      });
    } else {
      dispatch({ type: "USER_EMAIL_NOT_EXIST" });
    }
  };
};

export const clearUserEmail = () => {
  return {
    type: "CLEAR_USER_EMAIL"
  };
};

export const resendVerivication = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    let user = firebase.auth().currentUser;
    if (user.emailVerified === false) {
      user.sendEmailVerification().then(resp => {
        console.log("email sent.");
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Verification email was sent."
        });
      });
    }
  };
};

export const become_creator = (data, tags, socialLinks, bio, history) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const state = getState();
    const profile = state.firebase.profile;

    async function requestCreator(formData) {
      try {
        const response = await sendEmailCreatorRequest(formData);
        const data = await response.json();
        if (data.status === "success") {
          const skills = tags.map(tag => tag["id"]);
          firebase
            .database()
            .ref(`Users/${profile.User_ID}`)
            .update({
              User_IsCreator: "Pending",
              User_SocialLinks: {
                ...socialLinks
              },
              User_Skills: skills.join(),
              User_Bio: bio
            })
            .then(() => {
              dispatch({
                type: "SET_NOTIFICATION",
                message: "Your request for becoming Creator accepted!"
              });
              send_message_telegram_bot(
                `Creator request: ${profile.User_FullName} ${profile.User_Email} ! User URL: https://platform.arize.io/prime/users/${profile.User_ID}`
              );
              history.push("/");
            });
        }
      } catch (err) {
        console.log("Error", err.message);
      }
    }

    requestCreator(data);
  };
};

export const contact_creator = data => {
  return (dispatch, getState, { getFirebase }) => {
    let url = "https://arize.io/assets/php/contact_creator.php";
    fetch(url, {
      method: "POST",
      body: data
    })
      .then(res => res.json())
      .then(data => {
        if (data.status === "success") {
          dispatch({
            type: "SET_NOTIFICATION",
            message: "Your message has been sent to creator!"
          });
        }
      })
      .catch(err => console.log(err));
  };
};

export const alertNotification = message => dispatch => {
  console.log(message);
  dispatch({ type: "SET_NOTIFICATION", message });
};

export const clearAuthError = () => {
  return {
    type: "CLEAR_AUTH_ERRORS"
  };
};
