import { Auth_Actions } from './auth.types';
const INITIAL_STATE = {
  authError: null
};

const authReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Auth_Actions.LOGIN_ERROR:
      return {
        ...state,
        authError: action.err.message
      };
    case 'LOGIN_SUCCESS':
      return {
        ...state,
        authError: null
      };
    case 'SIGNOUT_SUCCESS':
      return state;
    case 'SIGNUP_SUCCESS':
      return {
        ...state,
        authError: null
      };
    case 'SIGNUP_ERROR':
      console.log('signup error', action.err);
      return {
        ...state,
        authError: action.err.message
      };
    case 'FACEBOOK_GOOGLE_SIGNIN_SUCCESS':
      console.log('facebook sign in success');
      return {
        ...state,
        authError: null
      };
    case 'FACEBOOK_GOOGLE_SIGNIN_ERROR':
      console.log('facebook signin error', action.err);
      return {
        ...state,
        authError: action.err.message
      };
    case 'RESET_USER_PASSWORD':
      console.log('email was sent');
      return {
        ...state,
        authError: null
      };
    case 'RESET_USER_PASSWORD_ERROR':
      console.log('error email was not sent');
      return {
        ...state,
        authError: action.message
      };
    case 'RESET_USER_EMAIL':
      console.log('email was Updated');
      return {
        ...state,
        authError: null
      };
    case 'RESET_USER_EMAIL_ERROR':
      console.log('error email was not updated');
      return {
        ...state,
        authError: action.err.message
      };
    case 'USER_INFO_UPDATE_ERROR':
      console.log('error info is not updated', action.err);
      return {
        ...state,
        authError: action.err.message
      };
    case 'USER_INFO_UPDATE_SUCCESS':
      console.log('User info updated success!!!');
      return {
        ...state,
        authError: null
      };
    case 'USER_NICKNAME_EXISTS':
      console.log('eror nickname is alreaady in use');
      return {
        ...state,
        authError: 'nickname is alreaady in use'
      };
    case 'USER_NICKNAME_NOT_EXIST':
      return {
        ...state,
        authError: null
      };
    case 'CLEAR_AUTH_FORWARD_USER_ID':
      return {
        ...state,
        forwardUserID: '',
        forwardUserEmail: '',
        forwardedPostName: '',
        authError: null
      };
    case 'USER_EMAIL_EXIST':
      return {
        ...state,
        forwardUser: action.user,
        authError: null
      };
    case 'USER_EMAIL_NOT_EXIST':
      return {
        ...state,
        authError: 'There is no user with such email in database'
      };
    case 'CLEAR_USER_EMAIL':
      return {
        ...state,
        forwardUser: null
      };
    case 'CLEAR_AUTH_ERRORS':
      return {
        ...state,
        authError: null
      };
    default:
      return state;
  }
};

export default authReducer;
