import { createSelector } from 'reselect';
import { getRecentPayout } from '../../helpers';

const selectFirebase = state => state.firebase;
const selectAuth = state => state.auth;

export const selectAuthUser = createSelector(
  [selectFirebase],
  firebase => firebase.auth
);

export const selectProfile = createSelector(
  [selectFirebase],
  firebase => firebase.profile
);

export const selectAuthError = createSelector(
  [selectAuth],
  auth => auth.authError
);

export const selectForwardUser = createSelector(
  [selectAuth],
  auth => auth.forwardUser
);

export const selectCreatorBalance = createSelector(
  [selectFirebase],
  firebase => {
    if (firebase.auth.uid && firebase.profile.isLoaded) {
      const creator = firebase.profile;
      if (creator.Buyers) {
        const buyers = Object.values(creator.Buyers);
        const payouts = creator.Payout && Object.values(creator.Payout);
        const lastPayout = getRecentPayout(payouts);
        let sum = buyers.reduce((amount, buyer) => {
          if (
            isNaN(buyer.Amount) ||
            (lastPayout &&
              !(new Date(buyer.DateTime) > new Date(lastPayout[0].DateTime)))
          ) {
            return amount;
          }
          return +buyer.Amount + amount;
        }, 0);
        sum -= (sum * 10) / 100;
        return sum;
      }
    }
  }
);
