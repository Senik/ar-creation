export function sendEmailCreatorRequest(data) {
  const url = 'https://arize.io/assets/php/become_creator_request.php';
  return fetch(url, {
    method: 'POST',
    body: data
  });
}
