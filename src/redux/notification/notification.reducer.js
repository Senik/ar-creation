import { handleOpen } from '../../components/layout/Notifier';

const initialState = {
  isOpen: false,
  message: 'Hello Ninjas'
};

const notifyReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'SET_NOTIFICATION':
      handleOpen();
      return {
        ...state,
        isOpen: true,
        message: action.message,
        error: action.error
      };
    default:
      return state;
  }
};

export default notifyReducer;
