import { createSelector } from 'reselect';

const selectNotif = state => state.notification;

export const selectNotification = createSelector(
  [selectNotif],
  notification => notification
);
