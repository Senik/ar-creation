import {
  createNotification,
  countCreatorAmount,
  sendNotificationToFollowers,
  send_message_telegram_bot
} from "../../helpers";
import axios from "axios";
import moment from "moment";

export const getAllPosts = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref("/Posts")
      .on("value", snapshot => {
        if (snapshot.exists()) {
          let postsSnapshot = snapshot.val();
          dispatch({
            type: "GET_ALL_POSTS",
            posts: postsSnapshot
          });
        }
      });
  };
};

export const createPost = (newPost, history) => {
  return (dispatch, getState, { getFirebase }) => {
    //make async call to db
    const firebase = getFirebase();
    const state = getState();
    const profile = state.firebase.profile;

    const postTargetData = new FormData();
    postTargetData.append("targetImage", newPost.postTargetImage);
    postTargetData.append("targetImageName", newPost.arTargetName);

    function uploadNewPostFirebase(post) {
      firebase
        .database()
        .ref(`Posts/${post.PostID}`)
        .set(post)
        .then(() => {
          dispatch({ type: "CREATE_POST", post });
        })
        .catch(err => {
          dispatch({ type: "CREATE_POST_ERROR", err });
        });
    }

    async function uploadPost() {
      try {
        const post = {};
        if (
          newPost.model_type === "xrGallery" ||
          newPost.arType === "FaceAR_2DImage"
        ) {
          const _uplodRes = await uploadThumbnail(postTargetData);

          if (_uplodRes.data) {
            if (newPost.arType === "FaceAR_2DImage") {
              delete post.ARTriggerPosition;
              post.EyeTransparency = "True";
              post.MeanColor = "100,100,100";
            }
          }
        } else {
          dispatch({
            type: "SET_FETCH_STATUS",
            fetchStatus: "Uploading target"
          });
          await updateTargetImageInFolder(postTargetData);

          let resVuf = await uploadNewImageVuforia(postTargetData);
          let text = await resVuf.text();
          let index = text.indexOf("{");
          let responseData = JSON.parse(text.slice(index));
          if (responseData.result_code === "TargetCreated") {
            post.ARTargetID = responseData.target_id;
            dispatch({
              type: "SET_FETCH_STATUS",
              fetchStatus: "Checking for target similarity"
            });
            let checkRes = await checkTargetDublicate(post.ARTargetID);
            let text = await checkRes.text();
            let index = text.indexOf("{");
            let res = JSON.parse(text.slice(index));
            if (res.result_code === "Success") {
              if (res.similar_targets && res.similar_targets.length > 0) {
                const formData = new FormData();
                formData.append("ImageTargetID", post.ARTargetID);
                formData.append("ImageTargetName", `${post.ARTargetName}.jpg`);
                try {
                  let deleteRes = await deleteTargetFolderAndVuforia(formData);
                  let text = await deleteRes.text();
                  let index = text.indexOf("{");
                  let res = JSON.parse(text.slice(index));
                  console.log(res);
                  if (res.result_code === "TargetStatusProcessing") {
                    const data = new FormData();
                    data.append("target_id", post.ARTargetID);
                    await saveTargetIdToSql(data);
                    dispatch({
                      type: "SOMETHING_WRONG",
                      wrong: "Sorry this target is already in use!"
                    });
                  }
                } catch (err) {
                  console.log(err);
                }
                return null;
              }
            }
            dispatch({
              type: "SET_FETCH_STATUS",
              fetchStatus: "Creating post"
            });
            const parcentCompleted = 0;
            dispatch({ type: "UPLOAD_PROGRESS", parcentCompleted });
          } else {
            dispatch({
              type: "SOMETHING_WRONG",
              wrong: "Oops! Something went wrong. Please try again a bit later"
            });
          }
        }
        switch (newPost.arType) {
          case "Youtube":
          case "360YoutubeVideo":
          case "TransParentYoutubeVideo":
          case "WebURL":
            post.ARContentURL = newPost.arContentURL;
            if (newPost.arType === "360YoutubeVideo") {
              post.IsYoutube = "True";
            }
            if (newPost.isTransparent === "True") {
              post.CutOutColor = newPost.cutOutColor;
            } else {
              post.IsTransparent = newPost.isTransparent;
            }
            break;
          case "Video":
          case "360Video":
          case "TransParentVideo":
          case "zipFile":
          case "unityPackage":
          case "FaceAR_2DImage":
            const postContentData = new FormData();
            postContentData.append("file-type", newPost.arType.toLowerCase());
            postContentData.append("file", newPost.contentFile[0]);
            const _contentRes = await upladFileARContent(
              postContentData,
              dispatch
            );
            if (newPost.arType === "360Video") {
              post.IsYoutube = "False";
            }
            if (_contentRes.data.status === "success") {
              post.ARContentURL = _contentRes.data.url.toLowerCase();
            }
            if (newPost.isTransparent === "True") {
              post.CutOutColor = newPost.cutOutColor;
            } else {
              post.IsTransparent = newPost.isTransparent;
            }
            break;
          case "sketchfab":
          case "googlepoly":
            post.SoundReactivity = newPost.soundReactivity ? "True" : "False";
            if (newPost.audioFile) {
              const audioData = new FormData();
              audioData.append("file-type", "audio");
              audioData.append("file", newPost.audioFile[0]);
              const _uploadMp3 = await upladFileARContent(audioData, dispatch);
              if (_uploadMp3.data.status === "success") {
                post.AudioFile = _uploadMp3.data.url;
              }
            }

            if (newPost.arType === "sketchfab") {
              post.ARSource = "SF";
              post.ARSourceURL = newPost.arContentURL;
            } else if (newPost.arType === "googlepoly") {
              post.ARSource = "GP";
              post.ARSourceURL = newPost.arContentURL;
            }
            post.ARContentURL = `${post.ARSource}${
              newPost.postAuthorID
              }${moment(new Date())
                .format("DD/MM/YYYY")
                .replace(/\//g, "")}${Date.now()}`
              .replace(/\s/g, "")
              .replace(/[^a-z0-9]/gi, "")
              .toLowerCase();
            break;
          default:
        }
        post.CastShadow = newPost.castingShadow ? "True" : "False";
        post.ProximityActivation = newPost.proximityActivation
          ? "True"
          : "False";
        post.ARTargetName = newPost.arTargetName;
        post.AllowDownload = newPost.allowDownload;
        post.ARTriggerPosition = newPost.triggerPosition;
        post.ImageTargetURL = "NONE";
        post.SubmissionDate = post.LastEdit = new Date().toISOString();
        post.ARType = newPost.arType;
        post.NumberOfViews = 0;
        post.Location = newPost.postLocation;
        post.Position = "0,0,0";
        post.PostAuthor = newPost.postAuthor;
        post.PostAuthorID = newPost.postAuthorID;
        post.PostAuthorNickName = newPost.postAuthorNickName;
        post.PostID = newPost.postID;
        post.PostName = newPost.postName;
        post.PostPhotoURL = "NONE";
        post.PostText = newPost.postText;
        post.Rotation = "0,0,0";
        post.Scale = "0,0,0";
        post.Update = "0";
        post.PostStatus = newPost.postStatus;
        post.PostPrivacy = newPost.postPrivacy;
        if (newPost.isPaid) {
          post.IsPaid = "True";
          post.Price = newPost.model_price;
        }
        if (newPost.tags) {
          post.ARTags = newPost.tags;
        }
        if (newPost.proximityActivation) {
          post.InteractionType = newPost.interactionType;
        }
        if (newPost.arType === "360YoutubeVideo") {
          post.ARType = "360Video";
        }
        if (
          newPost.arType === "sketchfab" ||
          newPost.arType === "googlepoly" ||
          newPost.arType === "zipFile" ||
          newPost.arType === "unityPackage"
        ) {
          post.ARType = "3DBundle";
          post.ARFusion = "True";
          post.PostStatus = "InReview";
          if (
            newPost.model_type === "xrGallery" ||
            newPost.model_type === "both"
          ) {
            if (newPost.model_type === "xrGallery") {
              delete post.ARTriggerPosition;
            }
            post.XRGallery = "True";
          }
          if (
            newPost.arType === "zipFile" ||
            newPost.arType === "unityPackage"
          ) {
            post.ARSource = "ZF";
            if (newPost.arType === "unityPackage") {
              post.ARSource = "UP";
            }
            post.ARSourceURL = post.ARContentURL;
            post.ARContentURL = `${post.ARSource}${
              newPost.postAuthorID
              }${moment(new Date())
                .format("DD/MM/YYYY")
                .replace(/\//g, "")}${Date.now()}`
              .replace(/\s/g, "")
              .replace(/[^a-z0-9]/gi, "")
              .toLowerCase();
          }
        }

        if (newPost.arType === "WebURL") {
          delete post.ARTriggerPosition;
        }
        if (newPost.postAuthorIsCreator) {
          post.PostAuthorIsCreator = newPost.postAuthorIsCreator;
        }
        uploadNewPostFirebase(post);
        const postType =
          profile.Subscription &&
            moment(profile.Subscription.ExpirationDate) > moment()
            ? profile.Subscription.Type
            : "Free";

        send_message_telegram_bot(
          `New ${postType} post: "${post.PostName}" submitted! Post URL https://platform.arize.io/prime/posts/${post.PostID}`
        )
          .then(() => console.log("message sent"))
          .catch(err => console.log(err));
        if (
          post.XRGallery === "True" &&
          post.postAuthorID === "zFodTuG0dmOQgPw0VyHcSWI9NX63"
        ) {
          sendNotificationToFollowers(post);
        }
        dispatch(postCreationPopup());
        dispatch({
          type: "SET_NOTIFICATION",
          message: "POST created successfully"
        });

        const parcentCompleted = 0;
        dispatch({ type: "UPLOAD_PROGRESS", parcentCompleted });
      } catch (err) {
        console.log(err);
      }
    }
    uploadPost();
  };
};

export const setKeyword = keyword => {
  return {
    type: "SET_SEARCH_KEYWORD",
    keyword
  };
};
export const clearKeyword = () => {
  return {
    type: "CLEAR_SEARCH_KEYWORD"
  };
};
export const setSearchLength = searchLength => {
  return {
    type: "SET_SEARCH_LENGTH",
    result: searchLength
  };
};
export const clearSearchLength = () => {
  return {
    type: "CLEAR_SEARCH_LENGTH"
  };
};
export const somethingWrong = () => {
  return { type: "SOMETHING_WRONG", wrong: null };
};
export const editPost = (data, history) => {
  return (dispatch, getState, { getFirebase }) => {
    const state = getState();
    const firebase = getFirebase();
    const currentUser = state.firebase.profile;
    const updatedPost = {
      PostName: data.PostName,
      PostText: data.PostText,
      ExtendedTracking: data.ExtTracking,
      ARFusion: data.ARFusion,
      PostAuthorID: data.ForwardedUserID,
      PostPrivacy: data.postPrivacy
    };
    async function editPostData() {
      try {
        if (
          !(
            data.targetImage.length === 0 ||
            (data.model_type === "xrGallery" && !data.targetImageID) ||
            data.model_type === "FaceAR_2DImage"
          )
        ) {
          const formData = new FormData();
          formData.append("targetImageName", data.targetImageNameNew);
          formData.append("targetImage", data.targetImage);
          formData.append("targetImageID", data.targetImageID);
          //Creating new Target Image in our server
          await updateTargetImageInFolder(formData);

          //Updating Vuforia target image
          const response = await updateTargetVuforia(formData);
          dispatch({
            type: "SET_FETCH_STATUS",
            fetchStatus: "Updating  target image"
          });
          let text = await response.text();
          let index = text.indexOf("{");
          let responseData = JSON.parse(text.slice(index));
          if (responseData.result_code === "Success") {
            dispatch({
              type: "SET_FETCH_STATUS",
              fetchStatus: "Checking for target similarity"
            });
            //Checking for new uploaded target image similarity
            let checkRes = await checkTargetDublicate(data.targetImageID);
            let text = await checkRes.text();
            let index = text.indexOf("{");
            let res = JSON.parse(text.slice(index));
            if (res.result_code === "Success") {
              //check if has similarity
              if (
                res.similar_targets &&
                res.similar_targets.length > 0 &&
                !(
                  res.similar_targets.length === 1 &&
                  res.similar_targets[0] === data.targetImageID
                )
              ) {
                const formData = new FormData();
                formData.append("ImageTargetName", data.targetImageNameNew);
                formData.append("targetImageName", data.targetImageName);

                let deleteRes = await deleteTargetImageOurHost(formData);
                let res = await deleteRes.json();

                if (res.status === "success") {
                  const oldTargetData = new FormData();
                  oldTargetData.append("targetImageID", data.targetImageID);
                  oldTargetData.append("targetImageName", data.targetImageName);
                  oldTargetData.append("postId", data.postId);

                  let res = await save_target_data_to_sql(oldTargetData);

                  if (res.data.status === "success") {
                    dispatch({
                      type: "SET_NOTIFICATION",
                      message: "Sorry this target is already in use!",
                      error: true
                    });
                    firebase
                      .database()
                      .ref(`Posts/${data.postId}`)
                      .update({ ARTargetStatus: "Processing" });
                  }
                }
                return;
              }
              // do not has similarity (need to delete old target from our server)
              else {
                dispatch({
                  type: "SET_FETCH_STATUS",
                  fetchStatus: "Check Passes, deleting old target"
                });
                const oldFormData = new FormData();
                oldFormData.append("ImageTargetName", data.targetImageName);
                await deleteTargetImageOurHost(oldFormData);
                updatedPost.ARTargetName = data.targetImageNameNew;
                dispatch({
                  type: "SET_FETCH_STATUS",
                  fetchStatus: "Updating target image"
                });
              }
            }
          } else {
            const formData = new FormData();
            formData.append("ImageTargetName", data.targetImageNameNew);
            await deleteTargetImageOurHost(formData);
            dispatch({
              type: "SOMETHING_WRONG",
              wrong: "Oops! Something went wrong. Please try again a bit later"
            });
            return;
          }
        }

        if (data.soundReactivityFile || data.soundReactivity) {
          updatedPost.SoundReactivity = data.soundReactivity ? "True" : "False";
          if (data.soundReactivityFile) {
            const audioData = new FormData();
            audioData.append("file-type", "audio");
            audioData.append("file", data.soundReactivityFile[0]);
            if (data.oldSoundFileName) {
              audioData.append("old_file_name", data.oldSoundFileName);
            }
            const _uploadMp3 = await upladFileARContent(audioData, dispatch);
            if (_uploadMp3.data.status === "success") {
              updatedPost.AudioFile = _uploadMp3.data.url;
            }
          }
        }
        if (data.contentFile) {
          const contentData = new FormData();

          if (data.model_source === "ZF") {
            contentData.append("file-type", "zipfile");
          } else if (data.model_source === "UP") {
            contentData.append("file-type", "unitypackage");
          } else {
            contentData.append("file-type", data.postArType.toLowerCase());
          }
          contentData.append("old_file_contentURL", data.ARContentURL);
          contentData.append("file", data.contentFile[0]);
          contentData.append("action_type", "update");

          let res = await uploadNewContentFile(contentData);
          if (res.data.status === "success") {
            if (data.model_source === "ZF" || data.model_source === "UP") {
              updatedPost.ARSourceURL = res.data.url;
            } else {
              updatedPost.ARContentURL = res.data.url.toLowerCase();
            }
          }
        }
        if (
          (data.model_type === "xrGallery" ||
            data.model_type === "FaceAR_2DImage") &&
          data.targetImage.length > 0 &&
          !data.targetImageID
        ) {
          const formData = new FormData();
          formData.append("targetImage", data.targetImage);
          formData.append("targetImageID", data.targetImageID);
          formData.append("targetImageName", data.targetImageName);
          if (data.model_type) {
            formData.append("model_type", data.model_type);
          }
          await updateTargetImageInFolder(formData);
        }

        if (currentUser.User_ID !== data.ForwardedUserID) {
          const transferData = new FormData();
          transferData.append(
            "transfer_user_email",
            state.auth.forwardUserEmail
          );
          transferData.append("post_name", state.auth.forwardedPostName);
          transferData.append("post_owner_name", currentUser.User_FullName);

          await fetch("https://arize.io/assets/php/forward_post_email.php", {
            method: "POST",
            body: transferData
          });
        }
        firebase
          .database()
          .ref(`Posts/${data.postId}`)
          .update(updatedPost)
          .then(() => {
            dispatch({ type: "EDIT_POST_SUCCESS" });
            dispatch({
              type: "SET_NOTIFICATION",
              message: "POST updated successfully"
            });
            if (
              updatedPost.PostStatus &&
              updatedPost.PostStatus === "InReview"
            ) {
              let postType =
                currentUser.Subscription &&
                  moment(currentUser.Subscription.ExpirationDate > moment())
                  ? currentUser.Subscription.Type
                  : "Free";
              send_message_telegram_bot(
                `Post ${postType} "${updatedPost.PostName}" updated!. Post URL: https://platform.arize.io/prime/posts/${data.postId}`
              )
                .then(() => console.log("message sent"))
                .catch(err => console.log(err));
            }

            history.push("/");
          });
      } catch (err) {
        console.log("Error", err.message);
        dispatch({
          type: "SOMETHING_WRONG",
          wrong: "Oops! Something went wrong. Please try again a bit later"
        });
      }
    }
    if (
      data.isPaid === "True" &&
      (data.postArType === "3DBundle" || data.postArType === "FaceAR_2DImage")
    ) {
      updatedPost.IsPaid = "True";
      updatedPost.Price = data.price;
    } else if (
      data.isPaid === "False" &&
      (data.postArType === "3DBundle" || data.postArType === "FaceAR_2DImage")
    ) {
      updatedPost.IsPaid = "False";
      updatedPost.Price = null;
    }
    if (data.xrGallery) {
      updatedPost.XRGallery = "True";
    } else {
      updatedPost.XRGallery = null;
    }
    if (data.hasOwnProperty("ARTags")) {
      updatedPost.ARTags = data.ARTags;
    }
    if (state.firebase.auth.uid !== data.ForwardedUserID) {
      updatedPost.PostAuthor = data.ForwardedUserFullname;
      updatedPost.PostAuthorNickName = data.ForwardedUserNickname;
      updatedPost.PostAuthorID = data.ForwardedUserID;
    }
    if (data.model_source === "SF" || data.model_source === "GP") {
      updatedPost.ARSourceURL = data.ARContentURL;
    }
    if (
      data.postArType === "Youtube" ||
      (data.postArType === "360Video" && data.isYoutube) ||
      data.postArType === "TransParentYoutubeVideo" ||
      data.postArType === "WebURL"
    ) {
      updatedPost.ARContentURL = data.ARContentURL;
    }
    if (data.PostStatus) {
      updatedPost.PostStatus = data.PostStatus;
      updatedPost.LastEdit = new Date().toISOString();
    }
    if (data.CutOutColor && data.CutOutColor !== "empty") {
      updatedPost.CutOutColor = data.CutOutColor;
    } else if (data.CutOutColor === "empty") {
      updatedPost.CutOutColor = null;
    }

    editPostData();
  };
};

//Delete Post
export const deletePost = post => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const formData = new FormData();
    formData.append("ImageTargetID", post.ARTargetID);
    formData.append("targetImageID", post.ARTargetID);
    formData.append("ImageTargetName", post.ARTargetName);
    async function deletePostData() {
      try {
        const data = await deleteTargetImageVuforia(formData);
        const text = await data.text();
        let index = text.indexOf("{");
        let res = JSON.parse(text.slice(index));
        if (res.result_code === "TargetStatusProcessing") {
          dispatch({
            type: "SET_NOTIFICATION",
            message: "Can not delete post try later",
            error: true
          });
          return;
        } else {
          await deleteTargetImageOurHost(formData);
          const post_content_data = new FormData();
          if (
            post.ARType === "Video" ||
            post.ARType === "TransParentVideo" ||
            (post.ARType === "360Video" && post.IsYoutube !== "True") ||
            post.ARType === "FaceAR_2DImage"
          ) {
            post_content_data.append("PostType", post.ARType.toLowerCase());
            post_content_data.append("ContentURL", post.ARContentURL);
            deletePostContent(post_content_data);
          } else if (post.ARSource === "ZF" || post.ARSource === "UP") {
            post_content_data.append("PostType", post.ARSource);
            post_content_data.append("ContentURL", post.ARSourceURL);
            deletePostContent(post_content_data);
          }
          if (post.AudioFile) {
            const audio_content_data = new FormData();
            audio_content_data.append("AudioFile", post.AudioFile);
            deleteAudioContent(audio_content_data);
          }
          const post_thumbnail_data = new FormData();
          if (
            (post.XRGallery === "True" && !post.ARTargetID) ||
            post.ARType === "FaceAR_2DImage"
          ) {
            post_thumbnail_data.append("TargetName", post.ARTargetName);
            deleteThumbnailImage(post_thumbnail_data);
          }
          firebase
            .database()
            .ref(`Posts/${post.PostID}`)
            .remove()
            .then(() => {
              dispatch({
                type: "SET_NOTIFICATION",
                message: "Yor post has been successfully deleted"
              });
            })
            .catch(err => console.log("deleteError", err));
        }
      } catch (err) {
        console.log(err);
      }
    }
    deletePostData();
  };
};

//Delete paid post
export const deletePaidPost = post => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref(`Posts/${post.PostID}`)
      .update({ PostDeleteStatus: "deleted" })
      .then(() => {
        console.log("Paid post deleted");
      })
      .catch(err => console.log("Paid post delete ERROR", err));
  };
};

export const getSubscriptionInfo = userID => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    let subscriberInfo;
    if (!userID) {
      dispatch({
        type: "WRONG_DATA"
      });
      return;
    }
    firebase
      .database()
      .ref("Users")
      .orderByChild("User_ID")
      .equalTo(userID)
      .on("value", snapshot => {
        if (!snapshot.exists()) {
          dispatch({
            type: "WRONG_DATA"
          });
          return;
        }
        subscriberInfo = snapshot.val()[userID];
        dispatch({
          type: "SUBSCRIPTION_INFO",
          subscriberInfo
        });
      });
  };
};

export const getPaymentInfos = (postID, userID) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    let postInfo;
    let buyerInfo;
    let authorInfo;
    if (!postID || !userID) {
      dispatch({
        type: "WRONG_DATA"
      });
      return;
    }
    firebase
      .database()
      .ref("Posts")
      .orderByChild("PostID")
      .equalTo(postID)
      .on("value", snapshot => {
        if (!snapshot.exists()) {
          dispatch({
            type: "WRONG_DATA"
          });
          return;
        }
        postInfo = snapshot.val()[postID];
        firebase
          .database()
          .ref("Users")
          .orderByChild("User_ID")
          .equalTo(userID)
          .on("value", snapshot => {
            if (!snapshot.exists()) {
              dispatch({
                type: "WRONG_DATA"
              });
              return;
            }
            buyerInfo = snapshot.val()[userID];

            firebase
              .database()
              .ref("Users")
              .orderByChild("User_ID")
              .equalTo(postInfo.PostAuthorID)
              .on("value", snapshot => {
                if (snapshot.exists()) {
                  authorInfo = snapshot.val()[postInfo.PostAuthorID];
                  dispatch({
                    type: "PAYMENT_INFOS",
                    postInfo,
                    buyerInfo,
                    authorInfo
                  });
                }
              });
          });
      });
  };
};

//insert payment in post
export const addPostPayment = (post, newBuyer, user) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref(`Posts/${post.PostID}/Buyers`)
      .push(newBuyer)
      .then(() => {
        firebase
          .database()
          .ref(`Users/${newBuyer.UserID}/Purchuases/${post.PostID}`)
          .set({
            Amount: newBuyer.Amount,
            PostID: post.PostID,
            PaymentID: newBuyer.PaymentID,
            DateTime: newBuyer.DateTime,
            InvoiceURL: newBuyer.InvoiceURL,
            PostName: newBuyer.PostName
          })
          .then(() => {
            newBuyer.PostID = post.PostID;
            firebase
              .database()
              .ref(`Users/${post.PostAuthorID}/Buyers`)
              .push(newBuyer)
              .then(() => {
                async function checkSellerBalance() {
                  const sellerRef = await firebase
                    .database()
                    .ref(`Users/${post.PostAuthorID}`)
                    .once("value");
                  const seller = sellerRef.val();
                  const sellerBalance = countCreatorAmount(
                    seller.Buyers,
                    seller.Payout
                  );
                  if (sellerBalance > 100) {
                    const data = new FormData();
                    data.append("creator_name", seller.User_FullName);
                    data.append("creator_email", seller.User_Email);
                    const res = await sendEmailCretorBalance(data);
                    console.log(res.data);
                  }
                }
                checkSellerBalance();
                dispatch({
                  type: "SET_NOTIFICATION",
                  message: "Your payment was accepted successfully"
                });
                createNotification(
                  "purchase",
                  post,
                  `${user.User_FullName} has bought your post: ${post.PostName}`,
                  user
                );
              });
          });
      })
      .catch(err => console.log("Buyer error", err));
  };
};

export const addSubscriptionPayment = (data, subscriber) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const subscriptionData = {
      Type: data.Type,
      Activated: data.Activated,
      ExpirationDate: data.ExpirationDate,
      CurrentInvoiceID: data.InvoiceID,
      Allow_Extend: null
    };
    firebase
      .database()
      .ref(`Users/${subscriber.User_ID}/Subscription`)
      .update(subscriptionData)
      .then(() => {
        firebase
          .database()
          .ref(
            `Users/${subscriber.User_ID}/Subscription/Invoices/${data.InvoiceID}`
          )
          .set({
            amount: data.Amount,
            InvoiceURL: data.Invoice,
            Promocode: data.coupon || null
          })
          .then(() => {
            if (data.coupon) {
              firebase
                .database()
                .ref(`Promocodes/${data.coupon}/Users/${subscriber.User_ID}`)
                .set({
                  BoughtAt: data.Activated,
                  PackageName: data.Type,
                  Amount: data.Amount,
                  PaymentPeriod: data.SubsciptionPeriod
                })
                .then(() => {
                  dispatch({
                    type: "SET_NOTIFICATION",
                    message: "Your payment was accepted successfully"
                  });
                });
            } else {
              dispatch({
                type: "SET_NOTIFICATION",
                message: "Your payment was accepted successfully"
              });
            }
            send_message_telegram_bot(
              `${data.Type} Package. ${subscriber.User_FullName} ${subscriber.User_Email}. Payment Period: ${data.SubsciptionPeriod}. Payment Amount: ${data.Amount}`
            );
          });
      })
      .catch(err => console.log(err));
  };
};

export const setPaymentStatus = status => {
  return {
    type: "SET_PAYMENT_STATUS",
    status
  };
};

export const postCreationPopup = () => ({
  type: "POST_CREATION_POPUP"
});

function updateTargetImageInFolder(data) {
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/upDateTargetImageFolderSenik.php",
    {
      method: "POST",
      body: data
    }
  );
}

function upladFileARContent(data, dispatch) {
  const options = {
    onUploadProgress: progressEvent => {
      const { loaded, total } = progressEvent;
      const parcentCompleted = Math.round((loaded * 100) / total);
      dispatch({ type: "UPLOAD_PROGRESS", parcentCompleted });
    }
  };
  return axios.post(
    "https://triplee.info/Triple_E_WebService/uploadFileSenik.php",
    data,
    options
  );
}
function uploadThumbnail(data) {
  return axios.post(
    "https://triplee.info/Triple_E_WebService/uploadThumbnailSenik.php",
    data
  );
}

function deleteTargetImageVuforia(data) {
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/DeleteTargetSenik.php",
    {
      method: "POST",
      body: data
    }
  );
}

function sendEmailCretorBalance(data) {
  return axios.post(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/creator_payout_send_notif_email.php",
    data
  );
}

export function deleteThumbnailImage(data) {
  return axios.post(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/DeleteThumbnailSenik.php",
    data
  );
}

export function deleteTargetImageOurHost(data) {
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/DeleteTargetOurHostSenik.php",
    {
      method: "POST",
      body: data
    }
  );
}

export function deletePostContent(data) {
  return axios.post(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/DeleteContentSenik.php",
    data
  );
}

function deleteAudioContent(data) {
  return axios.post(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/DeleteAudioSenik.php",
    data
  );
}

function uploadNewImageVuforia(data) {
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/PostNewTargetSenik.php",
    {
      method: "POST",
      body: data
    }
  );
}

function updateTargetVuforia(data) {
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/UpdateTargetVuforiaSenik.php",
    {
      method: "POST",
      body: data
    }
  );
}
function checkTargetDublicate(id) {
  const data = new FormData();
  data.append("targetID", id);
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/CheckTargetExistsVWS.php",
    {
      method: "POST",
      body: data
    }
  );
}
function uploadNewContentFile(contentData) {
  return axios.post(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/uploadFileSenik.php",
    contentData
  );
}

function deleteTargetFolderAndVuforia(data) {
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/DeletImageTargetVuforiaOpp.php",
    {
      method: "POST",
      body: data
    }
  );
}

function saveTargetIdToSql(data) {
  return fetch(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/save_target_id_sql.php",
    {
      method: "POST",
      body: data
    }
  );
}

function save_target_data_to_sql(data) {
  return axios.post(
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/save_target_data_to_sql.php",
    data
  );
}