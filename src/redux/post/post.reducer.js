const initState = {
  allPosts: null,
  posts: null,
  ARPosts: null,
  FaceARPosts: null,
  users: [],
  keyword: "",
  result: 0,
  fetchStatus: null,
  paymentStatus: "pending",
  status: null,
  openPopup: false
};

const postReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_POSTS":
      return {
        ...state,
        allPosts: action.posts
      };
    case "CREATE_POST":
      console.log("created post", action.post);
      return state;
    case "CREATE_POST_ERROR":
      console.log("create post error", action.err);
      return state;
    case "EDIT_POST_SUCCESS":
      console.log("Post updated");
      return {
        ...state,
        authError: null
      };
    case "EDIT_POST_ERROR":
      console.log("Post update error", action.err);
      return {
        ...state,
        authError: action.err.message
      };
    case "POST_DELETE_SUCCESS":
      const newPosts = [...state.posts];
      let filteredPost = newPosts.filter(post => {
        return post.PostID !== action.id;
      });
      return {
        ...state,
        posts: filteredPost
      };
    case "SOMETHING_WRONG":
      return {
        ...state,
        somethingWrong: action.wrong,
        fileUploadProgress: 0
      };
    case "UPLOAD_PROGRESS":
      return {
        ...state,
        fileUploadProgress: action.parcentCompleted
      };
    case "SET_SEARCH_KEYWORD":
      return {
        ...state,
        keyword: action.keyword
      };
    case "CLEAR_SEARCH_KEYWORD":
      return {
        ...state,
        keyword: ""
      };
    case "SET_SEARCH_LENGTH":
      return {
        ...state,
        result: action.result
      };
    case "CLEAR_SEARCH_LENGTH":
      return {
        ...state,
        result: 0
      };
    case "SET_FETCH_STATUS":
      return {
        ...state,
        fetchStatus: action.fetchStatus
      };
    case "PAYMENT_INFOS":
      return {
        ...state,
        postInfo: action.postInfo,
        buyerInfo: action.buyerInfo,
        authorInfo: action.authorInfo
      };
    case "SUBSCRIPTION_INFO":
      return {
        ...state,
        subscriberInfo: action.subscriberInfo
      };
    case "WRONG_DATA":
      return {
        ...state,
        status: "wrong"
      };
    case "SET_PAYMENT_STATUS":
      return {
        ...state,
        paymentStatus: action.status
      };
    case "POST_CREATION_POPUP":
      return {
        ...state,
        openPopup: !state.openPopup
      }
    default:
      return state;
  }
};

export default postReducer;
