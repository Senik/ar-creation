import { createSelector } from "reselect";
import { checkLimitForCreation } from "../../helpers";

const selectPost = state => state.post;
const selectUser = state => state.firebase.profile;

export const selectAllPosts = createSelector([selectPost], post =>
  post.allPosts ? Object.values(post.allPosts) : []
);

export const selectAllPostsObj = createSelector(
  [selectPost],
  post => post.allPosts
);

export const selectARPosts = path =>
  createSelector([selectAllPosts], posts => {
    if (posts) {
      let filteredPosts = posts.filter(post => {
        if (path === "/faceargallery") {
          return (
            post.PostStatus === "TestMode" &&
            (post.ARType === "FaceAR_2DImage" ||
              post.ARType === "FaceAR_3DBundle")
          );
        }
        return (
          post.PostStatus === "TestMode" &&
          post.XRGallery === "True" &&
          post.PostAuthorIsCreator === "True"
        );
      });
      return sortDataByDate(filteredPosts);
    }
    return null;
  });

export const selectUserPosts = createSelector(
  [selectAllPosts, selectUser],
  (posts, profile) => {
    if (posts && !profile.isEmpty) {
      let filteredPosts = [];
      filteredPosts =
        posts.filter(post => profile.User_ID === post.PostAuthorID) || [];
      filteredPosts.length && sortDataByDate(filteredPosts);

      return filteredPosts;
    }
    return null;
  }
);
export const selectUsersPostsById = id =>
  createSelector([selectAllPosts], allPosts => {
    if (allPosts.length > 0) {
      const userPosts = allPosts.filter(post => post.PostAuthorID === id);
      return userPosts.length > 0 ? sortDataByDate(userPosts) : null;
    } else {
      return "pending";
    }
  });

export const select_post_by_id = id =>
  createSelector([selectAllPostsObj], allPosts => {
    if (allPosts) {
      const post = allPosts[id];
      return post;
    }
    return null;
  });

export const selectWrong = createSelector(
  [selectPost],
  post => post.somethingWrong
);

export const selectResult = createSelector([selectPost], post => post.result);

export const selectKeyword = createSelector([selectPost], post => post.keyword);

export const selectProgress = createSelector(
  [selectPost],
  post => post.fileUploadProgress
);

export const selectFetchStatus = createSelector(
  [selectPost],
  post => post.fetchStatus
);

export const selectUserLimitations = createSelector(
  [selectUserPosts, selectUser],
  (posts, profile) => {
    if (posts && !profile.isEmpty) {
      const userLimits = checkLimitForCreation(profile, posts);
      return userLimits;
    }
    return {};
  }
);

export const selectOpenPopup = createSelector(
  [selectPost],
  post => post.openPopup
);

function sortDataByDate(data) {
  return data
    .sort((a, b) => {
      return new Date(a.LastEdit) - new Date(b.LastEdit);
    })
    .reverse();
}
