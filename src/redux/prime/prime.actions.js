import { snapshotToArray } from "../../helpers";
import { deletePostContent, deleteThumbnailImage } from "../post/post.actions";
import { createNotification, sendNotificationToFollowers } from "../../helpers";
import axios from "axios";

export const changeUserStatus = (user, status, history, message) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const state = getState();
    const emailData = new FormData();
    const URL =
      "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/send_mail_creator_status.php";
    emailData.append("user_email", user.User_Email);
    emailData.append("user_name", user.User_FullName);
    emailData.append("action_type", "accept");
    let userData = {};
    let notificationMessage, notifType;
    if (status === "False") {
      notificationMessage =
        "Sorry. Your request of becoming creator has been declined.";
      notifType = "creatorStatusRejected";
      emailData.append("action_type", "reject");
      emailData.append("message", message);
      userData = {
        User_IsCreator: status,
        Creator_Credentials: null,
        User_Bio: null,
        User_Skills: null,
        User_SocialLinks: null
      };
    } else if (status === "True") {
      notificationMessage =
        "Congrats!!! Your request of becoming creator has been approved.";
      notifType = "creatorStatusAccepted";
      userData = {
        User_IsCreator: status
      };
    }
    const allPosts = Object.values(state.post.allPosts);
    const userPosts = allPosts.filter(
      post => post.PostAuthorID === user.User_ID
    );

    const promises = [];
    userPosts.forEach(post => {
      promises.push(
        firebase
          .database()
          .ref(`Posts/${post.PostID}`)
          .update({ PostAuthorIsCreator: status })
      );
    });

    axios
      .post(URL, emailData)
      .then(res => {
        if (res.data.status === "success") {
          promises.push(
            firebase
              .database()
              .ref(`Users/${user.User_ID}`)
              .update({
                ...userData
              })
          );
          Promise.all(promises)

            .then(() => {
              console.log("all promises suceed");
              dispatch({ type: "USER_STATUS_UPDATED" });
              dispatch({
                type: "SET_NOTIFICATION",
                message: "User Status changed successfully"
              });
              createNotification(notifType, user, notificationMessage);
              history.push("/prime/users");
            })
            .catch(err => console.log("User Status update Error", err));
        }
      })
      .catch(err => console.log(err));
  };
};

export const changePostStatus = (post, message, link, history, id) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const state = getState();
    const postAuthor = state.users.allUsers[post.PostAuthorID];
    let newStatus, actionType, notificationMessage;
    let reason = null;
    if (id === "MAKE PUBLIC") {
      newStatus = "TestMode";
      actionType = "accept";
      notificationMessage = `Post: ${post.PostName}, has been Published!`;
    } else if (id === "REJECT") {
      newStatus = "Rejected";
      actionType = "reject";
      notificationMessage = `Post: ${post.PostName}, has been Rejected!`;
      reason = message;
    }
    const updatePostData = async () => {
      try {
        await firebase.ref(`Posts/${post.PostID}`).update({
          PostStatus: newStatus,
          ReviewDate: new Date().toISOString()
        });

        if (
          post.PostPrivacy === "Public" &&
          post.XRGallery === "True" &&
          actionType === "accept"
        ) {
          sendNotificationToFollowers(post);
        }

        const data = new FormData();
        data.append("user_email", postAuthor.User_Email);
        data.append("user_name", postAuthor.User_FullName);
        data.append("post_id", post.PostID);
        data.append("post_link", link);
        data.append("action_type", actionType);
        data.append("message", message);
        const res = await fetch(
          "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/send_mail_template.php",
          {
            method: "POST",
            body: data
          }
        );
        const resData = await res.json();
        if (resData.status === "success") {
          dispatch({
            type: "SET_NOTIFICATION",
            message: notificationMessage
          });
          createNotification(
            actionType,
            post,
            `Your ${notificationMessage}`,
            null,
            reason
          );

          return history.push("/prime/posts");
        }
      } catch (err) {
        console.log(err);
      }
    };
    updatePostData();
  };
};

export const changePostUpdateVersion = (post, updateVersion) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref(`Posts/${post.PostID}`)
      .update({ Update: updateVersion })
      .then(() => {
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Post Update version has been successfully updated"
        });
      })
      .catch(err => console.log(err));
  };
};
export const upload3DBundlePackage = (post, packageName, file) => {
  return (dispatch, getState, { getFirebase }) => {
    const url = "https://triplee.info/Triple_E_WebService/uploadPackages.php";
    const data = new FormData();
    data.append("ar_content_url", post.ARContentURL);
    data.append("package_name", packageName);
    data.append("file", file);

    const uploadPackage = async () => {
      try {
        const options = {
          onUploadProgress: progressEvent => {
            const { loaded, total } = progressEvent;
            const parcentCompleted = Math.round((loaded * 100) / total);
            dispatch({ type: "UPLOAD_PROGRESS", parcentCompleted });
          }
        };
        const res = await axios.post(url, data, options);
        if (res.data.status === "success") {
          dispatch({
            type: "SET_NOTIFICATION",
            message: `${packageName.toUpperCase()} package was successfully uploaded!`
          });
        } else {
          dispatch({
            type: "SET_NOTIFICATION",
            message: "Oops something went wrong, try again later.",
            error: true
          });
        }
      } catch (err) {
        console.log(err);
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Oops something went wrong, try again later.",
          error: true
        });
      }
    };

    uploadPackage();
  };
};

export const changePostStatusToInReview = (id, history) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref(`/Posts/${id}`)
      .update({
        PostStatus: "InReview",
        ReviewDate: new Date().toISOString()
      })
      .then(() => {
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Post status changed to InReview!"
        });
        return history.push("/prime/posts");
      })
      .catch(err => console.log(err));
  };
};

export const deleteUserPost = (post, message, history) => {
  return (dispatch, getState, { getFirebase }) => {
    const {
      ARType,
      PostID,
      ARSource,
      XRGallery,
      IsYoutube,
      ARTargetID,
      ARSourceURL,
      ARContentURL,
      ARTargetName
    } = post;

    const firebase = getFirebase();
    const formData = new FormData();
    const post_content_data = new FormData();
    formData.append("ImageTargetID", ARTargetID);
    formData.append("ImageTargetName", `${ARTargetName}.jpg`);

    if (
      ARType === "Video" ||
      ARType === "TransParentVideo" ||
      (ARType === "360Video" && IsYoutube !== "True") ||
      ARType === "FaceAR_2DImage"
    ) {
      post_content_data.append("PostType", ARType.toLowerCase());
      post_content_data.append("ContentURL", ARContentURL);
      deletePostContent(post_content_data);
    } else if (ARSource === "ZF" || ARSource === "UP") {
      post_content_data.append("PostType", ARSource);
      post_content_data.append("ContentURL", ARSourceURL);
      deletePostContent(post_content_data);
    }

    const post_thumbnail_data = new FormData();
    if ((XRGallery === "True" && !ARTargetID) || ARType === "FaceAR_2DImage") {
      post_thumbnail_data.append("TargetName", ARTargetName);
      deleteThumbnailImage(post_thumbnail_data);
    }
    firebase
      .database()
      .ref("Posts")
      .child(PostID)
      .remove()
      .then(async res => {
        await fetch(
          "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_WebService/DeletImageTargetVuforiaOpp.php",
          {
            method: "POST",
            body: formData
          }
        );

        firebase
          .database()
          .ref("Users")
          .orderByChild("User_ID")
          .equalTo(post.PostAuthorID)
          .once("value")
          .then(snapshot => {
            if (snapshot.exists()) {
              let userSnapshot = snapshotToArray(snapshot);
              const data = new FormData();
              data.append("user_email", userSnapshot[0].User_Email);
              data.append("user_name", userSnapshot[0].User_FullName);
              data.append("post_id", post.PostID);
              data.append("post_name", post.PostName);
              data.append("post_description", post.PostText);
              data.append("message", message);
              data.append("action_type", "delete");

              fetch(
                "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/send_mail_template.php",
                {
                  method: "POST",
                  body: data
                }
              )
                .then(res => res.json())
                .then(data => {
                  if (data.status === "success") {
                    dispatch({
                      type: "SET_NOTIFICATION",
                      message: "Post has been deleted!"
                    });
                    createNotification(
                      "delete",
                      post,
                      `Your post ${post.PostName} has been deleted!`
                    );
                    return history.push("/prime/posts");
                  }
                })
                .catch(err => console.log(err));
            } else {
              dispatch({
                type: "SET_NOTIFICATION",
                message: "Post has been deleted!"
              });
              return history.push("/prime/posts");
            }
          });
      });
  };
};

export const cahngeUserPayoutStatus = (user, payoutId) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();

    const data = new FormData();
    data.append("user_email", user.User_Email);
    data.append("payout_url", user.Payout[payoutId].Invoice_URL);

    sendCreatorPayoutEmail(data)
      .then(res => {
        if (res.data.status === "success") {
          firebase
            .database()
            .ref(`Users/${user.User_ID}/Payout/${payoutId}`)
            .update({ Payout_Status: "Completed" })
            .then(() => {
              dispatch({
                type: "SET_NOTIFICATION",
                message: "Payout Status updated successfully"
              });
            });
        } else {
          dispatch({
            type: "SET_NOTIFICATION",
            message: "Payout Status update Error, try later",
            error: true
          });
        }
      })
      .catch(error => {
        console.log({ error });
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Payout Status update Error, try later",
          error: true
        });
      });
  };
};

export const changePremiumData = (id, price, discount, history) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref(`/Users/${id}`)
      .update({
        Premium_Package_Price: price,
        Premium_Package_Discount: discount
      })
      .then(() => {
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Premium package data has been successfully added"
        });
        return history.push("/prime/users");
      });
  };
};

function sendCreatorPayoutEmail(data) {
  const url =
    "https://cors-anywhere.herokuapp.com/https://triplee.info/Triple_E_Payment/send_creator_payout_email.php";
  return axios.post(url, data);
}
