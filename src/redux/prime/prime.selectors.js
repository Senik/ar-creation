import { createSelector } from "reselect";
import moment from "moment";

const selectPost = state => state.post;
const selectUsers = state => state.users.allUsers;

export const selectAllPosts = createSelector([selectPost], post =>
  post.allPosts ? Object.values(post.allPosts) : []
);

export const selectPrimePosts = createSelector([selectAllPosts], posts => {
  if (posts) {
    let indices = [6, 7, 8, 9, 3, 4, 0, 1];
    let sortedPosts = posts.sort((a, b) => {
      let r = 0;
      indices.find(
        i =>
          (r =
            moment(b.LastEdit).format("YYYYMMDD") -
            moment(a.LastEdit).format("YYYYMMDD"))
      );
      return r;
    });
    return sortedPosts;
  }
  return null;
});

export const selectPostsBySubscribtionPlan = subscriptionType =>
  createSelector([selectPrimePosts, selectUsers], (posts, users) => {
    if (posts && users) {
      switch (subscriptionType) {
        case "free":
          return posts.filter(post => {
            if (users[post.PostAuthorID]) {
              return !users[post.PostAuthorID].Subscription;
            }
          });
        case "plus":
          return posts.filter(post => {
            if (users[post.PostAuthorID]) {
              return (
                users[post.PostAuthorID].Subscription &&
                users[post.PostAuthorID].Subscription.Type === "Plus"
              );
            }
          });
        case "premium":
          return posts.filter(post => {
            if (users[post.PostAuthorID]) {
              return (
                users[post.PostAuthorID].Subscription &&
                users[post.PostAuthorID].Subscription.Type === "Premium"
              );
            }
          });
        default:
          return [];
      }
    }
    return null;
  });
