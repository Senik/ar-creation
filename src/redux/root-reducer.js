import { combineReducers } from "redux";
//reducers
import { firebaseReducer } from "react-redux-firebase";
import authReducer from "./auth/auth.reducer";
import postReducer from "./post/post.reducer";
import notifyReducer from "./notification/notification.reducer";
import userReducer from "./user/user.reducer";

const rootReducer = combineReducers({
  auth: authReducer,
  post: postReducer,
  notification: notifyReducer,
  firebase: firebaseReducer,
  users: userReducer
});

export default rootReducer;
