import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase';
import fbConfig from '../config/fbConfig';

import rootReducer from './root-reducer';

const middlewares = [thunk.withExtraArgument({ getFirebase })];

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(...middlewares),
    reactReduxFirebase(fbConfig, {
      useFirebaseForProfile: true,
      userProfile: 'Users',
      attachAuthIsReady: true
    }),
    process.env.NODE_ENV === "development" ?
      (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()) ||
      compose
      : compose

  )
);

export default store;
