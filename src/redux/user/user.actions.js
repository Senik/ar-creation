import axios from "axios";
import { send_message_telegram_bot } from "../../helpers/";

export const getAllUsers = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref("/Users")
      .on("value", snapshot => {
        if (snapshot.exists()) {
          let usersSnapshot = snapshot.val();
          dispatch({
            type: "GET_ALL_USERS",
            users: usersSnapshot
          });
        }
      });
  };
};

export const clearNotifications = uid => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref(`/Users/${uid}/Notifications`)
      .remove()
      .then(() => {
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Notifications have been removed!"
        });
      })
      .catch(err => console.log(err));
  };
};

export const countNotifications = notifLength => {
  return {
    type: "COUNT_NOTIFICATIONS",
    notifLength
  };
};

export const setPayoutData = (data, id) => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref(`/Users/${id}/Payout`)
      .push({ ...data })
      .then(() => {
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Your payout request has been accepted!"
        });
      })
      .catch(err => console.log(err));
  };
};

export const sendContactUsEmail = (data, url) => {
  return (dispatch, getState, { getFirebase }) => {
    const state = getState();
    const profile = state.firebase.profile;
    axios
      .post(url, data)
      .then(res => {
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Your request has been sent."
        });
        if (url.includes('contactForPremiumPrice')) {
          send_message_telegram_bot(
            `Premium Request: ${profile.User_FullName} ${profile.User_Email}`
          );
        }
      })
      .catch(err => {
        dispatch({
          type: "SET_NOTIFICATION",
          message: "Oops, something went wrong!!!",
          error: true
        });
      });
  };
};

export const makePromocodeFreeSubscription = data => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    const subscriptionData = {
      Type: data.Type,
      Activated: data.Activated,
      ExpirationDate: data.ExpirationDate,
      CurrentInvoiceID: data.coupon,
      Allow_Extend: null
    };
    firebase
      .database()
      .ref(`Users/${data.User_ID}/Subscription`)
      .update(subscriptionData)
      .then(() => {
        firebase
          .database()
          .ref(`Users/${data.User_ID}/Subscription/Invoices/${data.coupon}`)
          .set({ amount: data.Amount, Promocode: data.coupon })
          .then(() => {
            firebase
              .database()
              .ref(`Promocodes/${data.coupon}/Users/${data.User_ID}`)
              .set({
                BoughtAt: data.Activated,
                PackageName: data.Type,
                Amount: data.Amount,
                PaymentPeriod: data.SubsciptionPeriod
              })
              .then(() => {
                dispatch({
                  type: "SET_NOTIFICATION",
                  message: "Your package was successfully activated!!!"
                });
              });
          });
      })
      .catch(err => console.log(err));
  };
};

export const getAllPromocodes = () => {
  return (dispatch, getState, { getFirebase }) => {
    const firebase = getFirebase();
    firebase
      .database()
      .ref("Promocodes")
      .on("value", snapshot => {
        if (snapshot.exists()) {
          let promoSnapshot = snapshot.val();
          dispatch({
            type: "GET_ALL_PROMOCODES",
            promos: promoSnapshot
          });
        }
      });
  };
};