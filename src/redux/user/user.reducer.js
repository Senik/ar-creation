const initialState = {
  allUsers: null,
  notifLength: 0,
  promocedes: null
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_ALL_USERS':
      return {
        ...state,
        allUsers: action.users
      }
    case 'COUNT_NOTIFICATIONS':
      return {
        ...state,
        notifLength: action.notifLength
      }
    case "GET_ALL_PROMOCODES":
      return {
        ...state,
        promocedes: action.promos
      };
    default:
      return {
        ...state
      }
  }
}

export default userReducer; 