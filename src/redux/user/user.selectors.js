import { createSelector } from "reselect";

const selectUsers = state => state.users;

export const selectAllUsers = createSelector([selectUsers], users =>
  users.allUsers ? Object.values(users.allUsers) : []
);

export const selectAllUsersObj = createSelector(
  [selectUsers],
  users => users.allUsers
);
export const selectCount = createSelector(
  [selectUsers],
  users => users.notifLength
);

export const selectAllCreators = createSelector([selectAllUsers], allUsers => {
  if (allUsers) {
    const creators = allUsers.filter(user => user.User_IsCreator === "True");

    return creators;
  }
  return null;
});

export const selectUserById = id =>
  createSelector([selectAllUsersObj], allUsers => {
    if (allUsers) {
      const user = allUsers[id];
      return user;
    }
    return null;
  });

export const selectPromocodes = createSelector([selectUsers], users => {
  return users ? users.promocedes : null;
});
